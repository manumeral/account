Roles and Responsibilities
Account Management
Account Holder Management
Wallet Management
Account service is responsible for any calls coming from any providers or template transformers
see documentation [here](http://phab.corp.zeta.in/w/athena/account/)

Local Testing:
1. Uncomment local testing dependency in pom.xml
2. Comment include=git.properties in settings.properties
3. Create a new run configuration and add -Dlocal.testing.run=true in VM Options