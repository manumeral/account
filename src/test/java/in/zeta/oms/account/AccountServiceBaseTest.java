package in.zeta.oms.account;

import com.google.inject.Module;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.helper.AccountServiceDAOTestHelper;
import olympus.message.types.Payload;
import olympus.message.types.Request;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.ArgumentCaptor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;


public class AccountServiceBaseTest {

  public final ObjectMother om;
  private final AccountServiceDAOTestHelper accountServiceDAOTestHelper;

  public AccountServiceBaseTest(Module... modules) {
    final ArrayList<Module> moduleList = new ArrayList<>(Arrays.asList(modules));

    this.accountServiceDAOTestHelper = new AccountServiceDAOTestHelper(moduleList);
    this.om = new ObjectMother(moduleList);
  }

  @Before
  @After
  public void tearDown() throws Exception {
    accountServiceDAOTestHelper.cleanDb();
  }

  @BeforeClass
  public static void openDB() {
    AccountServiceDAOTestHelper.openDb();
  }

  @AfterClass
  public static void closeDB() throws SQLException {
    AccountServiceDAOTestHelper.closeDb();
  }

  protected  <T> T getInstance(Class<T> tClass) {
    return om.getInjector().getInstance(tClass);
  }

  public <T extends Payload> T captureResponsePayload(Request<?> request,
      Class<T> responsePayloadClazz) {
    ArgumentCaptor<T> responseCaptor = ArgumentCaptor.forClass(responsePayloadClazz);
    verify(om.getInjector().getInstance(ZetaHostMessagingService.class), timeout(2000))
        .sendResponse(eq(request), responseCaptor.capture());
    return responseCaptor.getValue();
  }

  public <T extends Exception> T captureExceptionThrown(Request request,
      Class<T> exceptionClass) {
    ArgumentCaptor<T> responseCaptor = ArgumentCaptor.forClass(exceptionClass);
    verify(om.getInjector().getInstance(ZetaHostMessagingService.class), timeout(2000))
        .sendError(eq(request), responseCaptor.capture());
    return exceptionClass.cast(responseCaptor.getValue());
  }
}
