package in.zeta.oms.account.helper;

import static in.zeta.oms.account.constant.AccountGroupID.ACCOUNT_GROUP_ID_DEFAULT;
import static in.zeta.oms.account.constant.AccountHolderGroupSchema.AH_WITH_MIN_KYC_WITH_PAN;
import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.EntityGroupID.ENTITY_GROUP_ID_DEFAULT;
import static in.zeta.oms.account.constant.GenericName.DEFAULT_NAME;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.constant.AccountID;
import in.zeta.oms.athenamanager.api.model.EntityGroup;
import in.zeta.oms.athenamanager.api.model.EntityType;
import in.zeta.oms.athenamanager.api.request.GetEntityGroupRequest;
import in.zeta.oms.athenamanager.api.request.GetEntityGroupRequest.Builder;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import java.util.HashMap;
import lombok.Getter;
import org.mockito.ArgumentMatcher;

@Getter
@Singleton
public class EntityGroupHelper {

  private final AthenaManagerClient athenaManagerClient;

  @Inject
  public EntityGroupHelper(AthenaManagerClient athenaManagerClient) {
    this.athenaManagerClient = athenaManagerClient;
  }

  public static EntityGroup getDefaultAccountGroupHelper() {
    return EntityGroup.builder()
        .entityType(EntityType.ACCOUNT)
        .id(ACCOUNT_GROUP_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .name(DEFAULT_NAME.getValue())
        .description(DEFAULT_NAME.getValue())
        .attributes(new HashMap<>())
        .build();
  }

  private static EntityGroup.Builder getDefaultEntityGroupForAccountHolderBuilder() {
    return getEntityGroup(
        EntityType.ACCOUNT_HOLDER,
        ACCOUNT_HOLDER_ID_DEFAULT.getValue());
  }

  private static EntityGroup.Builder getEntityGroupForAccountHolderBuilder() {
    return EntityGroup.builder()
        .id(ENTITY_GROUP_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .schema(AH_WITH_MIN_KYC_WITH_PAN.getSchema())
        .entityType(EntityType.ACCOUNT_HOLDER);
  }

  public static EntityGroup.Builder getDefaultEntityGroupForAccountBuilder() {
    return getEntityGroup(
        EntityType.ACCOUNT,
        AccountID.ACCOUNT_ID_DEFAULT.getValue());
  }

  private static EntityGroup.Builder getEntityGroup(EntityType account, String value) {
    return EntityGroup.builder()
        .id(ENTITY_GROUP_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .entityType(account);
  }

  private ArgumentMatcher<Builder> getDefaultEntityGroupMatcher() {
    return new ArgumentMatcher<Builder>() {
      @Override
      public boolean matches(Object argument) {
        if (argument instanceof Builder) {
          GetEntityGroupRequest getEntityGroupRequest = ((Builder) argument)
              .build()
              .payload();

          return getEntityGroupRequest.getEntityType().equals(EntityType.ACCOUNT_HOLDER) &&
              getEntityGroupRequest.getIfiID().equals(IFI_ID_DEFAULT.getValue()) &&
              getEntityGroupRequest.getId()
                  .equals(ENTITY_GROUP_ID_DEFAULT.getValue());
        }

        return false;
      }
    };
  }

  public void mockDefaultEntityGroupForAccountHolder() {
    when(athenaManagerClient.getEntityGroup(argThat(getDefaultEntityGroupMatcher())))
        .thenReturn(completedFuture(getDefaultEntityGroupForAccountHolderBuilder().build()));
  }

  public void mockDefaultEntityGroupForAccountHolder2() {
    when(athenaManagerClient.getEntityGroup(argThat(getDefaultEntityGroupMatcher())))
        .thenReturn(completedFuture(getEntityGroupForAccountHolderBuilder().build()));
  }
}
