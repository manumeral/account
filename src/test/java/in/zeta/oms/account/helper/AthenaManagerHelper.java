package in.zeta.oms.account.helper;

import in.zeta.oms.account.constant.AccountHolderProviderID;
import in.zeta.oms.account.constant.AccountProviderID;
import in.zeta.oms.account.constant.CardProgramID;
import in.zeta.oms.account.constant.CoaID;
import in.zeta.oms.account.constant.CoaNodeID;
import in.zeta.oms.account.constant.GenericBusinessID;
import in.zeta.oms.account.constant.GenericName;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.constant.ParentCoaNodeID;
import in.zeta.oms.account.constant.ProductFamilyID;
import in.zeta.oms.account.constant.ProductFamilyName;
import in.zeta.oms.account.constant.ProductID;
import in.zeta.oms.account.constant.ProductName;
import in.zeta.oms.account.constant.ProgramID;
import in.zeta.oms.athenamanager.api.enums.EntityProviderState;
import in.zeta.oms.athenamanager.api.enums.ProgramStatus;
import in.zeta.oms.athenamanager.api.model.*;
import in.zeta.oms.cloudcard.api.CardProgram;
import in.zeta.oms.cloudcard.api.IssuanceConfiguration;
import in.zeta.oms.cloudcard.model.ProductType;
import in.zeta.oms.ledger.api.AccountingType;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import static in.zeta.oms.account.helper.AccountHolderTestHelper.getDefaultAttributes;

public class AthenaManagerHelper {

  public static Product getDefaultProduct() {
    return Product.builder()
        .id(ProductID.PRODUCT_ID_DEFAULT.getValue())
        .name(ProductName.GPR_KYC_SHORTFALL_WITH_PAN.name())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .type(ProductCategory.ACCOUNT_BASED)
        .productFamilyID(ProductFamilyID.PRODUCT_FAMILY_ID_DEFAULT.getValue())
        .coaID(CoaID.COA_ID_DEFAULT.getValue())
        .coaNodeID(CoaNodeID.COA_NODE_ID_DEFAULT.getValue())
        .attributes(new HashMap<>())
        .build();
  }

  public static ProductFamily getDefaultProductFamily() {
    return ProductFamily.builder()
        .name(ProductFamilyName.GPR.name())
        .id(ProductFamilyID.PRODUCT_FAMILY_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .accountingType(AccountingType.LIABILITY)
        .coaID(CoaID.COA_ID_DEFAULT.getValue())
        .coaNodeID(CoaNodeID.COA_NODE_ID_DEFAULT.getValue())
        .parentCoaNodeID(ParentCoaNodeID.PARENT_COA_NODE_ID_DEFAULT.getValue())
        .attributes(new HashMap<>())
        .build();
  }

  public static CardProgram getDefaultCardProgram() {
    CardProgram dummyBase = new CardProgram.Builder()
        .businessID(GenericBusinessID.GENERIC_BUSINESS_ID_DEFAULT.getValue())
        .authToken("someToken")
        .productType(ProductType.Cash.name())
        .extraCardProgramConfig(null)
        .issuanceConfiguration(IssuanceConfiguration.newBuilder().isCardReloadable(true).types(Collections
            .emptyList()).build())
        .build().payload();
    return new CardProgram(
        GenericName.DEFAULT_NAME.getValue(),
        CardProgramID.CARD_PROGRAM_ID_DEFAULT.getValue(),
        Collections.emptyMap(),
        "1",
        dummyBase,
        IfiID.IFI_ID_DEFAULT.getValue(),
        GenericName.NAME_2.getValue());
  }

  public static Program getDefaultProgram() {
    return Program.builder()
        .id(ProgramID.PROGRAM_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .name(GenericName.DEFAULT_NAME.getValue())
        .code(GenericName.NAME_2.getValue())
        .status(ProgramStatus.ENABLED)
        .attributes(Collections.emptyMap())
        .build();
  }

  public static AccountProvider getDefaultAccountProvider() {
    return AccountProvider.builder()
        .id(AccountProviderID.ACCOUNT_PROVIDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .name(GenericName.DEFAULT_NAME.getValue())
        .state(EntityProviderState.ENABLED)
        .whitelistedProducts(Arrays.asList(ProductID.PRODUCT_ID_DEFAULT.getValue()))
        .whitelistedProductFamilies(Arrays.asList(ProductFamilyID.PRODUCT_FAMILY_ID_DEFAULT.getValue()))
        .accountProvisioningPolicies(
            AccountHolderProviderTestHelper.getDefaultAccountHolderProviderProvisioningPolicy())
        .build();
  }

  public static AccountHolderProvider getDefaultAccountHolderProvider() {
    return AccountHolderProvider
        .builder()
        .id(AccountHolderProviderID.ACCOUNT_HOLDER_PROVIDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .name(GenericName.DEFAULT_NAME.getValue())
        .state(EntityProviderState.ENABLED)
        .accountHolderProvisioningPolicies(
            AccountHolderProviderTestHelper.getDefaultAccountHolderProviderProvisioningPolicy())
        .attributes(getDefaultAttributes())
        .build();
  }
}
