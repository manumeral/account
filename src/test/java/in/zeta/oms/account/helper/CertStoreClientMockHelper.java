package in.zeta.oms.account.helper;

import com.google.inject.Inject;
import in.zeta.commons.crypto.SignableDocument;
import in.zeta.oms.certstore.client.CertStoreClient;
import org.mockito.Mockito;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.Matchers.any;

public class CertStoreClientMockHelper {
  private final CertStoreClient certStoreClient;

  @Inject
  public CertStoreClientMockHelper(CertStoreClient certStoreClient) {
    this.certStoreClient = certStoreClient;
  }

  public void mockVerifySignature() {
    Mockito.when(certStoreClient.verifySignature(any(SignableDocument.class)))
        .thenReturn(completedFuture(true));
  }
}
