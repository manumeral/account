package in.zeta.oms.account.helper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.accountRelation.AddAccountRelationRequest;
import in.zeta.oms.account.account.accountRelation.GetAccountRelationRequest;
import in.zeta.oms.account.account.accountRelation.UpdateAccountRelationRequest;
import in.zeta.oms.account.api.model.AccountRelation;
import in.zeta.oms.account.constant.AccountRelationID;
import in.zeta.oms.account.constant.Status;
import in.zeta.oms.account.account.relation.AccountRelationDAO;
import olympus.message.types.Request;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.MoreObjects.firstNonNull;
import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_2;
import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_DEFAULT;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static java.util.Collections.singletonList;

@Singleton
public class AccountRelationTestHelper extends AccountServiceBaseTest {
  private final AccountRelationDAO accountRelationDAO;

  @Inject
  public AccountRelationTestHelper(AccountRelationDAO accountRelationDAO) {
    this.accountRelationDAO = accountRelationDAO;
  }

  public static List<AccountRelation> getDefaultAccountRelations() {
    return singletonList(AccountRelation.builder()
        .id(AccountRelationID.ACCOUNT_RELATION_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .accountID(ACCOUNT_ID_DEFAULT.getValue())
        .relatedAccountID(ACCOUNT_ID_2.getValue())
        .relationshipType("funding")
        .status(Status.ENABLED.name())
        .attributes(Collections.emptyMap())
        .build());
  }

  public static Request<AddAccountRelationRequest> getDefaultAddAccountRelationRequest() {
    final AccountRelation accountRelation = getDefaultAccountRelations().get(0);
    return new AddAccountRelationRequest.Builder()
        .accountID(accountRelation.getAccountID())
        .relatedAccountID(firstNonNull(accountRelation.getRelatedAccountID(), ACCOUNT_ID_2.getValue()))
        .ifiID(accountRelation.getIfiID())
        .relationshipType(accountRelation.getRelationshipType())
        .status(accountRelation.getStatus())
        .attributes(accountRelation.getAttributes())
        .build();
  }

  public List<AccountRelation> getAccountRelationsForAccount(String accountID, Long ifiID) {
    return accountRelationDAO.getRelationships(accountID, ifiID).toCompletableFuture().join();
  }

  public void insertDefaultAccountRelations() {
    accountRelationDAO.insert(getDefaultAccountRelations().get(0)).toCompletableFuture().join();
  }

  public Optional<AccountRelation> getAccountRelationInDB(String accountRelationID, String accountID, Long ifiID) {
    return accountRelationDAO.get(accountRelationID, accountID, ifiID).toCompletableFuture().join();
  }

  public Request<GetAccountRelationRequest> getDefaultGetAccountRelationRequest() {
    final AccountRelation defaultAccountRelation = getDefaultAccountRelations().get(0);
    return new GetAccountRelationRequest.Builder()
        .accountRelationID(defaultAccountRelation.getId())
        .accountID(defaultAccountRelation.getAccountID())
        .ifiID(defaultAccountRelation.getIfiID())
        .build();
  }

  public Request<UpdateAccountRelationRequest> getUpdateAccountRelationRequest(AccountRelation accountRelation) {
    final AccountRelation defaultAccountRelation = getDefaultAccountRelations().get(0);
    return new UpdateAccountRelationRequest.Builder()
        .accountRelationID(defaultAccountRelation.getId())
        .accountID(defaultAccountRelation.getAccountID())
        .ifiID(defaultAccountRelation.getIfiID())
        .status(firstNonNull(accountRelation.getStatus(), accountRelation.getStatus()))
        .relatedAccountID(firstNonNull(accountRelation.getRelatedAccountID() ,defaultAccountRelation.getRelatedAccountID()))
        .relationshipType(firstNonNull(accountRelation.getRelationshipType(), defaultAccountRelation.getRelationshipType()))
        .attributes(firstNonNull(accountRelation.getAttributes(), defaultAccountRelation.getAttributes()))
        .build();
  }
}
