package in.zeta.oms.account.helper;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.account.*;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.model.*;
import in.zeta.oms.account.constant.*;
import in.zeta.oms.account.account.AccountDAO;
import java.time.LocalDateTime;
import lombok.Getter;
import olympus.message.types.Request;

import java.util.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.Range;

import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_DEFAULT;
import static in.zeta.oms.account.constant.GenericName.DEFAULT_NAME;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static in.zeta.oms.account.constant.IfiID.STAGE_IFI_SAMPLE_1;
import static in.zeta.oms.account.constant.ProductFamilyID.PRODUCT_FAMILY_ID_DEFAULT;
import static in.zeta.oms.account.constant.ProductID.PRODUCT_ID_DEFAULT;
import static in.zeta.oms.account.constant.ProgramID.PROGRAM_ID_2;
import static in.zeta.oms.account.constant.ProgramID.PROGRAM_ID_DEFAULT;
import static in.zeta.oms.account.constant.RequestID.REQUEST_ID_DEFAULT;
import static java.util.Collections.singletonList;

@Getter
@Singleton
public class AccountTestHelper {

  private final AccountDAO accountDAO;

  @Inject
  public AccountTestHelper(AccountDAO accountDAO) {
    this.accountDAO = accountDAO;
  }

  private static AccountStatus getDefaultAccountStatus() {
    return AccountStatus.ENABLED;
  }

  private static Map<String, String> getDefaultAttributes() {
    return new HashMap<>();
  }

  public static Account.Builder getDefaultAccountBuilder() {
    return Account.builder()
        .id(ACCOUNT_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .productFamilyID(PRODUCT_FAMILY_ID_DEFAULT.getValue())
        .ownerAccountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .productID(PRODUCT_ID_DEFAULT.getValue())
        .ledgerID(LedgerID.DEFAULT_LEDGER_ID.getValue())
        .programIDs(singletonList(PROGRAM_ID_DEFAULT.getValue()))
        .attributes(getDefaultAttributes())
        .accountProviderID(AccountProviderID.ACCOUNT_PROVIDER_ID_DEFAULT.getValue())
        .name(DEFAULT_NAME.getValue())
        .vectors(AccountVectorTestHelper.getDefaultAccountVectors())
        .accessors(AccessorTestHelper.getDefaultAccessors())
        .relationships(AccountRelationTestHelper.getDefaultAccountRelations())
        .status(getDefaultAccountStatus().name());
  }

  public static Account getDefaultAccount() {
    return getDefaultAccountBuilder().build();
  }

  public static Request<CreateAccountRequest> getDefaultCreateAccountRequest() {
    return getDefaultCreateAccountRequestBuilder().build();
  }

  public static CreateAccountRequest.Builder getDefaultCreateAccountRequestBuilder() {
    return CreateAccountRequest.builder()
        .requestID(REQUEST_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .productFamilyID(PRODUCT_FAMILY_ID_DEFAULT.getValue())
        .owner(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .productID(PRODUCT_ID_DEFAULT.getValue())
        .programIDs(singletonList(PROGRAM_ID_DEFAULT.getValue()))
        .attributes(getDefaultAttributes())
        .accountProviderID(AccountProviderID.ACCOUNT_PROVIDER_ID_DEFAULT.getValue())
        .name(DEFAULT_NAME.getValue())
        .status(Status.ENABLED.name())
        .accessors(singletonList(AccessorTestHelper.getDefaultAccessors().get(0).toBuilder().accountID(null).id(null).build()))
        .vectors(singletonList(AccountVectorTestHelper.getDefaultAccountVectors().get(0).toBuilder().accountID(null).id(null).build()))
        .relationships(singletonList(AccountRelationTestHelper.getDefaultAccountRelations().get(0).toBuilder().accountID(null).id(null).build()));
  }

  public static Request<CloseAccountRequest> getDefaultCloseAccountRequest(Account sourceAccount, String outboundAccountId) {
    return getDefaultCloseAccountRequestBuilder(sourceAccount, outboundAccountId).build();
  }

  public static CloseAccountRequest.Builder getDefaultCloseAccountRequestBuilder(Account sourceAccount, String outboundAccountID) {
    return CloseAccountRequest.builder()
            .requestID(REQUEST_ID_DEFAULT.getValue())
            .ifiID(sourceAccount.getIfiID())
            .accountID(sourceAccount.getId())
            .outboundAccountID(outboundAccountID)
            .remark("Closing Account")
            .attributes(getDefaultAttributes());
  }

  public static Request<CreateAccountRequest> getCreateAccountRequest(
      RequestID requestID,
      IfiID ifi,
      ProductFamilyID productFamilyID,
      AccountHolderID accountHolderID,
      ProductID productID,
      ProgramID programID,
      AccountProviderID accountProviderID,
      GenericName genericName) {
    return CreateAccountRequest.builder()
        .requestID(requestID.getValue())
        .ifiID(ifi.getValue())
        .productFamilyID(productFamilyID.getValue())
        .owner(accountHolderID.getValue())
        .productID(productID.getValue())
        .programIDs(singletonList(programID.getValue()))
        .attributes(getDefaultAttributes())
        .accountProviderID(accountProviderID.getValue())
        .name(genericName.getValue())
        .build();
  }

  public Account insertDefaultAccount() {
    return insertAccount(AccountTestHelper.getDefaultAccount(), REQUEST_ID_DEFAULT);
  }

  public void insertAccounts() {
    accountDAO.bulkInsertAccount(getAccounts()).toCompletableFuture().join();
  }

  public List<Account> getAccounts() {
    Map<String, String> attributes = new HashMap<>();
    attributes.put("card_Id", RandomStringUtils.randomNumeric(10));
    return ImmutableList.of(
        Account.builder()
            .id(UUID.randomUUID().toString())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .productFamilyID(PRODUCT_FAMILY_ID_DEFAULT.getValue())
            .ownerAccountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .productID(PRODUCT_ID_DEFAULT.getValue())
            .ledgerID(LedgerID.DEFAULT_LEDGER_ID.getValue())
            .programIDs(singletonList(PROGRAM_ID_DEFAULT.getValue()))
            .attributes(attributes)
            .accountProviderID(AccountProviderID.ACCOUNT_PROVIDER_ID_DEFAULT.getValue())
            .name(DEFAULT_NAME.getValue())
            .status(getDefaultAccountStatus().name())
            .createdAt(LocalDateTime.now())
            .updatedAt(LocalDateTime.now())
            .build());
  }

  public Account insertAccount(Account account, RequestID requestID) {
    return accountDAO.insert(account, requestID.getValue())
        .thenApply(accountID -> account.toBuilder().id(accountID).build())
        .toCompletableFuture().join();
  }

  public Optional<Account> getAccountBy(String accountID, Long ifiID) {
    return accountDAO.get(accountID, ifiID)
        .toCompletableFuture().join();
  }

  public Request<GetAccountListRequest> getGetAccountWithFilterRequestPayload(
      Long productID) {
    return GetAccountListRequest.builder()
        .ifiID(STAGE_IFI_SAMPLE_1.getValue())
        .programID(PROGRAM_ID_2.getValue())
        .productID(productID)
        .pageNumber(1L)
        .pageSize(10L)
        .build();
  }

  public GetAccountListRequestV2.Builder getGetAccountListRequestV2PayloadBuilder() {
    return GetAccountListRequestV2.builder()
        .ifiID(IFI_ID_DEFAULT.getValue())
        .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .status(getDefaultAccountStatus().name())
        .pageNumber(1L)
        .pageSize(10L);
  }

  public Request<UpdateAccountOwnerRequest> getDefaultUpdateAccountOwnerRequest(String id, Long ifiID, AccountHolderID accountHolderID) {
    return new UpdateAccountOwnerRequest.Builder()
        .accountHolderID(accountHolderID.getValue())
        .id(id)
        .ifiID(ifiID)
        .build();
  }

  public Request<UpdateAccountStatusRequest> getUpdateAccountStatusRequest(String accountID, Long ifiID, String status) {
    return UpdateAccountStatusRequest.builder()
        .id(accountID)
        .ifiID(ifiID)
        .status(status)
        .build();
  }

  public Request<UpdateProductForAccountRequest> getDefaultUpdateProductForAccountRequest(
      String accountID, Long ifiID, Long productID, String accountProviderID) {
    return UpdateProductForAccountRequest.builder()
        .id(accountID)
        .ifiID(ifiID)
        .productID(productID)
        .accountProviderID(accountProviderID)
        .build();
  }

  public Request<UpdateAccountOwnerRequest> getUpdateAccountOwnerRequestWithExternalID(String id, Long ifiID, ExternalID externalID, ExternalIDType externalIDType) {
    return new UpdateAccountOwnerRequest.Builder()
        .externalIDType(externalIDType)
        .externalAccountHolderID(externalID.getValue())
        .id(id)
        .ifiID(ifiID)
        .build();
  }

  public Request<GetAccountRequest> getDefaultGetAccountRequest() {
    return new GetAccountRequest.Builder()
        .accountID(ACCOUNT_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .build();
  }

  public Request<GetAccountInfoRequest> getDefaultGetAccountInfoRequest() {
    return new GetAccountInfoRequest.Builder()
        .ifiID(IFI_ID_DEFAULT.getValue())
        .accountID(ACCOUNT_ID_DEFAULT.getValue())
        .build();
  }

}
