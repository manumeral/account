package in.zeta.oms.account.helper;

import com.google.inject.Module;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.util.List;

public class AccountServiceDAOTestHelper {
  private static BasicDataSource basicDataSource;
  private static JdbcTemplate jdbcTemplate;

  public AccountServiceDAOTestHelper(List<Module> moduleList) {
    moduleList.add((binder) -> {
      if (basicDataSource == null) {
        openDb();
      }
      binder.bind(BasicDataSource.class).toInstance(basicDataSource);
      binder.bind(JdbcTemplate.class).toInstance(jdbcTemplate);
    });
  }

  public static void openDb() {
    basicDataSource = new BasicDataSource();
    basicDataSource.setMaxTotal(2);
    basicDataSource.setMaxWaitMillis(3000);
    basicDataSource.setPoolPreparedStatements(true);
    basicDataSource.setValidationQuery("select 1");
    basicDataSource.setDriverClassName("org.postgresql.Driver");
    basicDataSource.setUrl("jdbc:postgresql://localhost/cloud_card_test?user=cloud_card&password=");
    jdbcTemplate = new JdbcTemplate(basicDataSource);
  }

  public static void closeDb() throws SQLException {
    basicDataSource.close();
  }

  public void cleanDb() {
    jdbcTemplate.execute("truncate account cascade");
    jdbcTemplate.execute("truncate account_holder cascade");
    jdbcTemplate.execute("truncate whitelisted_account_holder_attributes");
    jdbcTemplate.execute("truncate account_relation");
    jdbcTemplate.execute("truncate account_vector");
    jdbcTemplate.execute("truncate accessor");
    jdbcTemplate.execute("truncate account_holder_vector");
    jdbcTemplate.execute("truncate pop");
    jdbcTemplate.execute("truncate account_holder_relation");
    jdbcTemplate.execute("truncate kyc_status cascade");
  }
}
