package in.zeta.oms.account.helper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.athenamanager.api.model.PolicyEntityType;
import in.zeta.oms.athenamanager.api.model.PolicyState;
import in.zeta.oms.athenamanager.api.request.GetIssuancePolicyListRequest;
import in.zeta.oms.athenamanager.api.request.GetIssuancePolicyListRequest.Builder;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyListResponse;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyResponse;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import lombok.Getter;
import org.mockito.ArgumentMatcher;
import org.mockito.stubbing.OngoingStubbing;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;

@Getter
@Singleton
public class IssuancePolicyTestHelper {

  private final AthenaManagerClient athenaManagerClient;

  @Inject
  public IssuancePolicyTestHelper(AthenaManagerClient athenaManagerClient) {
    this.athenaManagerClient = athenaManagerClient;
  }

  public static ArgumentMatcher<Builder> getProductIssuancePolicyListMatcher() {
    return getIssuancePolicyListRequestMatcher(PolicyEntityType.PRODUCT);
  }

  public static ArgumentMatcher<Builder> getProductFamilyIssuancePolicyListMatcher() {
    return getIssuancePolicyListRequestMatcher(PolicyEntityType.PRODUCT_FAMILY);
  }

  public static ArgumentMatcher<Builder> getProgramIssuancePolicyListMatcher() {
    return getIssuancePolicyListRequestMatcher(PolicyEntityType.PROGRAM);
  }

  public static ArgumentMatcher<Builder> getIssuancePolicyListRequestMatcher(PolicyEntityType entityType) {
    return new ArgumentMatcher<Builder>() {
      @Override
      public boolean matches(Object argument) {
        if (argument instanceof Builder) {
          GetIssuancePolicyListRequest getIssuancePolicyListRequest = ((Builder) argument).build()
              .payload();
          return getIssuancePolicyListRequest.getPolicyEntityType().equals(entityType);
        }

        return false;
      }
    };
  }

  public static IssuancePolicyResponse.Builder getActiveIssuancePolicyBuilder() {
    return IssuancePolicyResponse.builder()
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .state(PolicyState.ENABLED)
        .validFrom(LocalDateTime.now().minusDays(1))
        .validTill(LocalDateTime.now().plusDays(1))
        .allowedAccountHolderGroups(Collections.emptyList())
        .disAllowedAccountHolderGroups(Collections.emptyList());
  }

  public IssuancePolicyTestHelper mockDefaultIssuancePolicy() {
    final IssuancePolicyListResponse response = IssuancePolicyListResponse.builder()
        .issuancePolicyListResponse(new ArrayList<>())
        .build();
    mockIssuancePolicyBy(getProductFamilyIssuancePolicyListMatcher(), response);
    mockIssuancePolicyBy(getProductIssuancePolicyListMatcher(), response);
    mockIssuancePolicyBy(getProgramIssuancePolicyListMatcher(), response);

    return this;
  }

  public OngoingStubbing<CompletionStage<IssuancePolicyListResponse>> mockIssuancePolicyBy(
      ArgumentMatcher<Builder> CreateIssuancePolicyRequestMatcher,
      IssuancePolicyListResponse response) {
    return when(athenaManagerClient.getIssuancePolicyList(argThat(CreateIssuancePolicyRequestMatcher)))
        .thenReturn(completedFuture(response));
  }
}
