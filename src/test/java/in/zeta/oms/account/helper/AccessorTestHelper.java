package in.zeta.oms.account.helper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.accessor.AddAccountAccessorRequest;
import in.zeta.oms.account.account.accessor.GetAccountAccessorRequest;
import in.zeta.oms.account.account.accessor.UpdateAccountAccessorRequest;
import in.zeta.oms.account.api.model.Accessor;
import in.zeta.oms.account.constant.*;
import in.zeta.oms.account.account.accessor.AccessorDAO;
import olympus.message.types.Request;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.MoreObjects.firstNonNull;
import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_DEFAULT;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static java.util.Collections.singletonList;

@Singleton
public class AccessorTestHelper extends AccountServiceBaseTest {
  private final AccessorDAO accessorDAO;

  @Inject
  public AccessorTestHelper(AccessorDAO accessorDAO) {
    this.accessorDAO = accessorDAO;
  }


  public static List<Accessor> getDefaultAccessors() {
    return singletonList(
        Accessor.builder()
            .id(AccessorID.ACCESSOR_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .accountID(ACCOUNT_ID_DEFAULT.getValue())
            .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .status(Status.ENABLED.name())
            .transactionPolicyIDs(singletonList(TransactionPolicyID.POLICY_ID_DEFAULT.getValue()))
            .attributes(Collections.emptyMap())
            .build());
  }

  public List<Accessor> getAccessorsForAccount(String accountID, Long ifiID) {
    return accessorDAO.getAccessors(accountID, ifiID).toCompletableFuture().join();
  }

  public void insertDefaultAccessors() {
    accessorDAO.insert(getDefaultAccessors().get(0)).toCompletableFuture().join();
  }

  public Request<AddAccountAccessorRequest> getDefaultAddAccountAccessorRequest() {
    final Accessor defaultAccessor = getDefaultAccessors().get(0);
    return new AddAccountAccessorRequest.Builder()
        .accountID(defaultAccessor.getAccountID())
        .ifiID(defaultAccessor.getIfiID())
        .accountHolderID(defaultAccessor.getAccountHolderID())
        .attributes(defaultAccessor.getAttributes())
        .status(defaultAccessor.getStatus())
        .transactionPolicyIDs(defaultAccessor.getTransactionPolicyIDs())
        .build();
  }

  public Optional<Accessor> getAccountAccessorFromDB(String accessorID, String accountID, Long ifiID) {
    return accessorDAO.get(accessorID, accountID, ifiID).toCompletableFuture().join();
  }

  public Request<GetAccountAccessorRequest> getDefaultGetAccountAccessorRequest() {
    final Accessor defaultAccountAccessor = getDefaultAccessors().get(0);
    return new GetAccountAccessorRequest.Builder()
        .accountAccessorID(defaultAccountAccessor.getId())
        .accountID(defaultAccountAccessor.getAccountID())
        .ifiID(defaultAccountAccessor.getIfiID())
        .build();
  }

  public Request<UpdateAccountAccessorRequest> getUpdateAccountAccessorRequest(Accessor accessor) {
    final Accessor defaultAccountAccessor = getDefaultAccessors().get(0);
    return new UpdateAccountAccessorRequest.Builder()
        .accountAccessorID(defaultAccountAccessor.getId())
        .accountID(defaultAccountAccessor.getAccountID())
        .ifiID(defaultAccountAccessor.getIfiID())
        .status(firstNonNull(accessor.getStatus(), accessor.getStatus()))
        .attributes(firstNonNull(accessor.getAttributes(), defaultAccountAccessor.getAttributes()))
        .transactionPolicyIDs(firstNonNull(accessor.getTransactionPolicyIDs(), defaultAccountAccessor.getTransactionPolicyIDs()))
        .build();
  }
}
