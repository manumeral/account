package in.zeta.oms.account.helper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.accountVector.AddAccountVectorRequest;
import in.zeta.oms.account.account.accountVector.GetAccountVectorRequest;
import in.zeta.oms.account.account.accountVector.UpdateAccountVectorRequest;
import in.zeta.oms.account.api.model.AccountVector;
import in.zeta.oms.account.constant.AccountVectorID;
import in.zeta.oms.account.constant.Status;
import in.zeta.oms.account.account.vector.AccountVectorDAO;
import olympus.message.types.Request;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_DEFAULT;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

@Singleton
public class AccountVectorTestHelper extends AccountServiceBaseTest {
  private final AccountVectorDAO accountVectorDAO;

  @Inject
  public AccountVectorTestHelper(AccountVectorDAO accountVectorDAO) {
    this.accountVectorDAO = accountVectorDAO;
  }

  public static List<AccountVector> getDefaultAccountVectors() {
    return singletonList(AccountVector.builder()
        .id(AccountVectorID.ACCOUNT_VECTOR_ID_DEFAULT.getValue())
        .accountID(ACCOUNT_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .type("email")
        .value("abc@xyz.com")
        .status(Status.ENABLED.name())
        .attributes(Collections.emptyMap())
        .build());
  }

  public static Request<AddAccountVectorRequest> getDefaultAddAccountVectorRequest() {
    final AccountVector defaultAccountVector = getDefaultAccountVectors().get(0);
    return new AddAccountVectorRequest.Builder()
        .accountID(defaultAccountVector.getAccountID())
        .attributes(defaultAccountVector.getAttributes())
        .ifiID(defaultAccountVector.getIfiID())
        .status(defaultAccountVector.getStatus())
        .type(defaultAccountVector.getType())
        .value(defaultAccountVector.getValue())
        .build();
  }

  public List<AccountVector> getAccountVectorsForAccount(String accountID, Long ifiID) {
    return accountVectorDAO.getVectors(accountID, ifiID).toCompletableFuture().join();
  }

  public AccountVector insertDefaultAccountVector() {
    AccountVector defaultAccountVector = getDefaultAccountVectors().get(0);
    accountVectorDAO.insert(defaultAccountVector).toCompletableFuture().join();
    return defaultAccountVector;
  }

  public Optional<AccountVector> getAccountVectorFromDB(String accountVectorID, String accountID, Long ifiID) {
    return accountVectorDAO.get(accountVectorID, accountID, ifiID).toCompletableFuture().join();
  }

  public Request<GetAccountVectorRequest> getDefaultGetAccountVectorRequest() {
    final AccountVector defaultAccountVector = getDefaultAccountVectors().get(0);
    return new GetAccountVectorRequest.Builder()
        .accountID(defaultAccountVector.getAccountID())
        .accountVectorID(defaultAccountVector.getId())
        .ifiID(defaultAccountVector.getIfiID())
        .build();
  }

  public Request<UpdateAccountVectorRequest> getUpdateAccountVectorRequest(AccountVector accountVector) {
    final AccountVector defaultAccountVector = getDefaultAccountVectors().get(0);
    return new UpdateAccountVectorRequest.Builder()
        .accountVectorID(accountVector.getId())
        .accountID(accountVector.getAccountID())
        .ifiID(accountVector.getIfiID())
        .status(firstNonNull(accountVector.getStatus(), defaultAccountVector.getStatus()))
        .type(firstNonNull(accountVector.getType(), defaultAccountVector.getType()))
        .value(firstNonNull(accountVector.getValue(), defaultAccountVector.getValue()))
        .attributes(firstNonNull(accountVector.getAttributes(), defaultAccountVector.getAttributes()))
        .build();
  }
}
