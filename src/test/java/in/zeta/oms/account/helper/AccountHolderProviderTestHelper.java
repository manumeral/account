package in.zeta.oms.account.helper;

import com.google.gson.JsonObject;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.athenamanager.accountHolderProvider.AccountHolderProviderService;
import lombok.Getter;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Singleton
public class AccountHolderProviderTestHelper extends AccountServiceBaseTest {


  public AccountHolderProviderTestHelper() {
    super();
  }

  public static JsonObject getDefaultAccountHolderProviderProvisioningPolicy() {
    //TODO: Fix this once json schama validator is implemented
    return new JsonObject();
  }
}
