package in.zeta.oms.account.helper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyListResponse;
import in.zeta.oms.cloudcard.client.CloudCardClient;
import lombok.Getter;
import org.mockito.Mockito;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.Matchers.any;

@Getter
@Singleton
public class CloudCardClientMockHelper {

    private final CloudCardClient cloudCardClient;

    @Inject
    public CloudCardClientMockHelper(CloudCardClient cloudCardClient) {
        this.cloudCardClient = cloudCardClient;
    }

    public void mockCreateTransaction() {
        Mockito.when(cloudCardClient.createTransaction(any()))
                .thenReturn(completedFuture(null));
    }

}
