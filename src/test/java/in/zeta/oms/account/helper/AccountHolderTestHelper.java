package in.zeta.oms.account.helper;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.util.ZetaUrlConstants;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.UpdateAccountHolderRequest;
import in.zeta.oms.account.accountHolder.AccountHolderTransactionDAO;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusTestHelper;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.model.AccountHolder.Builder;
import in.zeta.oms.account.api.request.CreateAccountHolderRequest;
import in.zeta.oms.account.api.request.GetAccountHolderByVectorRequest;
import in.zeta.oms.account.api.request.GetAccountHolderRequest;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.EmailID;
import in.zeta.oms.account.constant.ExternalID;
import in.zeta.oms.account.constant.PhoneNumber;
import in.zeta.oms.account.constant.Status;
import in.zeta.oms.account.dao.AccountHolderDAO;
import olympus.message.types.Request;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static in.zeta.oms.account.api.enums.AccountHolderStatus.DISABLED;
import static in.zeta.oms.account.api.enums.AccountHolderStatus.ENABLED;
import static in.zeta.oms.account.api.enums.ExternalIDType.ZetaUserID;
import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_2;
import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.AccountHolderProviderID.ACCOUNT_HOLDER_PROVIDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.ExternalID.DEFAULT_EXTERNAL_ID;
import static in.zeta.oms.account.constant.GenericName.DEFAULT_NAME_ACCOUNT_HOLDER;
import static in.zeta.oms.account.constant.GenericName.NAME_2;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static in.zeta.oms.account.constant.KeyValue.KEY_ACCOUNT_HOLDER_CORP_ID;
import static in.zeta.oms.account.constant.KeyValue.KEY_ACCOUNT_HOLDER_CORP_NAME;
import static in.zeta.oms.account.constant.KeyValue.KEY_ENTITY_PROVIDER_1;
import static in.zeta.oms.account.constant.KeyValue.KEY_ENTITY_PROVIDER_2;
import static in.zeta.oms.account.constant.KeyValue.KEY_IS_ATM_ENABLED;
import static in.zeta.oms.account.constant.KeyValue.VALUE_ACCOUNT_HOLDER_CORP_DEFAULT;
import static in.zeta.oms.account.constant.KeyValue.VALUE_ACCOUNT_HOLDER_CORP_INVALID;
import static in.zeta.oms.account.constant.KeyValue.VALUE_ACCOUNT_HOLDER_CORP_NAME;
import static in.zeta.oms.account.constant.KeyValue.VALUE_ACCOUNT_HOLDER_CORP_NAME_1;
import static in.zeta.oms.account.constant.KeyValue.VALUE_ENTITY_PROVIDER_1;
import static in.zeta.oms.account.constant.KeyValue.VALUE_ENTITY_PROVIDER_2;
import static in.zeta.oms.account.constant.KeyValue.VALUE_IS_ATM_ENABLED;
import static in.zeta.oms.account.constant.RequestID.REQUEST_ID_2;
import static in.zeta.oms.account.constant.RequestID.REQUEST_ID_DEFAULT;
import static in.zeta.oms.athenamanager.api.enums.AccountHolderType.LEGAL;

@Singleton
public class AccountHolderTestHelper extends AccountServiceBaseTest {
  private final AccountHolderDAO accountHolderDAO;
  private final AccountHolderTransactionDAO accountHolderTransactionDAO;
  private final EntityGroupHelper entityGroupHelper;
  private final KYCStatusTestHelper kycStatusTestHelper;


  @Inject
  public AccountHolderTestHelper(AccountHolderDAO accountHolderDAO,
                                 AccountHolderTransactionDAO accountHolderTransactionDAO,
                                 EntityGroupHelper entityGroupHelper,
                                 KYCStatusTestHelper kycStatusTestHelper) {
    this.accountHolderDAO = accountHolderDAO;
    this.accountHolderTransactionDAO = accountHolderTransactionDAO;
    this.entityGroupHelper = entityGroupHelper;
    this.kycStatusTestHelper = kycStatusTestHelper;
  }

  public static Map<String, String> getDefaultAttributes() {
    return ATTRIBUTES;
  }

  private static final ImmutableMap<String, String> ATTRIBUTES = ImmutableMap
      .of(
          KEY_ENTITY_PROVIDER_1.getValue(), VALUE_ENTITY_PROVIDER_1.getValue(),
          KEY_ENTITY_PROVIDER_2.getValue(), VALUE_ENTITY_PROVIDER_2.getValue()
      );

  public static final Map<String, String> accountHolderAttributesValid =
      ImmutableMap.of(
          KEY_ENTITY_PROVIDER_1.getValue(), VALUE_ENTITY_PROVIDER_1.getValue(),
          KEY_ACCOUNT_HOLDER_CORP_ID.getValue(), VALUE_ACCOUNT_HOLDER_CORP_DEFAULT.getValue(),
          KEY_ACCOUNT_HOLDER_CORP_NAME.getValue(), VALUE_ACCOUNT_HOLDER_CORP_NAME.getValue(),
          KEY_IS_ATM_ENABLED.getValue(), VALUE_IS_ATM_ENABLED.getValue());


  public static final Map<String, String> accountHolderAttributesInvalid =
      ImmutableMap.of(
          KEY_ENTITY_PROVIDER_1.getValue(), VALUE_ENTITY_PROVIDER_1.getValue(),
          KEY_ACCOUNT_HOLDER_CORP_ID.getValue(), VALUE_ACCOUNT_HOLDER_CORP_INVALID.getValue(),
          KEY_ACCOUNT_HOLDER_CORP_NAME.getValue(), VALUE_ACCOUNT_HOLDER_CORP_NAME.getValue());

  public Map<String, String> accountHolderUpdatedAttributes =
          ImmutableMap.of(
                  KEY_ENTITY_PROVIDER_1.getValue(), VALUE_ENTITY_PROVIDER_1.getValue(),
                  KEY_ACCOUNT_HOLDER_CORP_ID.getValue(), VALUE_ACCOUNT_HOLDER_CORP_DEFAULT.getValue(),
                  KEY_ACCOUNT_HOLDER_CORP_NAME.getValue(), VALUE_ACCOUNT_HOLDER_CORP_NAME_1.getValue());

  public List<AccountHolderVector> vectors = ImmutableList.of(
      AccountHolderVector
          .builder()
          .value(EmailID.DEFAULT_EMAIL_ID.getValue())
          .type(ZetaUrlConstants.EMAIL_PREFIX)
          .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
          .build(),
      AccountHolderVector
          .builder()
          .value(PhoneNumber.PHONE_NUMBER_1.getValue())
          .type(ZetaUrlConstants.PHONE_NUMBER_PREFIX)
          .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
          .build());

  public List<AccountHolderVector> accountHolderVectors = ImmutableList.of(
      AccountHolderVector
          .builder()
          .id(new SecureRandomGenerator("SHA1PRNG").generateUUID())
          .ifiID(IFI_ID_DEFAULT.getValue())
          .status(Status.ENABLED.name())
          .value(EmailID.DEFAULT_EMAIL_ID.getValue())
          .type(ZetaUrlConstants.EMAIL_PREFIX)
          .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
          .attributes(Collections.emptyMap())
          .build(),
      AccountHolderVector
          .builder()
          .id(new SecureRandomGenerator("SHA1PRNG").generateUUID())
          .ifiID(IFI_ID_DEFAULT.getValue())
          .status(Status.ENABLED.name())
          .value(PhoneNumber.PHONE_NUMBER_1.getValue())
          .type(ZetaUrlConstants.PHONE_NUMBER_PREFIX)
          .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
          .attributes(Collections.emptyMap())
          .build(),
      AccountHolderVector
          .builder()
          .id(new SecureRandomGenerator("SHA1PRNG").generateUUID())
          .ifiID(IFI_ID_DEFAULT.getValue())
          .status(Status.ENABLED.name())
          .type(ZetaUserID.getAttributeKey())
          .value(DEFAULT_EXTERNAL_ID.getValue())
          .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
          .attributes(Collections.emptyMap())
          .build());
  public List<AccountHolderVector> accountHolder_2_Vectors = ImmutableList.of(
      AccountHolderVector
          .builder()
          .id(new SecureRandomGenerator("SHA1PRNG").generateUUID())
          .ifiID(IFI_ID_DEFAULT.getValue())
          .status(Status.ENABLED.name())
          .value(EmailID.DEFAULT_EMAIL_ID_2.getValue())
          .type(ZetaUserID.getAttributeKey())
          .accountHolderID(ACCOUNT_HOLDER_ID_2.getValue())
          .attributes(Collections.emptyMap())
          .build(),
      AccountHolderVector
          .builder()
          .id(new SecureRandomGenerator("SHA1PRNG").generateUUID())
          .ifiID(IFI_ID_DEFAULT.getValue())
          .status(Status.ENABLED.name())
          .value(PhoneNumber.PHONE_NUMBER_2.getValue())
          .type(ZetaUrlConstants.PHONE_NUMBER_PREFIX)
          .accountHolderID(ACCOUNT_HOLDER_ID_2.getValue())
          .attributes(Collections.emptyMap())
          .build(),
      AccountHolderVector
          .builder()
          .id(new SecureRandomGenerator("SHA1PRNG").generateUUID())
          .ifiID(IFI_ID_DEFAULT.getValue())
          .status(Status.ENABLED.name())
          .type(ZetaUserID.getAttributeKey())
          .value(ExternalID.ZETAUSER_EXTERNAL_ID.getValue())
          .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue())
          .attributes(Collections.emptyMap())
          .build()
          );

  public AccountHolder getDefaultAccountHolder() {
    return getDefaultAccountHolderBuilder()
        .build();
  }

  public Builder getDefaultAccountHolderBuilder() {
    return AccountHolder
        .builder()
        .requestID(REQUEST_ID_DEFAULT.getValue())
        .id(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .vectors(accountHolderVectors)
        .ifiID(IFI_ID_DEFAULT.getValue())
        .firstName(DEFAULT_NAME_ACCOUNT_HOLDER.getValue())
        .mothersMaidenName(NAME_2.getValue())
        .KYCStatus(kycStatusTestHelper.getDefaultEffectiveKycStatus())
        .type(LEGAL)
        .status(ENABLED.toString())
        .accountHolderProviderID(ACCOUNT_HOLDER_PROVIDER_ID_DEFAULT.getValue())
        .attributes(accountHolderAttributesValid)
        .tags(Collections.emptyList())
        .pops(Collections.emptyList());
  }

  public AccountHolder insertDefaultAccountHolder() {
    entityGroupHelper.mockDefaultEntityGroupForAccountHolder();
    return insertAccountHolder(getDefaultAccountHolder());
  }

  public AccountHolder insertDefaultAccountHolder3() {
    entityGroupHelper.mockDefaultEntityGroupForAccountHolder2();
    return insertAccountHolder(getDefaultAccountHolder());
  }

  public AccountHolder insertDefaultAccountHolder2() {
    entityGroupHelper.mockDefaultEntityGroupForAccountHolder();
    final AccountHolder accountHolder = AccountHolder.builder()
        .requestID(REQUEST_ID_2.getValue())
        .id(ACCOUNT_HOLDER_ID_2.getValue())
        .vectors(accountHolder_2_Vectors)
        .ifiID(IFI_ID_DEFAULT.getValue())
        .firstName(NAME_2.getValue())
        .type(LEGAL)
        .status(ENABLED.toString())
        .accountHolderProviderID(ACCOUNT_HOLDER_PROVIDER_ID_DEFAULT.getValue())
        .build();
    return insertAccountHolder(accountHolder);
  }

  public AccountHolder insertAccountHolder(AccountHolder defaultAccountHolder) {
    return accountHolderTransactionDAO
        .insertAccountHolder(defaultAccountHolder)
        .toCompletableFuture()
        .join();
  }

  public Request<CreateAccountHolderRequest> getDefaultCreateAccountHolderRequest() {
    return getDefaultCreateAccountHolderRequest(accountHolderAttributesValid);
  }

  private Request<CreateAccountHolderRequest> getDefaultCreateAccountHolderRequest(
      Map<String, String> attributes) {
    return getCreateAccountHolderRequestRequest(attributes, accountHolderVectors);
  }
  public Request<CreateAccountHolderRequest> getDefaultCreateAccountHolderWitHZetaUserIDRequest() {
    return getCreateAccountHolderRequestRequest(accountHolderAttributesValid, Collections.singletonList(accountHolderVectors.get(2)));
  }

  public Request<CreateAccountHolderRequest> getCreateAccountHolderWithInvalidVectorRequest() {
    return getCreateAccountHolderRequestRequest(accountHolderAttributesValid, Collections.singletonList(AccountHolderVector
        .builder()
        .id(new SecureRandomGenerator("SHA1PRNG").generateUUID())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .status(Status.ENABLED.name())
        .value("invalid_value")
        .type(ZetaUrlConstants.PHONE_NUMBER_PREFIX)
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .attributes(Collections.emptyMap())
        .build()));
  }

  private Request<CreateAccountHolderRequest> getCreateAccountHolderRequestRequest(
      Map<String, String> attributes, List<AccountHolderVector> vectors) {
    return CreateAccountHolderRequest
        .builder()
        .requestID(REQUEST_ID_DEFAULT.getValue())
        .vectors(vectors)
        .ifiID(IFI_ID_DEFAULT.getValue())
        .firstName(DEFAULT_NAME_ACCOUNT_HOLDER.getValue())
        .mothersMaidenName(NAME_2.getValue())
        .type(LEGAL)
        .accountHolderProviderID(ACCOUNT_HOLDER_PROVIDER_ID_DEFAULT.getValue())
        .attributes(attributes)
        .tags(Collections.emptyList())
        .build();
  }

  public Request<CreateAccountHolderRequest> getCreateAccountHolderRequestPayloadWithInvalidAttribute() {
    return getDefaultCreateAccountHolderRequest(accountHolderAttributesInvalid);
  }

  public Request<GetAccountHolderRequest> getGetAccountHolderByExternalIDRequestPayload() {
    return GetAccountHolderRequest
        .builder()
        .externalID(DEFAULT_EXTERNAL_ID.getValue())
        .externalIDType(ExternalIDType.ZetaUserID )
        .ifiID(IFI_ID_DEFAULT.getValue())
        .build();
  }

  public Request<GetAccountHolderRequest> getGetAccountHolderByIDRequestPayload() {
    return GetAccountHolderRequest
        .builder()
        .id(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .build();
  }

  public Request<UpdateAccountHolderRequest> getUpdateAccountHolderRequest() {
    return UpdateAccountHolderRequest
            .builder()
            .id(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .status(DISABLED.toString())
            .attributes(accountHolderUpdatedAttributes).build();
  }

  public Request<UpdateAccountHolderRequest> getInvalidUpdateAccountHolderRequest() {
    return UpdateAccountHolderRequest.builder()
        .id(AccountHolderID.ACCOUNT_HOLDER_ID_INVALID.getValue())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .status(DISABLED.toString())
        .attributes(accountHolderUpdatedAttributes)
        .build();
  }

  public Request<GetAccountHolderByVectorRequest> getAccountHolderByVectorRequest(String vectorType, String vectorValue) {
    return GetAccountHolderByVectorRequest.builder()
        .ifiID(IFI_ID_DEFAULT.getValue())
        .vectorType(vectorType)
        .vectorValue(vectorValue)
        .build();
  }
}
