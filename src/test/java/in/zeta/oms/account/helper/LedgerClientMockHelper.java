package in.zeta.oms.account.helper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.account.ledger.GetAccountBalanceResponse;
import in.zeta.oms.account.constant.AccountBalance;
import in.zeta.oms.account.constant.AccountingID;
import in.zeta.oms.account.constant.CoaNodeID;
import in.zeta.oms.ledger.api.AccountingType;
import in.zeta.oms.ledger.api.CreateLedgerResponsePayloadV4;
import in.zeta.oms.ledger.api.CreateLedgerResponsePayloadV5;
import in.zeta.oms.ledger.api.LedgerInfo;
import in.zeta.oms.ledger.api.coa.GetAllCoAsResponsePayload;
import in.zeta.oms.ledger.api.coa.GetCoAResponsePayload;
import in.zeta.oms.ledger.client.LedgerServiceClient;
import in.zeta.oms.ledger.model.CoAConfig;
import in.zeta.oms.ledger.model.CoaGraphNode;
import in.zeta.oms.ledger.model.CoaNodeType;
import lombok.Getter;
import olympus.message.types.EmptyPayload;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static java.util.concurrent.CompletableFuture.allOf;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.Matchers.any;

@Getter
@Singleton
public class LedgerClientMockHelper {

  private final LedgerServiceClient ledgerServiceClient;

  @Inject
  public LedgerClientMockHelper(LedgerServiceClient ledgerServiceClient) {
    this.ledgerServiceClient = ledgerServiceClient;
  }

  public static CreateLedgerResponsePayloadV5 getDefaultCreateLedgerResponse() {
    return CreateLedgerResponsePayloadV5.newBuilder()
        .ledgerID(AccountingID.ACCOUNTING_ID_DEFAULT.getValue())
        .accountingType(AccountingType.LIABILITY)
        .creditLimit(0L)
        .currency("INR")
        .debitors(Collections.emptyList())
        .ledgerAttrs(Collections.emptyMap())
        .build();
  }

  private GetCoAResponsePayload defaultGetCoaResponse() {
    return new GetCoAResponsePayload(new ArrayList<>(),new CoAConfig(
        CoaNodeID.COA_NODE_ID_2.getValue(),
        IFI_ID_DEFAULT.getValue(),
        "temp",
        CoaNodeID.COA_NODE_ID_2.getName(),
        1,
        2L,
        "INR"));
  }

  public CreateLedgerResponsePayloadV5 mockCreateLedger() {
    CreateLedgerResponsePayloadV5 response = getDefaultCreateLedgerResponse();
    Mockito.when(ledgerServiceClient.createLedgerV5(any()))
        .thenReturn(completedFuture(response));

    return response;
  }

  public GetCoAResponsePayload mockGetCoa() {
      GetCoAResponsePayload response = defaultGetCoaResponse();
    Mockito.when(ledgerServiceClient.getCoa(any()))
        .thenReturn(completedFuture(response));

    return response;
  }

  public void mockSetLedgerAttributes() {
    Mockito.when(ledgerServiceClient.setLedgerAttrs(any()))
        .thenReturn(completedFuture(null));
  }

  public void mockUpdateIFIProductType() {
    Mockito.when(ledgerServiceClient.updateIFIProductType(any()))
        .thenReturn(completedFuture(new EmptyPayload()));
  }

  public void mockUpdateLedgerState() {
    Mockito.when(ledgerServiceClient.updateLedgerState(any()))
        .thenReturn(completedFuture(new EmptyPayload()));
  }

  public void mockGetLedgerInfo() {
    Mockito.when(ledgerServiceClient.getLedgerInfo(any()))
        .thenReturn(completedFuture(new LedgerInfo.Builder()
            .balance(AccountBalance.DEFAULT_ACCOUNT_BALANCE.getBalance())
            .currency(AccountBalance.DEFAULT_ACCOUNT_BALANCE.getCurrency())
            .accountingType(AccountingType.LIABILITY)
            .build()));
  }

}
