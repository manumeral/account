package in.zeta.oms.account.helper;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.commons.json.JsonSchemaValidator;
import in.zeta.oms.athenamanager.api.model.AccountProvider;
import in.zeta.oms.athenamanager.api.model.Product;
import in.zeta.oms.athenamanager.api.model.ProductFamily;
import in.zeta.oms.athenamanager.api.request.GetAccountProviderRequest;
import in.zeta.oms.athenamanager.api.request.GetProgramByIDRequest;
import in.zeta.oms.athenamanager.api.response.GetProgramByIDResponse;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyListResponse;
import in.zeta.oms.athenamanager.baseSchema.BaseSchema;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import in.zeta.oms.cloudcard.api.CardProgram;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collections;

import static in.zeta.oms.account.helper.AthenaManagerHelper.getDefaultCardProgram;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@Getter
@Singleton
public class AthenaManagerClientMockHelper {

  private final AthenaManagerClient athenaManagerClient;

  @Inject
  public AthenaManagerClientMockHelper(AthenaManagerClient athenaManagerClient) {
    this.athenaManagerClient = athenaManagerClient;
  }


  public Product mockDefaultGetProduct() {
    final Product response = AthenaManagerHelper.getDefaultProduct();
    when(athenaManagerClient.getProduct(any()))
        .thenReturn(completedFuture(response));
    return response;
  }

  public Product mockGetProduct(Product product) {
    when(athenaManagerClient.getProduct(any()))
        .thenReturn(completedFuture(product));
    return product;
  }

  public ProductFamily mockDefaultGetProductFamily() {
    final ProductFamily response = AthenaManagerHelper.getDefaultProductFamily();
    when(athenaManagerClient.getProductFamily(any()))
        .thenReturn(completedFuture(response));
    return response;
  }

  public ProductFamily mockGetProductFamily(ProductFamily productFamily) {
    when(athenaManagerClient.getProductFamily(any()))
        .thenReturn(completedFuture(productFamily));
    return productFamily;
  }

  public CardProgram mockDefaultGetCardProgram() {
    final CardProgram cardProgram = getDefaultCardProgram();
    when(athenaManagerClient.getProgramByID(any(GetProgramByIDRequest.Builder.class)))
        .thenReturn(completedFuture(new GetProgramByIDResponse(null, getDefaultCardProgram())));
    return cardProgram;
  }

  public CardProgram mockGetCardProgram(CardProgram cardProgram) {
    when(athenaManagerClient.getProgramByID(any(GetProgramByIDRequest.Builder.class)))
        .thenReturn(completedFuture(new GetProgramByIDResponse(null, getDefaultCardProgram())));
    return cardProgram;
  }

  public AccountProvider mockDefaultGetAccountProvider() {
    AccountProvider accountProvider = AthenaManagerHelper.getDefaultAccountProvider();
    when(athenaManagerClient.getAccountProvider(any(GetAccountProviderRequest.Builder.class)))
        .thenReturn(completedFuture(accountProvider));
    return accountProvider;
  }

  public void mockDefaultGetAccountHolderProvider() {
    when(athenaManagerClient.getAccountHolderProvider(any()))
        .thenReturn(completedFuture(AthenaManagerHelper.getDefaultAccountHolderProvider()));
  }

  public void mockGetIssuancePolicyListRequest() {
    when(athenaManagerClient.getIssuancePolicyList(any()))
        .thenReturn(completedFuture(IssuancePolicyListResponse.builder().issuancePolicyListResponse(Collections.emptyList()).build()));
  }

  public void mockGetAccountProvider() {
    when(athenaManagerClient.getAccountProvider(any()))
        .thenReturn(completedFuture(AthenaManagerHelper.getDefaultAccountProvider()));
  }

  public void mockGetAccountHolderBaseSchema() {
    when(athenaManagerClient.getBaseSchema(any()))
        .thenReturn(completedFuture(BaseSchema.builder().schema(getJsonObject("account_holder_schema.json")).build()));
  }

  public void mockGetAccountBaseSchema() {
    when(athenaManagerClient.getBaseSchema(any()))
        .thenReturn(completedFuture(BaseSchema.builder().schema(getJsonObject("account_schema.json")).build()));
  }


  public static JsonObject getJsonObject(String resourceName) {
    try {
      InputStream in =
          JsonSchemaValidator.class.getClassLoader().getResourceAsStream(resourceName);
      return new Gson()
          .fromJson(IOUtils.toString(in, Charset.defaultCharset()), JsonObject.class);
    } catch (IOException e) {
      return new JsonObject();
    }
  }

}
