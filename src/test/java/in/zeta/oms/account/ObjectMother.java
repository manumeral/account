package in.zeta.oms.account;

import static net.javacrumbs.futureconverter.java8guava.FutureConverter.toCompletableFuture;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import in.zeta.commons.interceptors.authorization.ServiceAuthorizer;
import in.zeta.commons.json.JsonSchemaValidatorAsync;
import in.zeta.commons.postgres.PostgresModule;
import in.zeta.commons.settings.SettingsModule;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.server.pubsub.PubSubPublisher;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import in.zeta.oms.certstore.client.CertStoreClient;
import in.zeta.oms.cloudcard.client.CloudCardClient;
import in.zeta.oms.ledger.client.LedgerServiceClient;
import in.zeta.oms.user.client.UserServiceClient;
import in.zeta.oms.wallet.client.WalletClient;
import in.zeta.tags.TagService;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executors;
import olympus.common.JID;
import olympus.spartan.Spartan;

public class ObjectMother {

  private final PubSubPublisher mockPubsubPublisher = mock(PubSubPublisher.class);
  private final Injector injector;
  private final ZetaHostMessagingService mockMessagingService = mock(ZetaHostMessagingService.class);
  private final Spartan mockSpartan = mock(Spartan.class);
  private final AthenaManagerClient athenaManagerClient = mock(AthenaManagerClient.class);
  private final LedgerServiceClient ledgerServiceClient = mock(LedgerServiceClient.class);
  private final UserServiceClient userServiceClient = mock(UserServiceClient.class);
  private final CloudCardClient cloudCardClient = mock(CloudCardClient.class);
  private final WalletClient walletClient = mock(WalletClient.class);
  private final CertStoreClient certStoreClient = mock(CertStoreClient.class);
  private final ServiceAuthorizer serviceAuthorizer = mock(ServiceAuthorizer.class);
  private final JsonSchemaValidatorAsync jsonSchemaValidatorAsync = new JsonSchemaValidatorAsync(
      CacheBuilder.newBuilder(),
      mockMessagingService,
      Executors.newSingleThreadExecutor());

  public Injector getInjector() {
    return injector;
  }

  public ObjectMother(List<Module> modules) {
    createMockMessagingService();
    this.injector = Guice.createInjector(createModule(modules));
  }

  private List<Module> createModule(List<Module> modules) {
    List<Module> commonModules = getCommonModules();
    List<Module> moduleList = new ArrayList<>(modules);

    moduleList.addAll(commonModules);
    return moduleList;

  }

  private List<Module> getCommonModules() {
    List<Module> commonModuleList = new ArrayList<>();
    commonModuleList.add(binder -> {
        binder.bind(ZetaHostMessagingService.class).toInstance(mockMessagingService);
        binder.bind(AthenaManagerClient.class).toInstance(athenaManagerClient);
        binder.bind(LedgerServiceClient.class).toInstance(ledgerServiceClient);
        binder.bind(CloudCardClient.class).toInstance(cloudCardClient);
        binder.bind(WalletClient.class).toInstance(walletClient);
        binder.bind(UserServiceClient.class).toInstance(userServiceClient);
        binder.bind(PubSubPublisher.class).toInstance(mockPubsubPublisher);
        binder.bind(CertStoreClient.class).toInstance(certStoreClient);
        binder.bind(ServiceAuthorizer.class).toInstance(serviceAuthorizer);
        binder.bind(JsonSchemaValidatorAsync.class).toInstance(jsonSchemaValidatorAsync);
    });
    commonModuleList.add(new SettingsModule());
    commonModuleList.add(new AccountServiceTestModule());
    return commonModuleList;
  }

  private void createMockMessagingService() {
    when(mockSpartan.getBucket(isA(JID.class))).thenReturn(1);
    when(mockSpartan.getCurrentEntity()).thenReturn(new JID("test@account.zeta.in"));
    when(mockMessagingService.getSpartan()).thenReturn(mockSpartan);
    when(mockMessagingService.getInstanceJID()).thenReturn(new JID("1@account.services.olympus"));
    when(mockMessagingService.completeInSpartan(isA(CompletionStage.class)))
        .thenAnswer(inv -> inv.getArguments()[0]);
    when(mockMessagingService.completeInSpartan(isA(CompletionStage.class), isA(JID.class), isA(String.class)))
        .thenAnswer(inv -> inv.getArguments()[0]);
    when(mockMessagingService.completeInSpartan(isA(ListenableFuture.class)))
        .then(inv -> toCompletableFuture((ListenableFuture)inv.getArguments()[0]));
  }
}
