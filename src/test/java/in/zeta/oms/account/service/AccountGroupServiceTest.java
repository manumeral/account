package in.zeta.oms.account.service;

import com.google.common.collect.ImmutableList;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.constant.AccountGroupID;
import in.zeta.oms.account.helper.EntityGroupHelper;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.CompletionStage;

import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static in.zeta.oms.account.helper.AccountTestHelper.getDefaultAccount;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class AccountGroupServiceTest extends AccountServiceBaseTest {
    private AccountGroupService accountGroupService;
    private AthenaManagerClient athenaManagerClient;

    public AccountGroupServiceTest() {
        this.athenaManagerClient = getInstance(AthenaManagerClient.class);
        this.accountGroupService = getInstance(AccountGroupService.class);
    }

    @Test
    public void isPartOf_validParams_returnsTrue() {
        // Setup
        final Account account = getDefaultAccount();
        final List<String> accountGroups = ImmutableList.of(AccountGroupID.ACCOUNT_GROUP_ID_DEFAULT.getValue());
        when(athenaManagerClient.getEntityGroup(any()))
            .thenReturn(completedFuture(EntityGroupHelper.getDefaultEntityGroupForAccountBuilder().build()));

        // Run the test
        final CompletionStage<Boolean> result = accountGroupService.isPartOfAny(account, accountGroups, IFI_ID_DEFAULT.getValue());

        // Verify the results
        assertTrue(result.toCompletableFuture().join());
    }
}