package in.zeta.oms.account.service;


import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.accountHolder.AccountHolderTransactionDAO;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusService;
import in.zeta.oms.account.accountHolder.pop.POPService;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorService;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.request.CreateAccountHolderRequest;
import in.zeta.oms.account.athenamanager.accountHolderProvider.AccountHolderProviderService;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.AccountHolderProviderID;
import in.zeta.oms.account.constant.ExternalID;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.dao.AccountHolderDAO;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.account.helper.AthenaManagerClientMockHelper;
import in.zeta.tags.TagService;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class AccountHolderServiceTest extends AccountServiceBaseTest {

  private final AccountHolderTestHelper accountHolderTestHelper;

  private final AccountHolderService accountHolderServiceUnderTest;
  private final AthenaManagerClientMockHelper athenaManagerClientMockHelper;

  public AccountHolderServiceTest() {

    SecureRandomGenerator secureRandomGenerator = getInstance(SecureRandomGenerator.class);
    AccountHolderDAO accountHolderDAO = getInstance(AccountHolderDAO.class);
    AccountHolderTransactionDAO accountHolderTransactionDAO = getInstance(AccountHolderTransactionDAO.class);
    POPService popService = getInstance(POPService.class);
    AccountHolderVectorService accountHolderVectorService = getInstance(AccountHolderVectorService.class);
    AccountHolderProviderService accountHolderProviderService = getInstance(AccountHolderProviderService.class);
    BaseSchemaIssuerService baseSchemaIssuerService = getInstance(BaseSchemaIssuerService.class);
    KYCStatusService kycStatusService = getInstance(KYCStatusService.class);
    TagService tagService = getInstance(TagService.class);

    this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
    this.athenaManagerClientMockHelper = getInstance(AthenaManagerClientMockHelper.class);

    this.accountHolderServiceUnderTest = new AccountHolderService(
        accountHolderProviderService,
        secureRandomGenerator,
        accountHolderDAO,
        accountHolderTransactionDAO,
        accountHolderVectorService,
        popService,
        baseSchemaIssuerService,
        kycStatusService,
        tagService
    );
  }

  @Before
  public void setup() throws Exception {}

  @Test
  public void testCreate_valid_success() {
    // Setup
    athenaManagerClientMockHelper.mockGetAccountHolderBaseSchema();
    final CreateAccountHolderRequest payload = accountHolderTestHelper.getDefaultCreateAccountHolderRequest().payload();
    final AccountHolder expectedResult = accountHolderTestHelper.getDefaultAccountHolder();
    athenaManagerClientMockHelper.mockDefaultGetAccountHolderProvider();

    // Run the test 
    final AccountHolder result = accountHolderServiceUnderTest.create(payload)
        .toCompletableFuture().join();

    // Verify the results
    assertThat(result).usingRecursiveComparison()
        .ignoringFields("id", "vectors")
        .ignoringFields("pops")//TODO :: Doing this as CreatePayload has pops as null and but response has empty list
        .ignoringFields("KYCStatus")
        .isEqualTo(expectedResult);
  }

  @Test
  public void testGetByExternalID_valid_success() {
    // Setup 
    final String externalID = ExternalID.DEFAULT_EXTERNAL_ID.getValue();
    final String accountHolderProviderID = AccountHolderProviderID.ACCOUNT_HOLDER_PROVIDER_ID_DEFAULT.getValue();
    final Long ifiID = IfiID.IFI_ID_DEFAULT.getValue();
    final AccountHolder expectedResult = accountHolderTestHelper.getDefaultAccountHolder();

    // Run the test
    accountHolderTestHelper.insertDefaultAccountHolder();
    final AccountHolder result = accountHolderServiceUnderTest
        .getByVector(ifiID, ExternalIDType.ZetaUserID, externalID)
        .toCompletableFuture().join();

    // Verify the results
    assertThat(result).usingRecursiveComparison()
        .ignoringFields("vectors", "KYCStatus", "createdAt")
        .isEqualTo(expectedResult);

    // Verify vectors
    assertEquals(result.getVectors().size(), expectedResult.getVectors().size());
    assertThat(result.getVectors()).usingElementComparatorIgnoringFields("id", "createdAt", "updatedAt").isEqualTo(expectedResult.getVectors());
  }

  @Test
  public void testGetAccountHolderByID_valid_success() {
    // Setup 
    final Long ifiID = IfiID.IFI_ID_DEFAULT.getValue();
    final String id = AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue();
    final AccountHolder expectedResult = accountHolderTestHelper.getDefaultAccountHolder();

    // Run the test
    accountHolderTestHelper.insertDefaultAccountHolder();
    final AccountHolder result = accountHolderServiceUnderTest.get(ifiID, id)
        .toCompletableFuture().join();

    // Verify the results
    assertThat(result).usingRecursiveComparison()
        .ignoringFields("vectors", "KYCStatus", "createdAt")
        .isEqualTo(expectedResult);

    // Verify vectors
    assertEquals(result.getVectors().size(), expectedResult.getVectors().size());
    assertThat(result.getVectors()).usingElementComparatorIgnoringFields("id", "createdAt", "updatedAt").isEqualTo(expectedResult.getVectors());
  }
}
