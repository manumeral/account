package in.zeta.oms.account.service;

import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.AccountDAO;
import in.zeta.oms.account.account.AccountService;
import in.zeta.oms.account.account.AccountTransactionDAO;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributesService;
import in.zeta.oms.account.account.accessor.AccessorService;
import in.zeta.oms.account.account.relation.AccountRelationService;
import in.zeta.oms.account.account.vector.AccountVectorService;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusService;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorService;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.exception.AccountCreationException;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.athenamanager.accountProvider.AccountProviderService;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.AccountID;
import in.zeta.oms.account.constant.EntityGroupID;
import in.zeta.oms.account.constant.ExternalID;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.constant.RequestID;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.account.helper.AccountTestHelper;
import in.zeta.oms.account.helper.AthenaManagerClientMockHelper;
import in.zeta.oms.account.helper.IssuancePolicyTestHelper;
import in.zeta.oms.account.helper.LedgerClientMockHelper;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyListResponse;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyResponse;
import in.zeta.tags.TagService;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

import static in.zeta.oms.account.helper.AccountTestHelper.getDefaultAccount;
import static in.zeta.oms.account.helper.IssuancePolicyTestHelper.getProductIssuancePolicyListMatcher;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountServiceTest extends AccountServiceBaseTest {

  private final AccountService accountServiceUnderTest;
  private final AccountTestHelper accountTestHelper;
  private final AccountHolderTestHelper accountHolderTestHelper;
  private final IssuancePolicyTestHelper issuancePolicyTestHelper;
  private final AthenaManagerClientMockHelper athenaManagerClientMockHelper;
  private final LedgerClientMockHelper ledgerClientMockHelper;

  private final String[] fieldsToIgnore = {
      "accessors.createdAt",
      "accessors.transactionPolicyIDs",
      "accessors.updatedAt",
      "createdAt",
      "relationships.createdAt",
      "relationships.updatedAt",
      "updatedAt",
      "vectors.createdAt",
      "vectors.updatedAt",
      "programIDs",
      "tags"
  };


  public AccountServiceTest() {
    AccountDAO mockAccountDAO = getInstance(AccountDAO.class);
    LedgerService mockLedgerService = getInstance(LedgerService.class);
    CloudCardService mockCloudCardClient = getInstance(CloudCardService.class);
    AccountHolderService mockAccountHolderService = getInstance(AccountHolderService.class);
    IssuancePolicyService mockIssuancePolicyService = getInstance(IssuancePolicyService.class);
    SecureRandomGenerator secureRandomGenerator = getInstance(SecureRandomGenerator.class);
    AthenaManagerService athenaManagerService = getInstance(AthenaManagerService.class);
    AccountProviderService accountProviderService = getInstance(AccountProviderService.class);
    KYCStatusService mockKycStatusService = getInstance(KYCStatusService.class);
    AccessorService mockAccessorService = getInstance(AccessorService.class);
    AccountVectorService mockAccountVectorService = getInstance(AccountVectorService.class);
    AccountRelationService mockAccountRelationService = getInstance(AccountRelationService.class);
    AccountTransactionDAO mockAccountTransactionDAO = getInstance(AccountTransactionDAO.class);
    AccountHolderVectorService mockAccountHolderVectorService = getInstance(AccountHolderVectorService.class);
    BaseSchemaIssuerService baseSchemaIssuerService = getInstance(BaseSchemaIssuerService.class);
    WhitelistedAccountHolderAttributesService mockWhitelistedAccountHolderAttributesService = getInstance(WhitelistedAccountHolderAttributesService.class);
    TagService tagService = getInstance(TagService.class);

    this.accountTestHelper = getInstance(AccountTestHelper.class);
    this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
    this.issuancePolicyTestHelper = getInstance(IssuancePolicyTestHelper.class);
    this.athenaManagerClientMockHelper = getInstance(AthenaManagerClientMockHelper.class);
    this.ledgerClientMockHelper = getInstance(LedgerClientMockHelper.class);
    this.accountServiceUnderTest =
        new AccountService(
            mockAccountDAO,
            mockLedgerService,
            mockCloudCardClient,
            mockAccountHolderService,
            mockIssuancePolicyService,
            secureRandomGenerator,
            athenaManagerService,
            accountProviderService,
            mockAccountTransactionDAO,
            mockWhitelistedAccountHolderAttributesService,
            mockAccessorService,
            mockAccountVectorService,
            mockAccountRelationService,
            mockAccountHolderVectorService,
            baseSchemaIssuerService,
            mockKycStatusService,
            tagService);
  }

  @Before
  public void setup() throws Exception {
  }

  @Test
  public void testCreateAccount_valid_success() {
    // Setup
    final Account defaultAccount = AccountTestHelper.getDefaultAccount();
    accountHolderTestHelper.insertDefaultAccountHolder();
    issuancePolicyTestHelper.mockDefaultIssuancePolicy();
    athenaManagerClientMockHelper.mockDefaultGetProduct();
    athenaManagerClientMockHelper.mockDefaultGetProductFamily();
    ledgerClientMockHelper.mockCreateLedger();
    ledgerClientMockHelper.mockGetCoa();

    accountTestHelper.insertAccount(AccountTestHelper.getDefaultAccountBuilder().id(AccountID.ACCOUNT_ID_2.getValue()).build(), RequestID.REQUEST_ID_2);
    // Run the test
    final Account result =
        accountServiceUnderTest
            .createAccount(
                getDefaultAccount(),
                RequestID.REQUEST_ID_DEFAULT.getValue(),
                ExternalIDType.ZetaUserID,
                ExternalID.DEFAULT_EXTERNAL_ID.getValue())
            .toCompletableFuture()
            .join();
    Optional<Account> accountFromDB = accountTestHelper
        .getAccountBy(result.getId(), result.getIfiID());

    // Verify the results
    assertThat(result).usingRecursiveComparison()
        .ignoringFields("id", "attributes", "accessors.id", "relationships.id", "vectors.accountID", "vectors.id", "relationships.accountID", "accessors.accountID")
        .isEqualTo(defaultAccount);

    assertThat(accountFromDB).isPresent()
        .hasValueSatisfying(actual -> assertThat(actual)
            .usingRecursiveComparison()
            .ignoringFields("id", "attributes", "accessors", "vectors", "createdAt", "relationships", "updatedAt", "programIDs")
            .isEqualTo(defaultAccount));
  }

  @Test
  public void testCreateAccount_ProductPoliciesAccountHolderAllowed_success() {
    ledgerClientMockHelper.mockGetCoa();
    // Setup
    final Account account = AccountTestHelper.getDefaultAccount();
    final IssuancePolicyResponse issuancePolicyResponse = IssuancePolicyTestHelper
        .getActiveIssuancePolicyBuilder()
        .allowedAccountHolderGroups(
            Collections.singletonList(EntityGroupID.ENTITY_GROUP_ID_DEFAULT.getValue()))
        .build();
    final IssuancePolicyListResponse issuancePolicyListResponse = IssuancePolicyListResponse.builder()
        .issuancePolicyListResponse(Collections.singletonList(issuancePolicyResponse)).build();

    accountHolderTestHelper.insertDefaultAccountHolder();
    //add related account
    accountTestHelper.insertAccount(AccountTestHelper.getDefaultAccountBuilder().id(AccountID.ACCOUNT_ID_2.getValue()).build(), RequestID.REQUEST_ID_2);
    issuancePolicyTestHelper.mockDefaultIssuancePolicy()
        .mockIssuancePolicyBy(getProductIssuancePolicyListMatcher(), issuancePolicyListResponse);
    athenaManagerClientMockHelper.mockDefaultGetProduct();
    athenaManagerClientMockHelper.mockDefaultGetProductFamily();
    ledgerClientMockHelper.mockCreateLedger();

    // Run the test
    final Account result = accountServiceUnderTest.createAccount(
        getDefaultAccount(),
        RequestID.REQUEST_ID_DEFAULT.getValue(),
        ExternalIDType.ZetaUserID,
        ExternalID.DEFAULT_EXTERNAL_ID.getValue())
        .toCompletableFuture().join();

    // Verify the results
    assertThat(result).usingRecursiveComparison()
        .ignoringFields("id", "attributes", "accessors.id", "relationships.id", "vectors.accountID", "vectors.id", "relationships.accountID", "accessors.accountID")
        .isEqualTo(account);
  }

  @Test
  public void testCreateAccount_ProductPolicies_throwsException() {
    // Setup
    final IssuancePolicyResponse issuancePolicyResponse = IssuancePolicyTestHelper
        .getActiveIssuancePolicyBuilder()
        .allowedAccountHolderGroups(
            Collections.singletonList(EntityGroupID.ENTITY_GROUP_ID_DEFAULT.getValue()))
        .build();
    final IssuancePolicyListResponse issuancePolicyListResponse = IssuancePolicyListResponse.builder()
        .issuancePolicyListResponse(Collections.singletonList(issuancePolicyResponse)).build();

    accountHolderTestHelper.insertDefaultAccountHolder3();
    issuancePolicyTestHelper.mockDefaultIssuancePolicy()
        .mockIssuancePolicyBy(getProductIssuancePolicyListMatcher(), issuancePolicyListResponse);
    athenaManagerClientMockHelper.mockDefaultGetProduct();
    athenaManagerClientMockHelper.mockDefaultGetProductFamily();
    ledgerClientMockHelper.mockCreateLedger();
    ledgerClientMockHelper.mockGetCoa();

    // Run the test
    final CompletionStage<Account> result = accountServiceUnderTest.createAccount(
        getDefaultAccount(),
        RequestID.REQUEST_ID_DEFAULT.getValue(),
        ExternalIDType.ZetaUserID,
        ExternalID.DEFAULT_EXTERNAL_ID.getValue()).toCompletableFuture();

    // Verify the results
    hasFailedWithThrowableThat(result)
        .isInstanceOf(AccountCreationException.class)
        .hasMessage(
            String.format(
                "Account Holder group for account holder id: %s not permitted",
                AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue()));
  }

  // TODO: Move to athena commons
  private AbstractThrowableAssert<?, ? extends Throwable> hasFailedWithThrowableThat(CompletionStage<?> actual) {
    try {
      actual.toCompletableFuture().join();
      return AssertionsForClassTypes.assertThat((Throwable) null);
    } catch (CompletionException e) {
      return AssertionsForClassTypes.assertThat(e.getCause());
    }
  }

  @Test
  public void testGetAccount_valid_success() {
    // Setup 
    final Account defaultAccount = accountTestHelper.insertDefaultAccount();
    final Long ifiID = IfiID.IFI_ID_DEFAULT.getValue();
    final String id = defaultAccount.getId();

    // Run the test
    final Account result = accountServiceUnderTest.getAccount(ifiID, id).toCompletableFuture()
        .join();

    // Verify the results
    assertThat(result).usingRecursiveComparison()
        .ignoringFields("programIDs", "createdAt", "updatedAt", "accessors", "relationships", "vectors", "tags")
        .isEqualTo(defaultAccount);
  }
}
