package in.zeta.oms.account.service;

import com.google.gson.Gson;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletionStage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static in.zeta.oms.account.constant.EntityGroupID.ENTITY_GROUP_ID_DEFAULT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

public class AccountHolderGroupServiceTest extends AccountServiceBaseTest {

  private AccountHolderTestHelper accountHolderTestHelper;
  private AccountHolderGroupService accountHolderGroupServiceUnderTest;

  @Before
  public void setUp() {
    accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
    accountHolderGroupServiceUnderTest = getInstance(AccountHolderGroupService.class);
  }

  @Test
  public void testIsPartOfAny() {
    // Setup
    accountHolderTestHelper.insertDefaultAccountHolder();
    final AccountHolder accountHolder = accountHolderTestHelper.getDefaultAccountHolder();
    final List<String> groupIDs = Collections.singletonList(ENTITY_GROUP_ID_DEFAULT.getValue());

    // Run the test 
    final Boolean result = accountHolderGroupServiceUnderTest
        .isPartOfAny(IfiID.IFI_ID_DEFAULT.getValue(), accountHolder, groupIDs).toCompletableFuture()
        .join();

    // Verify the results 
    assertTrue(result);
  }
}
