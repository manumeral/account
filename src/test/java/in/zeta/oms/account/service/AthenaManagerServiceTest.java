package in.zeta.oms.account.service;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.helper.AthenaManagerClientMockHelper;
import in.zeta.oms.account.helper.AthenaManagerHelper;
import in.zeta.oms.athenamanager.api.model.AccountHolderProvider;
import in.zeta.oms.athenamanager.api.model.AccountProvider;
import in.zeta.oms.athenamanager.api.model.Product;
import in.zeta.oms.athenamanager.api.model.ProductFamily;
import in.zeta.oms.cloudcard.api.CardProgram;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class AthenaManagerServiceTest extends AccountServiceBaseTest {
  private final AthenaManagerService athenaManagerService;
  private final AthenaManagerClientMockHelper athenaManagerClientMockHelper;

  public AthenaManagerServiceTest() {
    this.athenaManagerService = getInstance(AthenaManagerService.class);
    this.athenaManagerClientMockHelper = getInstance(AthenaManagerClientMockHelper.class);
  }

  @Test
  public void getCachedProduct_validParam_success() {
    Product product = AthenaManagerHelper.getDefaultProduct();
    athenaManagerClientMockHelper.mockGetProduct(product);
    Product fetchedProduct = athenaManagerService.getProduct(product.getIfiID(), product.getId()).toCompletableFuture().join();
    assertThat(fetchedProduct)
        .usingRecursiveComparison()
        .isEqualTo(product);
    Product cachedProduct = athenaManagerService.getProduct(product.getIfiID(), product.getId()).toCompletableFuture().join();
    assertThat(cachedProduct)
        .usingRecursiveComparison()
        .isEqualTo(product);
    verify(athenaManagerClientMockHelper.getAthenaManagerClient(), times(1)).getProduct(any());
  }

  @Test
  public void getCachedProductFamily_validParam_success() {
    ProductFamily productFamily = AthenaManagerHelper.getDefaultProductFamily();
    athenaManagerClientMockHelper.mockGetProductFamily(productFamily);
    ProductFamily fetchedProductFamily = athenaManagerService.getProductFamily(productFamily.getIfiID(), productFamily.getId()).toCompletableFuture().join();
    assertThat(fetchedProductFamily)
        .usingRecursiveComparison()
        .isEqualTo(productFamily);
    ProductFamily cachedProductFamily = athenaManagerService.getProductFamily(productFamily.getIfiID(), productFamily.getId()).toCompletableFuture().join();
    assertThat(cachedProductFamily)
        .usingRecursiveComparison()
        .isEqualTo(productFamily);
    verify(athenaManagerClientMockHelper.getAthenaManagerClient(), times(1)).getProductFamily(any());
  }

  @Test
  public void getCachedAccountProvider_validParam_success() {
    athenaManagerClientMockHelper.mockGetAccountProvider();
    AccountProvider accountProvider = AthenaManagerHelper.getDefaultAccountProvider();
    AccountProvider fetchedAccountProvider = athenaManagerService.getAccountProvider(accountProvider.getId(), accountProvider.getIfiID()).toCompletableFuture().join();
    assertThat(fetchedAccountProvider)
        .usingRecursiveComparison()
        .isEqualTo(accountProvider);
    AccountProvider cachedAccountProvider = athenaManagerService.getAccountProvider(accountProvider.getId(), accountProvider.getIfiID()).toCompletableFuture().join();
    assertThat(cachedAccountProvider)
        .usingRecursiveComparison()
        .isEqualTo(accountProvider);
    verify(athenaManagerClientMockHelper.getAthenaManagerClient(), times(1)).getAccountProvider(any());
  }

  @Test
  public void getCachedAccountHolderProvider__validParam_success() {
    AccountHolderProvider accountHolderProvider = AthenaManagerHelper.getDefaultAccountHolderProvider();
    athenaManagerClientMockHelper.mockDefaultGetAccountHolderProvider();
    AccountHolderProvider fetchedAccountHolderProvider = athenaManagerService.getAccountHolderProvider(accountHolderProvider.getId(), accountHolderProvider.getIfiID()).toCompletableFuture().join();
    assertThat(fetchedAccountHolderProvider)
        .usingRecursiveComparison()
        .isEqualTo(accountHolderProvider);
    AccountHolderProvider cachedAccountHolderProvider = athenaManagerService.getAccountHolderProvider(accountHolderProvider.getId(), accountHolderProvider.getIfiID()).toCompletableFuture().join();
    assertThat(cachedAccountHolderProvider)
        .usingRecursiveComparison()
        .isEqualTo(accountHolderProvider);
    verify(athenaManagerClientMockHelper.getAthenaManagerClient(), times(1)).getAccountHolderProvider(any());
  }

}
