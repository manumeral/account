package in.zeta.oms.account.api;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.account.vector.AccountVectorHandler;
import in.zeta.oms.account.account.accountVector.AddAccountVectorRequest;
import in.zeta.oms.account.account.accountVector.GetAccountVectorRequest;
import in.zeta.oms.account.account.accountVector.UpdateAccountVectorRequest;
import in.zeta.oms.account.api.model.AccountVector;
import in.zeta.oms.account.helper.AccountTestHelper;
import in.zeta.oms.account.helper.AccountVectorTestHelper;
import olympus.message.types.Request;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AccountVectorHandlerTest extends AccountServiceBaseTest {

  private static final String UPDATED_VALUE = "updatedValue";
  private static final String UPDATED_TYPE = "updatedType";
  private static final Map<String, String> UPDATED_ATTRIBUTES = Collections.singletonMap("key", "value");
  private final AccountVectorTestHelper accountVectorTestHelper;
  private final AccountTestHelper accountTestHelper;
  private final AccountVectorHandler handler;

  public AccountVectorHandlerTest() {
    this.accountVectorTestHelper = getInstance(AccountVectorTestHelper.class);
    this.accountTestHelper = getInstance(AccountTestHelper.class);
    this.handler = getInstance(AccountVectorHandler.class);
  }

  @Before
  public void setup() {
    accountTestHelper.insertDefaultAccount();
  }

  @Test
  public void create_ValidParams_Success() {
    Request<AddAccountVectorRequest> request =
        AccountVectorTestHelper.getDefaultAddAccountVectorRequest();
    handler.on(request.payload(), request);
    final AccountVector response = captureResponsePayload(request, AccountVector.class);
    final Optional<AccountVector> accountVectorFromDB =
        accountVectorTestHelper.getAccountVectorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountVectorFromDB.isPresent());
    assertThat(accountVectorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void getByID_ValidParams_Success() {
    accountVectorTestHelper.insertDefaultAccountVector();
    Request<GetAccountVectorRequest> request = accountVectorTestHelper.getDefaultGetAccountVectorRequest();
    handler.on(request.payload(), request);
    final AccountVector response = captureResponsePayload(request, AccountVector.class);
    final Optional<AccountVector> accountVectorFromDB = accountVectorTestHelper.getAccountVectorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountVectorFromDB.isPresent());
    assertThat(accountVectorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void getList_ValidParams_Success() {
    //TODO : List API test will be added later
  }

  @Test
  public void updateStatus_ValidParams_Success() {
    final AccountVector defaultAccountVector = AccountVectorTestHelper.getDefaultAccountVectors().get(0);
    accountVectorTestHelper.insertDefaultAccountVector();
    Request<UpdateAccountVectorRequest> request =
        accountVectorTestHelper.getUpdateAccountVectorRequest(
            defaultAccountVector.toBuilder().status(Status.DISABLED.name()).build());
    handler.on(request.payload(), request);
    final AccountVector response = captureResponsePayload(request, AccountVector.class);
    final Optional<AccountVector> accountVectorFromDB = accountVectorTestHelper.getAccountVectorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountVectorFromDB.isPresent());
    assertThat(accountVectorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void updateTypeValue_ValidParams_Success() {
    final AccountVector defaultAccountVector = AccountVectorTestHelper.getDefaultAccountVectors().get(0);
    accountVectorTestHelper.insertDefaultAccountVector();
    Request<UpdateAccountVectorRequest> request = accountVectorTestHelper.getUpdateAccountVectorRequest(defaultAccountVector.toBuilder().type(UPDATED_TYPE).value(UPDATED_VALUE).build());
    handler.on(request.payload(), request);
    final AccountVector response = captureResponsePayload(request, AccountVector.class);
    final Optional<AccountVector> accountVectorFromDB = accountVectorTestHelper.getAccountVectorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountVectorFromDB.isPresent());
    assertThat(accountVectorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void updateAttributes_ValidParams_Success() {
    final AccountVector defaultAccountVector = AccountVectorTestHelper.getDefaultAccountVectors().get(0);
    accountVectorTestHelper.insertDefaultAccountVector();
    Request<UpdateAccountVectorRequest> request = accountVectorTestHelper.getUpdateAccountVectorRequest(defaultAccountVector.toBuilder().attributes(UPDATED_ATTRIBUTES).build());
    handler.on(request.payload(), request);
    final AccountVector response = captureResponsePayload(request, AccountVector.class);
    final Optional<AccountVector> accountVectorFromDB = accountVectorTestHelper.getAccountVectorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountVectorFromDB.isPresent());
    assertThat(accountVectorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }
}
