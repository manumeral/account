package in.zeta.oms.account.api;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.AccountHandler;
import in.zeta.oms.account.account.CloseAccountRequest;
import in.zeta.oms.account.account.CloseAccountResponse;
import in.zeta.oms.account.account.CreateAccountRequest;
import in.zeta.oms.account.account.GetAccountInfoRequest;
import in.zeta.oms.account.account.GetAccountListRequest;
import in.zeta.oms.account.account.GetAccountListRequestV2;
import in.zeta.oms.account.account.GetAccountRequest;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.account.UpdateAccountOwnerRequest;
import in.zeta.oms.account.account.UpdateAccountStatusRequest;
import in.zeta.oms.account.account.UpdateProductForAccountRequest;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusTestHelper;
import in.zeta.oms.account.api.enums.AccountHolderStatus;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.exception.AccountCreationException;
import in.zeta.oms.account.api.model.Accessor;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.model.AccountInfo;
import in.zeta.oms.account.api.model.AccountRelation;
import in.zeta.oms.account.api.model.AccountVector;
import in.zeta.oms.account.api.response.GetAccountListResponse;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.AccountHolderProviderID;
import in.zeta.oms.account.constant.AccountProviderID;
import in.zeta.oms.account.constant.AccountVectorID;
import in.zeta.oms.account.constant.CoaID;
import in.zeta.oms.account.constant.CoaNodeID;
import in.zeta.oms.account.constant.ExternalID;
import in.zeta.oms.account.constant.GenericName;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.constant.ProductFamilyID;
import in.zeta.oms.account.constant.ProductFamilyName;
import in.zeta.oms.account.constant.ProductID;
import in.zeta.oms.account.constant.ProductName;
import in.zeta.oms.account.constant.RequestID;
import in.zeta.oms.account.helper.AccessorTestHelper;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.account.helper.AccountRelationTestHelper;
import in.zeta.oms.account.helper.AccountTestHelper;
import in.zeta.oms.account.helper.AccountVectorTestHelper;
import in.zeta.oms.account.helper.AthenaManagerClientMockHelper;
import in.zeta.oms.account.helper.AthenaManagerHelper;
import in.zeta.oms.account.helper.CertStoreClientMockHelper;
import in.zeta.oms.account.helper.CloudCardClientMockHelper;
import in.zeta.oms.account.helper.LedgerClientMockHelper;
import in.zeta.oms.athenamanager.api.enums.AccountHolderType;
import in.zeta.oms.athenamanager.api.enums.ProductState;
import in.zeta.oms.athenamanager.api.model.Product;
import in.zeta.oms.athenamanager.api.model.ProductCategory;
import in.zeta.oms.athenamanager.baseSchema.BaseSchemaValidationException;
import in.zeta.oms.ledger.api.AccountingType;
import olympus.message.types.Request;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_2;
import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_2;
import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_DEFAULT;
import static in.zeta.oms.account.constant.AccountID.ACCOUNT_ID_INVALID;
import static in.zeta.oms.account.constant.GenericName.DEFAULT_NAME;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static in.zeta.oms.account.constant.ProductID.PRODUCT_ID_2;
import static in.zeta.oms.account.constant.ProductID.PRODUCT_ID_DEFAULT;
import static in.zeta.oms.account.constant.ProductID.PRODUCT_ID_INVALID;
import static in.zeta.oms.account.constant.RequestID.REQUEST_ID_2;
import static in.zeta.oms.account.constant.RequestID.REQUEST_ID_DEFAULT;
import static in.zeta.oms.athenamanager.api.model.EntityType.ACCOUNT;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(HierarchicalContextRunner.class)
public class AccountHandlerTest extends AccountServiceBaseTest {
    private final AccountTestHelper accountTestHelper;
    private final AccountVectorTestHelper accountVectorTestHelper;
    private final AccountRelationTestHelper accountRelationTestHelper;
    private final AccessorTestHelper accessorTestHelper;
    private final AccountHandler accountHandler;
    private final AccountHolderTestHelper accountHolderTestHelper;
    private final CertStoreClientMockHelper certStoreClientMockHelper;
    private final AthenaManagerClientMockHelper athenaManagerClientMockHelper;
    private final KYCStatusTestHelper kycStatusTestHelper;
    private final LedgerClientMockHelper ledgerClientMockHelper;
    private final CloudCardClientMockHelper cloudCardClientMockHelper;
    private final String[] fieldsToIgnore = {
        "accessors.createdAt",
        "accessors.transactionPolicyIDs",
        "accessors.updatedAt",
        "createdAt",
        "relationships.createdAt",
        "relationships.updatedAt",
        "updatedAt",
        "vectors.createdAt",
        "vectors.updatedAt",
        "programIDs",
        "tags"
    };

    public AccountHandlerTest() {
        this.accountTestHelper = getInstance(AccountTestHelper.class);
        this.accountHandler = getInstance(AccountHandler.class);
        this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
        this.certStoreClientMockHelper = getInstance(CertStoreClientMockHelper.class);
        this.athenaManagerClientMockHelper = getInstance(AthenaManagerClientMockHelper.class);
        this.ledgerClientMockHelper = getInstance(LedgerClientMockHelper.class);
        this.accountVectorTestHelper = getInstance(AccountVectorTestHelper.class);
        this.accountRelationTestHelper = getInstance(AccountRelationTestHelper.class);
        this.accessorTestHelper = getInstance(AccessorTestHelper.class);
        this.cloudCardClientMockHelper = getInstance(CloudCardClientMockHelper.class);
        this.kycStatusTestHelper = getInstance(KYCStatusTestHelper.class);
    }

    @Before
    public void setup() {
        athenaManagerClientMockHelper.mockGetIssuancePolicyListRequest();
        athenaManagerClientMockHelper.mockGetAccountProvider();
        athenaManagerClientMockHelper.mockGetAccountBaseSchema();
        ledgerClientMockHelper.mockGetCoa();
        ledgerClientMockHelper.mockCreateLedger();
        ledgerClientMockHelper.mockUpdateLedgerState();
        ledgerClientMockHelper.mockGetLedgerInfo();
        cloudCardClientMockHelper.mockCreateTransaction();
    }

    public class ListAccountsTest {

        @Before
        public void setup() {
            accountTestHelper.insertDefaultAccount();
            accountTestHelper.insertAccount(AccountTestHelper.getDefaultAccountBuilder()
                .id(ACCOUNT_ID_2.getValue())
                .productID(PRODUCT_ID_2.getValue())
                .ownerAccountHolderID(ACCOUNT_HOLDER_ID_2.getValue())
                .build(), REQUEST_ID_2);
            accountTestHelper.insertAccount(AccountTestHelper.getDefaultAccountBuilder()
                .id(ACCOUNT_ID_INVALID.getValue())
                .productID(PRODUCT_ID_INVALID.getValue())
                .build(), RequestID.REQUEST_ID_INVALID);
        }

        @Test
        public void WithAccountHolderAndStatus_Successfully() {
            Request<GetAccountListRequestV2> request1 =
                accountTestHelper.getGetAccountListRequestV2PayloadBuilder().build();

            accountHandler.on(request1.payload(), request1);
            final GetAccountListResponse response1 = captureResponsePayload(request1, GetAccountListResponse.class);
            assertNotNull(response1);
            assertThat(response1.getAccounts().size()).isEqualTo(2);
        }

        @Test
        public void WithAccountHolderStateAndProduct_Successfully() {
            Request<GetAccountListRequestV2> request2 =
                accountTestHelper.getGetAccountListRequestV2PayloadBuilder()
                    .accountHolderID(ACCOUNT_HOLDER_ID_2.getValue())
                    .productID(PRODUCT_ID_2.getValue())
                    .build();
            accountHandler.on(request2.payload(), request2);
            final GetAccountListResponse response2 = captureResponsePayload(request2, GetAccountListResponse.class);
            assertNotNull(response2);
            assertThat(response2.getAccounts().size()).isEqualTo(1);
        }

        @Test
        public void WithAccountHolder_Successfully() {
            Request<GetAccountListRequestV2> request3 =
                accountTestHelper.getGetAccountListRequestV2PayloadBuilder()
                    .status(null)
                    .build();
            accountHandler.on(request3.payload(), request3);
            final GetAccountListResponse response3 = captureResponsePayload(request3, GetAccountListResponse.class);
            assertNotNull(response3);
            assertThat(response3.getAccounts().size()).isEqualTo(2);
        }

        @Test
        public void WithAccountProviderID_Successfully() {
            Request<GetAccountListRequestV2> request4 =
                accountTestHelper.getGetAccountListRequestV2PayloadBuilder()
                    .accountProviderID(AccountProviderID.ACCOUNT_PROVIDER_ID_DEFAULT.getValue())
                    .accountHolderID(null)
                    .build();

            accountHandler.on(request4.payload(), request4);
            final GetAccountListResponse response4 = captureResponsePayload(request4, GetAccountListResponse.class);
            assertNotNull(response4);
            assertThat(response4.getAccounts().size()).isEqualTo(3);
        }
    }

    /**
     * Check if base schema is validated for account object successfully.
     * Check if object is not validated against schema, then throw exception.
     */
    public class SchemaValidationTest {

        @Before
        public void setup() {
            accountHolderTestHelper.insertDefaultAccountHolder();
            athenaManagerClientMockHelper.mockDefaultGetProductFamily();
            athenaManagerClientMockHelper.mockDefaultGetProduct();
            ledgerClientMockHelper.mockCreateLedger();
            ledgerClientMockHelper.mockGetCoa();
        }

        @Test
        public void create_ValidParamsWithSchemaValidation_success() {
            Request<CreateAccountRequest> request = AccountTestHelper.getDefaultCreateAccountRequestBuilder()
                .relationships(null)
                .build();
            accountHandler.on(request.payload(), request);
            final Account response = captureResponsePayload(request, Account.class);
            assertNotNull(response);
            assertEquals(response.getVectors().size(), 1);
        }

        @Test
        public void create_SchemaValidation_throwsException() {
            Request<CreateAccountRequest> request = AccountTestHelper.getDefaultCreateAccountRequestBuilder()
                .relationships(null)
                .vectors(singletonList(AccountVector.builder()
                    .id(AccountVectorID.ACCOUNT_VECTOR_ID_DEFAULT.getValue())
                    .accountID(ACCOUNT_ID_DEFAULT.getValue())
                    .ifiID(IFI_ID_DEFAULT.getValue())
                    .type("e")
                    .value("abcxyz.com")
                    .status(in.zeta.oms.account.constant.Status.ENABLED.name())
                    .attributes(Collections.emptyMap())
                    .build()))
                .build();
            accountHandler.on(request.payload(), request);
            BaseSchemaValidationException exception = captureExceptionThrown(request, BaseSchemaValidationException.class);
            assertThat(exception).hasMessageContaining(String.format("Base Schema for ifi %s and entityType %s failed", IFI_ID_DEFAULT.getValue(), ACCOUNT.name()));
        }
    }

    @Ignore("Data mocks are pending, this only runs on stage/local")
    @Test
    public void get_AccountsWithoutFilters_SuccessfullyReturned() {
        Request<GetAccountListRequest> request = accountTestHelper
                .getGetAccountWithFilterRequestPayload(null);
        accountHandler.on(request.payload(), request);
        final GetAccountListResponse response = captureResponsePayload(request, GetAccountListResponse.class);
        assertThat(response.getAccounts().size())
                .isGreaterThanOrEqualTo(1);
    }

    @Ignore("Data mocks are pending, this only runs on stage/local")
    @Test
    public void get_AccountsFilteredWithAccountId_SuccessfullyReturned() {
    Request<GetAccountListRequest> request =
        accountTestHelper.getGetAccountWithFilterRequestPayload(
            PRODUCT_ID_DEFAULT.getValue());
        accountHandler.on(request.payload(), request);
        final GetAccountListResponse response = captureResponsePayload(request, GetAccountListResponse.class);
        assertThat(response.getAccounts().size())
                .isEqualTo(1);
    }

    @Ignore("Data mocks are pending, this only runs on stage/local")
    @Test
    public void get_AccountsFilteredWithProductId_SuccessfullyReturned() {
        Request<GetAccountListRequest> request = accountTestHelper
                .getGetAccountWithFilterRequestPayload(PRODUCT_ID_DEFAULT.getValue());
        accountHandler.on(request.payload(), request);
        final GetAccountListResponse response = captureResponsePayload(request, GetAccountListResponse.class);
        assertThat(response.getAccounts().size())
            .isGreaterThanOrEqualTo(1);
    }

    @Ignore("Data mocks are pending, this only runs on stage/local")
    @Test
    public void get_AccountsFilteredWithProgramId_SuccessfullyReturned() {
        Request<GetAccountListRequest> request = accountTestHelper.getGetAccountWithFilterRequestPayload(PRODUCT_ID_DEFAULT.getValue());
        accountHandler.on(request.payload(), request);
        final GetAccountListResponse response = captureResponsePayload(request, GetAccountListResponse.class);
        assertThat(response.getAccounts().size())
                .isGreaterThan(1);
    }

    @Test
    public void create_ValidParams_success() {
        insertRelatedAccountForDefaultAccountRelation();
        Request<CreateAccountRequest> request = AccountTestHelper.getDefaultCreateAccountRequest();
        accountHolderTestHelper.insertDefaultAccountHolder();
        athenaManagerClientMockHelper.mockDefaultGetProductFamily();
        athenaManagerClientMockHelper.mockDefaultGetProduct();
        athenaManagerClientMockHelper.mockDefaultGetCardProgram();
        kycStatusTestHelper.addDefaultEffectiveKYCStatus();
        ledgerClientMockHelper.mockCreateLedger();
        accountHandler.on(request.payload(), request);
        final Account response = captureResponsePayload(request, Account.class);
        final Optional<Account> accountInDB = accountTestHelper.getAccountBy(response.getId(), response.getIfiID());
        final List<Accessor> accessors = accessorTestHelper.getAccessorsForAccount(response.getId(), IfiID.IFI_ID_DEFAULT.getValue());
        final List<AccountVector> accountVectors = accountVectorTestHelper.getAccountVectorsForAccount(response.getId(), IfiID.IFI_ID_DEFAULT.getValue());
        final List<AccountRelation> accountRelations = accountRelationTestHelper.getAccountRelationsForAccount(response.getId(), IfiID.IFI_ID_DEFAULT.getValue());
        assertNotNull(response);
        assertTrue(accountInDB.isPresent());
        assertThat(accountInDB.get().toBuilder()
            .vectors(accountVectors)
            .accessors(accessors)
            .relationships(accountRelations)
            .build())
            .usingRecursiveComparison()
            .ignoringFields(fieldsToIgnore)
            .isEqualTo(response);
    }

    private void insertRelatedAccountForDefaultAccountRelation() {
        accountTestHelper.insertAccount(AccountTestHelper.getDefaultAccount().toBuilder()
            .id(ACCOUNT_ID_2.getValue())
            .vectors(Collections.emptyList())
            .build(), REQUEST_ID_2);
    }

    @Test
    public void testAccountClosureTestWithNoOutboundAccountId() {
        Account sourceAccount = accountTestHelper.insertDefaultAccount();
        Request<CloseAccountRequest> request = AccountTestHelper.getDefaultCloseAccountRequest(sourceAccount, null);
        accountHandler.on(request.payload(), request);
        final CloseAccountResponse response = captureResponsePayload(request, CloseAccountResponse.class);
        assertEquals("account is not closed", Status.CLOSED.name() , response.getAccount().getStatus());
    }

    @Test
    public void testAccountClosureTestWithOutboundAccountId() {
        Account sourceAccount = accountTestHelper.insertDefaultAccount();
        Request<CloseAccountRequest> request = AccountTestHelper.getDefaultCloseAccountRequest(sourceAccount, ACCOUNT_ID_2.getValue());
        accountHandler.on(request.payload(), request);
        final CloseAccountResponse response = captureResponsePayload(request, CloseAccountResponse.class);
        assertEquals("account is not closed", Status.CLOSED.name(), response.getAccount().getStatus());
    }

    @Test
    public void testAccountClosureForClosedAccount() {
        Account sourceAccount = accountTestHelper.insertDefaultAccount();
        Request<CloseAccountRequest> request = AccountTestHelper.getDefaultCloseAccountRequest(sourceAccount, null);
        accountHandler.on(request.payload(), request);
        final CloseAccountResponse response = captureResponsePayload(request, CloseAccountResponse.class);
        assertEquals("account is not closed", Status.CLOSED.name() , response.getAccount().getStatus());

        // retrying closing of the account
        accountHandler.on(request.payload(), request);
        final CloseAccountResponse alreadyClosedAcctResponse = captureResponsePayload(request, CloseAccountResponse.class);
        assertEquals("account is not closed", Status.CLOSED.name() , alreadyClosedAcctResponse.getAccount().getStatus());
    }

    @Test
    public void create_SameDuplicateRequest_idempotentResponse() {
        certStoreClientMockHelper.mockVerifySignature();
        accountHolderTestHelper.insertDefaultAccountHolder();
        athenaManagerClientMockHelper.mockDefaultGetProductFamily();
        athenaManagerClientMockHelper.mockDefaultGetProduct();
        ledgerClientMockHelper.mockCreateLedger();

        insertRelatedAccountForDefaultAccountRelation();

        Request<CreateAccountRequest> request = AccountTestHelper.getDefaultCreateAccountRequest();
        accountHandler.on(request.payload(), request);
        final Account response1 = captureResponsePayload(request, Account.class);

        Request<CreateAccountRequest> request2 = AccountTestHelper.getDefaultCreateAccountRequest();
        accountHandler.on(request2.payload(), request2);
        final Account response2 = captureResponsePayload(request2, Account.class);

        assertThat(response1)
            .usingRecursiveComparison()
            .ignoringFields(fieldsToIgnore)
            .isEqualTo(response2);
    }

    @Test
    public void create_DifferentDuplicateRequest_throwsDuplicateException() {
        certStoreClientMockHelper.mockVerifySignature();
        accountHolderTestHelper.insertDefaultAccountHolder();
        athenaManagerClientMockHelper.mockDefaultGetProductFamily();
        athenaManagerClientMockHelper.mockDefaultGetProduct();
        ledgerClientMockHelper.mockCreateLedger();

        insertRelatedAccountForDefaultAccountRelation();

        Request<CreateAccountRequest> request = AccountTestHelper.getDefaultCreateAccountRequest();
        accountHandler.on(request.payload(), request);
        final Account response1 = captureResponsePayload(request, Account.class);

        final Request<CreateAccountRequest> request2 = request.payload().toBuilder().productID(PRODUCT_ID_2.getValue()).build();
        accountHandler.on(request2.payload(), request2);
        final AccountCreationException exception = captureExceptionThrown(request2, AccountCreationException.class);

        assertEquals(exception.getErrorCode(), AccountErrorCode.DUPLICATE_REQUEST_EXCEPTION.name());
    }

    @Ignore("Have to change the account vector too in betwen the two requests to accomodate the unique constraint on (type, value, ifi_id) on account_vector")
    @Test
    public void create_SameRequestIDDifferentProvider_successfullCreation() {
        certStoreClientMockHelper.mockVerifySignature();
        accountHolderTestHelper.insertDefaultAccountHolder();
        athenaManagerClientMockHelper.mockDefaultGetProductFamily();
        athenaManagerClientMockHelper.mockDefaultGetProduct();
        ledgerClientMockHelper.mockCreateLedger();

        insertRelatedAccountForDefaultAccountRelation();

        Request<CreateAccountRequest> request = AccountTestHelper.getDefaultCreateAccountRequest();
        accountHandler.on(request.payload(), request);
        final Account response1 = captureResponsePayload(request, Account.class);

        final Request<CreateAccountRequest> request2 = request.payload().toBuilder().accountProviderID(AccountProviderID.ACCOUNT_PROVIDER_ID_2.getValue()).build();
        accountHandler.on(request2.payload(), request2);
        final Account response2 = captureResponsePayload(request2, Account.class);

        assertNotEquals(response1.getId(), response2.getId());
    }

    @Ignore("This looks like testing some old code where we were checking if the product belongs to a product family passed in the request.")
    //TODO: This should be fixed in the code
      @Test
      public void create_ProductNotPartOfProductFamily_failure() {
        Request<CreateAccountRequest> request =
            AccountTestHelper.getDefaultCreateAccountRequest()
                .payload()
                .toBuilder()
                .productID(PRODUCT_ID_2.getValue())
                .build();
        certStoreClientMockHelper.mockVerifySignature();
        accountHolderTestHelper.insertDefaultAccountHolder();
        athenaManagerClientMockHelper.mockDefaultGetProductFamily();
        athenaManagerClientMockHelper.mockGetProduct(
            Product.builder()
                .id(ProductID.PRODUCT_ID_INVALID.getValue())
                .name(GenericName.NAME_2.getValue())
                .state(ProductState.ENABLED)
                .ifiID(IfiID.IFI_ID_INVALID.getValue())
                .coaID(CoaID.COA_ID_INVALID.getValue())
                .code(GenericName.NAME_2.getValue())
                .attributes(Collections.emptyMap())
                .coaNodeID(CoaNodeID.COA_NODE_ID_INVALID.getValue())
                .productFamilyID(ProductFamilyID.PRODUCT_FAMILY_ID_INVALID.getValue())
                .type(ProductCategory.ACCOUNT_BASED)
                .build());
        ledgerClientMockHelper.mockCreateLedger();
        accountHandler.on(request.payload(), request);
        final AccountCreationException exception =
            captureExceptionThrown(request, AccountCreationException.class);
        assertThat(exception)
            .usingRecursiveComparison()
            .ignoringFields() // TODO : Check why "546630c7-7854-4e93-ab77-dd364b96fedd" vs
                              // 546630c7-7854-4e93-ab77-dd364b96fedd is happening at all and failing
            .isEqualTo(new AccountCreationException(
                String.format(
                    "Product %s doesn't belong to product family %s",
                    ProductID.PRODUCT_ID_INVALID.getValue(),
                    request.payload().getProductFamilyID()),
                AccountErrorCode.INVALID_PRODUCT));
      }

    @Test
    public void get_ValidParams_Success() {
        insertRelatedAccountForDefaultAccountRelation();
        accountHolderTestHelper.insertDefaultAccountHolder();
        Account defaultAccount = AccountTestHelper.getDefaultAccount();
        accountTestHelper.insertAccount(defaultAccount, REQUEST_ID_DEFAULT);
        accountVectorTestHelper.insertDefaultAccountVector();
        accountRelationTestHelper.insertDefaultAccountRelations();
        accessorTestHelper.insertDefaultAccessors();
        Request<GetAccountRequest> request = accountTestHelper.getDefaultGetAccountRequest();
        accountHandler.on(request.payload(), request);
        Account response = captureResponsePayload(request, Account.class);
        assertNotNull(response);
        assertThat(response)
            .usingRecursiveComparison()
            .ignoringFields(fieldsToIgnore)
            .isEqualTo(defaultAccount);

    }

    @Test
    public void update_accountOwner_success() {
        Account defaultAccount = accountTestHelper.insertDefaultAccount();
        insertRelatedAccountForDefaultAccountRelation();
        accountHolderTestHelper.insertDefaultAccountHolder();
        final AccountHolder secondaryAccountHolder = accountHolderTestHelper.insertAccountHolder(
            AccountHolder.builder()
                .id(ACCOUNT_HOLDER_ID_2.getValue())
                .accountHolderProviderID(
                    AccountHolderProviderID.ACCOUNT_HOLDER_PROVIDER_ID_2.getValue())
                .type(AccountHolderType.REAL)
                .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
                .firstName(GenericName.DEFAULT_NAME_ACCOUNT_HOLDER.getValue())
                .status(AccountHolderStatus.ENABLED.toString())
                .build());
        accessorTestHelper.insertDefaultAccessors();
        accountVectorTestHelper.insertDefaultAccountVector();
        accountRelationTestHelper.insertDefaultAccountRelations();
        ledgerClientMockHelper.mockSetLedgerAttributes();
        Request<UpdateAccountOwnerRequest> request =
            accountTestHelper.getDefaultUpdateAccountOwnerRequest(
                defaultAccount.getId(), defaultAccount.getIfiID(), AccountHolderID.ACCOUNT_HOLDER_ID_2);
        accountHandler.on(request.payload(), request);
        final Account response = captureResponsePayload(request, Account.class);
        final Optional<Account> updatedAccountInDB =
            accountTestHelper.getAccountBy(defaultAccount.getId(), defaultAccount.getIfiID());
        assertTrue(updatedAccountInDB.isPresent());
    assertThat(response)
        .usingRecursiveComparison()
        .ignoringFields(addAll(fieldsToIgnore, "attributes"))
        .isEqualTo(
            defaultAccount
                .toBuilder()
                .ownerAccountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue())
                .build());
    }

    @Test
    public void update_Status_success() {
        Account defaultAccount = accountTestHelper.insertDefaultAccount();
        insertRelatedAccountForDefaultAccountRelation();
        accountHolderTestHelper.insertDefaultAccountHolder();
        accessorTestHelper.insertDefaultAccessors();
        accountVectorTestHelper.insertDefaultAccountVector();
        accountRelationTestHelper.insertDefaultAccountRelations();
        ledgerClientMockHelper.mockSetLedgerAttributes();
        ledgerClientMockHelper.mockUpdateLedgerState();
        Request<UpdateAccountStatusRequest> request =
        accountTestHelper.getUpdateAccountStatusRequest(defaultAccount.getId(), defaultAccount.getIfiID(), Status.BLOCKED.name());
        accountHandler.on(request.payload(), request);
        final Account response = captureResponsePayload(request, Account.class);
        final Optional<Account> updatedAccountInDB = accountTestHelper.getAccountBy(defaultAccount.getId(), defaultAccount.getIfiID());
        assertTrue(updatedAccountInDB.isPresent());
        assertThat(response)
            .usingRecursiveComparison()
            .ignoringFields(fieldsToIgnore)
            .isEqualTo(defaultAccount
                .toBuilder()
                .status(Status.BLOCKED.name())
                .build());
    }

    @Test
    public void update_accountOwnerWithExternalID_success() {
        insertRelatedAccountForDefaultAccountRelation();
        Account defaultAccount = accountTestHelper.insertDefaultAccount();
        accountHolderTestHelper.insertDefaultAccountHolder();
        accountVectorTestHelper.insertDefaultAccountVector();
        accountRelationTestHelper.insertDefaultAccountRelations();
        accessorTestHelper.insertDefaultAccessors();
        ledgerClientMockHelper.mockSetLedgerAttributes();
        accountHolderTestHelper.insertAccountHolder(
            AccountHolder.builder()
                .id(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue())
                .accountHolderProviderID(
                    AccountHolderProviderID.ACCOUNT_HOLDER_PROVIDER_ID_2.getValue())
                .vectors(accountHolderTestHelper.accountHolder_2_Vectors)
                .type(AccountHolderType.REAL)
                .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
                .firstName(GenericName.DEFAULT_NAME_ACCOUNT_HOLDER.getValue())
                .status(AccountHolderStatus.ENABLED.toString())
                .build());
        Request<UpdateAccountOwnerRequest> request =
            accountTestHelper.getUpdateAccountOwnerRequestWithExternalID(
                defaultAccount.getId(), defaultAccount.getIfiID(), ExternalID.ZETAUSER_EXTERNAL_ID, ExternalIDType.ZetaUserID);
        accountHandler.on(request.payload(), request);
        final Account response = captureResponsePayload(request, Account.class);
        final Optional<Account> updatedAccountInDB =
            accountTestHelper.getAccountBy(defaultAccount.getId(), defaultAccount.getIfiID());
        assertTrue(updatedAccountInDB.isPresent());
        assertThat(response)
            .usingRecursiveComparison()
            .ignoringFields(addAll(fieldsToIgnore, "attributes"))
            .isEqualTo(defaultAccount.toBuilder().ownerAccountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue()).build());
    }

    @Test
    public void update_accountProduct_success() {
        insertRelatedAccountForDefaultAccountRelation();
        Account defaultAccount = accountTestHelper.insertDefaultAccount();
        accountHolderTestHelper.insertDefaultAccountHolder();
        accountRelationTestHelper.insertDefaultAccountRelations();
        accountVectorTestHelper.insertDefaultAccountVector();
        accessorTestHelper.insertDefaultAccessors();
        ledgerClientMockHelper.mockSetLedgerAttributes();
        ledgerClientMockHelper.mockUpdateIFIProductType();
        athenaManagerClientMockHelper.mockDefaultGetProductFamily();
        Product productToBeUpdated = AthenaManagerHelper.getDefaultProduct().toBuilder()
            .id(PRODUCT_ID_2.getValue()).build();
        athenaManagerClientMockHelper.mockGetProduct(productToBeUpdated);
        Request<UpdateProductForAccountRequest> request =
            accountTestHelper.getDefaultUpdateProductForAccountRequest(
                defaultAccount.getId(),
                defaultAccount.getIfiID(),
                productToBeUpdated.getId(),
                defaultAccount.getAccountProviderID());
        accountHandler.on(request.payload(), request);
        final Account response = captureResponsePayload(request, Account.class);
        final Optional<Account> updatedAccountInDB =
            accountTestHelper.getAccountBy(defaultAccount.getId(), defaultAccount.getIfiID());
        assertTrue(updatedAccountInDB.isPresent());
        assertThat(response)
            .usingRecursiveComparison()
            .ignoringFields(addAll(fieldsToIgnore, "attributes"))
            .isEqualTo(defaultAccount
                    .toBuilder()
                    .productID(productToBeUpdated.getId())
                    .build());
    }

    @Test
    public void get_AccountInfo_successfullyReturned() {
        //Setup
        final Account defaultAccount = accountTestHelper.insertDefaultAccount();
        final Long ifiID = IfiID.IFI_ID_DEFAULT.getValue();
        final String id = defaultAccount.getId();
        athenaManagerClientMockHelper.mockDefaultGetProduct();
        athenaManagerClientMockHelper.mockDefaultGetProductFamily();
        ledgerClientMockHelper.mockGetLedgerInfo();

        //Run Test
        final Request<GetAccountInfoRequest> request = accountTestHelper.getDefaultGetAccountInfoRequest();
        accountHandler.on(request.payload(), request);
        final AccountInfo response = captureResponsePayload(request, AccountInfo.class);

        //Verify
        assertEquals(response.getId(), ACCOUNT_ID_DEFAULT.getValue());
        assertEquals(response.getOwnerAccountHolderID(), ACCOUNT_HOLDER_ID_DEFAULT.getValue());
        assertEquals(response.getName(), DEFAULT_NAME.getValue());
        assertEquals(response.getProductName(), ProductName.GPR_KYC_SHORTFALL_WITH_PAN.name());
        assertEquals(response.getProductFamilyName(), ProductFamilyName.GPR.name());
        assertEquals(response.getBalance(), 1L);
        assertEquals(response.getAccountingType(), AccountingType.LIABILITY);
        assertEquals(response.getCurrency(), "INR");

    }
}