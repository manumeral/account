package in.zeta.oms.account.api;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.account.relation.AccountRelationHandler;
import in.zeta.oms.account.account.accountRelation.AddAccountRelationRequest;
import in.zeta.oms.account.account.accountRelation.GetAccountRelationRequest;
import in.zeta.oms.account.account.accountRelation.UpdateAccountRelationRequest;
import in.zeta.oms.account.api.model.AccountRelation;
import in.zeta.oms.account.constant.AccountID;
import in.zeta.oms.account.constant.RequestID;
import in.zeta.oms.account.helper.AccountRelationTestHelper;
import in.zeta.oms.account.helper.AccountTestHelper;
import olympus.message.types.Request;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;

import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AccountRelationHandlerTest extends AccountServiceBaseTest {
  private static final Map<String, String> UPDATED_ATTRIBUTES = singletonMap("key", "value");
  private final AccountRelationTestHelper accountRelationTestHelper;
  private final AccountTestHelper accountTestHelper;
  private final AccountRelationHandler handler;

  public AccountRelationHandlerTest() {
    this.accountRelationTestHelper = getInstance(AccountRelationTestHelper.class);
    this.accountTestHelper = getInstance(AccountTestHelper.class);
    this.handler = getInstance(AccountRelationHandler.class);
  }

  @Before
  public void setup() {
    accountTestHelper.insertDefaultAccount();
    accountTestHelper.insertAccount(
        AccountTestHelper.getDefaultAccount()
            .toBuilder()
            .id(AccountID.ACCOUNT_ID_2.getValue())
            .build(),
        RequestID.REQUEST_ID_2);
  }

  @Test
  public void create_ValidParams_Success() {
    Request<AddAccountRelationRequest> request =
        AccountRelationTestHelper.getDefaultAddAccountRelationRequest();
    handler.on(request.payload(), request);
    final AccountRelation response = captureResponsePayload(request, AccountRelation.class);
    final Optional<AccountRelation> accountRelationInDB =
        accountRelationTestHelper.getAccountRelationInDB(
            response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountRelationInDB.isPresent());
    assertThat(accountRelationInDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void getByID_ValidParams_Success() {
    accountRelationTestHelper.insertDefaultAccountRelations();
    Request<GetAccountRelationRequest> request = accountRelationTestHelper.getDefaultGetAccountRelationRequest();
    handler.on(request.payload(), request);
    final AccountRelation response = captureResponsePayload(request, AccountRelation.class);
    final Optional<AccountRelation> accountRelationInDB = accountRelationTestHelper.getAccountRelationInDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountRelationInDB.isPresent());
    assertThat(accountRelationInDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void getList_ValidParams_Success() {
    //TODO : List API test will be added later
  }

  @Test
  public void updateStatus_ValidParams_Success() {
    final AccountRelation defaultAccountRelation = AccountRelationTestHelper.getDefaultAccountRelations().get(0);
    accountRelationTestHelper.insertDefaultAccountRelations();
    Request<UpdateAccountRelationRequest> request =
        accountRelationTestHelper.getUpdateAccountRelationRequest(
            defaultAccountRelation.toBuilder().status(Status.DISABLED.name()).build());
    handler.on(request.payload(), request);
    final AccountRelation response = captureResponsePayload(request, AccountRelation.class);
    final Optional<AccountRelation> accountRelationInDB = accountRelationTestHelper.getAccountRelationInDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountRelationInDB.isPresent());
    assertThat(accountRelationInDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void updateRelatedAccountID_ValidParams_Success() {
    accountTestHelper.insertAccount(
        AccountTestHelper.getDefaultAccount()
            .toBuilder()
            .id(AccountID.ACCOUNT_ID_3.getValue())
            .build(),
        RequestID.REQUEST_ID_3);
    final AccountRelation defaultAccountRelation = AccountRelationTestHelper.getDefaultAccountRelations().get(0);
    accountRelationTestHelper.insertDefaultAccountRelations();
    Request<UpdateAccountRelationRequest> request = accountRelationTestHelper.getUpdateAccountRelationRequest(defaultAccountRelation.toBuilder().relatedAccountID(AccountID.ACCOUNT_ID_3.getValue()).build());
    handler.on(request.payload(), request);
    final AccountRelation response = captureResponsePayload(request, AccountRelation.class);
    final Optional<AccountRelation> accountRelationInDB = accountRelationTestHelper.getAccountRelationInDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountRelationInDB.isPresent());
    assertThat(accountRelationInDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void updateAttributes_ValidParams_Success() {
    final AccountRelation defaultAccountRelation = AccountRelationTestHelper.getDefaultAccountRelations().get(0);
    accountRelationTestHelper.insertDefaultAccountRelations();
    Request<UpdateAccountRelationRequest> request = accountRelationTestHelper.getUpdateAccountRelationRequest(defaultAccountRelation.toBuilder().attributes(UPDATED_ATTRIBUTES).build());
    handler.on(request.payload(), request);
    final AccountRelation response = captureResponsePayload(request, AccountRelation.class);
    final Optional<AccountRelation> accountRelationInDB = accountRelationTestHelper.getAccountRelationInDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountRelationInDB.isPresent());
    assertThat(accountRelationInDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }
}
