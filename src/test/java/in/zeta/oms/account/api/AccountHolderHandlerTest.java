package in.zeta.oms.account.api;

import in.zeta.commons.util.ZetaUrlConstants;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.UpdateAccountHolderRequest;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusTestHelper;
import in.zeta.oms.account.accountHolder.pop.AccountHolderPOPHandler;
import in.zeta.oms.account.accountHolder.pop.AccountHolderPopTestHelper;
import in.zeta.oms.account.api.enums.AccountHolderStatus;
import in.zeta.oms.account.api.exception.AccountHolderInsertionException;
import in.zeta.oms.account.api.exception.AccountHolderNotFoundException;
import in.zeta.oms.account.api.exception.AccountHolderVectorNotFoundException;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.request.CreateAccountHolderRequest;
import in.zeta.oms.account.api.request.GetAccountHolderByVectorRequest;
import in.zeta.oms.account.api.request.GetAccountHolderRequest;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.PhoneNumber;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.account.helper.AthenaManagerClientMockHelper;
import in.zeta.oms.account.pop.AddAccountHolderPOPRequest;
import in.zeta.oms.account.pop.POP;
import in.zeta.oms.account.service.AccountHolderService;
import in.zeta.oms.athenamanager.baseSchema.BaseSchemaValidationException;
import olympus.message.types.Request;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static in.zeta.oms.account.api.enums.ExternalIDType.ZetaUserID;
import static in.zeta.oms.account.constant.ExternalID.DEFAULT_EXTERNAL_ID;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static in.zeta.oms.athenamanager.api.model.EntityType.ACCOUNT_HOLDER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AccountHolderHandlerTest extends AccountServiceBaseTest {
  private final AccountHolderTestHelper accountHolderTestHelper;
  private final AccountHolderHandler accountHolderHandler;
  private final AthenaManagerClientMockHelper athenaManagerClientMockHelper;
  private final AccountHolderService accountHolderService;
  private final AccountHolderPOPHandler popHandler;
  private final AccountHolderPopTestHelper popTestHelper;
  private final KYCStatusTestHelper kycStatusTestHelper;
  

  public AccountHolderHandlerTest() {
    super();
    this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
    this.accountHolderHandler = getInstance(AccountHolderHandler.class);
    this.athenaManagerClientMockHelper = getInstance(AthenaManagerClientMockHelper.class);
    this.accountHolderService = getInstance(AccountHolderService.class);
    this.popHandler = getInstance(AccountHolderPOPHandler.class);
    this.popTestHelper = getInstance(AccountHolderPopTestHelper.class);
    this.kycStatusTestHelper = getInstance(KYCStatusTestHelper.class);
  }

  @Before
  public void setUp() {
    athenaManagerClientMockHelper.mockDefaultGetAccountHolderProvider();
    athenaManagerClientMockHelper.mockGetAccountHolderBaseSchema();
  }

  @Test
  public void create_ValidParam_SuccessfullyCreated() {
    Request<CreateAccountHolderRequest> request = accountHolderTestHelper.getDefaultCreateAccountHolderRequest();
    createAccountHolder(request);
  }

  private void createAccountHolder(Request<CreateAccountHolderRequest> request) {
    //get create account holder payload
    final CreateAccountHolderRequest payload = request.payload();
    accountHolderHandler.on(payload, request);
    final AccountHolder createAccountHolderResponse = captureResponsePayload(request, AccountHolder.class);

    //compare create request response and create request payload
    assertNotNull(createAccountHolderResponse);
    assertThat(createAccountHolderResponse)
        .usingRecursiveComparison()
        .ignoringFields("vectors", "id", "status", "createdAt", "updatedAt")
        .isEqualTo(payload);
    final AccountHolder getAccountHolderResponse = accountHolderService.getByVector(IFI_ID_DEFAULT.getValue(), ZetaUserID, DEFAULT_EXTERNAL_ID.getValue())
        .toCompletableFuture().join();

    //compare get request response and create request response
    assertNotNull(getAccountHolderResponse);
    assertThat(createAccountHolderResponse)
        .usingRecursiveComparison()
        .ignoringFields("vectors", "pops", "createdAt")
        .isEqualTo(getAccountHolderResponse);

    // Verify vectors
    assertEquals(createAccountHolderResponse.getVectors().size(), getAccountHolderResponse.getVectors().size());
    assertThat(createAccountHolderResponse.getVectors()).usingElementComparatorIgnoringFields("id", "createdAt", "accountHolderID", "updatedAt").isEqualTo(getAccountHolderResponse.getVectors());
  }

  @Test
  public void create_ValidParamWithZetaUserVector_SuccessfullyCreated() {
    Request<CreateAccountHolderRequest> request = accountHolderTestHelper.getDefaultCreateAccountHolderWitHZetaUserIDRequest();
    createAccountHolder(request);
  }

  @Ignore("2 requests are not allowed for same zeta user id in vector")
  @Test
  public void create_DuplicateWithEmptyVectors_SuccessfullyCreated() {
    Request<CreateAccountHolderRequest> request = accountHolderTestHelper.getDefaultCreateAccountHolderWitHZetaUserIDRequest();
    createAccountHolder(request);
    createAccountHolder(request);
  }

  @Test
  public void create_DuplicateRequest_IdempotentResponse() {
    //create account holder request with valid param
    Request<CreateAccountHolderRequest> firstRequest = accountHolderTestHelper.getDefaultCreateAccountHolderRequest();
    accountHolderHandler.on(firstRequest.payload(), firstRequest);
    final AccountHolder responseForFirstRequest = captureResponsePayload(firstRequest, AccountHolder.class);

    //create account holder with valid param, should be not do anything, validate idempotent behaviour
    Request<CreateAccountHolderRequest> secondRequestWithSamePayload = accountHolderTestHelper.getDefaultCreateAccountHolderRequest();
    accountHolderHandler.on(secondRequestWithSamePayload.payload(), secondRequestWithSamePayload);
    final AccountHolder responseForSecondRequest = captureResponsePayload(secondRequestWithSamePayload, AccountHolder.class);

    //compare response of both the request should be same
    assertThat(responseForSecondRequest)
        .usingRecursiveComparison()
        .ignoringFields("vectors", "tags")
        .isEqualTo(responseForFirstRequest);
  }

  //TODO: fix this, once json schema validator is implemented
  @Test
  @Ignore
  public void create_InvalidAttributes_ThrowsException() {

    //get create account holder payload with invalid attributes
    Request<CreateAccountHolderRequest> request = accountHolderTestHelper
        .getCreateAccountHolderRequestPayloadWithInvalidAttribute();
    accountHolderHandler.on(request.payload(), request);
    final AccountServiceException exception = captureExceptionThrown(request, AccountServiceException.class);
    assertThat(exception).hasMessageContaining("Provisioning policy failed");

  }

  @Test
  public void create_checkInvalidSchemaValidation_throwsException() {
    Request<CreateAccountHolderRequest> request = accountHolderTestHelper
        .getCreateAccountHolderWithInvalidVectorRequest();
    accountHolderHandler.on(request.payload(), request);
    BaseSchemaValidationException exception = captureExceptionThrown(request, BaseSchemaValidationException.class);
    assertThat(exception).hasMessageContaining(String.format("Base Schema for ifi %s and entityType %s failed", IFI_ID_DEFAULT.getValue(), ACCOUNT_HOLDER.name()));
  }

  @Test
  public void getByExternalID_InvalidAccountHolderVector_ExceptionThrown() {
    Request<GetAccountHolderRequest> request = accountHolderTestHelper.getGetAccountHolderByExternalIDRequestPayload();
    accountHolderHandler.on(request.payload(), request);
    final AccountHolderVectorNotFoundException exception = captureExceptionThrown(request, AccountHolderVectorNotFoundException.class);

    final AccountHolderVectorNotFoundException expected = new AccountHolderVectorNotFoundException(
        String.format("Get AccountHolderVector failed for type: zeta.user-id value : %s ifiID : %s",
            DEFAULT_EXTERNAL_ID.getValue(), IFI_ID_DEFAULT.getValue()));
    assertThat(exception).hasMessage(expected.getMessage());
    assertThat(exception.getErrorCode()).isEqualTo(expected.getErrorCode());
  }

  @Test
  public void getByID_InvalidAccount_ExceptionThrown() {
    Request<GetAccountHolderRequest> request = accountHolderTestHelper.getGetAccountHolderByIDRequestPayload();
    accountHolderHandler.on(request.payload(), request);
    final AccountHolderNotFoundException exception = captureExceptionThrown(request, AccountHolderNotFoundException.class);

    final AccountHolderNotFoundException expected = new AccountHolderNotFoundException(
        String.format("Account holder not found for id: %s",
            request.payload().getId()));
    assertThat(exception).hasMessage(expected.getMessage());
    assertThat(exception.getErrorCode()).isEqualTo(expected.getErrorCode());
  }

  @Test
  public void get_ValidParam_SuccessfullyReturned() {
    AccountHolder accountHolder = accountHolderTestHelper.getDefaultAccountHolder();
    accountHolderTestHelper.insertDefaultAccountHolder();

    Request<GetAccountHolderRequest> request = accountHolderTestHelper.getGetAccountHolderByExternalIDRequestPayload();
    accountHolderHandler.on(request.payload(), request);
    final AccountHolder response = captureResponsePayload(request, AccountHolder.class);

    assertThat(response)
        .usingRecursiveComparison()
        .ignoringFields("vectors", "KYCStatus", "createdAt")
        .isEqualTo(accountHolder);
  }

  @Test
  public void getByID_ValidPops_And_KYCStatus_SuccessfullyReturned() {
    AccountHolder accountHolder = accountHolderTestHelper.getDefaultAccountHolder();
    accountHolderTestHelper.insertDefaultAccountHolder();

    // Adding pop
    Request<AddAccountHolderPOPRequest> addPoprequest = popTestHelper.getDefaultAddPOPRequest();
    popHandler.on(addPoprequest.payload(), addPoprequest);
    final POP popResponse = captureResponsePayload(addPoprequest, POP.class);

    // Run Test
    Request<GetAccountHolderRequest> request = accountHolderTestHelper.getGetAccountHolderByIDRequestPayload();
    accountHolderHandler.on(request.payload(), request);
    final AccountHolder response = captureResponsePayload(request, AccountHolder.class);

    assertThat(response)
        .usingRecursiveComparison()
        .ignoringFields("vectors")
        .ignoringFields("pops")
        .ignoringFields("KYCStatus")
        .ignoringFields("createdAt")
        .isEqualTo(accountHolder);

    //Verify Pops
    assertEquals(response.getPops().size(), 1);
    assertEquals(response.getPops().get(0), popResponse);

    // Verify KYC Status
    kycStatusTestHelper.assertEqualsWithDefault(response.getKYCStatus(), "updateTime");
  }

  @Test
  public void update_AccountHolder_WorkSuccessfully() {
    AccountHolder updatedAccountHolder = accountHolderTestHelper.getDefaultAccountHolder()
        .toBuilder()
        .status(AccountHolderStatus.DISABLED.toString())
        .attributes(accountHolderTestHelper.accountHolderUpdatedAttributes)
        .build();
    accountHolderTestHelper.insertDefaultAccountHolder();

    Request<UpdateAccountHolderRequest> request = accountHolderTestHelper.getUpdateAccountHolderRequest();
    accountHolderHandler.on(request.payload(), request);

    final AccountHolder response = captureResponsePayload(request, AccountHolder.class);
    assertThat(response)
            .usingRecursiveComparison()
            .ignoringFields("vectors", "KYCStatus", "createdAt")
            .isEqualTo(updatedAccountHolder);
  }

  @Test
  public void update_AccountHolder_InvalidAccountHolder() {
    AccountHolder updatedAccountHolder = accountHolderTestHelper.getDefaultAccountHolder()
        .toBuilder()
        .accountHolderProviderID(AccountHolderID.ACCOUNT_HOLDER_ID_INVALID.getValue())
        .status(AccountHolderStatus.DISABLED.toString())
        .build();
    accountHolderTestHelper.insertDefaultAccountHolder();

    Request<UpdateAccountHolderRequest> request = accountHolderTestHelper.getInvalidUpdateAccountHolderRequest();
    accountHolderHandler.on(request.payload(), request);

    final AccountHolderNotFoundException exception = captureExceptionThrown(request, AccountHolderNotFoundException.class);
    assertNotNull(exception);
    final AccountHolderNotFoundException expected = new AccountHolderNotFoundException(
        String.format("Account holder not found for id: %s",
            request.payload().getId()));
    assertThat(exception).hasMessage(expected.getMessage());
    assertThat(exception.getErrorCode()).isEqualTo(expected.getErrorCode());
  }

  //TODO: Write separate test case for this
  @Ignore("External id is not supported now in account holder vector. Handle it using vector")
  @Test
  public void create_DuplicateRequestWithDifferentExternalID_DuplicateRequestExceptionThrown() {
    //create account holder request with valid param
    Request<CreateAccountHolderRequest> firstRequest = accountHolderTestHelper.getDefaultCreateAccountHolderRequest();
    accountHolderHandler.on(firstRequest.payload(), firstRequest);
    final AccountHolder responseForFirstRequest = captureResponsePayload(firstRequest, AccountHolder.class);

    //create account holder with valid param, should be not do anything, validate idempotent behaviour
    Request<CreateAccountHolderRequest> secondRequestWithSamePayload = accountHolderTestHelper.getDefaultCreateAccountHolderRequest()
        .payload()
        .toBuilder()
        .build();
    accountHolderHandler.on(secondRequestWithSamePayload.payload(), secondRequestWithSamePayload);
    final AccountHolderInsertionException exception = captureExceptionThrown(secondRequestWithSamePayload, AccountHolderInsertionException.class);

    //compare response of both the request should be same
    assertThat(exception)
        .hasMessageContaining("An account holder is already created with this request");
  }

  @Test
  public void get_AccountHolderByVector_success() {
    Request<CreateAccountHolderRequest> createAccountHolderRequest = accountHolderTestHelper.getDefaultCreateAccountHolderRequest();
    //get create account holder payload
    final CreateAccountHolderRequest createAccountHolderPayload = createAccountHolderRequest.payload();
    accountHolderHandler.on(createAccountHolderPayload, createAccountHolderRequest);
    final AccountHolder createAccountHolderResponse = captureResponsePayload(createAccountHolderRequest, AccountHolder.class);

    Request<GetAccountHolderByVectorRequest> getAccountHolderByVectorRequestRequest = accountHolderTestHelper.getAccountHolderByVectorRequest(ZetaUrlConstants.PHONE_NUMBER_PREFIX, PhoneNumber.PHONE_NUMBER_1.getValue());
    final GetAccountHolderByVectorRequest getAccountHolderByVectorPayload = getAccountHolderByVectorRequestRequest.payload();
    accountHolderHandler.on(getAccountHolderByVectorPayload, getAccountHolderByVectorRequestRequest);

    final AccountHolder getAccountHolderByVectorResponse = captureResponsePayload(getAccountHolderByVectorRequestRequest, AccountHolder.class);

    assertNotNull(getAccountHolderByVectorResponse);
    assertThat(createAccountHolderResponse)
        .usingRecursiveComparison()
        .ignoringFields("vectors", "pops", "createdAt")// TODO :: Doing this as CreatePayload has pops as null and but response has empty list
        .isEqualTo(getAccountHolderByVectorResponse);
  }

}