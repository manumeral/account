package in.zeta.oms.account.api;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.accessor.AccessorHandler;
import in.zeta.oms.account.account.accessor.AddAccountAccessorRequest;
import in.zeta.oms.account.account.accessor.GetAccountAccessorRequest;
import in.zeta.oms.account.account.accessor.UpdateAccountAccessorRequest;
import in.zeta.oms.account.api.model.Accessor;
import in.zeta.oms.account.constant.Status;
import in.zeta.oms.account.constant.TransactionPolicyID;
import in.zeta.oms.account.helper.AccessorTestHelper;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.account.helper.AccountTestHelper;
import olympus.message.types.Request;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AccessorHandlerTest extends AccountServiceBaseTest {
  public static final Map<String, String> UPDATED_ATTRIBUTES = Collections.singletonMap("key", "value");
  private final AccessorTestHelper accessorTestHelper;
  private final AccountTestHelper accountTestHelper;
  private final AccountHolderTestHelper accountHolderTestHelper;
  private final AccessorHandler handler;

  public AccessorHandlerTest() {
    this.accessorTestHelper = getInstance(AccessorTestHelper.class);
    this.accountTestHelper = getInstance(AccountTestHelper.class);
    this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
    this.handler = getInstance(AccessorHandler.class);
  }

  @Before
  public void setup() {
    accountTestHelper.insertDefaultAccount();
    accountHolderTestHelper.insertDefaultAccountHolder();
  }

  @Test
  public void create_ValidParams_Success() {
    Request<AddAccountAccessorRequest> request =
        accessorTestHelper.getDefaultAddAccountAccessorRequest();
    handler.on(request.payload(), request);
    final Accessor response = captureResponsePayload(request, Accessor.class);
    final Optional<Accessor> accountAccessorFromDB =
        accessorTestHelper.getAccountAccessorFromDB(
            response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountAccessorFromDB.isPresent());
    assertThat(accountAccessorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void getByID_ValidParams_Success() {
    accessorTestHelper.insertDefaultAccessors();
    Request<GetAccountAccessorRequest> request = accessorTestHelper.getDefaultGetAccountAccessorRequest();
    handler.on(request.payload(), request);
    final Accessor response = captureResponsePayload(request, Accessor.class);
    final Optional<Accessor> accountAccessorFromDB = accessorTestHelper.getAccountAccessorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountAccessorFromDB.isPresent());
    assertThat(accountAccessorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Ignore("List API test will be added later")
  @Test
  public void getList_ValidParams_Success() {
    //TODO : List API test will be added later
  }

  @Test
  public void updateStatus_ValidParams_Success() {
    final Accessor defaultAccountAccessor = AccessorTestHelper.getDefaultAccessors().get(0);
    accessorTestHelper.insertDefaultAccessors();
    Request<UpdateAccountAccessorRequest> request =
        accessorTestHelper.getUpdateAccountAccessorRequest(
            defaultAccountAccessor.toBuilder().status(Status.DISABLED.name()).build());
    handler.on(request.payload(), request);
    final Accessor response = captureResponsePayload(request, Accessor.class);
    final Optional<Accessor> accountAccessorFromDB = accessorTestHelper.getAccountAccessorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountAccessorFromDB.isPresent());
    assertThat(accountAccessorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void updateAttributes_ValidParams_Success() {
    final Accessor defaultAccountAccessor = AccessorTestHelper.getDefaultAccessors().get(0);
    accessorTestHelper.insertDefaultAccessors();
    Request<UpdateAccountAccessorRequest> request = accessorTestHelper.getUpdateAccountAccessorRequest(defaultAccountAccessor.toBuilder().attributes(UPDATED_ATTRIBUTES).build());
    handler.on(request.payload(), request);
    final Accessor response = captureResponsePayload(request, Accessor.class);
    final Optional<Accessor> accountAccessorFromDB = accessorTestHelper.getAccountAccessorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountAccessorFromDB.isPresent());
    assertThat(accountAccessorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }

  @Test
  public void updateTransactionPolicy_ValidParams_Success() {
    final Accessor defaultAccountAccessor = AccessorTestHelper.getDefaultAccessors().get(0);
    accessorTestHelper.insertDefaultAccessors();
    Request<UpdateAccountAccessorRequest> request = accessorTestHelper.getUpdateAccountAccessorRequest(defaultAccountAccessor.toBuilder().transactionPolicyIDs(Collections.singletonList(TransactionPolicyID.POLICY_ID_1.getValue())).build());
    handler.on(request.payload(), request);
    final Accessor response = captureResponsePayload(request, Accessor.class);
    final Optional<Accessor> accountAccessorFromDB = accessorTestHelper.getAccountAccessorFromDB(response.getId(), response.getAccountID(), response.getIfiID());
    assertNotNull(response);
    assertTrue(accountAccessorFromDB.isPresent());
    assertThat(accountAccessorFromDB.get())
        .usingRecursiveComparison()
        .ignoringFields("createdAt", "updatedAt")
        .isEqualTo(response);
  }
}
