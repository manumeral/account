package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CoaID {
  COA_ID_DEFAULT(103L),
  COA_ID_2(209L),
  COA_ID_INVALID(327L);

  private Long value;
}
