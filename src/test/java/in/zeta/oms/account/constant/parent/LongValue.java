package in.zeta.oms.account.constant.parent;

public interface LongValue {
  Long getValue();
}
