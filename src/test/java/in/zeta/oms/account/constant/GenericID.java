package in.zeta.oms.account.constant;

import in.zeta.oms.account.constant.parent.StringValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GenericID implements StringValue {
  DEFAULT_ID("1ad48202-d590-4bef-a1ad-5739961a1da1"),
  INVALID_ID("c482d651-73fa-4de8-ba09-bbc23112d22a"),
  ID_2("7991d2dd-8f73-4f8f-87d7-0a99b39c2c0f");

  private final String value;
}
