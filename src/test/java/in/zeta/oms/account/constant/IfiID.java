package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IfiID {
  IFI_ID_DEFAULT(1L),
  IFI_ID_2(2L),
  IFI_ID_INVALID(123L),
  STAGE_IFI_SAMPLE_1(140793L),
  IFI_SCHEMA_DEFAULT(3L);

  private Long value;
}
