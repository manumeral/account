package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TransactionPolicyID {
  POLICY_ID_DEFAULT("c482d651-73fa-4de8-1111-bbc231121111"),
  POLICY_ID_1("c482d651-73fa-4de8-1111-bbc231121112"),
  POLICY_ID_INVALID("c482d651-73fa-4de8-1111-bbc231120000");

  private final String value;
}
