package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExpiryTime {
    DEFAULT_EXPIRY_TIME("11/09/2020");
    private String value;
}
