package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountGroupID {
  ACCOUNT_GROUP_ID_DEFAULT("1ad48202-d590-4bef-a1ad-5739961a1da1"),
  ACCOUNT_GROUP_ID_2("c482d651-73fa-4de8-ba09-bbc23112d22a"),
  ACCOUNT_GROUP_ID_INVALID("7991d2dd-8f73-4f8f-87d7-0a99b39c2c0f");
  private final String value;
}
