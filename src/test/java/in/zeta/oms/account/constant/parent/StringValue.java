package in.zeta.oms.account.constant.parent;

public interface StringValue {
  String getValue();
}
