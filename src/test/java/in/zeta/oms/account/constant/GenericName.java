package in.zeta.oms.account.constant;

import in.zeta.oms.account.constant.parent.StringValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GenericName implements StringValue {
  DEFAULT_NAME_ACCOUNT_HOLDER("Test Name"),
  DEFAULT_NAME("Default Name"),
  UPDATED_NAME("Updated Name"),
  ACCOUNT_NAME("Account test Name"),
  NAME_2("Name 2");

  private final String value;
}
