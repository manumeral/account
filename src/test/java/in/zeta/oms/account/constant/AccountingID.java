package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountingID {
  ACCOUNTING_ID_DEFAULT(1L),
  ACCOUNTING_ID_2(2L),
  ACCOUNTING_ID_INVALID(123L);

  private Long value;

}
