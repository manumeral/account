package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountHolderProviderID {
  ACCOUNT_HOLDER_PROVIDER_ID_DEFAULT("f74ea5b5-840b-4842-b6f5-5426d415d8ed"),
  ACCOUNT_HOLDER_PROVIDER_ID_2("737ff230-d7ec-4700-9608-bc3993e22699"),
  ACCOUNT_HOLDER_PROVIDER_ID_INVALID("8d2c1762-d866-4293-b4c7-42057485a5d2");

  private String value;
}
