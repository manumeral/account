package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountHolderID {
  ACCOUNT_HOLDER_ID_DEFAULT("80bf9157-f50c-44e2-8881-54d83ea5253d"),
  ACCOUNT_HOLDER_ID_2("3907f370-2aaf-445a-b8db-3294f4dacc25"),
  ACCOUNT_HOLDER_ID_INVALID("c063414d-38f1-432a-85c2-1a11093951b3");
  private final String value;
}
