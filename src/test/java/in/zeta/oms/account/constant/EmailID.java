package in.zeta.oms.account.constant;

import in.zeta.oms.account.constant.parent.StringValue;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum EmailID implements StringValue {
  DEFAULT_EMAIL_ID("test1@gmail.com"),
  DEFAULT_EMAIL_ID_2("test_default_2@gmail.com"),
  UPDATE_EMAIL_ID("test2@gmail.com");

  private final String value;
}
