package in.zeta.oms.account.constant;

public enum Status {
  ENABLED,
  DISABLED,
  BLOCKED,
  DELETED
}
