package in.zeta.oms.account.constant;

import in.zeta.oms.account.constant.parent.StringValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum  ExternalID implements StringValue {
  DEFAULT_EXTERNAL_ID("10"),
  EXTERNAL_ID_2("12"),
  NON_ZETAUSER_EXTERNAL_ID("external_id"),
  ZETAUSER_EXTERNAL_ID("123"),
  ZETAUSER_EXTERNAL_ID_2("1234");

  private final String value;
}
