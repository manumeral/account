package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ParentCoaNodeID {
  PARENT_COA_NODE_ID_DEFAULT(1109L, "DEFAULT"),
  PARENT_COA_NODE_ID_2(2109L, "Node2");

  private Long value;
  private String name;
}
