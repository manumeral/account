package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccessorID {
  ACCESSOR_ID_DEFAULT("c482d651-73fa-4de8-ba09-bbc231121111"),
  ACCESSOR_ID_1("c482d651-73fa-4de8-ba09-bbc231121112"),
  ACCESSOR_ID_INVALID("c482d651-73fa-4de8-ba09-bbc231120000");

  private final String value;
}
