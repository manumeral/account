package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductFamilyID {
  PRODUCT_FAMILY_ID_DEFAULT(1L),
  PRODUCT_FAMILY_ID_2(2L),
  PRODUCT_FAMILY_ID_INVALID(123L);

  private Long value;

}
