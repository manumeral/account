package in.zeta.oms.account.constant;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountHolderGroupSchema {
  AH_WITH_MIN_KYC_WITH_PAN(
      new Gson()
          .fromJson(
              "{\"type\":\"object\",\"title\":\"User\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"required\":[\"KYCStatus\"],\"properties\":{\"KYCStatus\":{\"type\":\"object\",\"properties\":{\"kycStatus\":{\"type\":\"string\",\"pattern\":\"MIN_KYC_WITH_PAN\"}},\"description\":\"KYCStatus of AH\"}},\"description\":\"MIN KYC status\"}",
              JsonObject.class));

  private final JsonObject schema;
}
