package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountVectorID {
  ACCOUNT_VECTOR_ID_DEFAULT("c284d651-73fa-4de8-ba09-bbc231122222"),
  ACCOUNT_VECTOR_ID_1("c284d651-73fa-4de8-ba09-bbc231122223"),
  ACCOUNT_VECTOR_ID_INVALID("c284d651-73fa-4de8-ba09-bbc231123333");

  private final String value;
}
