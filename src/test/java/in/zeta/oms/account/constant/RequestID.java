package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RequestID {
  REQUEST_ID_DEFAULT("546630c7-7854-4e93-ab77-dd364b96fedd"),
  REQUEST_ID_2("f4e5e4c8-b303-4aef-9690-bc8545f06372"),
  REQUEST_ID_3("ave5e4c8-b303-4aef-9690-bc8545f06372"),
  REQUEST_ID_INVALID("5ccbdb05-fc62-41da-8b93-1f1c5605407c");

  private String value;

}
