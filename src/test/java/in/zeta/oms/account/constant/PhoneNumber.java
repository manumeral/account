package in.zeta.oms.account.constant;

import in.zeta.oms.account.constant.parent.StringValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PhoneNumber implements StringValue {
  PHONE_NUMBER_1("+918888888888"),
  PHONE_NUMBER_2("+911111111111");

  private final String value;
}
