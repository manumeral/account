package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LedgerID {
    DEFAULT_LEDGER_ID(1L);
    private final Long value;
}
