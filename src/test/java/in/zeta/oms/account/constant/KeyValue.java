package in.zeta.oms.account.constant;

import in.zeta.oms.account.constant.parent.StringValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum KeyValue implements StringValue {

  KEY_ENTITY_PROVIDER_1("attribute1"),
  KEY_ENTITY_PROVIDER_2("attribute2"),
  KEY_ACCOUNT_HOLDER_CORP_ID("accountHolder.corpID"),
  KEY_ACCOUNT_HOLDER_CORP_NAME("accountHolder.corpName"),
  KEY_IS_ATM_ENABLED("isAtmEnabled"),
  VALUE_IS_ATM_ENABLED("true"),
  VALUE_ENTITY_PROVIDER_1("value1"),
  VALUE_ENTITY_PROVIDER_2("value2"),
  VALUE_ACCOUNT_HOLDER_CORP_DEFAULT("1"),
  VALUE_ACCOUNT_HOLDER_CORP_INVALID("2"),
  VALUE_ACCOUNT_HOLDER_CORP_NAME("accenture"),
  VALUE_ACCOUNT_HOLDER_CORP_NAME_1("wellsfargo");

  private final String value;
}
