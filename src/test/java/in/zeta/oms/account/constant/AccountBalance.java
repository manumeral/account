package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountBalance {
    DEFAULT_ACCOUNT_BALANCE(1L, "INR");
    private final Long balance;
    private final String currency;
}
