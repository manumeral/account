package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GenericBusinessID {
  GENERIC_BUSINESS_ID_DEFAULT(1L),
  GENERIC_BUSINESS_ID_1(2L);

  private final Long value;
}
