package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountRelationID {
  ACCOUNT_RELATION_ID_DEFAULT("c482d651-73fa-4de8-ba09-bbc231122222"),
  ACCOUNT_RELATION_ID_1("c482d651-73fa-4de8-ba09-bbc231122223"),
  ACCOUNT_RELATION_ID_INVALID("c482d651-73fa-4de8-ba09-bbc231123333");

  private final String value;
}
