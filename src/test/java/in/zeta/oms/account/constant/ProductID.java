package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductID {
  PRODUCT_ID_DEFAULT(1L),
  PRODUCT_ID_2(2L),
  PRODUCT_ID_INVALID(123L);

  private Long value;

}
