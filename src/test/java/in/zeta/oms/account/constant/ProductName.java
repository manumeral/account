package in.zeta.oms.account.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductName {
  PRODUCT_NAME_DEFAULT,
  PRODUCT_NAME_2,
  PRODUCT_NAME_INVALID,
  GPR_KYC_SHORTFALL_WITH_PAN;
}
