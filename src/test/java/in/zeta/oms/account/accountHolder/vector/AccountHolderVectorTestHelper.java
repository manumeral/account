package in.zeta.oms.account.accountHolder.vector;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.constant.AccountHolderVectorID;
import in.zeta.oms.account.constant.Status;
import olympus.message.types.Request;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

@Singleton
public class AccountHolderVectorTestHelper extends AccountServiceBaseTest {
    private final AccountHolderVectorDAO accountHolderVectorDAO;

    @Inject
    public AccountHolderVectorTestHelper(AccountHolderVectorDAO accountHolderVectorDAO) {
        this.accountHolderVectorDAO = accountHolderVectorDAO;
    }

    public static List<AccountHolderVector> getDefaultAccountHolderVectors() {
        return singletonList(AccountHolderVector.builder()
            .id(AccountHolderVectorID.ACCOUNT_HOLDER_VECTOR_ID_DEFAULT.getValue())
            .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .type("email")
            .value("abc@xyz.com")
            .status(Status.ENABLED.name())
            .attributes(Collections.emptyMap())
            .build());
    }

    public static Request<AddAccountHolderVectorRequest> getDefaultAddAccountHolderVectorRequest() {
        final AccountHolderVector defaultAccountHolderVector = getDefaultAccountHolderVectors().get(0);
        return new AddAccountHolderVectorRequest.Builder()
            .accountHolderID(defaultAccountHolderVector.getAccountHolderID())
            .attributes(defaultAccountHolderVector.getAttributes())
            .ifiID(defaultAccountHolderVector.getIfiID())
            .status(defaultAccountHolderVector.getStatus())
            .type(defaultAccountHolderVector.getType())
            .value(defaultAccountHolderVector.getValue())
            .build();
    }

    public AccountHolderVector insertDefaultAccountHolderVector() {
        AccountHolderVector defaultAccountHolderVector = getDefaultAccountHolderVectors().get(0);
        accountHolderVectorDAO.insert(defaultAccountHolderVector).toCompletableFuture().join();
        return defaultAccountHolderVector;
    }

    public Optional<AccountHolderVector> getAccountHolderVectorFromDB(String accountHolderVectorID, String accountID, Long ifiID) {
        return accountHolderVectorDAO.get(accountHolderVectorID, accountID, ifiID).toCompletableFuture().join();
    }

    public Request<UpdateAccountHolderVectorRequest> getUpdateAccountHolderVectorRequest(AccountHolderVector accountHolderVector) {
        final AccountHolderVector defaultAccountHolderVector = getDefaultAccountHolderVectors().get(0);
        return new UpdateAccountHolderVectorRequest.Builder()
            .id(accountHolderVector.getId())
            .accountHolderID(accountHolderVector.getAccountHolderID())
            .ifiID(accountHolderVector.getIfiID())
            .status(firstNonNull(accountHolderVector.getStatus(), defaultAccountHolderVector.getStatus()))
            .type(firstNonNull(accountHolderVector.getType(), defaultAccountHolderVector.getType()))
            .value(firstNonNull(accountHolderVector.getValue(), defaultAccountHolderVector.getValue()))
            .attributes(firstNonNull(accountHolderVector.getAttributes(), defaultAccountHolderVector.getAttributes()))
            .build();
    }
}