package in.zeta.oms.account.accountHolder.kyc;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.ExpiryTime;
import in.zeta.oms.account.constant.IfiID;
import olympus.message.types.Request;

import java.sql.Timestamp;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class KYCStatusTestHelper {
  public static final String PAN = "pan";
  public static final String DEFAULT_PAN = "default_pan";
  public static final String AADHAAR_OTP = "AADHAAR_OTP";
  public static final String KYC_TYPE = "kycType";
  public final KYCStatusService KYCStatusService;

  @Inject
  public KYCStatusTestHelper(KYCStatusService KYCStatusService) {
    this.KYCStatusService = KYCStatusService;
  }

  public Request<AddKycStatusRequest> addDefaultEffectiveKycStatusRequest() {
    return AddKycStatusRequest.builder()
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .expiryTime(new Timestamp(new Date(ExpiryTime.DEFAULT_EXPIRY_TIME.getValue()).getTime()).toLocalDateTime())
        .kycStatus(KYCStatusConstant.KYC_EXPIRED_CORP_WITH_PAN.toString())
        .kycStatusPostExpiry(KYCStatusConstant.KYC_EXPIRED_CORP_WITH_PAN.toString())
        .attributes(ImmutableMap.of(
            PAN, DEFAULT_PAN,
            KYC_TYPE, AADHAAR_OTP))
        .build();
  }

  public KYCStatus get(String accountHolderID, long ifiID) {
    return KYCStatusService.get(accountHolderID, ifiID).toCompletableFuture().join();
  }

  public KYCStatus addDefaultEffectiveKYCStatus() {
    KYCStatus addKYCStatus = KYCStatus.builder()
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .expiryTime(new Timestamp(new Date("11/09/2020").getTime()).toLocalDateTime())
        .kycStatus(KYCStatusConstant.KYC_EXPIRED_CORP_WITH_PAN.toString())
        .kycStatusPostExpiry(KYCStatusConstant.KYC_EXPIRED_CORP_WITH_PAN.toString())
        .attributes(ImmutableMap.of(
            PAN, DEFAULT_PAN,
            KYC_TYPE, AADHAAR_OTP))
        .build();
    return KYCStatusService.createKycStatus(addKYCStatus).toCompletableFuture().join();
  }

  public Request<GetKycStatusRequest> getEffectiveKycStatusRequestRequest(String accountHolderID, long ifi) {
    return GetKycStatusRequest.builder()
        .accountHolderID(accountHolderID)
        .ifiID(ifi)
        .build();
  }

  public Request<UpdateKycStatusRequest> updateEffectiveKycStatusRequest(String accountHolderID) {
    return UpdateKycStatusRequest.builder()
        .accountHolderID(accountHolderID)
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .kycStatus(KYCStatusConstant.AADHAAR_OTP_KYC_WITH_PAN.toString())
        .kycStatusPostExpiry(KYCStatusConstant.KYC_EXPIRED_WITH_PAN.toString())
        .build();
  }

  public Request<UpdateKycStatusRequest> updateEffectiveKycStatusRequestWithAttrs(String accountHolderID) {
    return UpdateKycStatusRequest.builder()
        .accountHolderID(accountHolderID)
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .kycStatus(KYCStatusConstant.AADHAAR_OTP_KYC_WITH_PAN.toString())
        .kycStatusPostExpiry(KYCStatusConstant.KYC_EXPIRED_WITH_PAN.toString())
        .attributes(ImmutableMap.of(PAN, DEFAULT_PAN))
        .build();
  }

  public KYCStatus getDefaultEffectiveKycStatus() {
    return KYCStatus.builder()
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .expiryTime(new Timestamp(new Date("11/09/2020").getTime()).toLocalDateTime())
        .kycStatus(KYCStatusConstant.KYC_EXPIRED_CORP_WITH_PAN.toString())
        .kycStatusPostExpiry(KYCStatusConstant.KYC_EXPIRED_CORP_WITH_PAN.toString())
        .attributes(ImmutableMap.of(
            PAN, DEFAULT_PAN,
            KYC_TYPE, AADHAAR_OTP))
        .build();
  }

  public void assertEqualsWithDefault(KYCStatus KYCStatus, String... propertiesToIgnore){
    assertThat(KYCStatus).isEqualToIgnoringGivenFields(getDefaultEffectiveKycStatus(), propertiesToIgnore);
  }

}
