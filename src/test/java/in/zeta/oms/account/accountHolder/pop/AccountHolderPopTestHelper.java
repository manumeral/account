package in.zeta.oms.account.accountHolder.pop;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.api.model.Address;
import in.zeta.oms.account.api.model.Contact;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.account.pop.AddAccountHolderPOPRequest;
import in.zeta.oms.account.pop.DeleteAccountHolderPOPRequest;
import in.zeta.oms.account.pop.POP;
import in.zeta.oms.account.pop.UpdateAccountHolderPOPRequest;
import olympus.message.types.Request;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static in.zeta.oms.account.pop.EntityType.ACCOUNT_HOLDER;

@Singleton
public class AccountHolderPopTestHelper extends AccountServiceBaseTest {
    private final AccountHolderTestHelper accountHolderTestHelper;
    private final POPService popService;

    @Inject
    public AccountHolderPopTestHelper(POPService popService) {
        this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
        this.popService = popService;
    }

    private static final Map<String, String> popAccountHolderAttributesValid = AccountHolderTestHelper.accountHolderAttributesValid.entrySet().stream()
        .collect(Collectors.toMap(Map.Entry::getKey, e -> (String)e.getValue()));

    private static final Address address =
        Address.builder()
            .line1("zeta")
            .city( "bangalore")
            .state( "karnataka")
            .country( "INDIA")
            .build();

    public static final Address address2 =
        Address.builder()
            .line1("zeta")
            .city( "mumbai")
            .state( "maharashtra")
            .country( "INDIA")
            .build();


    public Request<AddAccountHolderPOPRequest> getDefaultAddPOPRequest() {
        return getDefaultAddPOPRequestBuilder().build();
    }

    public AddAccountHolderPOPRequest.Builder getDefaultAddPOPRequestBuilder() {
        Contact contact =
            Contact.builder()
                .firstName("zeta")
                .lastName( "user")
                .vectors(accountHolderTestHelper.vectors)
                .build();
        return AddAccountHolderPOPRequest.builder()
            .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .address(address)
            .contactList(ImmutableList.of(contact))
            .ifiID(IFI_ID_DEFAULT.getValue())
            .label("home")
            .attributes(popAccountHolderAttributesValid);
    }

    public Request<UpdateAccountHolderPOPRequest> getUpdatePOPRequest() {
        return getUpdatePOPRequestBuilder().build();
    }

    public UpdateAccountHolderPOPRequest.Builder getUpdatePOPRequestBuilder() {
        return UpdateAccountHolderPOPRequest.builder()
            .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .address(address2)
            .label("office");
    }


    public Request<DeleteAccountHolderPOPRequest> getDeletePOPRequest(String id) {
        return DeleteAccountHolderPOPRequest.builder()
            .ID(id)
            .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .build();
    }

    public List<POP> listPOPs(Long ififID, String accountHolderID) {
        return popService.listPOPs(ififID, accountHolderID, ACCOUNT_HOLDER).toCompletableFuture().join();
    }
}
