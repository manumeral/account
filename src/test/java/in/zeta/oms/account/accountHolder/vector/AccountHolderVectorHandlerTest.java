package in.zeta.oms.account.accountHolder.vector;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import olympus.message.types.Request;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AccountHolderVectorHandlerTest extends AccountServiceBaseTest {

    private static final String UPDATED_VALUE = "updatedValue";
    private static final String UPDATED_TYPE = "updatedType";
    private static final Map<String, String> UPDATED_ATTRIBUTES = Collections.singletonMap("key", "value");
    private final AccountHolderVectorTestHelper accountHolderVectorTestHelper;
    private final AccountHolderTestHelper accountHolderTestHelper;
    private final AccountHolderVectorHandler handler;

    public AccountHolderVectorHandlerTest() {
        this.accountHolderVectorTestHelper = getInstance(AccountHolderVectorTestHelper.class);
        this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
        this.handler = getInstance(AccountHolderVectorHandler.class);
    }

    @Before
    public void setup() {
        accountHolderTestHelper.insertDefaultAccountHolder();
    }

    @Test
    public void create_ValidParams_Success() {
        Request<AddAccountHolderVectorRequest> request =
            AccountHolderVectorTestHelper.getDefaultAddAccountHolderVectorRequest();
        handler.on(request.payload(), request);
        final AccountHolderVector response = captureResponsePayload(request, AccountHolderVector.class);
        final Optional<AccountHolderVector> accountHolderVectorFromDB =
            accountHolderVectorTestHelper.getAccountHolderVectorFromDB(response.getId(), response.getAccountHolderID(), response.getIfiID());
        assertNotNull(response);
        assertTrue(accountHolderVectorFromDB.isPresent());
        assertThat(accountHolderVectorFromDB.get())
            .usingRecursiveComparison()
            .ignoringFields("createdAt", "updatedAt")
            .isEqualTo(response);
    }

    @Test
    public void updateStatus_ValidParams_Success() {
        final AccountHolderVector defaultAccountHolderVector = AccountHolderVectorTestHelper.getDefaultAccountHolderVectors().get(0);
        accountHolderVectorTestHelper.insertDefaultAccountHolderVector();
        Request<UpdateAccountHolderVectorRequest> request =
            accountHolderVectorTestHelper.getUpdateAccountHolderVectorRequest(
                defaultAccountHolderVector.toBuilder().status(Status.DISABLED.name()).build());
        handler.on(request.payload(), request);
        final AccountHolderVector response = captureResponsePayload(request, AccountHolderVector.class);
        final Optional<AccountHolderVector> accountHolderVectorFromDB = accountHolderVectorTestHelper.getAccountHolderVectorFromDB(response.getId(), response.getAccountHolderID(), response.getIfiID());
        assertNotNull(response);
        assertTrue(accountHolderVectorFromDB.isPresent());
        assertThat(accountHolderVectorFromDB.get())
            .usingRecursiveComparison()
            .ignoringFields("createdAt", "updatedAt")
            .isEqualTo(response);
    }

    @Test
    public void updateTypeValue_ValidParams_Success() {
        final AccountHolderVector defaultAccountHolderVector = AccountHolderVectorTestHelper.getDefaultAccountHolderVectors().get(0);
        accountHolderVectorTestHelper.insertDefaultAccountHolderVector();
        Request<UpdateAccountHolderVectorRequest> request = accountHolderVectorTestHelper.getUpdateAccountHolderVectorRequest(defaultAccountHolderVector.toBuilder().type(UPDATED_TYPE).value(UPDATED_VALUE).build());
        handler.on(request.payload(), request);
        final AccountHolderVector response = captureResponsePayload(request, AccountHolderVector.class);
        final Optional<AccountHolderVector> accountHolderVectorFromDB = accountHolderVectorTestHelper.getAccountHolderVectorFromDB(response.getId(), response.getAccountHolderID(), response.getIfiID());
        assertNotNull(response);
        assertTrue(accountHolderVectorFromDB.isPresent());
        assertThat(accountHolderVectorFromDB.get())
            .usingRecursiveComparison()
            .ignoringFields("createdAt", "updatedAt")
            .isEqualTo(response);
    }

    @Test
    public void updateAttributes_ValidParams_Success() {
        final AccountHolderVector defaultAccountHolderVector = AccountHolderVectorTestHelper.getDefaultAccountHolderVectors().get(0);
        accountHolderVectorTestHelper.insertDefaultAccountHolderVector();
        Request<UpdateAccountHolderVectorRequest> request = accountHolderVectorTestHelper.getUpdateAccountHolderVectorRequest(defaultAccountHolderVector.toBuilder().attributes(UPDATED_ATTRIBUTES).build());
        handler.on(request.payload(), request);
        final AccountHolderVector response = captureResponsePayload(request, AccountHolderVector.class);
        final Optional<AccountHolderVector> accountHolderVectorFromDB = accountHolderVectorTestHelper.getAccountHolderVectorFromDB(response.getId(), response.getAccountHolderID(), response.getIfiID());
        assertNotNull(response);
        assertTrue(accountHolderVectorFromDB.isPresent());
        assertThat(accountHolderVectorFromDB.get())
            .usingRecursiveComparison()
            .ignoringFields("createdAt", "updatedAt")
            .isEqualTo(response);
    }
}