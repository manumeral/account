package in.zeta.oms.account.accountHolder.whitelistedAccountHolderAttributes;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.AddWhitelistedAccountHolderAttributeRequest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.GetWhitelistedAccountHolderAttributesRequest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.UpdateWhitelistedAccountHolderAttributesRequest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributes;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributesHandler;
import olympus.message.types.Request;
import org.junit.Test;

import java.util.List;

import static in.zeta.oms.account.accountHolder.whitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributesHelper.ACCOUNT_HOLDER_VECTORS_STATUS_PATH;
import static junit.framework.TestCase.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

public class WhitelistedAccountHolderAttributesHandlerTest extends AccountServiceBaseTest {

  private WhitelistedAccountHolderAttributesHandler handler;
  private WhitelistedAccountHolderAttributesHelper whitelistedAccountHolderAttributesHelper;


  public WhitelistedAccountHolderAttributesHandlerTest() {
    this.handler = getInstance(WhitelistedAccountHolderAttributesHandler.class);
    this.whitelistedAccountHolderAttributesHelper = getInstance(WhitelistedAccountHolderAttributesHelper.class);
  }

  @Test
  public void create_ValidParams_Success() {
    final Request<AddWhitelistedAccountHolderAttributeRequest> request = whitelistedAccountHolderAttributesHelper.getDefaultAddWhitelistedAccountHolderAttributesRequest();
    final AddWhitelistedAccountHolderAttributeRequest payload = request.payload();
    handler.on(payload, request);
    final WhitelistedAccountHolderAttributes response = captureResponsePayload(request, WhitelistedAccountHolderAttributes.class);
    List<WhitelistedAccountHolderAttributes> whitelistedAccountHolderAttributes = whitelistedAccountHolderAttributesHelper.getWhitelistedAccountHolderAttributesFromDB(payload.getIfiID(), payload.getAttributeName()).toCompletableFuture().join();
    assertEquals(1, whitelistedAccountHolderAttributes.size());
    assertThat(whitelistedAccountHolderAttributes.get(0))
        .usingRecursiveComparison()
        .isEqualTo(whitelistedAccountHolderAttributesHelper.getDefaultWhitelistedAttributes());
    assertThat(response)
        .usingRecursiveComparison()
        .isEqualTo(whitelistedAccountHolderAttributesHelper.getDefaultWhitelistedAttributes());
  }

  @Test
  public void get_ValidParams_Success() {
    whitelistedAccountHolderAttributesHelper.insertDefaultWhitelistedAccountHolderAttributesInDB().toCompletableFuture().join();
    final Request<GetWhitelistedAccountHolderAttributesRequest> request = whitelistedAccountHolderAttributesHelper.getDefaultGetWhitelistedAccountHolderAttributesRequest();
    final GetWhitelistedAccountHolderAttributesRequest payload = request.payload();
    handler.on(payload, request);
    final WhitelistedAccountHolderAttributes response = captureResponsePayload(request, WhitelistedAccountHolderAttributes.class);
    List<WhitelistedAccountHolderAttributes> whitelistedAccountHolderAttributes = whitelistedAccountHolderAttributesHelper.getWhitelistedAccountHolderAttributesFromDB(payload.getIfiID(), payload.getAttributeName()).toCompletableFuture().join();
    assertEquals(1, whitelistedAccountHolderAttributes.size());
    assertThat(whitelistedAccountHolderAttributes.get(0))
        .usingRecursiveComparison()
        .isEqualTo(whitelistedAccountHolderAttributesHelper.getDefaultWhitelistedAttributes());
    assertThat(response)
        .usingRecursiveComparison()
        .isEqualTo(whitelistedAccountHolderAttributesHelper.getDefaultWhitelistedAttributes());
  }

  @Test
  public void update_ValidParams_Success() {
    whitelistedAccountHolderAttributesHelper.insertDefaultWhitelistedAccountHolderAttributesInDB().toCompletableFuture().join();
    final Request<UpdateWhitelistedAccountHolderAttributesRequest> request = whitelistedAccountHolderAttributesHelper.getDefaultUpdateWhitelistedAccountHolderAttributesRequest();
    final UpdateWhitelistedAccountHolderAttributesRequest payload = request.payload();
    handler.on(payload, request);
    final WhitelistedAccountHolderAttributes response = captureResponsePayload(request, WhitelistedAccountHolderAttributes.class);
    List<WhitelistedAccountHolderAttributes> whitelistedAccountHolderAttributes = whitelistedAccountHolderAttributesHelper.getWhitelistedAccountHolderAttributesFromDB(payload.getIfiID(), payload.getAttributeName()).toCompletableFuture().join();
    assertEquals(1, whitelistedAccountHolderAttributes.size());
    assertThat(whitelistedAccountHolderAttributes.get(0))
        .usingRecursiveComparison()
        .isEqualTo(whitelistedAccountHolderAttributesHelper.getDefaultWhitelistedAttributes().toBuilder().attributePath(ACCOUNT_HOLDER_VECTORS_STATUS_PATH).build());
    assertThat(whitelistedAccountHolderAttributes.get(0))
        .usingRecursiveComparison()
        .isEqualTo(response);
  }
}
