package in.zeta.oms.account.accountHolder.pop;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.api.exception.AccountHolderNotFoundException;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import in.zeta.oms.account.pop.AddAccountHolderPOPRequest;
import in.zeta.oms.account.pop.DeleteAccountHolderPOPRequest;
import in.zeta.oms.account.pop.GetAccountHolderPOPRequest;
import in.zeta.oms.account.pop.POP;
import in.zeta.oms.account.pop.POPNotFoundException;
import in.zeta.oms.account.pop.UpdateAccountHolderDefaultPOPRequest;
import in.zeta.oms.account.pop.UpdateAccountHolderPOPRequest;
import olympus.message.types.Request;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_INVALID;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AccountHolderPopHandlerTest extends AccountServiceBaseTest {
    private final AccountHolderTestHelper accountHolderTestHelper;
    private final AccountHolderPOPHandler popHandler;
    private final AccountHolderPopTestHelper popTestHelper;


    public AccountHolderPopHandlerTest() {
        super();
        this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
        this.popHandler = getInstance(AccountHolderPOPHandler.class);
        this.popTestHelper = getInstance(AccountHolderPopTestHelper.class);
    }

    @Before
    public void setup() throws Exception {
        accountHolderTestHelper.insertDefaultAccountHolder();
    }

    @Test
    public void addPOP_ValidParam_SuccessfullyReturned() {
        AccountHolder accountHolder = accountHolderTestHelper.getDefaultAccountHolder();
        Request<AddAccountHolderPOPRequest> request = popTestHelper.getDefaultAddPOPRequest();
        popHandler.on(request.payload(), request);
        final POP response = captureResponsePayload(request, POP.class);
        final List<POP> pops = popTestHelper.listPOPs(accountHolder.getIfiID(), accountHolder.getId());
        assertEquals(1, pops.size());
        Assertions.assertThat(pops.get(0)).usingRecursiveComparison().ignoringFields("updatedAt", "createdAt").isEqualTo(response);
    }

    @Test
    public void addPOP_checkIdempotency_Successfully() {
        Request<AddAccountHolderPOPRequest> request1 = popTestHelper.getDefaultAddPOPRequest();
        Request<AddAccountHolderPOPRequest> request2 = popTestHelper.getDefaultAddPOPRequest();
        popHandler.on(request1.payload(), request1);
        final POP response1 = captureResponsePayload(request1, POP.class);
        popHandler.on(request2.payload(), request2);
        final POP response2 = captureResponsePayload(request2, POP.class);
        Assertions.assertThat(response1).usingRecursiveComparison().ignoringFields("updatedAt", "createdAt").isEqualTo(response2);
    }

    @Test
    public void addPOP_invalidAccountHolder_throwsException() {
        Request<AddAccountHolderPOPRequest> request = popTestHelper.getDefaultAddPOPRequestBuilder()
            .accountHolderID(ACCOUNT_HOLDER_ID_INVALID.getValue())
            .build();
        popHandler.on(request.payload(), request);
        AccountHolderNotFoundException exception = captureExceptionThrown(request, AccountHolderNotFoundException.class);
        assertNotNull(exception);
        assertEquals(exception.getErrorCode(), AccountErrorCode.ACCOUNT_HOLDER_NOT_FOUND.name());
    }

    @Test
    public void deletePOP_ValidParam_SuccessfullyReturned() {
        Request<AddAccountHolderPOPRequest> request = popTestHelper.getDefaultAddPOPRequest();
        popHandler.on(request.payload(), request);
        final POP response = captureResponsePayload(request, POP.class);
        Request<DeleteAccountHolderPOPRequest> deletePOPRequest = popTestHelper.getDeletePOPRequest(response.getId());
        popHandler.on(deletePOPRequest.payload(), deletePOPRequest);
        final POP delete_response = captureResponsePayload(deletePOPRequest, POP.class);
        assertNotNull(delete_response);
        assertEquals(delete_response.getStatus(), "DELETED");
    }

    @Test
    public void updatePOP_ValidParam_Successfully() {
        Request<AddAccountHolderPOPRequest> request1 = popTestHelper.getDefaultAddPOPRequest();
        popHandler.on(request1.payload(), request1);
        final POP response1 = captureResponsePayload(request1, POP.class);
        Request<UpdateAccountHolderPOPRequest> request2 = popTestHelper.getUpdatePOPRequestBuilder()
            .id(response1.getId())
            .build();
        popHandler.on(request2.payload(), request2);
        final POP response2 = captureResponsePayload(request2, POP.class);
        Assertions.assertThat(response1).usingRecursiveComparison().ignoringFields("updatedAt", "modifiedAt", "label", "address").isEqualTo(response2);
        assertEquals(response2.getLabel(), "office");
        Assertions.assertThat(response2.getAddress()).usingRecursiveComparison().isEqualTo(AccountHolderPopTestHelper.address2);
    }

    @Test
    public void updatePOP_InvalidId_throwsException() {
        Request<AddAccountHolderPOPRequest> request = popTestHelper.getDefaultAddPOPRequest();
        popHandler.on(request.payload(), request);
        Request<UpdateAccountHolderPOPRequest> request2 = popTestHelper.getUpdatePOPRequestBuilder()
            .id(ACCOUNT_HOLDER_ID_INVALID.getValue())
            .build();
        popHandler.on(request2.payload(), request2);
        POPNotFoundException exception = captureExceptionThrown(request2, POPNotFoundException.class);
        assertNotNull(exception);
        assertEquals(exception.getErrorCode(), AccountErrorCode.POP_NOT_FOUND.name());
    }

    @Test
    public void getPOP_ValidParam_Successfully() {
        Request<AddAccountHolderPOPRequest> request1 = popTestHelper.getDefaultAddPOPRequest();
        popHandler.on(request1.payload(), request1);
        final POP response1 = captureResponsePayload(request1, POP.class);
        Request<GetAccountHolderPOPRequest> request2 = GetAccountHolderPOPRequest.builder()
            .id(response1.getId())
            .ifiID(response1.getIfiID())
            .accountHolderID(response1.getEntityID())
            .build();
        popHandler.on(request2.payload(), request2);
        final POP response = captureResponsePayload(request2, POP.class);
        final List<POP> pops = popTestHelper.listPOPs(response1.getIfiID(), response1.getEntityID());
        Assertions.assertThat(response).usingRecursiveComparison().ignoringFields("updatedAt", "createdAt").isEqualTo(pops.get(0));
    }

    @Test
    public void updateDefault_ValidParam_Successfully() {
        Request<AddAccountHolderPOPRequest> request = popTestHelper.getDefaultAddPOPRequest();

        //Run the test
        popHandler.on(request.payload(), request);
        final POP response = captureResponsePayload(request, POP.class);

        Request<UpdateAccountHolderDefaultPOPRequest> defaultPOPRequest = UpdateAccountHolderDefaultPOPRequest.builder()
            .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .id(response.getId())
            .build();
        popHandler.on(defaultPOPRequest.payload(), defaultPOPRequest);
        final POP defaultPOPResponse = captureResponsePayload(defaultPOPRequest, POP.class);

        //Verify the results
        assertEquals(response.isDefault(), false);
        assertEquals(defaultPOPResponse.isDefault(), true);
    }

    @Test
    public void updateDefault_ValidateSingleDefault_Successfully() {
        //Setup
        Request<AddAccountHolderPOPRequest> request1 = popTestHelper.getDefaultAddPOPRequest();
        Request<AddAccountHolderPOPRequest> request2 = popTestHelper.getDefaultAddPOPRequestBuilder()
            .address(AccountHolderPopTestHelper.address2)
            .label("office")
            .build();
        // Run test
        popHandler.on(request1.payload(), request1);
        popHandler.on(request2.payload(), request2);

        final POP response1 = captureResponsePayload(request1, POP.class);
        final POP response2 = captureResponsePayload(request2, POP.class);

        //Make response1 as default address
        Request<UpdateAccountHolderDefaultPOPRequest> defaultPOPRequest = UpdateAccountHolderDefaultPOPRequest.builder()
            .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .id(response1.getId())
            .build();
        popHandler.on(defaultPOPRequest.payload(), defaultPOPRequest);
        final POP addressResponse1 = captureResponsePayload(defaultPOPRequest, POP.class);

        //Verify address as default
        assertEquals(addressResponse1.isDefault(), true);

        //Make response2 as default address
        Request<UpdateAccountHolderDefaultPOPRequest> updateDefaultPOPRequest = UpdateAccountHolderDefaultPOPRequest.builder()
            .accountHolderID(ACCOUNT_HOLDER_ID_DEFAULT.getValue())
            .ifiID(IFI_ID_DEFAULT.getValue())
            .id(response2.getId())
            .build();
        popHandler.on(updateDefaultPOPRequest.payload(), updateDefaultPOPRequest);
        final POP updatesDefaultPOPResponse = captureResponsePayload(updateDefaultPOPRequest, POP.class);

        //
        //Verify updatesDefaultPOPResponse address as default
        assertEquals(updatesDefaultPOPResponse.isDefault(), true);
        final List<POP> pops = popTestHelper.listPOPs(IFI_ID_DEFAULT.getValue(), ACCOUNT_HOLDER_ID_DEFAULT.getValue());

        pops.stream().forEach(pop -> {
            if(pop.getId() == response1.getId())
                assertEquals(pop.isDefault(), false);
            if(pop.getId() == response2.getId())
                assertEquals(pop.isDefault(), true);
        });
    }
}
