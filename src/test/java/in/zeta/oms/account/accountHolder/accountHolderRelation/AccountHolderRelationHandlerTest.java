package in.zeta.oms.account.accountHolder.accountHolderRelation;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.constant.GenericID;
import in.zeta.oms.account.constant.IfiID;
import olympus.message.types.Request;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_2;
import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static in.zeta.oms.account.constant.GenericName.UPDATED_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AccountHolderRelationHandlerTest extends AccountServiceBaseTest {
  private final AccountHolderRelationTestHelper accountHolderRelationTestHelper;
  private final AccountHolderRelationHandler accountHolderRelationHandler;

  public AccountHolderRelationHandlerTest() {
    this.accountHolderRelationHandler = getInstance(AccountHolderRelationHandler.class);
    this.accountHolderRelationTestHelper = getInstance(AccountHolderRelationTestHelper.class);
  }

  @Test
  public void create_validParams_success() {
    // Setup
    accountHolderRelationTestHelper.insertDefaultAccountHolders();
    final Request<AddAccountHolderRelationRequest> request =
        accountHolderRelationTestHelper.addDefaultAccountHolderRelationRequest();

    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    // Verify the results
    final AccountHolderRelation accountHolderRelation =
        captureResponsePayload(request, AccountHolderRelation.class);
    final List<AccountHolderRelation> accountHolderRelationByAccountHolderID =
        accountHolderRelationTestHelper.getAccountHolderRelationByAccountHolderID(
            request.payload().getAccountHolderID(), accountHolderRelation.getIfiID());

    assertNotNull(accountHolderRelationByAccountHolderID);
    assertEquals(1, accountHolderRelationByAccountHolderID.size());
    assertThat(accountHolderRelationByAccountHolderID.get(0)).isEqualToIgnoringGivenFields(accountHolderRelation, "updatedAt", "createdAt");
  }


  @Test
  public void create_repeatedRequest_idempotentResponse() {
    // Setup
    accountHolderRelationTestHelper.insertDefaultAccountHolders();
    final Request<AddAccountHolderRelationRequest> request =
        accountHolderRelationTestHelper.addDefaultAccountHolderRelationRequest();
    final Request<AddAccountHolderRelationRequest> request2 =
        accountHolderRelationTestHelper.addDefaultAccountHolderRelationRequest();

    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    final AccountHolderRelation accountHolderRelation =
        captureResponsePayload(request, AccountHolderRelation.class);
    final List<AccountHolderRelation> accountHolderRelationByAccountHolderID =
        accountHolderRelationTestHelper.getAccountHolderRelationByAccountHolderID(
            request.payload().getAccountHolderID(), accountHolderRelation.getIfiID());

    accountHolderRelationHandler.on(request2.payload(), request2);

    //verify The results
    final AccountHolderRelation accountHolderRelation2 = captureResponsePayload(request2, AccountHolderRelation.class);
    final List<AccountHolderRelation> accountHolderRelationByAccountHolderID2 =
        accountHolderRelationTestHelper.getAccountHolderRelationByAccountHolderID(
            request.payload().getAccountHolderID(), accountHolderRelation.getIfiID());

    assertNotNull(accountHolderRelationByAccountHolderID);
    assertEquals(1, accountHolderRelationByAccountHolderID.size());
    assertThat(accountHolderRelationByAccountHolderID.get(0)).isEqualToIgnoringGivenFields(accountHolderRelation, "updatedAt", "createdAt");
  }

  @Test
  public void create_AccountHolderNotPresent_throwsException() {
    // Setup
    final Request<AddAccountHolderRelationRequest> request =
        accountHolderRelationTestHelper.addDefaultAccountHolderRelationRequest();

    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    // Verify the results
    final AccountServiceException exception = captureExceptionThrown(request, AccountServiceException.class);

    //TODO: Fix in accountHolderrelation DAO to return ACCOUNT HOLDER not found exception
    assertNotNull(exception);
    assertEquals(AccountErrorCode.ACCOUNT_HOLDER_NOT_FOUND.name(), exception.getErrorCode());
    assertEquals(String.format("Account Holder not Found for id: %s", request.payload().getAccountHolderID()), exception.getMessage());
  }
  
  @Test
  public void getByID_validParams_success() {
    // Setup
    accountHolderRelationTestHelper.insertDefaultAccountHolders();
    final AccountHolderRelation accountHolderRelation = accountHolderRelationTestHelper.addDefaultAccountHolderRelation();
    final Request<GetAccountHolderRelationByIDRequest> request =
        accountHolderRelationTestHelper.getAccountHolderRelationByIDRequest(accountHolderRelation.getId(), accountHolderRelation.getAccountHolderID(), accountHolderRelation.getIfiID());


    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    // Verify the results
    final AccountHolderRelation accountHolderRelationResponse = captureResponsePayload(request, AccountHolderRelation.class);

    assertNotNull(accountHolderRelation);
    assertThat(accountHolderRelation).isEqualToIgnoringGivenFields(accountHolderRelationResponse, "updatedAt", "createdAt");
  }

  @Test
  public void getByID_inValidParams_success() {
    // Setup
    accountHolderRelationTestHelper.insertDefaultAccountHolders();
    final Request<GetAccountHolderRelationByIDRequest> request =
        accountHolderRelationTestHelper.getAccountHolderRelationByIDRequest(UUID.randomUUID().toString(), ACCOUNT_HOLDER_ID_2.getValue(), IfiID.IFI_ID_2.getValue());

    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    // Verify the results
    final AccountServiceException exception = captureExceptionThrown(request, AccountServiceException.class);

    assertNotNull(exception);
    assertEquals(exception.getErrorCode(), AccountErrorCode.ACCOUNT_HOLDER_RELATION_NOT_FOUND.name());
  }

  @Test
  public void getListByAccountHolderID_validParams_success() {
    // Setup
    accountHolderRelationTestHelper.insertDefaultAccountHolders();
    final AccountHolderRelation accountHolderRelation = accountHolderRelationTestHelper.addDefaultAccountHolderRelation();
    final Request<GetAccountHolderRelationForAccountHolderRequest>
        request =
            accountHolderRelationTestHelper.getAccountHolderRelationByAHIDRequest(
                accountHolderRelation.getAccountHolderID(), accountHolderRelation.getIfiID());

    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    // Verify the results
    final GetAccountHolderRelationListResponse accountHolderRelationListResponse= captureResponsePayload(request, GetAccountHolderRelationListResponse.class);

    assertNotNull(accountHolderRelationListResponse);
    assertEquals(1, accountHolderRelationListResponse.getRelationships().size());
    assertThat(accountHolderRelationListResponse.getRelationships().get(0)).isEqualToIgnoringGivenFields(accountHolderRelation, "updatedAt", "createdAt");
  }

  @Test
  public void update_validParams_success() {
    // Setup
    accountHolderRelationTestHelper.insertDefaultAccountHolders();
    final AccountHolderRelation accountHolderRelation = accountHolderRelationTestHelper.addDefaultAccountHolderRelation();
    final Request<UpdateAccountHolderRelationRequest> request = accountHolderRelationTestHelper.updateRelationshipTypeRequest(accountHolderRelation.getId(), UPDATED_NAME.getValue());

    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    // Verify the results
    final AccountHolderRelation responsePayload = captureResponsePayload(request, AccountHolderRelation.class);
    final AccountHolderRelation updatedAccountHolderRelation = accountHolderRelationTestHelper.getAccountHolderRelationByID(accountHolderRelation.getId(), accountHolderRelation.getAccountHolderID(), accountHolderRelation.getIfiID());

    assertEquals(UPDATED_NAME.getValue(), updatedAccountHolderRelation.getRelationshipType());
    assertEquals(UPDATED_NAME.getValue(), responsePayload.getRelationshipType());
    accountHolderRelationTestHelper.assertEqualsWithDefault(updatedAccountHolderRelation, "updatedAt", "createdAt", "id", "status", "relationshipType");
  }

  @Test
  public void update_invalidRelationID_throwsRelationNotFoundException() {
    // Setup
    accountHolderRelationTestHelper.insertDefaultAccountHolders();
    final AccountHolderRelation accountHolderRelation = accountHolderRelationTestHelper.addDefaultAccountHolderRelation();
    final Request<UpdateAccountHolderRelationRequest> request = accountHolderRelationTestHelper.updateRelationshipTypeRequest(GenericID.INVALID_ID.getValue(), UPDATED_NAME.getValue());

    // Run the test
    accountHolderRelationHandler.on(request.payload(), request);

    // Verify the results
    final AccountServiceException exception = captureExceptionThrown(request, AccountServiceException.class);

    assertNotNull(exception);
    assertEquals(exception.getErrorCode(), AccountErrorCode.ACCOUNT_HOLDER_RELATION_NOT_FOUND.name());
  }

}
