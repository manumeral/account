package in.zeta.oms.account.accountHolder.whitelistedAccountHolderAttributes;


import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.AddWhitelistedAccountHolderAttributeRequest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.GetWhitelistedAccountHolderAttributesRequest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.UpdateWhitelistedAccountHolderAttributesRequest;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributes;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributesDAO;
import in.zeta.oms.account.constant.GenericName;
import in.zeta.oms.account.constant.IfiID;
import lombok.NonNull;
import olympus.message.types.Request;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

@Singleton
public class WhitelistedAccountHolderAttributesHelper extends AccountServiceBaseTest {

  public static final String ACCOUNT_HOLDER_STATUS_PATH = "$.status";
  public static final String ACCOUNT_HOLDER_VECTORS_STATUS_PATH = "$.vectors[*].status";
  private final WhitelistedAccountHolderAttributesDAO whitelistedAccountHolderAttributesDAO;

  @Inject
  public WhitelistedAccountHolderAttributesHelper(WhitelistedAccountHolderAttributesDAO whitelistedAccountHolderAttributesDAO) {
    this.whitelistedAccountHolderAttributesDAO = whitelistedAccountHolderAttributesDAO;
  }


  Request<AddWhitelistedAccountHolderAttributeRequest> getDefaultAddWhitelistedAccountHolderAttributesRequest() {
    WhitelistedAccountHolderAttributes defaultWhitelistedAccountHolderAttributes = getDefaultWhitelistedAttributes();
    return AddWhitelistedAccountHolderAttributeRequest.builder()
        .ifiID(defaultWhitelistedAccountHolderAttributes.getIfiID())
        .attributeName(defaultWhitelistedAccountHolderAttributes.getAttributeName())
        .attributePath(defaultWhitelistedAccountHolderAttributes.getAttributePath())
        .build();
  }

  Request<GetWhitelistedAccountHolderAttributesRequest> getDefaultGetWhitelistedAccountHolderAttributesRequest() {
    WhitelistedAccountHolderAttributes defaultWhitelistedAccountHolderAttributes = getDefaultWhitelistedAttributes();
    return GetWhitelistedAccountHolderAttributesRequest.builder()
        .ifiID(defaultWhitelistedAccountHolderAttributes.getIfiID())
        .attributeName(defaultWhitelistedAccountHolderAttributes.getAttributeName())
        .build();
  }

  Request<UpdateWhitelistedAccountHolderAttributesRequest> getDefaultUpdateWhitelistedAccountHolderAttributesRequest() {
    WhitelistedAccountHolderAttributes defaultWhitelistedAccountHolderAttributes = getDefaultWhitelistedAttributes();
    return UpdateWhitelistedAccountHolderAttributesRequest.builder()
        .ifiID(defaultWhitelistedAccountHolderAttributes.getIfiID())
        .attributeName(defaultWhitelistedAccountHolderAttributes.getAttributeName())
        .attributePath(ACCOUNT_HOLDER_VECTORS_STATUS_PATH)
        .build();
  }

  CompletionStage<List<WhitelistedAccountHolderAttributes>> getWhitelistedAccountHolderAttributesFromDB(@NonNull Long ifiID,
                                                                                                        @NonNull String attributeName) {
    return whitelistedAccountHolderAttributesDAO.get(ifiID)
        .thenApply(whitelistedAccountHolderAttributes -> whitelistedAccountHolderAttributes
            .stream()
            .filter(attribute -> attribute.getAttributeName().equalsIgnoreCase(attributeName))
            .collect(Collectors.toList()));
  }

  CompletionStage<Void> insertDefaultWhitelistedAccountHolderAttributesInDB() {
    return whitelistedAccountHolderAttributesDAO.insert(getDefaultWhitelistedAttributes());
  }

  WhitelistedAccountHolderAttributes getDefaultWhitelistedAttributes() {
    return WhitelistedAccountHolderAttributes.builder()
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .attributeName(GenericName.DEFAULT_NAME.getValue())
        .attributePath(ACCOUNT_HOLDER_STATUS_PATH)
        .build();
  }
}
