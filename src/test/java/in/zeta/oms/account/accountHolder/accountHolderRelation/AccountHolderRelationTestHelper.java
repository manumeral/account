package in.zeta.oms.account.accountHolder.accountHolderRelation;

import com.google.inject.Inject;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.GenericName;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import olympus.message.types.Request;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountHolderRelationTestHelper {
  public final AccountHolderRelationService accountHolderRelationService;
  public final AccountHolderTestHelper accountHolderTestHelper;

  @Inject
  public AccountHolderRelationTestHelper(AccountHolderRelationService accountHolderRelationService, AccountHolderTestHelper accountHolderTestHelper) {
    this.accountHolderRelationService = accountHolderRelationService;
    this.accountHolderTestHelper = accountHolderTestHelper;
  }

  public Request<AddAccountHolderRelationRequest> addDefaultAccountHolderRelationRequest() {
    return AddAccountHolderRelationRequest.builder()
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .relatedAccountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue())
        .relationshipType(GenericName.DEFAULT_NAME.getValue())
        .build();
  }

  public List<AccountHolderRelation> getAccountHolderRelationByAccountHolderID(String accountHolderID, long ifiID) {
    return accountHolderRelationService.getRelationsByAccountHolderID(accountHolderID, ifiID).toCompletableFuture().join();
  }

  public AccountHolderRelation getAccountHolderRelationByID(String id, String accountHolderID, long ifiID) {
    return accountHolderRelationService.get(id, accountHolderID, ifiID).toCompletableFuture().join();
  }

  public AccountHolderRelation addDefaultAccountHolderRelation() {
    AccountHolderRelation relation = AccountHolderRelation.builder()
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .relatedAccountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue())
        .relationshipType(GenericName.DEFAULT_NAME.getValue())
        .build();
    return accountHolderRelationService.createAccountHolderRelation(relation).toCompletableFuture().join();
  }

  public Request<GetAccountHolderRelationByIDRequest> getAccountHolderRelationByIDRequest(String id, String accountHolderID, long ifi) {
    return GetAccountHolderRelationByIDRequest.builder()
        .id(id)
        .ifiID(ifi)
        .accountHolderID(accountHolderID)
        .build();
  }

  public Request<GetAccountHolderRelationForAccountHolderRequest> getAccountHolderRelationByAHIDRequest(String accountHolderID, long ifi) {
    return GetAccountHolderRelationForAccountHolderRequest.builder()
        .accountHolderID(accountHolderID)
        .ifiID(ifi)
        .build();
  }

  public Request<UpdateAccountHolderRelationRequest> updateRelationshipTypeRequest(String id, String updatedRelationshipType) {
    return UpdateAccountHolderRelationRequest.builder()
        .id(id)
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .relatedAccountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue())
        .relationshipType(updatedRelationshipType)
        .build();
  }

  public AccountHolderRelation getDefaultRelation() {
    return AccountHolderRelation.builder()
        .accountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue())
        .ifiID(IfiID.IFI_ID_DEFAULT.getValue())
        .relatedAccountHolderID(AccountHolderID.ACCOUNT_HOLDER_ID_2.getValue())
        .relationshipType(GenericName.DEFAULT_NAME.getValue())
        .build();
  }

  public void assertEqualsWithDefault(AccountHolderRelation accountHolderRelation, String... propertiesToIgnore){
    assertThat(accountHolderRelation).isEqualToIgnoringGivenFields(getDefaultRelation(), propertiesToIgnore);
  }

  public void insertDefaultAccountHolders() {
    accountHolderTestHelper.insertDefaultAccountHolder();
    accountHolderTestHelper.insertDefaultAccountHolder2();
  }

}
