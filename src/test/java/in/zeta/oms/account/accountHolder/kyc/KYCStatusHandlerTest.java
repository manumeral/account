package in.zeta.oms.account.accountHolder.kyc;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorHandler;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorService;
import in.zeta.oms.account.api.AccountHolderHandler;
import in.zeta.oms.account.api.exception.AccountHolderNotFoundException;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.helper.AccountHolderTestHelper;
import olympus.message.types.Request;
import org.junit.Test;

import java.util.UUID;

import static in.zeta.oms.account.accountHolder.kyc.KYCStatusTestHelper.DEFAULT_PAN;
import static in.zeta.oms.account.constant.AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class KYCStatusHandlerTest extends AccountServiceBaseTest {
  private final KYCStatusTestHelper KYCStatusTestHelper;
  private final KYCStatusHandler KYCStatusHandler;
  private final AccountHolderTestHelper accountHolderTestHelper;
  private final AccountHolderVectorService accountHolderVectorService;

  public KYCStatusHandlerTest() {
    this.KYCStatusTestHelper = getInstance(KYCStatusTestHelper.class);
    this.KYCStatusHandler = getInstance(KYCStatusHandler.class);
    this.accountHolderTestHelper = getInstance(AccountHolderTestHelper.class);
    this.accountHolderVectorService = getInstance(AccountHolderVectorService.class);
  }

  @Test
  public void create_validParams_success() {
    // Setup
    accountHolderTestHelper.insertDefaultAccountHolder();
    final Request<AddKycStatusRequest> request =
        KYCStatusTestHelper.addDefaultEffectiveKycStatusRequest();

    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    // Verify the results
    final KYCStatus KYCStatus =
        captureResponsePayload(request, KYCStatus.class);

    final KYCStatus KYCStatusByAccountHolderID =
        KYCStatusTestHelper.get(
            request.payload().getAccountHolderID(), KYCStatus.getIfiID());

    assertNotNull(KYCStatusByAccountHolderID);
    assertThat(KYCStatusByAccountHolderID).isEqualToIgnoringGivenFields(KYCStatus, "updateTime");
  }


  @Test
  public void create_repeatedRequest_idempotentResponse() {
    // Setup
    accountHolderTestHelper.insertDefaultAccountHolder();
    final Request<AddKycStatusRequest> request =
        KYCStatusTestHelper.addDefaultEffectiveKycStatusRequest();
    final Request<AddKycStatusRequest> request2 =
        KYCStatusTestHelper.addDefaultEffectiveKycStatusRequest();

    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    final KYCStatus KYCStatus =
        captureResponsePayload(request, KYCStatus.class);
    final KYCStatus KYCStatus1 =
        KYCStatusTestHelper.get(
            request.payload().getAccountHolderID(), KYCStatus.getIfiID());

    KYCStatusHandler.on(request2.payload(), request2);

    //verify The results
    final KYCStatus KYCStatus2 = captureResponsePayload(request2, KYCStatus.class);
    final KYCStatus getKYCStatus =
        KYCStatusTestHelper.get(
            request.payload().getAccountHolderID(), KYCStatus.getIfiID());

    assertNotNull(KYCStatus);
    assertThat(KYCStatus1).isEqualToIgnoringGivenFields(KYCStatus2, "updateTime");
  }

  @Test
  public void create_AccountHolderNotPresent_throwsException() {
    // Setup
    final Request<AddKycStatusRequest> request =
        KYCStatusTestHelper.addDefaultEffectiveKycStatusRequest();

    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    // Verify the results
    final AccountServiceException exception = captureExceptionThrown(request, AccountHolderNotFoundException.class);

    assertNotNull(exception);
    assertEquals(AccountErrorCode.ACCOUNT_HOLDER_NOT_FOUND.name(), exception.getErrorCode());
    assertEquals(String.format("Account Holder not Found for id: %s", ACCOUNT_HOLDER_ID_DEFAULT.getValue()), exception.getMessage());
  }
  
  @Test
  public void getByID_validParams_success() {
    // Setup
    accountHolderTestHelper.insertDefaultAccountHolder();
    final KYCStatus KYCStatus = KYCStatusTestHelper.addDefaultEffectiveKYCStatus();
    final Request<GetKycStatusRequest> request =
        KYCStatusTestHelper.getEffectiveKycStatusRequestRequest(KYCStatus.getAccountHolderID(), KYCStatus.getIfiID());


    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    // Verify the results
    final KYCStatus KYCStatusResponse = captureResponsePayload(request, KYCStatus.class);

    assertNotNull(KYCStatus);
    assertThat(KYCStatus).isEqualToIgnoringGivenFields(KYCStatusResponse, "updateTime");
  }

  @Test
  public void getByID_inValidParams_success() {
    // Setup
    accountHolderTestHelper.insertDefaultAccountHolder();
    final Request<GetKycStatusRequest> request =
        KYCStatusTestHelper.getEffectiveKycStatusRequestRequest(UUID.randomUUID().toString(), IfiID.IFI_ID_2.getValue());
    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    // Verify the results
    final AccountServiceException exception = captureExceptionThrown(request, AccountServiceException.class);

    assertNotNull(exception);
    assertEquals(exception.getErrorCode(), AccountErrorCode.KYC_STATUS_NOT_FOUND.name());
  }

  @Test
  public void update_validParams_success() {
    // Setup
    accountHolderTestHelper.insertDefaultAccountHolder();
    final KYCStatus KYCStatus = KYCStatusTestHelper.addDefaultEffectiveKYCStatus();
    final Request<UpdateKycStatusRequest> request = KYCStatusTestHelper.updateEffectiveKycStatusRequest(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue());

    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    // Verify the results
    final KYCStatus responsePayload = captureResponsePayload(request, KYCStatus.class);
    final KYCStatus updatedKYCStatus = KYCStatusTestHelper.get(KYCStatus.getAccountHolderID(), KYCStatus.getIfiID());

    assertEquals(KYCStatusConstant.AADHAAR_OTP_KYC_WITH_PAN.toString(), updatedKYCStatus.getKycStatus());
    assertEquals(KYCStatusConstant.KYC_EXPIRED_WITH_PAN.toString(), responsePayload.getKycStatusPostExpiry());
    KYCStatusTestHelper.assertEqualsWithDefault(updatedKYCStatus, "updateTime", "kycStatus", "kycStatusPostExpiry");
  }

  @Test
  public void update_invalidKYC_throwsException() {
    // Setup
    accountHolderTestHelper.insertDefaultAccountHolder();
    KYCStatusTestHelper.addDefaultEffectiveKYCStatus();
    final Request<UpdateKycStatusRequest> request = KYCStatusTestHelper.updateEffectiveKycStatusRequest(AccountHolderID.ACCOUNT_HOLDER_ID_INVALID.getValue());

    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    // Verify the results
    final AccountServiceException exception = captureExceptionThrown(request, AccountServiceException.class);

    assertNotNull(exception);
    assertEquals(AccountErrorCode.KYC_STATUS_NOT_FOUND.name(), exception.getErrorCode());
    assertEquals(String.format("Effective KYC status not found for account holder id %s", AccountHolderID.ACCOUNT_HOLDER_ID_INVALID.getValue()), exception.getMessage());

  }

  @Test
  public void update_KYC_with_Vectors() {
    accountHolderTestHelper.insertDefaultAccountHolder();
    final KYCStatus KYCStatus = KYCStatusTestHelper.addDefaultEffectiveKYCStatus();
    final Request<UpdateKycStatusRequest> request = KYCStatusTestHelper.updateEffectiveKycStatusRequestWithAttrs(AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue());
    // Run the test
    KYCStatusHandler.on(request.payload(), request);

    // Verify the results
    final KYCStatus responsePayload = captureResponsePayload(request, KYCStatus.class);

    AccountHolderVector accountHolderVector = accountHolderVectorService.getByVector("pan", DEFAULT_PAN, KYCStatus.getIfiID()).toCompletableFuture().join();
    assertNotNull(accountHolderVector);
    assertEquals(accountHolderVector.getValue(), DEFAULT_PAN);
  }

}
