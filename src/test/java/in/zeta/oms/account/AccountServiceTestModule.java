package in.zeta.oms.account;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.matcher.Matchers;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.gson.ZetaGsonBuilder;
import in.zeta.oms.account.server.pubsub.EventPublishInterceptor;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import javax.inject.Named;
import javax.inject.Singleton;

public class AccountServiceTestModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(Gson.class).toInstance(new ZetaGsonBuilder().build());
    EventPublishInterceptor eventPublishInterceptor = new EventPublishInterceptor();
    requestInjection(eventPublishInterceptor);
    bindInterceptor(Matchers.any(), Matchers.annotatedWith(PublishEvent.class),
        eventPublishInterceptor);
  }

  @Provides
  @Singleton
  public SecureRandomGenerator secureRandomGenerator(@Named("rng.algorithm") final String rngAlgorithm) {
    return new SecureRandomGenerator(rngAlgorithm);
  }
}
