package in.zeta.oms.account.dao;

import static in.zeta.athenacommons.util.AssertHelper.OptionalAssertExtended.extendedAssertThat;
import static in.zeta.oms.account.constant.AppConstants.TAG_OBJECT_TYPE_ACCOUNT;
import static in.zeta.oms.account.constant.GenericName.DEFAULT_NAME;
import static in.zeta.oms.account.constant.IfiID.IFI_ID_DEFAULT;
import static in.zeta.oms.account.constant.ProductFamilyID.PRODUCT_FAMILY_ID_DEFAULT;
import static in.zeta.oms.account.constant.ProductID.PRODUCT_ID_DEFAULT;
import static in.zeta.oms.account.constant.ProgramID.PROGRAM_ID_DEFAULT;
import static in.zeta.oms.account.helper.AccountTestHelper.getDefaultAccount;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import in.zeta.oms.account.AccountServiceBaseTest;
import in.zeta.oms.account.account.AccountDAO;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.api.model.AccountStatus;
import in.zeta.oms.account.constant.AccountHolderID;
import in.zeta.oms.account.constant.AccountID;
import in.zeta.oms.account.constant.AccountProviderID;
import in.zeta.oms.account.constant.IfiID;
import in.zeta.oms.account.constant.RequestID;
import in.zeta.oms.account.helper.AccountTestHelper;
import in.zeta.tags.TagService;
import in.zeta.tags.model.Tag;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletionException;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Optional;
import java.util.concurrent.CompletionException;

import static in.zeta.athenacommons.util.AssertHelper.OptionalAssertExtended.extendedAssertThat;
import static in.zeta.oms.account.helper.AccountTestHelper.getDefaultAccount;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountDAOTest extends AccountServiceBaseTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  private final AccountTestHelper accountTestHelper;
  private final TagService tagService;
  private final AccountDAO accountDAO;

  public AccountDAOTest() {
    this.accountTestHelper = getInstance(AccountTestHelper.class);
    this.tagService = getInstance(TagService.class);
    this.accountDAO = getInstance(AccountDAO.class);
  }

  @Test
  public void testAdd_validArgs_success() {
    // Setup
    final Account expectedResult = getDefaultAccount();

    // Run the test 
    final Account result = accountTestHelper.insertDefaultAccount();

    // Verify the results
    assertThat(result).isEqualToComparingFieldByFieldRecursively(expectedResult);
  }

  @Test
  public void testAdd_duplicate_throwsException() {
    // Setup
    final Account expectedResult = getDefaultAccount();
    accountTestHelper.insertDefaultAccount();

    // Expected exception
    expectedException.expect(CompletionException.class);
    expectedException.expectCause(IsInstanceOf.instanceOf(AccountServiceException.class));
    expectedException.expectMessage(String.format("Failed to add account to account holder id %s",
                                                  AccountHolderID.ACCOUNT_HOLDER_ID_DEFAULT.getValue()));

    // Run the test
    accountTestHelper.insertDefaultAccount();
  }

  @Test
  public void testGet_validArgs_success() {
    // Setup
    final String accountID = AccountID.ACCOUNT_ID_DEFAULT.getValue();
    final Long ifiID = IfiID.IFI_ID_DEFAULT.getValue();
    final Account expectedResult = getDefaultAccount();

    // Run the test
    accountTestHelper.insertDefaultAccount();
    final Optional<Account> result = accountTestHelper.getAccountBy(accountID, ifiID);

    // Verify the results
    extendedAssertThat(result).isEqualToIgnoringGivenFields(expectedResult, "programIDs", "createdAt", "updatedAt", "vectors", "accessors", "relationships");
  }

  @Test
  public void testGet_noEntry_returnsEmptyOptional() {
    // Setup
    final String accountID = AccountID.ACCOUNT_ID_2.getValue();
    final Long ifiID = IfiID.IFI_ID_DEFAULT.getValue();
    final Account expectedResult = getDefaultAccount();

    // Run the test
    accountTestHelper.insertDefaultAccount();
    final Optional<Account> result = accountTestHelper.getAccountBy(accountID, ifiID);

    // Verify the results
    assertThat(result).isEmpty();
  }

  @Test
  public void testAddInBulk_success() {
    accountTestHelper.insertAccounts();
  }

  @Test
  public void testGetAccountList_vboFiltering() {
    Tag vbo1 = Tag.builder().type("vbo-id").value("1").build();
    Tag vbo2 = Tag.builder().type("vbo-id").value("2").build();

    String ah1 = UUID.randomUUID().toString();
    String ah2 = UUID.randomUUID().toString();

    insertAccountHolder(ah1, vbo1);
    insertAccountHolder(ah2, vbo1);
    insertAccountHolder(ah2, vbo1);
    insertAccountHolder(ah2, vbo1);
    insertAccountHolder(ah1, vbo2);
    insertAccountHolder(ah2, vbo2);
    insertAccountHolder(ah2, vbo2);

    List<Account> accounts = getAccounts(ah1, vbo1);
    assertThat(accounts).hasSize(1);

    accounts = getAccounts(ah2, vbo1);
    assertThat(accounts).hasSize(3);

    accounts = getAccounts(ah1, vbo2);
    assertThat(accounts).hasSize(1);

    accounts = getAccounts(ah2, vbo2);
    assertThat(accounts).hasSize(2);
  }

  private List<Account> getAccounts(String ahID, Tag tag) {
    return accountDAO.getAccounts(IFI_ID_DEFAULT.getValue(), ahID, null, null, null, null, 1L, 20L, tag)
        .toCompletableFuture()
        .join();
  }

  private Account insertAccountHolder(String accountHolderID, Tag tag) {
    SecureRandom random = new SecureRandom();
    Account account = Account.builder()
        .id(UUID.randomUUID().toString())
        .ifiID(IFI_ID_DEFAULT.getValue())
        .productFamilyID(PRODUCT_FAMILY_ID_DEFAULT.getValue())
        .ownerAccountHolderID(accountHolderID)
        .productID(PRODUCT_ID_DEFAULT.getValue())
        .ledgerID(random.nextLong())
        .programIDs(singletonList(PROGRAM_ID_DEFAULT.getValue()))
        .attributes(new HashMap<>())
        .accountProviderID(AccountProviderID.ACCOUNT_PROVIDER_ID_DEFAULT.getValue())
        .name(DEFAULT_NAME.getValue())
        .status(AccountStatus.ENABLED.name())
        .build();
    accountDAO.insert(account, UUID.randomUUID().toString())
        .thenApply(accountID -> account.toBuilder().id(accountID).build())
        .toCompletableFuture().join();
    tagService.assignTags(IFI_ID_DEFAULT.getValue(), TAG_OBJECT_TYPE_ACCOUNT, account.getId(), Collections.singletonList(tag)).toCompletableFuture().join();
    return account;
  }

}
