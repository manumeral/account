package in.zeta.oms.account.account;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import in.zeta.athenacommons.util.FieldQuery;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.Money;
import in.zeta.commons.concurrency.CompletableFutures;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.util.ZetaUrlConstants;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributes;
import in.zeta.oms.account.account.WhitelistedAccountHolderAttributes.WhitelistedAccountHolderAttributesService;
import in.zeta.oms.account.account.accessor.AccessorService;
import in.zeta.oms.account.account.ledger.GetAccountBalanceResponse;
import in.zeta.oms.account.account.relation.AccountRelationService;
import in.zeta.oms.account.account.vector.AccountVectorService;
import in.zeta.oms.account.accountHolder.kyc.KYCStatus;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusService;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorService;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.exception.AccountCreationException;
import in.zeta.oms.account.api.exception.AccountNotFoundException;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.api.model.Accessor;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.model.AccountInfo;
import in.zeta.oms.account.api.model.AccountRelation;
import in.zeta.oms.account.api.model.AccountVector;
import in.zeta.oms.account.athenamanager.accountProvider.AccountProviderService;
import in.zeta.oms.account.constant.AccountConstant;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.oms.account.service.AccountHolderService;
import in.zeta.oms.account.service.AthenaManagerService;
import in.zeta.oms.account.service.BaseSchemaIssuerService;
import in.zeta.oms.account.service.CloudCardService;
import in.zeta.oms.account.service.IssuancePolicyService;
import in.zeta.oms.account.service.LedgerService;
import in.zeta.oms.athenamanager.api.model.EntityType;
import in.zeta.oms.athenamanager.api.model.PolicyEntityType;
import in.zeta.oms.athenamanager.api.model.Product;
import in.zeta.oms.athenamanager.api.model.ProductFamily;
import in.zeta.oms.ledger.api.CreateLedgerRequestPayloadV5;
import in.zeta.oms.ledger.api.LedgerState;
import in.zeta.oms.ledger.api.policy.IfiProductType;
import in.zeta.spectra.capture.SpectraLogger;
import in.zeta.tags.TagService;
import in.zeta.tags.model.Tag;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static in.zeta.commons.concurrency.CompletableFutures.allOf;
import static in.zeta.commons.concurrency.CompletableFutures.allResultsOf;
import static in.zeta.oms.account.constant.AccountConstant.ACCOUNT_ACCOUNT_HOLDER_ID;
import static in.zeta.oms.account.constant.AccountConstant.ACCOUNT_EXTERNAL_ID;
import static in.zeta.oms.account.constant.AccountConstant.ACCOUNT_PRODUCT_FAMILY_ID;
import static in.zeta.oms.account.constant.AccountConstant.ACCOUNT_PRODUCT_ID;
import static in.zeta.oms.account.constant.AccountConstant.ACCOUNT_PROGRAM_ID;
import static in.zeta.oms.account.constant.AccountConstant.ATHENA_ACCOUNT_PROVIDER_ID;
import static in.zeta.oms.account.constant.AccountConstant.ZETA_CARD_PROGRAM_IDS;
import static in.zeta.oms.account.constant.AccountConstant.ZETA_IFI_PRODUCT_TYPE;
import static in.zeta.oms.account.constant.AccountConstant.ZETA_PRODUCT_TYPE;
import static in.zeta.oms.account.constant.AppConstants.TAG_OBJECT_TYPE_ACCOUNT;
import static java.util.Collections.singletonList;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;


@EagerSingleton
public class AccountService {

  private static final SpectraLogger logger = OlympusSpectra.getLogger(AccountService.class);
  private static final String MCC_VOUCHER_RECIPIENT = "MCC_VOUCHER_RECIPIENT";
  private static final String GENERIC_PRODUCT_TYPE_VALUE = "Generic";
  private static final String LEDGER_ZETA_USER_ID = "zeta.user-id";
  private static final String GPR = "GPR";
  private static final String KYC_TYPE = "kycType";
  private static final String PAN = "pan";
  private static final String IS_ATM_ENABLED = "isAtmEnabled";
  public static final String LEDGER_PRODUCT_TYPE_KEY = "ledgerProductType";
  public static final String PPI_TYPE = "OPEN";

  private final AccountDAO accountDAO;
  private final AccountTransactionDAO accountTransactionDAO;
  private final WhitelistedAccountHolderAttributesService whitelistedAccountHolderAttributesService;
  private final AccessorService accessorService;
  private final AccountVectorService accountVectorService;
  private final AccountRelationService accountRelationService;
  private final LedgerService ledgerService;
  private final CloudCardService cloudCardService;
  private final AccountHolderService accountHolderService;
  private final IssuancePolicyService issuancePolicyService;
  private final SecureRandomGenerator secureRandomGenerator;
  private final AthenaManagerService athenaManagerService;
  private final AccountProviderService accountProviderService;
  private final AccountHolderVectorService accountHolderVectorService;
  private final BaseSchemaIssuerService baseSchemaIssuerService;
  private final KYCStatusService kycStatusService;
  private final TagService tagService;

  @Inject
  public AccountService(AccountDAO accountDAO,
                        LedgerService ledgerService,
                        CloudCardService cloudCardService,
                        AccountHolderService accountHolderService,
                        IssuancePolicyService issuancePolicyService,
                        SecureRandomGenerator secureRandomGenerator,
                        AthenaManagerService athenaManagerService,
                        AccountProviderService accountProviderService,
                        AccountTransactionDAO accountTransactionDAO,
                        WhitelistedAccountHolderAttributesService whitelistedAccountHolderAttributesService,
                        AccessorService accessorService,
                        AccountVectorService accountVectorService,
                        AccountRelationService accountRelationService,
                        AccountHolderVectorService accountHolderVectorService,
                        BaseSchemaIssuerService baseSchemaIssuerService,
                        KYCStatusService kycStatusService,
                        TagService tagService) {
    this.accountDAO = accountDAO;
    this.ledgerService = ledgerService;
    this.kycStatusService = kycStatusService;
    this.accountHolderService = accountHolderService;
    this.cloudCardService = cloudCardService;
    this.issuancePolicyService = issuancePolicyService;
    this.secureRandomGenerator = secureRandomGenerator;
    this.athenaManagerService = athenaManagerService;
    this.accountProviderService = accountProviderService;
    this.accountTransactionDAO = accountTransactionDAO;
    this.whitelistedAccountHolderAttributesService = whitelistedAccountHolderAttributesService;
    this.accountRelationService = accountRelationService;
    this.accessorService = accessorService;
    this.accountVectorService = accountVectorService;
    this.accountHolderVectorService = accountHolderVectorService;
    this.baseSchemaIssuerService = baseSchemaIssuerService;
    this.tagService = tagService;
  }

  //Assuming TransactionPolicy has been created earlier
  @PublishEvent(topicName = PubSubTopics.CREATE_ACCOUNT_EVENT_TOPIC)
  public CompletionStage<Account> createAccount(Account account,
                                                String requestID,
                                                ExternalIDType externalIDType,
                                                String externalAccountHolderID) {
    //TODO : Fill in default values for fields based on customSchema from provider
    final String accountID = secureRandomGenerator.generateUUID();
    final Account.Builder accountBuilder = account.toBuilder()
        .id(accountID);
    final long ifiID = account.getIfiID();
    final String ownerAccountHolderID = account.getOwnerAccountHolderID();

    return getAccountWithRequestID(account, requestID)
        .thenCompose(accountOptional -> {
          if (accountOptional.isPresent()) {
            if (matchesWithExistingAccount(account, accountOptional.get())) {
              return completedFuture(accountOptional.get());
            }
            throw AccountCreationException.duplicateRequestException("An account is already created with this request");
          }

          List<AccountVector> vectors = assignIDsForVectors(account.getVectors(), accountID);
          List<AccountRelation> relationships = assignIDsForRelationships(account.getRelationships(), accountID);
          List<Accessor> accessors = assignIDsForAccessors(account.getAccessors(), accountID);
          accountBuilder
              .clearVectors()
              .clearAccessors()
              .clearRelationships()
              .vectors(vectors)
              .relationships(relationships)
              .accessors(accessors);

          final CompletionStage<AccountHolder> accountHolderFuture = getAccountHolder(
              ifiID,
              externalIDType,
              externalAccountHolderID,
              ownerAccountHolderID);


            return accountHolderFuture
              .thenCompose(accountHolder -> {
                accountBuilder.ownerAccountHolderID(accountHolder.getId());
                return applyPolicies(accountBuilder.build(), accountHolder);
              }).thenCompose(ignore -> accountHolderFuture)
                // Vectors can only contain phone, email and zeta.user-id
                .thenCompose(accountHolder -> {
                    if (externalIDType != null) {
                        return accountHolderVectorService.getByVector(externalIDType.getAttributeKey(), externalAccountHolderID, ifiID);
                    } else {
                        AccountHolderVector accountHolderVector = accountHolder.getVectors().stream()
                            .filter(vector -> vector.getType().equals(ZetaUrlConstants.USER_ID_PREFIX))
                            .findFirst()
                            .orElse(AccountHolderVector.builder().type(null).value(null).build());
                        return CompletableFuture.completedFuture(accountHolderVector);
                    }

                })
              .thenCompose(accountHolderVector -> createLedger(accountBuilder.build(), accountHolderVector, accountHolderFuture.toCompletableFuture().join()))
              .thenApply(accountBuilder::ledgerID)
              .thenCompose(ignored -> {
                final Account finalAccount = accountBuilder.build();
                return accountTransactionDAO.insertAccount(finalAccount, requestID);
              })
              .thenCompose(this::assignTags);
        });

  }

  public CompletionStage<CloseAccountResponse> closeAccount(CloseAccountRequest payload) {
      CompletableFuture<Account> accountFuture = getAccount(payload.getIfiID(), payload.getAccountID()).toCompletableFuture();
      CloseAccountResponse.Builder responseBldr = CloseAccountResponse.builder()
                                                      .outboundAccountID(payload.getOutboundAccountID());
      return accountFuture.thenCompose(account -> {
                  if (StringUtils.equals(account.getStatus(), Status.CLOSED.name()) || StringUtils.isBlank(payload.getOutboundAccountID())) {
                      return completedFuture(null);
                  }
                  return ledgerService.getLedgerInfo(account.getLedgerID())
                              .thenCompose(accountBalance -> {
                                  Money amountToTransfer = new Money(accountBalance.getCurrency(), accountBalance.getBalance());
                                  if (amountToTransfer.getAmount() == 0L) {
                                      return completedFuture(null);
                                  }
                                  responseBldr.transferredAmout(amountToTransfer);
                                  return cloudCardService.transferAmount(payload.getAccountID(), payload.getOutboundAccountID(), "Account Closure Transfer", amountToTransfer,
                                          payload.getIfiID(), payload.getRequestID(), payload.getAttributes(), AccountConstant.ACCOUNT_CLOSE_VOUCHER_CODE);
                              });
              })
              .thenCompose(ignore -> updateStatus(accountFuture.join(), Status.CLOSED.name()))
              .thenApply(account -> responseBldr.account(account).build());
  }

  private CompletionStage<Account> assignTags(Account account) {
    return tagService.assignTags(account.getIfiID(), TAG_OBJECT_TYPE_ACCOUNT, account.getId(), account.getTags())
        .thenApply(ignored -> account);
  }

  private CompletionStage<Optional<Account>> getAccountWithRequestID(Account account, String requestID) {
    return accountDAO
        .getAccountWithRequestID(requestID, account.getAccountProviderID(), account.getIfiID())
        .thenCompose(
            accountOptional -> {
              if (accountOptional.isPresent()) {
                return populateAccountView(accountOptional.get()).thenApply(Optional::of);
              }
              return completedFuture(Optional.empty());
            });
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_EVENT)
  public CompletionStage<Account> updateNameAndAttribute(UpdateAccountRequest payload) {
    final Account account = Account.builder()
        .id(payload.getAccountID())
        .name(payload.getName())
        .attributes(payload.getAttributes())
        .build();
    return update(account);
  }

  public CompletionStage<Account> getAccount(Long ifiID, String id) {
    return accountDAO.get(id, ifiID)
        .thenApply(accountOptional -> accountOptional.orElseThrow(() -> new AccountNotFoundException(id)))
        .thenCompose(this::populateAccountView);
  }

  CompletionStage<GetAccountBalanceResponse> getAccountBalance(Long ifiID, String id) {
    return getAccount(ifiID, id)
        .thenCompose(account -> ledgerService.getLedgerInfo(account.getLedgerID()));
  }

  private CompletionStage<Account> populateAccountView(Account account) {
    CompletableFuture<List<Accessor>> accessorsFuture = accessorService.getAccessors(account.getId(), account.getIfiID()).toCompletableFuture();
    CompletableFuture<List<AccountVector>> accountVectorsFuture = accountVectorService.getVectors(account.getId(), account.getIfiID()).toCompletableFuture();
    CompletableFuture<List<AccountRelation>> accountRelationshipsFuture = accountRelationService.getRelationships(account.getId(), account.getIfiID()).toCompletableFuture();
    CompletableFuture<List<Tag>> tagsFuture = tagService.getTagsForObject(account.getIfiID(), TAG_OBJECT_TYPE_ACCOUNT, account.getId()).toCompletableFuture();
    return CompletableFuture.allOf(accessorsFuture, accountVectorsFuture, accountRelationshipsFuture, tagsFuture)
        .thenApply(ignore -> account.toBuilder()
              .clearAccessors()
              .clearVectors()
              .clearRelationships()
              .vectors(accountVectorsFuture.join())
              .accessors(accessorsFuture.join())
              .relationships(accountRelationshipsFuture.join())
              .tags(tagsFuture.join())
              .build());
  }

  public CompletionStage<List<Account>> getAccountsV2(GetAccountListRequestV2 payload) {
    Tag tag = null;
    if (payload.getTags() != null && !payload.getTags().isEmpty()) {
      // TODO: Filtering is currently supported only on one tag. Update when need arises.
      tag = payload.getTags().get(0);
    }
    return accountDAO.getAccounts(payload.getIfiID(),
        payload.getAccountHolderID(),
        payload.getAccountProviderID(),
        payload.getStatus(),
        payload.getProductID(),
        payload.getProductFamilyID(),
        payload.getPageNumber(),
        payload.getPageSize(),
        tag);
  }

  public CompletionStage<AccountInfo> getAccountInfo(Long ifiID, String id) {
    return getAccount(ifiID, id)
        .thenApply(AccountInfo::from)
        .thenCompose(accountInfo -> {
          CompletionStage<Product> productCompletionStage = athenaManagerService.getProduct(ifiID, accountInfo.getProductID());
          CompletionStage<ProductFamily> productFamilyCompletionStage = athenaManagerService.getProductFamily(ifiID, accountInfo.getProductFamilyID());
          CompletionStage<GetAccountBalanceResponse> accountBalanceCompletionStage = getAccountBalance(ifiID, accountInfo.getId());

          return CompletableFutures.allOf(productCompletionStage, productFamilyCompletionStage, accountBalanceCompletionStage)
              .thenApply(ignore -> accountInfo.from(
                  productCompletionStage.toCompletableFuture().join().getName(),
                  productFamilyCompletionStage.toCompletableFuture().join().getName(),
                  accountBalanceCompletionStage.toCompletableFuture().join().getBalance(),
                  accountBalanceCompletionStage.toCompletableFuture().join().getCurrency(),
                  accountBalanceCompletionStage.toCompletableFuture().join().getAccountingType()
                  ));
        });
  }

  public CompletionStage<Void> validate(Account account) {
      return allOf(accountProviderService.validate(account), baseSchemaIssuerService.validate(account,
          account.getIfiID(), EntityType.ACCOUNT.name()));
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_EVENT)
  public CompletionStage<Account> updateAccountOwner(UpdateAccountOwnerRequest payload) {
    Long ifiID = payload.getIfiID();
    String accountID = payload.getId();

    CompletableFuture<Account> accountFuture = getAccount(ifiID, accountID).toCompletableFuture();
    CompletableFuture<AccountHolder> accountHolderFuture =
        getAccountHolder(
                ifiID,
                payload.getExternalIDType(),
                payload.getExternalAccountHolderID(),
                payload.getAccountHolderID())
            .toCompletableFuture();
    final Map<String, String> attributes = new HashMap<>();
    return accountHolderFuture
        .thenCombine(accountFuture, (accountHolder, account) -> applyPolicies(account, accountHolder))
        .thenCompose(
            ignored -> {
              Account account = accountFuture.join();
              AccountHolder accountHolder = accountHolderFuture.join();
              if (null != account.getAttributes()) {
                attributes.putAll(account.getAttributes());
              }
              Optional<AccountHolderVector> accountHolderVectorOptional = accountHolder
                  .getVectors()
                  .stream()
                  .filter(v -> v.getType().equals(ZetaUrlConstants.USER_ID_PREFIX))
                  .findFirst();
              if (accountHolderVectorOptional.isPresent()) {
                  AccountHolderVector accountHolderVector = accountHolderVectorOptional.get();
                  attributes.put(LEDGER_ZETA_USER_ID, accountHolderVector.getValue());
                  attributes.put(ACCOUNT_EXTERNAL_ID, accountHolderVector.getValue());
                  attributes.put(ACCOUNT_ACCOUNT_HOLDER_ID, accountHolder.getId());
                }

              return ledgerService.setLedgerAttributes(
                  account.getLedgerID(),
                  attributes,
                  Collections.emptySet(),
                  "UPDATE_OWNER-"+account.getLedgerID()+"-"+System.currentTimeMillis());
            })
        .thenCompose(
            ignore ->
                update(
                    accountFuture.join().toBuilder()
                        .ownerAccountHolderID(accountHolderFuture.join().getId())
                        .attributes(attributes)
                        .build()))
        .exceptionally(throwable -> {
          throwable = unwrapCompletionStateException(throwable);
          logger.error("Update Account Owner failed", throwable)
              .attr("accountID", payload.getId())
              .attr("ifiID", payload.getIfiID())
              .log();
          throw new CompletionException(throwable);
        });
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_EVENT)
  public CompletionStage<Account> updateStatus(String id,
                                               Long ifiID,
                                               String status) {
    return getAccount(ifiID, id)
        .thenCompose(account -> updateStatus(account, status));
  }

  private CompletionStage<Account> updateStatus(Account account, String status) {
      if (StringUtils.equals(account.getStatus(), status)) {
          return completedFuture(account);
      }
      LedgerState ledgerState;
      try {
          switch (Status.valueOf(status)) {
              case ENABLED:
                  ledgerState = LedgerState.ENABLED;
                  break;
              case DISABLED:
                  ledgerState = LedgerState.DEBITS_DISABLED;
                  break;
              case BLOCKED:
              case DELETED:
                  ledgerState = LedgerState.DISABLED;
                  break;
              case CLOSED:
                  ledgerState = LedgerState.CLOSED;
                  break;
              default:
                  throw new AccountServiceException(
                          "Cannot map " + status + " to a ledger status",
                          AccountErrorCode.INVALID_ACCOUNT_STATUS);
          }
      }
      catch (IllegalArgumentException t) {
          throw new AccountServiceException(
                  "Cannot map " + status + " to a ledger status cz of invalid account status provided",
                  AccountErrorCode.INVALID_ACCOUNT_STATUS);
      }
      return ledgerService.updateLedgerState(account.getLedgerID(), ledgerState)
            .thenCompose(ignore -> update(Account.builder().id(account.getId()).ifiID(account.getIfiID()).status(status).build()));
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_EVENT)
  public CompletionStage<Account> updateProduct(String id, Long ifiID, Long productID) {
    return fetchProductAndUpdateAccount(ifiID, productID, id)
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              logger
                  .warn("Update Product for Account exception", throwable)
                  .fill("accountID", id)
                  .fill("throwable", throwable)
                  .log();

              throw new CompletionException(throwable);
            });
  }

  private CompletionStage<Account> fetchProductAndUpdateAccount(Long ifiID,
                                                                Long productID,
                                                                String accountID) {
    CompletionStage<Account> accountFuture = getAccount(ifiID, accountID);
    if (null != productID) {
      return accountFuture
          .thenCombine(athenaManagerService.getProduct(ifiID, productID), this::updateProduct)
          .thenCompose(a -> a);
    } else {
      return accountFuture.thenCompose(
          account ->
              athenaManagerService
                  .getProductFamily(ifiID, account.getProductFamilyID())
                  .thenCombine(
                      getAccountHolder(ifiID, null, null, account.getOwnerAccountHolderID()),
                      (productFamily, accountHolder) -> {
                        String ifiProductType =
                            resolveIfiProductType(
                                productFamily,
                                Optional.ofNullable(accountHolder.getKYCStatus()),
                                accountHolder.getAttributes());
                        logger.info("IFI Product Type")
                            .attr("type", ifiProductType)
                            .log();
                        return athenaManagerService
                            .getProduct(
                                ifiID,
                                productFamily.getId(),
                                productFamily.getCoaID(),
                                ifiProductType)
                            .thenCompose(product -> updateProduct(account, product));
                      })
                  .thenCompose(a -> a));
    }
  }

  private CompletionStage<Account> updateProduct(Account account, Product product) {
    final Map<String, String> attributes = new HashMap<>();
    final Account updatedAccount = account.toBuilder().productID(product.getId()).build();
    final Account.Builder updatedAccountBuilder = updatedAccount.toBuilder();

    Long ifiID = account.getIfiID();
    return accountHolderService
        .get(ifiID, account.getOwnerAccountHolderID())
        .thenCompose(accountHolder -> applyPolicies(updatedAccount, accountHolder))
        .thenCompose(
            ignore -> athenaManagerService.getProductFamily(ifiID, product.getProductFamilyID()))
        .thenCompose(
            productFamily -> {
              throwIfProductDoesNotBelongToProductFamily(productFamily, product);
              attributes.putAll(updatedAccount.getAttributes());
              attributes.putAll(getProductRelatedLedgerAttributes(product));
              updatedAccountBuilder.attributes(attributes);
              return ledgerService.setLedgerAttributes(
                  account.getLedgerID(),
                  attributes,
                  Collections.emptySet(),
                  "UPDATE_PRODUCT-" + account.getLedgerID() + "-" + System.currentTimeMillis());
            })
        .thenCompose(
            ignore ->
                ledgerService.updateIFIProductType(
                    product.getCoaID(),
                    account.getLedgerID(),
                    // NOTE : Assuming product name is same as the value for ifiProductType
                    // TODO : Change it after changing ledger's requirement of IFIProductType to
                    // String (Account Group Name)
                    IfiProductType.valueOf(product.getName()),
                    product.getCoaNodeID(),
                    product.getProductFamilyID(),
                    product.getId()))
        .thenCompose(ignore -> update(updatedAccountBuilder.build()));
  }

  private CompletionStage<Long> createLedger(Account account, AccountHolderVector accountHolderVector, AccountHolder accountHolder) {
    final Long ifiID = account.getIfiID();

    CompletableFuture<ProductFamily> productFamilyFuture = athenaManagerService.getProductFamily(ifiID, account.getProductFamilyID()).toCompletableFuture();
    CompletableFuture<Product> productFuture = athenaManagerService.getProduct(ifiID, account.getProductID()).toCompletableFuture();
    CompletableFuture<List<WhitelistedAccountHolderAttributes>> whitelistedAttributesFuture = whitelistedAccountHolderAttributesService.get(ifiID).toCompletableFuture();
    CompletableFuture<Optional<KYCStatus>> kycStatusFuture = kycStatusService.getOptional(accountHolder.getId(), ifiID).toCompletableFuture();
    // NOTE : Currently the assumption is that account cannot be created without a program\
    return CompletableFuture.allOf(productFamilyFuture, productFuture, whitelistedAttributesFuture,kycStatusFuture)
        .thenCompose(aVoid -> ledgerService.getCoa(productFamilyFuture.join().getCoaID(), ifiID))
        .thenApply(coAResponsePayload -> {
          ProductFamily productFamily = productFamilyFuture.join();
          Product product = productFuture.join();
          List<WhitelistedAccountHolderAttributes> whitelistedAttributes = whitelistedAttributesFuture.join();

          throwIfProductDoesNotBelongToProductFamily(productFamily, product);
          ArrayList<Long> userIDs = new ArrayList<>();
          if(ZetaUrlConstants.USER_ID_PREFIX.equals(accountHolderVector.getType())) {
            userIDs.add(Long.valueOf(accountHolderVector.getValue()));
          }

          //TODO: adding this to support funding account creation,
          // should be removed once ledger type dependency is removed from ledger
          final String mccVoucherRecipient = account.getAttributes()
              .getOrDefault(LEDGER_PRODUCT_TYPE_KEY, MCC_VOUCHER_RECIPIENT);

          //TODO : Add accountHolderID also as debitor.

          return new CreateLedgerRequestPayloadV5.Builder()
              .ifi(ifiID)
              .parentNodeId(product.getCoaNodeID())
              .coaId(product.getCoaID())
              .currency(coAResponsePayload.getCoAConfig().getCurrency())
              .ledgerType(mccVoucherRecipient)
              .accountingType(productFamily.getAccountingType())
              .ppiType(PPI_TYPE) //TODO: check if its still required on ledger
              .accountHolderDebitors(singletonList(accountHolder.getId()))
              .accountHolderId(accountHolder.getId())
              .debitors(userIDs)
              .attrs(getLedgerAttributes(account, accountHolderVector, accountHolder, product, productFamily, whitelistedAttributes,kycStatusFuture.join()));
        })
        .thenCompose(ledgerService::createLedger)
        .thenApply(createLedgerResponse -> createLedgerResponse.ledgerID);
  }

  private void throwIfProductDoesNotBelongToProductFamily(ProductFamily productFamily, Product product) {
    if (!product.getProductFamilyID().equals(productFamily.getId())) {
      logger.warn("Account Creation failure : Product doesn't belong to product family")
          .attr("productID", product.getId())
          .attr("productFamilyID", productFamily.getId())
          .log();
      throw new AccountCreationException(
          String.format(
              "Product %s doesn't belong to product family %s",
              product.getId(), productFamily.getId()),
          AccountErrorCode.INVALID_PRODUCT);
    }
  }

  private Map<String, String> getLedgerAttributes(@Nonnull Account account,
                                                  @Nonnull AccountHolderVector accountHolderVector,
                                                  @Nonnull AccountHolder accountHolder,
                                                  @Nonnull Product product,
                                                  @Nonnull ProductFamily productFamily,
                                                  @NonNull List<WhitelistedAccountHolderAttributes> whitelistedAccountHolderAttributes,
                                                  Optional<KYCStatus> kycStatus) {
    final Map<String, String> accountAttributes = new HashMap<>();
    final String ifiProductType = resolveIfiProductType(productFamily, kycStatus, accountHolder.getAttributes());
    if(null != ifiProductType) {
      accountAttributes.putIfAbsent(ZETA_IFI_PRODUCT_TYPE, ifiProductType);
    }
    accountAttributes.put(ACCOUNT_PRODUCT_FAMILY_ID, String.valueOf(productFamily.getId()));
    accountAttributes.put(ZETA_PRODUCT_TYPE, GENERIC_PRODUCT_TYPE_VALUE);
    accountAttributes.putAll(account.getAttributes());
    accountAttributes.put(ATHENA_ACCOUNT_PROVIDER_ID, String.valueOf(account.getAccountProviderID()));
    accountAttributes.put(ZETA_CARD_PROGRAM_IDS, String.join(",", account.getProgramIDs()));
    addProgramAttributes(account.getProgramIDs(), accountAttributes);
    accountAttributes.putAll(getAccountHolderRelatedAttributes(accountHolderVector, accountHolder, whitelistedAccountHolderAttributes));
    accountAttributes.putAll(getProductRelatedLedgerAttributes(product));
    return accountAttributes;
  }

  private void addProgramAttributes(
      List<String> programIDs, Map<String, String> accountAttributes) {
    if (programIDs != null && !programIDs.isEmpty()) {
      programIDs.stream()
          .filter(programID -> programID != null && !programID.isEmpty())
          .forEach(
              programID ->
                  accountAttributes.put(
                      String.format("%s_%s", ACCOUNT_PROGRAM_ID, programID), programID));
    }
  }

  private Map<String, String> getAccountHolderRelatedAttributes(AccountHolderVector accountHolderVector,
                                                                AccountHolder accountHolder,
                                                                List<WhitelistedAccountHolderAttributes> whitelistedAccountHolderAttributes) {
    Map<String, String> attributes = new HashMap<>();
    //NOTE: Currently assuming only one externalAccountHolderAssociated with an Account
    if (!Strings.isNullOrEmpty(accountHolderVector.getType())) {
      attributes.put(accountHolderVector.getType(),accountHolderVector.getValue());
    }
    if(!Strings.isNullOrEmpty(accountHolderVector.getValue())) {
      attributes.put(ACCOUNT_EXTERNAL_ID, accountHolderVector.getValue());
    }
    if(ZetaUrlConstants.USER_ID_PREFIX.equals(accountHolderVector.getType())) {
      attributes.put(LEDGER_ZETA_USER_ID, accountHolderVector.getValue());
    }
    attributes.put(ACCOUNT_ACCOUNT_HOLDER_ID, accountHolder.getId());
    Map<String, String> accountHolderAttributes = new HashMap<>();
    whitelistedAccountHolderAttributes.stream().forEach(whitelistedAttributes -> {
      final Object whitelistedAttribute = FieldQuery.getField(accountHolder, whitelistedAttributes.getAttributePath());
      if(null != whitelistedAttribute) {
        accountHolderAttributes.putIfAbsent(whitelistedAttributes.getAttributeName(), whitelistedAttribute.toString());
      }
    });
    attributes.putAll(accountHolderAttributes);
    return attributes;
  }

  private String resolveIfiProductType(ProductFamily productFamily, Optional<KYCStatus> kycStatus, Map<String, String> accountHolderAttributes) {
    if (GPR.equalsIgnoreCase(productFamily.getName())) {
      return LedgerService.getLedgerPolicyConfig()
          .get(GPR)
          .getIfiProductType(
              (kycStatus.map(KYCStatus::getKycStatus).orElse(null)),
              ((kycStatus.isPresent() && null != kycStatus.get().getAttributes()) && kycStatus.get().getAttributes().containsKey(PAN)),
              accountHolderAttributes.containsKey(IS_ATM_ENABLED)
                  && Boolean.parseBoolean(accountHolderAttributes.get(IS_ATM_ENABLED)))
          .orElse(null);
    }
    return null;
  }

  private Map<String, String> getProductRelatedLedgerAttributes(Product product) {
    Map<String, String> attributes = new HashMap<>();
    attributes.put(ACCOUNT_PRODUCT_ID, String.valueOf(product.getId()));
    return attributes;
  }

  private CompletionStage<Void> applyPolicies(Account account, AccountHolder accountHolder) {

    final Long ifiID = account.getIfiID();
    return issuancePolicyService
        .applyIssuancePolicy(
            ifiID,
            PolicyEntityType.PRODUCT_FAMILY,
            account.getProductFamilyID(),
            accountHolder)
        .thenCompose(ignored -> issuancePolicyService
            .applyIssuancePolicy(
                ifiID,
                PolicyEntityType.PRODUCT,
                account.getProductID(),
                accountHolder))
        .thenCompose(ignored -> allResultsOf(account
            .getProgramIDs()
            .stream()
            .map(programID -> issuancePolicyService
                .applyIssuancePolicy(
                    ifiID,
                    PolicyEntityType.PROGRAM,
                    programID,
                    accountHolder))
            .collect(Collectors.toList())))
        .thenAccept(x -> {})
        .exceptionally(t -> {
          t = unwrapCompletionStateException(t);

          logger.warn("Apply issuance policy exception", t)
              .fill(account)
              .log();

          throw new CompletionException(t);
        });
  }

  private CompletionStage<Account> update(Account accountPropertiesToUpdate) {
    final CompletionStage<Account> getAccountFuture = getAccount(accountPropertiesToUpdate.getIfiID(), accountPropertiesToUpdate.getId());

    CompletionStage<Void> updateLedgerIfRequiredFuture = completedFuture(null);
    if (accountPropertiesToUpdate.getAttributes() != null) {
      updateLedgerIfRequiredFuture =
          updateLedgerIfRequiredFuture
              .thenCompose(aVoid -> getAccountFuture)
              .thenCompose(account ->
                  ledgerService.setLedgerAttributes(
                      account.getLedgerID(),
                      accountPropertiesToUpdate.getAttributes(),
                      Collections.emptySet(),
                      "UPDATE-"+account.getLedgerID()+"-"+System.currentTimeMillis()))
              .thenAccept(setLedgerInfoResponse -> {});
    }

    return updateLedgerIfRequiredFuture
        .thenCompose(ignore -> accountDAO.update(accountPropertiesToUpdate))
        .thenCompose(ignore -> getAccount(accountPropertiesToUpdate.getIfiID(), accountPropertiesToUpdate.getId()));
  }

  private List<AccountVector> assignIDsForVectors(List<AccountVector> accountVectors, String id) {
    List<AccountVector> vectors = new ArrayList<>();
    accountVectors.forEach(
        accountVector ->
            vectors.add(
                accountVector
                    .toBuilder()
                    .id(secureRandomGenerator.generateUUID())
                    .accountID(id)
                    .build()));
    return vectors;
  }

  private List<Accessor> assignIDsForAccessors(List<Accessor> accessorList, String id) {
    List<Accessor> accessors = new ArrayList<>();
    accessorList.forEach(accessor -> accessors.add(accessor.toBuilder().id(secureRandomGenerator.generateUUID())
        .accountID(id)
        .build()));
    return accessors;
  }

  private List<AccountRelation> assignIDsForRelationships(List<AccountRelation> accountRelations, String id) {
    List<AccountRelation> relationships = new ArrayList<>();
    accountRelations.forEach(accountRelation -> relationships.add(accountRelation.toBuilder().id(secureRandomGenerator.generateUUID())
        .accountID(id)
        .build()));
    return relationships;
  }

  //TODO : write comparators. This implementation is shitty af.

  private boolean matchesWithExistingAccount(Account newAccount, Account existingAccount) {
    return newAccount.getIfiID() == existingAccount.getIfiID()
        && (null != newAccount.getName() && newAccount.getName().equals(existingAccount.getName()))
        && newAccount.getProductID() == existingAccount.getProductFamilyID()
        && newAccount.getProductFamilyID() == existingAccount.getProductFamilyID()
        //TODO: Add a generic comparator with ability to ignore select field comparision (vectors, accessor, relation).
        //TODO : compare program ids
        && (null != newAccount.getAccountProviderID() && newAccount.getAccountProviderID().equals(existingAccount.getAccountProviderID()))
        && (null != newAccount.getOwnerAccountHolderID() && newAccount.getOwnerAccountHolderID().equals(existingAccount.getOwnerAccountHolderID()))
        && (((null == newAccount.getAttributes() || newAccount.getAttributes().isEmpty())
          && (null == existingAccount.getAttributes() || existingAccount.getAttributes().isEmpty())) ||
              newAccount.getAttributes().equals(existingAccount.getAttributes()));
  }

  private CompletionStage<AccountHolder> getAccountHolder(
      long ifiID,
      ExternalIDType externalIDType,
      String externalID,
      String accountHolderID){
         return accountHolderID != null
               ? accountHolderService.get(ifiID, accountHolderID)
              : accountHolderService.getByVector(ifiID, externalIDType, externalID);
  }

  public CompletionStage<Void> bulkCreateAccount(List<Account> accounts) {
    return accountDAO.bulkInsertAccount(accounts);
  }
}
