package in.zeta.oms.account.account;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.service.AthenaManagerService;
import in.zeta.oms.account.service.CloudCardService;
import in.zeta.oms.account.service.WalletService;
import in.zeta.oms.athenamanager.api.model.Product;
import in.zeta.oms.cloudcard.api.enums.CardStatus;
import in.zeta.oms.cloudcard.api.model.CloudCard;
import in.zeta.oms.wallet.api.CardProgramInfo;
import in.zeta.oms.wallet.api.CardState;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Collectors;

import static in.zeta.athenacommons.util.TimeUtils.ofMilliSecondInUTC;

@EagerSingleton
public class GetAccountListService {

  private final CloudCardService cloudCardService;
  private final AthenaManagerService athenaManagerService;
  private final WalletService walletService;

  @Inject
  public GetAccountListService(
      CloudCardService cloudCardService,
      AthenaManagerService athenaManagerService,
      WalletService walletService) {
    this.cloudCardService = cloudCardService;
    this.athenaManagerService = athenaManagerService;
    this.walletService = walletService;
  }

  public CompletionStage<List<Account>> getAccounts(GetAccountListRequest payload) {
    CompletionStage<Optional<Product>> optionalProductFuture =
        CompletableFuture.completedFuture(Optional.empty());
    if (payload.getProductID() != null) {
      optionalProductFuture =
          athenaManagerService
              .getProduct(payload.getIfiID(), payload.getProductID())
              .thenApply(Optional::of);
    } else if (!Strings.isNullOrEmpty(payload.getProductName())) {
      optionalProductFuture =
          athenaManagerService.getOptionalProductByName(
              payload.getProductName(), payload.getIfiID());
    }
    return optionalProductFuture.thenCompose(
        optionalProduct ->
            optionalProduct
                .map(
                    product ->
                        getCloudCards(payload)
                            .thenCompose(
                                cloudCardList -> {
                                  if (cloudCardList.isEmpty()) {
                                    return CompletableFuture.completedFuture(
                                        ImmutableList.<Account>of());
                                  } else {
                                    return getCardsFromWallet(
                                            payload, cloudCardList.get(0).getUserID())
                                        .thenApply(
                                            cardIDCardProgramInfoMap ->
                                                getAccountListFromWalletAndCloudCard(
                                                    payload.getIfiID(),
                                                    cloudCardList,
                                                    cardIDCardProgramInfoMap));
                                  }
                                }))
                .orElseGet(
                    () ->
                        getCloudCards(payload)
                            .thenApply(
                                cards ->
                                    cards.stream()
                                        .map(
                                            cloudCard ->
                                                getAccountObject(
                                                    cloudCard,
                                                    payload.getIfiID(),
                                                    accountStateFromCardState(
                                                        cloudCard.getCardStatus())))
                                        .collect(Collectors.toList()))));
  }

  private CompletionStage<List<CloudCard>> getCloudCards(GetAccountListRequest payload) {
    return cloudCardService.getCloudCards(
        payload.getIfiID(),
        payload.getCardID(),
        payload.getAccountHolderVector() != null
            ? payload.getAccountHolderVector().getType()+":"+payload.getAccountHolderVector().getValue()
            : null,
        payload.getProgramID(),
        payload.getProductID(),
        payload.getPageNumber(),
        payload.getPageSize());
  }

  private List<Account> getAccountListFromWalletAndCloudCard(
      Long ifiID,
      List<CloudCard> cloudCardList,
      Map<Long, CardProgramInfo> cardIDCardProgramInfoMap) {

    return cloudCardList.stream()
        .filter(cloudCard -> cardIDCardProgramInfoMap.containsKey(cloudCard.getCardID()))
        .map(
            cloudCard ->
                getAccountObject(
                    cloudCard,
                    ifiID,
                    accountStateFromWalletCardState(
                        cardIDCardProgramInfoMap.get(cloudCard.getCardID()).getCardState())))
        .collect(Collectors.toList());
  }

  private CompletionStage<Map<Long, CardProgramInfo>> getCardsFromWallet(
      GetAccountListRequest payload, Long userID) {
    return walletService
        .getCardsByProductType(userID, payload.getIfiID())
        .thenApply(
            cardProgramInfoList ->
                cardProgramInfoList.stream()
                    .collect(Collectors.toMap(CardProgramInfo::getCardID, Function.identity())));
  }

  private Account getAccountObject(CloudCard card, Long ifiID, String status) {
    Map<String, String> attributes = new HashMap<>(card.getAttributes());
    return Account.builder()
        .cardID(card.getCardID())
        .ifiID(ifiID)
        .productID(card.getProductID())
        .programID(card.getCardProgramID())
        .status(status)
        .ownerAccountHolderID(card.getUserID().toString())
        .ledgerID(card.getRecipientLedgerID())
        .attributes(attributes)
        .createdAt(ofMilliSecondInUTC(card.getCreatedAt()))
        .updatedAt(ofMilliSecondInUTC(card.getModifiedAt()))
        .build();
  }

  private String accountStateFromWalletCardState(CardState cardStatus) {
    return cardStatus.name();
  }

  // TODO: fix this
  private String accountStateFromCardState(CardStatus cardStatus) {
    return cardStatus.name();
  }
}
