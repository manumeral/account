package in.zeta.oms.account.account.accessor;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import in.zeta.oms.account.api.model.Accessor;
import olympus.message.types.Request;

@EagerSingleton
public class AccessorHandler extends AccountServiceBaseRequestHandler {

  private final AccessorService accessorService;

  @Inject
  public AccessorHandler(ZetaHostMessagingService zetaHostMessagingService,
                         AccessorService accessorService) {
    super(zetaHostMessagingService);
    this.accessorService = accessorService;
  }

  @Authorized(anyOf = {"api.account.accessor.get", "api.account.admin"})
  public void on(GetAccountAccessorRequest payload, Request<GetAccountAccessorRequest> request) {
    onRequest(payload, request);
    accessorService.get(payload.getAccountAccessorID(), payload.getAccountID(), payload.getIfiID())
        .thenAccept(accountAccessor -> onResult(accountAccessor, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accessor.get", "api.account.admin"})
  public void on(GetAccountAccessorListRequest payload, Request<GetAccountAccessorListRequest> request) {
    onRequest(payload, request);
    accessorService.getAccessors(payload.getAccountID(), payload.getIfiID())
        .thenAccept(accountAccessors -> onResult(GetAccountAccessorListResponse.builder().accessors(accountAccessors).build(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accessor.update", "api.account.admin"})
  public void on(UpdateAccountAccessorRequest payload, Request<UpdateAccountAccessorRequest> request) {
    onRequest(payload, request);
    Accessor accessor = Accessor.builder()
        .accountID(payload.getAccountID())
        .attributes(payload.getAttributes())
        .accountHolderID(payload.getAccountHolderID())
        .ifiID(payload.getIfiID())
        .status(payload.getStatus())
        .id(payload.getAccountAccessorID())
        .transactionPolicyIDs(payload.getTransactionPolicyIDs())
        .build();

    accessorService.update(accessor)
        .thenAccept(accountAccessor -> onResult(accountAccessor, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accessor.delete", "api.account.admin"})
  public void on(DeleteAccountAccessorRequest payload, Request<DeleteAccountAccessorRequest> request) {
    onRequest(payload, request);
    Accessor accessor = Accessor.builder()
        .accountID(payload.getAccountID())
        .ifiID(payload.getIfiID())
        .id(payload.getAccountAccessorID())
        .status(Status.DELETED.name())
        .build();

    accessorService.update(accessor)
        .thenAccept(accountAccessor -> onResult(accountAccessor, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accessor.add", "api.account.admin"})
  public void on(AddAccountAccessorRequest payload, Request<AddAccountAccessorRequest> request) {
    onRequest(payload, request);
    Accessor accountAccessor = Accessor.builder()
        .accountID(payload.getAccountID())
        .accountHolderID(payload.getAccountHolderID())
        .ifiID(payload.getIfiID())
        .attributes(payload.getAttributes())
        .status(payload.getStatus())
        .transactionPolicyIDs(payload.getTransactionPolicyIDs())
        .build();
    accessorService.addAccessor(accountAccessor)
        .thenAccept(accountAccessorResponse -> onResult(accountAccessorResponse, request))
        .exceptionally(t -> onError(request, t));
  }
}
