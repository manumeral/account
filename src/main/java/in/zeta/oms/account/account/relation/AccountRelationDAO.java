package in.zeta.oms.account.account.relation;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.model.AccountRelation;
import in.zeta.oms.account.util.LoggerUtils;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class AccountRelationDAO extends PostgresDAO implements RowMapper<AccountRelation> {

  private static final LoggerUtils loggerUtils = LoggerUtils.builder()
      .logger(OlympusSpectra.getLogger(AccountRelationDAO.class))
      .build();

  //-----------------------------------------Column Names-----------------------------------------//
  private static final String TABLE_NAME = "account_relation";
  private static final String ID = "id";
  private static final String IFI_ID = "ifi_id";
  private static final String ACCOUNT_ID = "account_id";
  private static final String RELATED_ACCOUNT_ID = "related_account_id";
  private static final String RELATIONSHIP_TYPE = "relationship_type";
  private static final String STATUS = "status";
  private static final String ATTRIBUTES = "attributes";
  private static final String CREATED_AT = "created_at";
  private static final String UPDATED_AT = "modified_at"; //TODO: Make this column name same across all services

  private static final List<String> INSERT_COLUMNS = new ImmutableList.Builder<String>()
      .add(ID)
      .add(IFI_ID)
      .add(ACCOUNT_ID)
      .add(RELATED_ACCOUNT_ID)
      .add(RELATIONSHIP_TYPE)
      .add(STATUS)
      .add(ATTRIBUTES)
      .build();

  private static final List<String> SELECT_COLUMNS = new ImmutableList.Builder<String>()
      .addAll(INSERT_COLUMNS)
      .add(CREATED_AT)
      .add(UPDATED_AT)
      .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
      .tableName(TABLE_NAME)
      .insertColumns(INSERT_COLUMNS)
      .selectColumns(SELECT_COLUMNS)
      .customColumnType(ID, UUID_TYPE)
      .customColumnType(ACCOUNT_ID, UUID_TYPE)
      .customColumnType(RELATED_ACCOUNT_ID, UUID_TYPE)
      .customColumnType(ATTRIBUTES, JSONB_TYPE)
      .build();

  private final Gson gson;

  @Inject
  public AccountRelationDAO(ZetaHostMessagingService hostMessagingService,
                            BasicDataSource basicDataSource,
                            @Named("postgres.jdbc.pool.size") int poolSize,
                            Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson =gson;
  }

  @TimeLogger
  public CompletionStage<Void> insert(@NonNull AccountRelation accountRelation) {
    return update(getInsertInstruction(accountRelation))
        .thenAccept(ignore -> {});
  }

  public PostgresInstruction getInsertInstruction(@NonNull AccountRelation accountRelation) {
    List<Object> args = new ArrayList<>();
    args.add(accountRelation.getId());
    args.add(accountRelation.getIfiID());
    args.add(accountRelation.getAccountID());
    args.add(accountRelation.getRelatedAccountID());
    args.add(accountRelation.getRelationshipType());
    args.add(accountRelation.getStatus());
    args.add(getJsonbObject(gson.toJson(accountRelation.getAttributes())));

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
  }

  @TimeLogger
  public CompletionStage<List<AccountRelation>> getRelationships(@NonNull String accountID,@NonNull Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(accountID);
    args.add(ifiID);

    final ImmutableList<String> criteria = new ImmutableList.Builder<String>()
        .add(ACCOUNT_ID)
        .add(IFI_ID)
        .build();
    return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(t -> {
          t = unwrapCompletionStateException(t);
          throw loggerUtils.logDBException(String.format("Error fetching relations for AccountID : %s, and ifiID : %S ", accountID, ifiID), t);
        });
  }

  @TimeLogger
  public CompletionStage<Optional<AccountRelation>> get(@NonNull String id,@NonNull String accountID,@NonNull Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(id);
    args.add(accountID);
    args.add(ifiID);

    final ImmutableList<String> criteria = new ImmutableList.Builder<String>()
        .add(ID)
        .add(ACCOUNT_ID)
        .add(IFI_ID)
        .build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(throwable -> {
          throwable = unwrapCompletionStateException(throwable);
          throw loggerUtils.logDBException(
              String.format("Get AccountRelation failed for accountRelationID: %s, and accountID : %s, and ifiID : %S", id, accountID, ifiID), throwable);
        });
  }

  @TimeLogger
  public CompletionStage<Void> update(@NonNull AccountRelation accountRelation) {
    final List<String> whereClauseColumnNames = Arrays.asList(ID, ACCOUNT_ID, IFI_ID);
    ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
    ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
    if(!Strings.isNullOrEmpty(accountRelation.getStatus())) {
      updateColumnNames.add(STATUS);
      argsList.add(accountRelation.getStatus());
    }
    if(null != accountRelation.getAttributes()) {
      updateColumnNames.add(ATTRIBUTES);
      argsList.add(getJsonbObject(gson.toJson(accountRelation.getAttributes())));
    }
    if(!Strings.isNullOrEmpty(accountRelation.getRelationshipType())) {
      updateColumnNames.add(RELATIONSHIP_TYPE);
      argsList.add(accountRelation.getRelationshipType());
    }
    argsList.add(accountRelation.getId());
    argsList.add(accountRelation.getAccountID());
    argsList.add(accountRelation.getIfiID());
    return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(updateColumnNames.build(), whereClauseColumnNames), argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(throwable -> {
          throwable = unwrapCompletionStateException(throwable);
          throw loggerUtils.logDBException(String.format("Error Updating accountRelationID : %s for accountID : %s",accountRelation.getId(),accountRelation.getAccountID()), throwable);
        });
  }

  @Override
  public AccountRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
    return AccountRelation.builder()
        .id(rs.getString(ID))
        .ifiID(rs.getLong(IFI_ID))
        .accountID(rs.getString(ACCOUNT_ID))
        .relatedAccountID(rs.getString(RELATED_ACCOUNT_ID))
        .relationshipType(rs.getString(RELATIONSHIP_TYPE))
        .status(rs.getString(STATUS))
        .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .createdAt(rs.getTimestamp(CREATED_AT))
        .updatedAt(rs.getTimestamp(UPDATED_AT))
        .build();
  }
}
