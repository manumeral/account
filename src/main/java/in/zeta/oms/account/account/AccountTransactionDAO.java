package in.zeta.oms.account.account;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.account.accessor.AccessorDAO;
import in.zeta.oms.account.account.relation.AccountRelationDAO;
import in.zeta.oms.account.account.vector.AccountVectorDAO;
import in.zeta.oms.account.api.exception.AccountCreationException;
import in.zeta.oms.account.api.model.Account;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.dao.DuplicateKeyException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;
import static in.zeta.oms.account.constant.AccountErrorCode.DB_ERROR;
import static in.zeta.oms.account.constant.AccountErrorCode.INTERNAL_ERROR;

@Singleton
public class AccountTransactionDAO extends PostgresDAO {
  private static final SpectraLogger logger = OlympusSpectra.getLogger(AccountTransactionDAO.class);

  private final AccountDAO accountDAO;
  private final AccountVectorDAO accountVectorDAO;
  private final AccountRelationDAO accountRelationDAO;
  private final AccessorDAO accessorDAO;

  @Inject
  public AccountTransactionDAO(
      ZetaHostMessagingService zhms,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      AccountDAO accountDAO,
      AccountVectorDAO accountVectorDAO,
      AccessorDAO accessorDAO,
      AccountRelationDAO accountRelationDAO) {
    super(zhms, basicDataSource, poolSize);
    this.accountDAO = accountDAO;
    this.accountVectorDAO = accountVectorDAO;
    this.accountRelationDAO = accountRelationDAO;
    this.accessorDAO = accessorDAO;
  }

  public CompletionStage<Account> insertAccount(Account account, String requestID){
    List<PostgresInstruction> queryInTransaction = new ArrayList<>();

    //TODO : SCHEMA Level Validation of DuplicateEntry
    queryInTransaction.add(accountDAO.getInsertInstruction(account, requestID));
    account.getVectors().forEach(vector -> queryInTransaction.add(accountVectorDAO.getInsertInstruction(vector)));
    account.getAccessors().forEach(accessor -> queryInTransaction.add(accessorDAO.getInsertInstruction(accessor)));
    account.getRelationships().forEach(accountRelation -> queryInTransaction.add(accountRelationDAO.getInsertInstruction(accountRelation)));
    return updateInTransaction(queryInTransaction)
        .handle((ignored, throwable)-> {
          if (throwable == null)
            return account;
          throwable = unwrapCompletionStateException(throwable);
          if (throwable instanceof DuplicateKeyException) {
            logger
                .info("Duplicate key exception")
                .fill("error", throwable)
                .fill("account", account)
                .log();
            throw new AccountCreationException("Account already exists with provided account vector types", throwable, INTERNAL_ERROR);
          }
          logger
              .error("Error while inserting account, vectors, relationships and accessors in transaction", throwable)
              .fill("account", account)
              .log();
          //TODO: Change The Exception
          throw new AccountCreationException("Error while inserting account holder and vector in transaction", throwable,DB_ERROR);
        });
  }
}
