package in.zeta.oms.account.account.relation;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.api.exception.AccountRelationCreationException;
import in.zeta.oms.account.api.exception.AccountRelationNotFoundException;
import in.zeta.oms.account.api.model.AccountRelation;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.spectra.capture.SpectraLogger;

import java.util.List;
import java.util.concurrent.CompletionStage;

import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;
import static olympus.trace.OlympusSpectra.getLogger;

@Singleton
public class AccountRelationService {
  private static final SpectraLogger logger = getLogger(AccountRelationService.class);

  private final AccountRelationDAO accountRelationDAO;
  private final SecureRandomGenerator secureRandomGenerator;

  @Inject
  public AccountRelationService(AccountRelationDAO accountRelationDAO,
                                SecureRandomGenerator secureRandomGenerator) {
    this.accountRelationDAO = accountRelationDAO;
    this.secureRandomGenerator = secureRandomGenerator;
  }

  public CompletionStage<List<AccountRelation>> getRelationships(String accountID, Long ifiID) {
    return accountRelationDAO.getRelationships(accountID, ifiID);
  }

  @PublishEvent(topicName = PubSubTopics.ADD_ACCOUNT_RELATION_EVENT_TOPIC)
  public CompletionStage<AccountRelation> addRelation(AccountRelation accountRelation) {
    final AccountRelation accountRelationToAdd = accountRelation.toBuilder().id(secureRandomGenerator.generateUUID()).build();
    return accountRelationDAO
        .insert(accountRelationToAdd)
        .thenCompose(
            ignore ->
                get(
                    accountRelationToAdd.getId(),
                    accountRelationToAdd.getAccountID(),
                    accountRelationToAdd.getIfiID()))
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              logger.warn("Account Relation creation failed")
                  .attr("accountID", accountRelation.getAccountID())
                  .attr("ifiID", accountRelation.getIfiID())
                  .attr("relatedAccountID", accountRelation.getRelatedAccountID())
                  .attr("relationshipType", accountRelation.getRelationshipType())
                  .attr("errMsg", t.getMessage())
                  .log();
              throw new AccountRelationCreationException(
                  String.format("Account Relation Creation failed for account ID : %s",accountRelationToAdd.getAccountID()),
                  t,
                  AccountErrorCode.INTERNAL_ERROR);
            });
  }

  public CompletionStage<AccountRelation> get(String id, String accountID, Long ifiID) {
    return accountRelationDAO.get(id, accountID, ifiID)
        .thenApply(
            accountRelationOptional ->
                accountRelationOptional.orElseThrow(
                    () ->
                        new AccountRelationNotFoundException(
                            String.format(
                                "Account Relation for account ID : %s and account accountRelationID : %s not found",
                                accountID, id),
                            AccountErrorCode.ACCOUNT_RELATION_NOT_FOUND)));
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_RELATION_EVENT_TOPIC)
  public CompletionStage<AccountRelation> update(AccountRelation accountRelation) {
    return accountRelationDAO.update(accountRelation)
        .thenCompose(ignore -> get(accountRelation.getId(), accountRelation.getAccountID(), accountRelation.getIfiID()));
  }
}
