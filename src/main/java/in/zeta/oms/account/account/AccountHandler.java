package in.zeta.oms.account.account;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.interceptors.authorization.ServiceAuthorizer;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.api.response.GetAccountListResponse;
import olympus.message.types.Request;

@EagerSingleton
public class AccountHandler extends AccountServiceBaseRequestHandler {

  private final AccountService accountService;
  private final GetAccountListService getAccountListService;

  @Inject
  public AccountHandler(ZetaHostMessagingService zhms,
                        AccountService accountService,
                        GetAccountListService getAccountListService,
                        ServiceAuthorizer serviceAuthorizer,
                        AccountAuthorizer accountAuthorizer) {
    super(zhms);
    this.accountService = accountService;
    this.getAccountListService = getAccountListService;
    serviceAuthorizer.addAuthorization(CreateAccountRequest.class, accountAuthorizer.createAccountAuthorizationStrategy());
  }

  @Authorized(anyOf = {"api.account.create", "api.account.admin"})
  public void on(CreateAccountRequest payload, Request<CreateAccountRequest> request) {
    onRequest(payload, request);
    Account account = Account.Builder.from(payload).build();
    accountService.validate(account)
        .thenCompose(aVoid -> accountService.createAccount(account, payload.getRequestID(), payload.getExternalIDType(), payload.getExternalAccountHolderID()))
        .thenAccept(responseAccount -> onResult(responseAccount, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.get", "api.account.admin"})
  public void on(GetAccountRequest payload, Request<GetAccountRequest> request) {
    onRequest(payload, request);
    accountService.getAccount(payload.getIfiID(), payload.getAccountID())
        .thenAccept(account -> onResult(account, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.close", "api.account.admin"})
  public void on(CloseAccountRequest payload, Request<CloseAccountRequest> request) {
    onRequest(payload, request);
    accountService.closeAccount(payload)
            .thenAccept(account -> onResult(account, request))
            .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.getBalance", "api.account.admin"})
  public void on(GetAccountBalanceRequest payload, Request<GetAccountBalanceRequest> request) {
    onRequest(payload, request);
    accountService.getAccountBalance(payload.getIfiID(), payload.getId())
        .thenAccept(account -> onResult(account, request))
        .exceptionally(t -> onError(request, t));
  }

  //TODO
  @Authorized(anyOf = {"api.account.get", "api.account.admin"})
  public void on(GetAccountListRequest payload, Request<GetAccountListRequest> request) {
    onRequest(payload, request);
    getAccountListService
        .getAccounts(payload)
        .thenAccept(accounts -> onResult(
            GetAccountListResponse.builder().accounts(accounts).build(),
            request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.update", "api.account.admin"})
  public void on(UpdateAccountRequest payload, Request<UpdateAccountRequest> request) {
    onRequest(payload, request);

    accountService
        .updateNameAndAttribute(payload)
        .thenAccept(account -> onResult(account, request))
        .exceptionally(t -> onError(request, t));
  }
  
  @Authorized( anyOf = {"api.account.accountHolder.update", "api.account.admin"})
  public void on(UpdateAccountOwnerRequest payload, Request<UpdateAccountOwnerRequest> request) {
    onRequest(payload, request);
    accountService.updateAccountOwner(payload)
        .thenAccept(account -> onResult(account, request))
        .exceptionally(throwable -> onError(request, throwable));
  }

  @Authorized( anyOf = {"api.account.state.update", "api.account.admin"})
  public void on(UpdateAccountStatusRequest payload, Request<UpdateAccountStatusRequest> request) {
    onRequest(payload, request);
    accountService.updateStatus(payload.getId(), payload.getIfiID(), payload.getStatus())
        .thenAccept(account -> onResult(account, request))
        .exceptionally(throwable -> onError(request, throwable));
  }

  @Authorized( anyOf = {"api.account.get", "api.account.admin"})
  public void on(GetAccountListRequestV2 payload, Request<GetAccountListRequestV2> request) {
    onRequest(payload, request);
    accountService.getAccountsV2(payload)
        .thenAccept(accounts -> onResult(
            GetAccountListResponse.builder().accounts(accounts).build(),
            request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized( anyOf = {"api.account.accountInfo.get", "api.account.admin"})
  public void on(GetAccountInfoRequest payload, Request<GetAccountInfoRequest> request) {
    onRequest(payload, request);
    accountService.getAccountInfo(payload.getIfiID(), payload.getAccountID())
        .thenAccept(account -> onResult(account, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized( anyOf = {"api.account.product.update", "api.account.admin"})
  public void on(UpdateProductForAccountRequest payload, Request<UpdateProductForAccountRequest> request) {
    onRequest(payload, request);
    accountService.updateProduct(payload.getId(), payload.getIfiID(), payload.getProductID())
        .thenAccept(account -> onResult(account, request))
        .exceptionally(throwable -> onError(request, throwable));

  }
}
