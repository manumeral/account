package in.zeta.oms.account.account;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.commons.interceptors.authorization.AuthorizationResult;
import in.zeta.commons.interceptors.authorization.AuthorizationStrategy;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.athenamanager.accountProvider.AccountProviderService;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.message.types.Payload;
import olympus.message.types.Request;
import olympus.trace.OlympusSpectra;

import java.util.concurrent.CompletionStage;

import static in.zeta.commons.interceptors.authorization.AuthorizationResult.AUTHORIZED;
import static in.zeta.commons.interceptors.authorization.AuthorizationResult.UNAUTHORIZED;

@Singleton
public class AccountAuthorizer {
  private final AccountProviderService accountProviderService;
  private final SpectraLogger log = OlympusSpectra.getLogger(this.getClass());

  @Inject
  public AccountAuthorizer(AccountProviderService accountProviderService) {
    this.accountProviderService = accountProviderService;
  }

  public AuthorizationStrategy createAccountAuthorizationStrategy() {
    return new AuthorizationStrategy() {
      @Override
      public <T extends Payload> CompletionStage<AuthorizationResult> authorize(
          T t, Request<T> request, String s) {
        T payload = request.payload();
        final CreateAccountRequest requestPayload;
        if (payload instanceof CreateAccountRequest) {
          requestPayload = (CreateAccountRequest) payload;
        } else {
          throw new RuntimeException(
              "Request class "
                  + request.getClass().getSimpleName()
                  + " not supported for authorization for action : "
                  + s);
        }

        final Long product_id = requestPayload.getProductID();
        final Long product_family_id = requestPayload.getProductFamilyID();

        return accountProviderService
            .get(requestPayload.getAccountProviderID(), requestPayload.getIfiID())
            .thenApply(
                accountProvider -> {
                  boolean isProductWhitelisted =
                      accountProviderService.isProductWhitelisted(accountProvider, product_id);
                  if (!isProductWhitelisted) {
                    log.warn("Account Creation Failed")
                        .attr("reason", "Product not whitelisted")
                        .attr("accountProvider" , requestPayload.getAccountProviderID())
                        .attr("ifiID", requestPayload.getIfiID())
                        .log();
                    throw new AccountServiceException(
                        "Bad request: Product ID is not whitelisted", AccountErrorCode.BAD_REQUEST);
                  }
                  boolean isProductFamilyWhitelisted =
                      accountProviderService.isProductFamilyWhitelisted(
                          accountProvider, product_family_id);
                  if (!isProductFamilyWhitelisted) {
                    log.warn("Account Creation Failed")
                        .attr("reason", "Product family not whitelisted")
                        .attr("accountProvider" , requestPayload.getAccountProviderID())
                        .attr("ifiID", requestPayload.getIfiID())
                        .log();
                    throw new AccountServiceException(
                        "Bad request: Product Family ID is not whitelisted",
                        AccountErrorCode.BAD_REQUEST);
                  }
                  return true;
                })
            .handle((aVoid, throwable) -> throwable == null ? AUTHORIZED : UNAUTHORIZED);
      }
    };
  }
}
