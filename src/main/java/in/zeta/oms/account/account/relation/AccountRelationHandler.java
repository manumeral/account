package in.zeta.oms.account.account.relation;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.account.accountRelation.AddAccountRelationRequest;
import in.zeta.oms.account.account.accountRelation.DeleteAccountRelationRequest;
import in.zeta.oms.account.account.accountRelation.GetAccountRelationListRequest;
import in.zeta.oms.account.account.accountRelation.GetAccountRelationListResponse;
import in.zeta.oms.account.account.accountRelation.GetAccountRelationRequest;
import in.zeta.oms.account.account.accountRelation.UpdateAccountRelationRequest;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import in.zeta.oms.account.api.model.AccountRelation;
import olympus.message.types.Request;

@EagerSingleton
public class AccountRelationHandler extends AccountServiceBaseRequestHandler {

  private final AccountRelationService accountRelationService;

  @Inject
  public AccountRelationHandler(ZetaHostMessagingService zetaHostMessagingService,
                                AccountRelationService accountRelationService) {
    super(zetaHostMessagingService);
    this.accountRelationService = accountRelationService;
  }

  @Authorized(anyOf = {"api.account.accountRelation.get", "api.account.admin"})
  public void on(GetAccountRelationRequest payload, Request<GetAccountRelationRequest> request) {
    onRequest(payload, request);
    accountRelationService.get(payload.getAccountRelationID(), payload.getAccountID(), payload.getIfiID())
        .thenAccept(accountRelation -> onResult(accountRelation, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountRelation.get", "api.account.admin"})
  public void on(GetAccountRelationListRequest payload, Request<GetAccountRelationListRequest> request) {
    onRequest(payload, request);
    accountRelationService.getRelationships(payload.getAccountID(), payload.getIfiID())
        .thenAccept(accountRelations -> onResult(GetAccountRelationListResponse.builder().accountRelations(accountRelations).build(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountRelation.update", "api.account.admin"})
  public void on(UpdateAccountRelationRequest payload, Request<UpdateAccountRelationRequest> request) {
    onRequest(payload, request);
    AccountRelation accountRelation = AccountRelation.builder()
        .accountID(payload.getAccountID())
        .attributes(payload.getAttributes())
        .ifiID(payload.getIfiID())
        .status(payload.getStatus())
        .id(payload.getAccountRelationID())
        .relatedAccountID(payload.getRelatedAccountID())
        .relationshipType(payload.getRelationshipType())
        .build();

    accountRelationService.update(accountRelation)
        .thenAccept(updatedAccountRelation -> onResult(updatedAccountRelation, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountRelation.delete", "api.account.admin"})
  public void on(DeleteAccountRelationRequest payload, Request<DeleteAccountRelationRequest> request) {
    onRequest(payload, request);
    AccountRelation accountRelation = AccountRelation.builder()
        .accountID(payload.getAccountID())
        .ifiID(payload.getIfiID())
        .id(payload.getAccountRelationID())
        .status(Status.DELETED.name())
        .build();

    accountRelationService.update(accountRelation)
        .thenAccept(updatedAccountRelation -> onResult(updatedAccountRelation, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountRelation.add", "api.account.admin"})
  public void on(AddAccountRelationRequest payload, Request<AddAccountRelationRequest> request) {
    onRequest(payload, request);
    AccountRelation accountRelation = AccountRelation.builder()
        .accountID(payload.getAccountID())
        .ifiID(payload.getIfiID())
        .attributes(payload.getAttributes())
        .status(payload.getStatus())
        .relationshipType(payload.getRelationshipType())
        .relatedAccountID(payload.getRelatedAccountID())
        .build();
    accountRelationService.addRelation(accountRelation)
        .thenAccept(accountRelationResponse -> onResult(accountRelationResponse, request))
        .exceptionally(t -> onError(request, t));
  }
}
