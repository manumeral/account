package in.zeta.oms.account.account.vector;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.api.exception.AccountVectorCreationException;
import in.zeta.oms.account.api.exception.AccountVectorNotFoundException;
import in.zeta.oms.account.api.exception.AccountVectorUpdateException;
import in.zeta.oms.account.api.model.AccountVector;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;
import java.util.concurrent.CompletionStage;

import static in.zeta.oms.account.constant.AccountErrorCode.INTERNAL_ERROR;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class AccountVectorService {
  private static final SpectraLogger logger = OlympusSpectra.getLogger(AccountVectorService.class);

  private final AccountVectorDAO accountVectorDAO;
  private final SecureRandomGenerator secureRandomGenerator;

  @Inject
  public AccountVectorService(
      AccountVectorDAO accountVectorDAO, SecureRandomGenerator secureRandomGenerator) {
    this.accountVectorDAO = accountVectorDAO;
    this.secureRandomGenerator = secureRandomGenerator;
  }

  public CompletionStage<List<AccountVector>> getVectors(String accountID, Long ifiID) {
    return accountVectorDAO.getVectors(accountID, ifiID);
  }

  @PublishEvent(topicName = PubSubTopics.ADD_ACCOUNT_VECTOR_EVENT_TOPIC)
  public CompletionStage<AccountVector> addVector(AccountVector accountVector) {
    final AccountVector accountVectorToAdd =
        accountVector.toBuilder().id(secureRandomGenerator.generateUUID()).build();
    return accountVectorDAO
        .insert(accountVectorToAdd)
        .thenCompose(
            ignore ->
                get(
                    accountVectorToAdd.getId(),
                    accountVectorToAdd.getAccountID(),
                    accountVectorToAdd.getIfiID()))
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              if (t instanceof DuplicateKeyException) {
                //TODO : Handle duplicate exception
                throw new AccountVectorCreationException(String.format("Account Vector already exists with type:%s and value: %s", accountVector.getType(), accountVector.getValue()), t, INTERNAL_ERROR);
              }
              logger.warn("Account Vector creation failed")
                  .attr("accountID", accountVector.getAccountID())
                  .attr("ifiID", accountVector.getIfiID())
                  .attr("type", accountVector.getType())
                  .attr("value", accountVector.getValue())
                  .attr("errMsg", t.getMessage())
                  .log();
              throw new AccountVectorCreationException(
                  String.format(
                      "Account Vector Creation failed for account ID : %s",
                      accountVectorToAdd.getAccountID()),
                  t,
                  INTERNAL_ERROR);
            });
  }

  public CompletionStage<AccountVector> get(String id, String accountID, Long ifiID) {
    return accountVectorDAO
        .get(id, accountID, ifiID)
        .thenApply(
            accountVectorOptional ->
                accountVectorOptional.orElseThrow(
                    () ->
                        new AccountVectorNotFoundException(
                            String.format(
                                "Account Vector for account ID : %s and account vector id : %s not found",
                                accountID, id),
                            AccountErrorCode.ACCOUNT_VECTOR_NOT_FOUND)));
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_VECTOR_EVENT_TOPIC)
  public CompletionStage<AccountVector> update(AccountVector accountVector) {
    AccountVector.Builder accountVectorBuilder = accountVector.toBuilder();
    CompletionStage<String> accountVectorIDFuture = completedFuture(accountVector.getId());
    if (null == accountVector.getId()) {
      accountVectorIDFuture =
          accountVectorDAO
              .get(
                  accountVector.getType(),
                  accountVector.getValue(),
                  accountVector.getIfiID(),
                  accountVector.getAccountID())
              .thenApply(
                  accountVectorOptional ->
                      accountVectorOptional
                          .map(AccountVector::getId)
                          .orElseThrow(
                              () ->
                                  AccountVectorNotFoundException.accountNotFoundForTypeValuePair(
                                      accountVector.getType(),
                                      accountVector.getValue(),
                                      accountVector.getIfiID(),
                                      accountVector.getId())));
    }

    return accountVectorIDFuture
        .thenApply(accountVectorBuilder::id)
        .thenCompose(builder -> accountVectorDAO.update(builder.build()))
        .thenCompose(
            ignore ->
                accountVectorDAO.get(
                    accountVectorBuilder.build().getId(),
                    accountVector.getAccountID(),
                    accountVector.getIfiID()))
        .thenApply(
            accountVectorOptional ->
                accountVectorOptional.orElseThrow(
                    () ->
                        new AccountVectorUpdateException(
                            String.format(
                                "Account Vector update for account ID : %s and account vector id : %s failed",
                                accountVector.getAccountID(), accountVectorBuilder.build().getId()),
                            INTERNAL_ERROR)));
  }
}
