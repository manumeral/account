package in.zeta.oms.account.account;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import in.zeta.oms.account.api.model.Account;
import in.zeta.tags.ObjectProvider;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

@Singleton
public class AccountObjectProvider implements ObjectProvider<Account> {

  private final Provider<AccountService> accountServiceProvider;

  @Inject
  public AccountObjectProvider(Provider<AccountService> accountServiceProvider) {
    this.accountServiceProvider = accountServiceProvider;
  }

  @Override
  public CompletionStage<Optional<Account>> getObject(Long ifiId, String objectId) {
    return accountServiceProvider.get().getAccount(ifiId, objectId)
        .thenApply(Optional::of);
  }
}
