package in.zeta.oms.account.account.WhitelistedAccountHolderAttributes;

import com.google.inject.Inject;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.oms.account.api.exception.WhitelistedAccountHolderAttributesNotFoundException;

import java.util.List;
import java.util.concurrent.CompletionStage;

@EagerSingleton
public class WhitelistedAccountHolderAttributesService {
  private final WhitelistedAccountHolderAttributesDAO whitelistedAccountHolderAttributesDAO;

  @Inject
  public WhitelistedAccountHolderAttributesService(WhitelistedAccountHolderAttributesDAO whitelistedAccountHolderAttributesDAO) {
    this.whitelistedAccountHolderAttributesDAO = whitelistedAccountHolderAttributesDAO;
  }

  public CompletionStage<WhitelistedAccountHolderAttributes> create(WhitelistedAccountHolderAttributes whitelistedAccountHolderAttributes) {
    return whitelistedAccountHolderAttributesDAO.insert(whitelistedAccountHolderAttributes)
        .thenCompose(ignore -> get(whitelistedAccountHolderAttributes.getAttributeName(),whitelistedAccountHolderAttributes.getIfiID()));
  }

  public CompletionStage<WhitelistedAccountHolderAttributes> update(WhitelistedAccountHolderAttributes whitelistedAccountHolderAttributes) {
    return whitelistedAccountHolderAttributesDAO.update(whitelistedAccountHolderAttributes)
        .thenCompose(ignore -> get(whitelistedAccountHolderAttributes.getAttributeName(),whitelistedAccountHolderAttributes.getIfiID()));
  }

  public CompletionStage<WhitelistedAccountHolderAttributes> get(String attributeName, Long ifiID) {
    return whitelistedAccountHolderAttributesDAO.get(ifiID)
        .thenApply(whitelistedAccountHolderAttributes -> whitelistedAccountHolderAttributes.stream().filter(attribute -> attributeName.equalsIgnoreCase(attribute.getAttributeName())).findFirst())
        .thenApply(attributesOptional ->  attributesOptional.orElseThrow(() -> WhitelistedAccountHolderAttributesNotFoundException.invalidAttributeName(attributeName, ifiID)));
  }

  public CompletionStage<List<WhitelistedAccountHolderAttributes>> get(Long ifiID) {
    return whitelistedAccountHolderAttributesDAO.get(ifiID);
  }


}
