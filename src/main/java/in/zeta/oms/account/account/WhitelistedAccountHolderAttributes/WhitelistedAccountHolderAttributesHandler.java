package in.zeta.oms.account.account.WhitelistedAccountHolderAttributes;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import olympus.message.types.Request;

@EagerSingleton
public class WhitelistedAccountHolderAttributesHandler extends AccountServiceBaseRequestHandler {
  private final WhitelistedAccountHolderAttributesService whitelistedAccountHolderAttributesService;

  @Inject
  public WhitelistedAccountHolderAttributesHandler(ZetaHostMessagingService zetaHostMessagingService,
                                                   WhitelistedAccountHolderAttributesService whitelistedAccountHolderAttributesService) {
    super(zetaHostMessagingService);
    this.whitelistedAccountHolderAttributesService = whitelistedAccountHolderAttributesService;
  }

  @Authorized(anyOf = {"api.whitelistedAccountHolderAttributes.create", "api.whitelistedAccountHolderAttributes.admin"})
  public void on(AddWhitelistedAccountHolderAttributeRequest payload, Request<AddWhitelistedAccountHolderAttributeRequest> request) {
    onRequest(payload, request);
    WhitelistedAccountHolderAttributes whitelistedAccountHolderAttributes = WhitelistedAccountHolderAttributes.builder().attributePath(payload.getAttributePath()).attributeName(payload.getAttributeName()).ifiID(payload.getIfiID()).build();
    whitelistedAccountHolderAttributesService.create(whitelistedAccountHolderAttributes)
        .thenAccept(response -> onResult(response, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.whitelistedAccountHolderAttributes.get", "api.whitelistedAccountHolderAttributes.admin"})
  public void on(GetWhitelistedAccountHolderAttributesListRequest payload, Request<GetWhitelistedAccountHolderAttributesListRequest> request) {
    onRequest(payload, request);
    whitelistedAccountHolderAttributesService.get(payload.getIfiID())
        .thenAccept(response -> onResult(GetWhitelistedAccountHolderAttributesResponse.builder().attributes(response).build(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.whitelistedAccountHolderAttributes.get", "api.whitelistedAccountHolderAttributes.admin"})
  public void on(GetWhitelistedAccountHolderAttributesRequest payload, Request<GetWhitelistedAccountHolderAttributesRequest> request) {
    onRequest(payload, request);
    whitelistedAccountHolderAttributesService.get(payload.getAttributeName(), payload.getIfiID())
        .thenAccept(response -> onResult(response, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.whitelistedAccountHolderAttributes.update", "api.whitelistedAccountHolderAttributes.admin"})
  public void on(UpdateWhitelistedAccountHolderAttributesRequest payload, Request<UpdateWhitelistedAccountHolderAttributesRequest> request) {
    onRequest(payload, request);
    WhitelistedAccountHolderAttributes whitelistedAccountHolderAttributes = WhitelistedAccountHolderAttributes.builder().attributePath(payload.getAttributePath()).attributeName(payload.getAttributeName()).ifiID(payload.getIfiID()).build();
    whitelistedAccountHolderAttributesService.update(whitelistedAccountHolderAttributes)
        .thenAccept(response -> onResult(response, request))
        .exceptionally(t -> onError(request, t));
  }
}
