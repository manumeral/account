package in.zeta.oms.account.account;

public enum Status {
  ENABLED,
  DISABLED,
  DELETED,
  BLOCKED,
  CLOSED;
}
