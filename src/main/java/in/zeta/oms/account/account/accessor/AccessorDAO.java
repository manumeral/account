package in.zeta.oms.account.account.accessor;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.model.Accessor;
import in.zeta.oms.account.util.LoggerUtils;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_ARRAY_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static in.zeta.athenacommons.util.Utils.getPgArrayFormat;
import static in.zeta.athenacommons.util.Utils.mapPgArrayToList;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class AccessorDAO extends PostgresDAO implements RowMapper<Accessor> {

  private static final LoggerUtils loggerUtils = LoggerUtils.builder()
      .logger(OlympusSpectra.getLogger(AccessorDAO.class))
      .build();

  //-----------------------------------------Column Names-----------------------------------------//
  private static final String TABLE_NAME = "accessor";
  private static final String ID = "id";
  private static final String IFI_ID = "ifi_id";
  private static final String ACCOUNT_ID = "account_id";
  private static final String ACCOUNT_HOLDER_ID= "account_holder_id";
  //TODO : Normalize this in a separate table, to ease operations on individual Txn Policy IDs
  private static final String TRANSACTION_POLICY_IDS = "transaction_policy_ids";
  private static final String STATUS = "status";
  private static final String ATTRIBUTES = "attributes";
  private static final String CREATED_AT = "created_at";
  private static final String UPDATED_AT = "modified_at"; //TODO: Make this column name same across all services

  private static final List<String> INSERT_COLUMNS = new ImmutableList.Builder<String>()
      .add(ID)
      .add(IFI_ID)
      .add(ACCOUNT_ID)
      .add(ACCOUNT_HOLDER_ID)
      .add(TRANSACTION_POLICY_IDS)
      .add(STATUS)
      .add(ATTRIBUTES)
      .build();

  private static final List<String> SELECT_COLUMNS = new ImmutableList.Builder<String>()
      .addAll(INSERT_COLUMNS)
      .add(CREATED_AT)
      .add(UPDATED_AT)
      .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
      .tableName(TABLE_NAME)
      .insertColumns(INSERT_COLUMNS)
      .selectColumns(SELECT_COLUMNS)
      .customColumnType(ID, UUID_TYPE)
      .customColumnType(ACCOUNT_ID, UUID_TYPE)
      .customColumnType(ACCOUNT_HOLDER_ID, UUID_TYPE)
      .customColumnType(TRANSACTION_POLICY_IDS, UUID_ARRAY_TYPE)
      .customColumnType(ATTRIBUTES, JSONB_TYPE)
      .build();

  private final Gson gson;

  @Inject
  public AccessorDAO(ZetaHostMessagingService hostMessagingService,
                          BasicDataSource basicDataSource,
                          @Named("postgres.jdbc.pool.size") int poolSize,
                          Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  public PostgresInstruction getInsertInstruction(@NonNull Accessor accessor) {
    List<Object> args = new ArrayList<>();
    args.add(accessor.getId());
    args.add(accessor.getIfiID());
    args.add(accessor.getAccountID());
    args.add(accessor.getAccountHolderID());
    args.add(getPgArrayFormat(accessor.getTransactionPolicyIDs()));
    args.add(accessor.getStatus());
    args.add(getJsonbObject(gson.toJson(accessor.getAttributes())));

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
  }

  @TimeLogger
  public CompletionStage<Void> insert(@NonNull Accessor accessor) {
    return update(getInsertInstruction(accessor))
        .thenAccept(ignore -> {});
  }

  @TimeLogger
  public CompletionStage<Optional<Accessor>> get(@NonNull String id,@NonNull String accountID,@NonNull Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(id);
    args.add(accountID);
    args.add(ifiID);

    final ImmutableList<String> criteria = new ImmutableList.Builder<String>()
        .add(ID)
        .add(ACCOUNT_ID)
        .add(IFI_ID)
        .build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(throwable -> {
          throwable = unwrapCompletionStateException(throwable);
          throw loggerUtils.logDBException(
              String.format("Get Accessor failed for accessorID: %s and accountID: %s and ifiID : %s", id, accountID, ifiID), (Throwable) throwable);
        });
  }

  @TimeLogger
  public CompletionStage<List<Accessor>> getAccessors(@NonNull String accountID,@NonNull Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(accountID);
    args.add(ifiID);

    final ImmutableList<String> criteria = new ImmutableList.Builder<String>()
        .add(ACCOUNT_ID)
        .add(IFI_ID)
        .build();

    return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(throwable -> {
          throwable = unwrapCompletionStateException(throwable);
          throw loggerUtils.logDBException(
              String.format("Get Accessors failed for accountID: %s, ifiID : %s", accountID, ifiID), throwable);
        });
  }

  @TimeLogger
  public CompletionStage<Void> update(@NonNull Accessor accessor) {
    final List<String> whereClauseColumnNames = Arrays.asList(ID, ACCOUNT_ID, IFI_ID);
    ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
    ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
    if(!Strings.isNullOrEmpty(accessor.getStatus())) {
      updateColumnNames.add(STATUS);
      argsList.add(accessor.getStatus());
    }
    if(null != accessor.getAttributes()) {
      updateColumnNames.add(ATTRIBUTES);
      argsList.add(getJsonbObject(gson.toJson(accessor.getAttributes())));
    }
    if(null != accessor.getTransactionPolicyIDs()) {
      updateColumnNames.add(TRANSACTION_POLICY_IDS);
      argsList.add(getPgArrayFormat(accessor.getTransactionPolicyIDs()));
    }
    argsList.add(accessor.getId());
    argsList.add(accessor.getAccountID());
    argsList.add(accessor.getIfiID());
    return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(updateColumnNames.build(), whereClauseColumnNames), argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(throwable -> {
          throwable = unwrapCompletionStateException(throwable);
          throw loggerUtils.logDBException(String.format("Error Updating accessorID : %s for accountID : %s",accessor.getId(),accessor.getAccountID()), throwable);
        });
  }

  @Override
  public Accessor mapRow(ResultSet rs, int rowNum) throws SQLException {
    return Accessor.builder()
        .id(rs.getString(ID))
        .ifiID(rs.getLong(IFI_ID))
        .accountHolderID(rs.getString(ACCOUNT_HOLDER_ID))
        .transactionPolicyIDs(mapPgArrayToList(rs.getArray(TRANSACTION_POLICY_IDS)))
        .accountID(rs.getString(ACCOUNT_ID))
        .status(rs.getString(STATUS))
        .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .createdAt(rs.getTimestamp(CREATED_AT))
        .updatedAt(rs.getTimestamp(UPDATED_AT))
        .build();
  }
}
