package in.zeta.oms.account.account;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_ARRAY_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static in.zeta.athenacommons.util.Utils.getPgArrayFormat;
import static in.zeta.athenacommons.util.Utils.mapPgArrayToList;
import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.util.LoggerUtils;
import in.zeta.tags.model.Tag;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

@Singleton
public class AccountDAO extends PostgresDAO implements RowMapper<Account> {

  private static final LoggerUtils loggerUtils = LoggerUtils.builder()
      .logger(OlympusSpectra.getLogger(AccountDAO.class))
      .build();

  //-----------------------------------------Column Names-----------------------------------------//
  private static final String TABLE_NAME = "account";
  private static final String REQUEST_ID = "request_id";
  private static final String ID = "id";
  private static final String IFI_ID = "ifi_id";
  private static final String PRODUCT_FAMILY_ID = "product_family_id";
  private static final String PROGRAMS = "program_ids";
  private static final String PRODUCT_ID = "product_id";
  private static final String LEDGER_ID = "ledger_id";
  private static final String ACCOUNT_PROVIDER_ID = "account_provider_id";
  private static final String ACCOUNT_HOLDER_ID = "account_holder_id";
  private static final String ATTRIBUTES = "attributes";
  private static final String STATUS = "status";
  private static final String NAME = "name";
  private static final String CREATED_AT = "created_at";
  private static final String UPDATED_AT = "modified_at"; //TODO: Make this column name same across all services

  private static final List<String> INSERT_COLUMNS = new ImmutableList.Builder<String>()
      .add(REQUEST_ID)
      .add(ID)
      .add(IFI_ID)
      .add(PRODUCT_FAMILY_ID)
      .add(PRODUCT_ID)
      .add(PROGRAMS)
      .add(ACCOUNT_HOLDER_ID)
      .add(ACCOUNT_PROVIDER_ID)
      .add(LEDGER_ID)
      .add(ATTRIBUTES)
      .add(STATUS)
      .add(NAME)
      .build();

  private static final List<String> SELECT_COLUMNS = new ImmutableList.Builder<String>()
      .addAll(INSERT_COLUMNS)
      .add(CREATED_AT)
      .add(UPDATED_AT)
      .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
      .tableName(TABLE_NAME)
      .insertColumns(INSERT_COLUMNS)
      .selectColumns(SELECT_COLUMNS)
      .customColumnType(PROGRAMS, UUID_ARRAY_TYPE)
      .customColumnType(ID, UUID_TYPE)
      .customColumnType(ACCOUNT_PROVIDER_ID, UUID_TYPE)
      .customColumnType(ACCOUNT_HOLDER_ID, UUID_TYPE)
      .customColumnType(ATTRIBUTES, JSONB_TYPE)
      .build();

  private final Gson gson;

  @Inject
  public AccountDAO(ZetaHostMessagingService hostMessagingService,
                    BasicDataSource basicDataSource,
                    @Named("postgres.jdbc.pool.size") int poolSize,
                    Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  @TimeLogger
  public CompletionStage<String> insert(@NonNull Account account,@NonNull String requestID) {
    return update(getInsertInstruction(account, requestID))
        .thenApply(ignore -> account.getId())
        .exceptionally(throwable -> {
            throwable = unwrapCompletionStateException(throwable);
            throw loggerUtils.logDBException(String.format(
                "Failed to add account to account holder id %s", account.getOwnerAccountHolderID()), throwable);
        });
  }

  @TimeLogger
  public PostgresInstruction getInsertInstruction(@NonNull Account account,@NonNull String requestID) {
    List<Object> args = new ArrayList<>();
    args.add(requestID);
    args.add(account.getId());
    args.add(account.getIfiID());
    args.add(account.getProductFamilyID());
    args.add(account.getProductID());
    args.add(getPgArrayFormat(account.getProgramIDs()));
    args.add(account.getOwnerAccountHolderID());
    args.add(account.getAccountProviderID());
    args.add(account.getLedgerID());
    args.add(getJsonbObject(gson.toJson(account.getAttributes())));
    args.add(account.getStatus());
    args.add(account.getName());

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
  }

  @TimeLogger
  public CompletionStage<Void> update(@NonNull Account account) {
    final List<String> whereClauseColumnNames = Arrays.asList(ID, IFI_ID);
    ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
    ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
    if(!Strings.isNullOrEmpty(account.getStatus())) {
      updateColumnNames.add(STATUS);
      argsList.add(account.getStatus());
    }
    if(null != account.getAttributes()) {
      updateColumnNames.add(ATTRIBUTES);
      argsList.add(getJsonbObject(gson.toJson(account.getAttributes())));
    }
    if(!Strings.isNullOrEmpty(account.getOwnerAccountHolderID())) {
      updateColumnNames.add(ACCOUNT_HOLDER_ID);
      argsList.add(account.getOwnerAccountHolderID());
    }
    if(account.getProductID() > 0) {
      updateColumnNames.add(PRODUCT_ID);
      argsList.add(account.getProductID());
    }

    if(!Strings.isNullOrEmpty(account.getName())) {
      updateColumnNames.add(NAME);
      argsList.add(account.getName());
    }

    argsList.add(account.getId());
    argsList.add(account.getIfiID());
    return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(updateColumnNames.build(), whereClauseColumnNames), argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(throwable -> {
          throw loggerUtils.logDBException(String.format("Error Updating accountID : %s for ifiID : %s",account.getId(), account.getIfiID()), throwable);
        });
  }

  @TimeLogger
  public CompletionStage<Optional<Account>> get(@NonNull String id,@NonNull Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(id);
    args.add(ifiID);

    final ImmutableList<String> criteria = new Builder<String>()
        .add(ID)
        .add(IFI_ID)
        .build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(t -> {
          throw loggerUtils.logDBException(
              String.format("Get Account failed by accountId: %s, ifiId: %s", id, ifiID), t);
        });
  }

  @TimeLogger
  public CompletionStage<List<Account>> getAccounts(@NonNull Long ifiID,
                                                    String accountHolderID,
                                                    String accountProviderID,
                                                    String status,
                                                    Long productID,
                                                    Long productFamilyID,
                                                    @NonNull Long pageNumber,
                                                    @NonNull Long pageSize,
                                                    Tag tag) {

    if (tag == null) {
      return getAccountsWithoutTags(ifiID,
          accountHolderID,
          accountProviderID,
          status,
          productID,
          productFamilyID,
          pageNumber,
          pageSize);
    }

    List<Object> args = new ArrayList<>();

    String accountAlias = "a.";
    Map<String, String> filterCols = new HashMap<>();

    args.add(tag.getValue());
    filterCols.put("t.tag_value", "");

    args.add(tag.getType());
    filterCols.put("t.tag_type", "");

    args.add(ifiID);
    filterCols.put(accountAlias + IFI_ID, "");

    if (!Strings.isNullOrEmpty(accountProviderID)) {
      args.add(accountProviderID);
      filterCols.put(accountAlias + ACCOUNT_PROVIDER_ID, "::UUID");
    }

    if (!Strings.isNullOrEmpty(accountHolderID)) {
      args.add(accountHolderID);
      filterCols.put(accountAlias + ACCOUNT_HOLDER_ID, "::UUID");
    }

    if (!Strings.isNullOrEmpty(status)) {
      args.add(status);
      filterCols.put(accountAlias + STATUS, "");
    }

    if (null != productID) {
      args.add(productID);
      filterCols.put(accountAlias + PRODUCT_ID, "");
    }

    if (null != productFamilyID) {
      args.add(productFamilyID);
      filterCols.put(accountAlias + PRODUCT_FAMILY_ID, "");
    }

    String selectClause = "SELECT " + SELECT_COLUMNS.stream().map(col -> "a." + col).collect(Collectors.joining(","));
    String fromClause = " FROM account a INNER JOIN tags t ON (a.ifi_id = t.ifi_id AND t.object_type = 'account' AND t.object_id = a.id::text)";
    String whereClause = " WHERE " + filterCols.entrySet().stream().map(entry -> entry.getKey() + " = ?"+ entry.getValue()).collect(Collectors.joining(" AND "));

    String query = selectClause + fromClause + whereClause + " order by created_at desc LIMIT ? offset ? ";
    args.add(pageSize);
    args.add((pageNumber - 1) * pageSize);

    return query(query, this,args)
        .exceptionally(throwable -> {
      throwable = unwrapCompletionStateException(throwable);
      throw loggerUtils.logDBException(String.format("Get Accounts failed for ifiID: %s failed", ifiID), throwable);
    });
  }

  @TimeLogger
  public CompletionStage<List<Account>> getAccountsWithoutTags(@NonNull Long ifiID,
      String accountHolderID,
      String accountProviderID,
      String status,
      Long productID,
      Long productFamilyID,
      @NonNull Long pageNumber,
      @NonNull Long pageSize) {


    List<Object> args = new ArrayList<>();
    final List<String> selectionCriteria = new ArrayList<>();
    args.add(ifiID);
    selectionCriteria.add(IFI_ID);

    if (!Strings.isNullOrEmpty(accountProviderID)) {
      args.add(accountProviderID);
      selectionCriteria.add(ACCOUNT_PROVIDER_ID);
    }

    if (!Strings.isNullOrEmpty(accountHolderID)) {
      args.add(accountHolderID);
      selectionCriteria.add(ACCOUNT_HOLDER_ID);
    }

    if (!Strings.isNullOrEmpty(status)) {
      args.add(status);
      selectionCriteria.add(STATUS);
    }

    if (null != productID) {
      args.add(productID);
      selectionCriteria.add(PRODUCT_ID);
    }

    if (null != productFamilyID) {
      args.add(productFamilyID);
      selectionCriteria.add(PRODUCT_FAMILY_ID);
    }

    String query = PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionCriteria);
    query = query + " order by created_at desc LIMIT ? offset ? ";
    args.add(pageSize);
    args.add((pageNumber - 1) * pageSize);

    return query(query, this,args)
        .exceptionally(throwable -> {
          throwable = unwrapCompletionStateException(throwable);
          throw loggerUtils.logDBException(String.format("Get Accounts failed for ifiID: %s failed", ifiID), throwable);
        });
  }

  public CompletionStage<Optional<Account>> getAccountWithRequestID(String requestID, String accountProviderID, Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(requestID);
    args.add(ifiID);
    args.add(accountProviderID);

    final ImmutableList<String> criteria = new Builder<String>()
        .add(REQUEST_ID)
        .add(IFI_ID)
        .add(ACCOUNT_PROVIDER_ID)
        .build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(t -> {
          throw loggerUtils.logDBException(
              String.format("Get Account failed by requestID: %s, ifiId: %s", requestID, ifiID), t);
        });

  }

  public CompletionStage<Void> bulkInsertAccount(List<Account> accounts) {
    return batchUpdate(
            "INSERT INTO account (id, ifi_id, ledger_id, account_holder_id, product_family_id, product_id, program_ids, "
                + "                     attributes, status, account_provider_id, created_at, modified_at, name, request_id) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            new BatchPreparedStatementSetter() {
              @Override
              public void setValues(final PreparedStatement preparedStatement, final int i)
                  throws SQLException {
                Account account = accounts.get(i);
                preparedStatement.setObject(1, UUID.fromString(account.getId()));
                preparedStatement.setLong(2, account.getIfiID());
                preparedStatement.setLong(3, account.getLedgerID());
                preparedStatement.setObject(4, UUID.fromString(account.getOwnerAccountHolderID()));
                preparedStatement.setLong(5, account.getProductFamilyID());
                preparedStatement.setLong(6, account.getProductID());
                preparedStatement.setObject(7, account.getProgramIDs().toArray(new String[]{}));
                preparedStatement.setObject(8, getJsonbObject(gson.toJson(account.getAttributes())));
                preparedStatement.setString(9, account.getStatus());
                preparedStatement.setObject(10, UUID.fromString(account.getAccountProviderID()));
                preparedStatement.setDate(11, getDate(account.getCreatedAt()));
                preparedStatement.setDate(12, getDate(account.getUpdatedAt()));
                preparedStatement.setString(13, account.getName());
                preparedStatement.setString(14, "MIGRATION_" + account.getCardID());
              }

              private java.sql.Date getDate(LocalDateTime localDateTime) {
                return new java.sql.Date(Date
                    .from(localDateTime.atZone( ZoneId.systemDefault()).toInstant()).getTime());
              }

              @Override
              public int getBatchSize() {
                return accounts.size();
              }
            })
        .thenAccept(__ -> {});
  }

  @Override
  public Account mapRow(ResultSet rs, int i) throws SQLException {
    return Account.builder()
        .id(rs.getString(ID))
        .accountProviderID(rs.getString(ACCOUNT_PROVIDER_ID))
        .ifiID(rs.getLong(IFI_ID))
        .productFamilyID(rs.getLong(PRODUCT_FAMILY_ID))
        .productID(rs.getLong(PRODUCT_ID))
        .programIDs(mapPgArrayToList(rs.getArray(PROGRAMS)))
        .status(rs.getString(STATUS))
        .ownerAccountHolderID(rs.getString(ACCOUNT_HOLDER_ID))
        .ledgerID(rs.getLong(LEDGER_ID))
        .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .name(rs.getString(NAME))
        .createdAt(rs.getTimestamp(CREATED_AT).toLocalDateTime())
        .updatedAt(rs.getTimestamp(UPDATED_AT).toLocalDateTime())
        .build();
  }


}
