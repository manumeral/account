package in.zeta.oms.account.account.accessor;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.api.exception.AccessorCreationException;
import in.zeta.oms.account.api.exception.AccessorNotFoundException;
import in.zeta.oms.account.api.model.Accessor;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;

import java.util.List;
import java.util.concurrent.CompletionStage;

import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class AccessorService {
  private static final SpectraLogger logger = OlympusSpectra.getLogger(AccessorService.class);

  private final AccessorDAO accessorDAO;
  private final SecureRandomGenerator secureRandomGenerator;

  @Inject
  public AccessorService(AccessorDAO accessorDAO,
                         SecureRandomGenerator secureRandomGenerator) {
    this.accessorDAO = accessorDAO;
    this.secureRandomGenerator = secureRandomGenerator;
  }

  public CompletionStage<List<Accessor>> getAccessors(String accountID, Long ifiID) {
    return accessorDAO.getAccessors(accountID, ifiID);
  }

  @PublishEvent(topicName = PubSubTopics.ADD_ACCOUNT_ACCESSOR_EVENT_TOPIC)
  public CompletionStage<Accessor> addAccessor(Accessor accessor) {
    final Accessor accessorToAdd = accessor.toBuilder().id(secureRandomGenerator.generateUUID()).build();
    return accessorDAO
        .insert(accessorToAdd)
        .thenCompose(
            ignore ->
                get(accessorToAdd.getId(), accessorToAdd.getAccountID(), accessorToAdd.getIfiID()))
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              logger.warn("Accessor creation failed")
                  .attr("accountID", accessor.getAccountID())
                  .attr("ifiID", accessor.getIfiID())
                  .attr("accountHolderID", accessor.getAccountHolderID())
                  .attr("errMsg", t.getMessage())
                  .log();
              throw new AccessorCreationException(
                  String.format(
                      "Accessor Creation failed for account ID : %s, and ifiID : %s",
                      accessorToAdd.getAccountID(), accessorToAdd.getIfiID()),
                  t,
                  AccountErrorCode.INTERNAL_ERROR);
            });
  }

  public CompletionStage<Accessor> get(String id, String accountID, Long ifiID) {
    return accessorDAO.get(id, accountID, ifiID)
        .thenApply(
            accessorOptional ->
                accessorOptional.orElseThrow(
                    () ->
                        new AccessorNotFoundException(
                            String.format(
                                "Accessor for account ID : %s, and accessor id : %s, ifiID : %s not found",
                                accountID, id, ifiID),
                            AccountErrorCode.ACCESSOR_NOT_FOUND)));
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_ACCESSOR_EVENT_TOPIC)
  public CompletionStage<Accessor> update(Accessor accessor) {
    return accessorDAO.update(accessor)
        .thenCompose(ignore -> get(accessor.getId(), accessor.getAccountID(), accessor.getIfiID()));
  }
}
