package in.zeta.oms.account.account.vector;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.model.AccountVector;
import in.zeta.oms.account.util.LoggerUtils;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import javax.inject.Singleton;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class AccountVectorDAO extends PostgresDAO implements RowMapper<AccountVector> {
  private static final LoggerUtils loggerUtils =
      LoggerUtils.builder().logger(OlympusSpectra.getLogger(AccountVectorDAO.class)).build();

  // -----------------------------------------Column
  // Names-----------------------------------------//
  private static final String TABLE_NAME = "account_vector";
  private static final String ID = "id";
  private static final String IFI_ID = "ifi_id";
  private static final String ACCOUNT_ID = "account_id";
  private static final String TYPE = "type";
  private static final String VALUE = "value";
  private static final String STATUS = "status";
  private static final String ATTRIBUTES = "attributes";
  private static final String CREATED_AT = "created_at";
  private static final String UPDATED_AT =
      "modified_at"; // TODO: Make this column name same across all services

  private static final List<String> INSERT_COLUMNS =
      new ImmutableList.Builder<String>()
          .add(ID)
          .add(IFI_ID)
          .add(ACCOUNT_ID)
          .add(TYPE)
          .add(VALUE)
          .add(STATUS)
          .add(ATTRIBUTES)
          .build();

  private static final List<String> SELECT_COLUMNS =
      new ImmutableList.Builder<String>()
          .addAll(INSERT_COLUMNS)
          .add(CREATED_AT)
          .add(UPDATED_AT)
          .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR =
      PgQueryGenerator.builder()
          .tableName(TABLE_NAME)
          .insertColumns(INSERT_COLUMNS)
          .selectColumns(SELECT_COLUMNS)
          .customColumnType(ID, UUID_TYPE)
          .customColumnType(ACCOUNT_ID, UUID_TYPE)
          .customColumnType(ATTRIBUTES, JSONB_TYPE)
          .build();

  private final Gson gson;

  @Inject
  public AccountVectorDAO(
      ZetaHostMessagingService hostMessagingService,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  @TimeLogger
  public CompletionStage<Void> insert(@NonNull AccountVector accountVector) {
    return update(getInsertInstruction(accountVector)).thenAccept(ignore -> {});
  }

  public PostgresInstruction getInsertInstruction(AccountVector accountVector) {
    List<Object> args = new ArrayList<>();
    args.add(accountVector.getId());
    args.add(accountVector.getIfiID());
    args.add(accountVector.getAccountID());
    args.add(accountVector.getType());
    args.add(accountVector.getValue());
    args.add(accountVector.getStatus());
    args.add(getJsonbObject(gson.toJson(accountVector.getAttributes())));

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
  }

  @TimeLogger
  public CompletionStage<Optional<AccountVector>> get(
      @NonNull String id, @NonNull String accountID, @NonNull Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(id);
    args.add(accountID);
    args.add(ifiID);

    final ImmutableList<String> criteria =
        new ImmutableList.Builder<String>().add(ID).add(ACCOUNT_ID).add(IFI_ID).build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Get AccountVectors failed for accountVectorID: %s", id),
                  throwable);
            });
  }

  @TimeLogger
  public CompletionStage<Optional<AccountVector>> get(
      @NonNull String type,@NonNull String value,@NonNull Long ifiID,@NonNull String accountID) {
    List<Object> args = new ArrayList<>();
    args.add(type);
    args.add(value);
    args.add(ifiID);
    args.add(accountID);

    final ImmutableList<String> criteria =
        new ImmutableList.Builder<String>()
            .add(TYPE)
            .add(VALUE)
            .add(IFI_ID)
            .add(ACCOUNT_ID)
            .build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Get AccountVectors failed for account with type: %s value : %s ifiID : %s accountID : %s",
                      type, value, ifiID, accountID),
                  throwable);
            });
  }

  @TimeLogger
  public CompletionStage<List<AccountVector>> getVectors(
      String accountID, Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(accountID);
    args.add(ifiID);

    final ImmutableList<String> criteria =
        new ImmutableList.Builder<String>().add(ACCOUNT_ID).add(IFI_ID).build();

    return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Get AccountVectors failed for account: %s and ifiID : %s", accountID, ifiID),
                  throwable);
            });
  }

  @TimeLogger
  public CompletionStage<Void> update(AccountVector accountVector) {
    final List<String> whereClauseColumnNames = Arrays.asList(ID, ACCOUNT_ID, IFI_ID);
    ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
    ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
    if (!Strings.isNullOrEmpty(accountVector.getType())) {
      updateColumnNames.add(TYPE);
      argsList.add(accountVector.getType());
    }
    if (null != accountVector.getAttributes()) {
      updateColumnNames.add(ATTRIBUTES);
      argsList.add(getJsonbObject(gson.toJson(accountVector.getAttributes())));
    }
    if (!Strings.isNullOrEmpty(accountVector.getStatus())) {
      updateColumnNames.add(STATUS);
      argsList.add(accountVector.getStatus());
    }

    if (!Strings.isNullOrEmpty(accountVector.getValue())) {
      updateColumnNames.add(VALUE);
      argsList.add(accountVector.getValue());
    }

    argsList.add(accountVector.getId());
    argsList.add(accountVector.getAccountID());
    argsList.add(accountVector.getIfiID());
    return update(
            PG_QUERY_GENERATOR.getUpdateQueryAndJoin(
                updateColumnNames.build(), whereClauseColumnNames),
            argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Error Updating vectorID : %s for accountID : %s and ifiID : %s",
                      accountVector.getId(),
                      accountVector.getAccountID(),
                      accountVector.getIfiID()),
                  throwable);
            });
  }

  @Override
  public AccountVector mapRow(ResultSet rs, int rowNum) throws SQLException {
    return AccountVector.builder()
        .id(rs.getString(ID))
        .ifiID(rs.getLong(IFI_ID))
        .accountID(rs.getString(ACCOUNT_ID))
        .type(rs.getString(TYPE))
        .value(rs.getString(VALUE))
        .status(rs.getString(STATUS))
        .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .createdAt(rs.getTimestamp(CREATED_AT))
        .updatedAt(rs.getTimestamp(UPDATED_AT))
        .build();
  }
}
