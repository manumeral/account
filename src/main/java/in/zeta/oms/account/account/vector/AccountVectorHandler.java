package in.zeta.oms.account.account.vector;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.account.accountVector.AddAccountVectorRequest;
import in.zeta.oms.account.account.accountVector.DeleteAccountVectorRequest;
import in.zeta.oms.account.account.accountVector.GetAccountVectorListRequest;
import in.zeta.oms.account.account.accountVector.GetAccountVectorListResponse;
import in.zeta.oms.account.account.accountVector.GetAccountVectorRequest;
import in.zeta.oms.account.account.accountVector.UpdateAccountVectorRequest;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import in.zeta.oms.account.api.model.AccountVector;
import olympus.message.types.Request;

@EagerSingleton
public class AccountVectorHandler extends AccountServiceBaseRequestHandler {

  private final AccountVectorService accountVectorService;

  @Inject
  public AccountVectorHandler(
      ZetaHostMessagingService zetaHostMessagingService,
      AccountVectorService accountVectorService) {
    super(zetaHostMessagingService);
    this.accountVectorService = accountVectorService;
  }

  @Authorized(anyOf = {"api.account.accountVector.add", "api.account.admin"})
  public void on(AddAccountVectorRequest payload, Request<AddAccountVectorRequest> request) {
    onRequest(payload, request);
    AccountVector accountVector =
        AccountVector.builder()
            .accountID(payload.getAccountID())
            .attributes(payload.getAttributes())
            .status(payload.getStatus())
            .type(payload.getType())
            .value(payload.getValue())
            .ifiID(payload.getIfiID())
            .build();

    accountVectorService
        .addVector(accountVector)
        .thenAccept(accountVectorResponse -> onResult(accountVectorResponse, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountVector.get", "api.account.admin"})
  public void on(
      GetAccountVectorListRequest payload, Request<GetAccountVectorListRequest> request) {
    onRequest(payload, request);
    accountVectorService
        .getVectors(payload.getAccountID(), payload.getIfiID())
        .thenAccept(
            accountVectorResponse ->
                onResult(
                    GetAccountVectorListResponse.builder()
                        .accountVectors(accountVectorResponse)
                        .build(),
                    request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountVector.get", "api.account.admin"})
  public void on(GetAccountVectorRequest payload, Request<GetAccountVectorRequest> request) {
    onRequest(payload, request);
    accountVectorService
        .get(payload.getAccountVectorID(), payload.getAccountID(), payload.getIfiID())
        .thenAccept(accountVectorResponse ->  onResult(accountVectorResponse, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountVector.update", "api.account.admin"})
  public void on(UpdateAccountVectorRequest payload, Request<UpdateAccountVectorRequest> request) {
    onRequest(payload, request);
    AccountVector accountVector = AccountVector.builder()
        .ifiID(payload.getIfiID())
        .accountID(payload.getAccountID())
        .id(payload.getAccountVectorID())
        .type(payload.getType())
        .value(payload.getValue())
        .status(payload.getStatus())
        .attributes(payload.getAttributes())
        .build();
    accountVectorService
        .update(accountVector)
        .thenAccept(accountVectorResponse ->  onResult(accountVectorResponse, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountVector.delete", "api.account.admin"})
  public void on(DeleteAccountVectorRequest payload, Request<DeleteAccountVectorRequest> request) {
    onRequest(payload, request);
    AccountVector accountVector = AccountVector.builder()
        .ifiID(payload.getIfiID())
        .accountID(payload.getAccountID())
        .id(payload.getAccountVectorID())
        .status(Status.DELETED.name())
        .build();
    accountVectorService
        .update(accountVector)
        .thenAccept(accountVectorResponse ->  onResult(accountVectorResponse, request))
        .exceptionally(t -> onError(request, t));
  }
}
