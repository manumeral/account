package in.zeta.oms.account.account;

import com.google.gson.JsonObject;

import java.util.Map;
import java.util.Optional;

public class LedgerPolicyConfig {
  private static final String WITH_PAN = "WITH_PAN";
  private static final String WITHOUT_PAN = "WITHOUT_PAN";
  private static final String WITH_ATM = "WITH_ATM";
  private static final String DEFAULT = "DEFAULT";

  private Map<String, JsonObject> config;

  public LedgerPolicyConfig(Map<String, JsonObject> config) {
    this.config = config;
  }

  public Optional<String> getIfiProductType(String kycStatus, boolean hasPan, boolean hasAtm) {
    if (kycStatus == null) {
      if (config.get(DEFAULT) == null) {
        return Optional.empty();
      }
      return Optional.ofNullable(config.get(DEFAULT).getAsJsonPrimitive().getAsString());
    }
    JsonObject kycConfig = config.get(kycStatus);

    if (kycConfig == null) {
      return Optional.empty();
    }

    JsonObject panConfig = kycConfig.get(hasPan ? WITH_PAN : WITHOUT_PAN).getAsJsonObject();

    String ifiProductType = Optional.ofNullable(panConfig.get(hasAtm ? WITH_ATM : DEFAULT))
        .orElse(panConfig.get(DEFAULT))
        .getAsString();

    return Optional.of(ifiProductType);
  }
}
