package in.zeta.oms.account.account.WhitelistedAccountHolderAttributes;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.util.LoggerUtils;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;

@Singleton
public class WhitelistedAccountHolderAttributesDAO extends PostgresDAO
    implements RowMapper<WhitelistedAccountHolderAttributes> {

  private static final LoggerUtils loggerUtils =
      LoggerUtils.builder()
          .logger(OlympusSpectra.getLogger(WhitelistedAccountHolderAttributesDAO.class))
          .build();

  // -----------------------------------------Column Names-----------------------------------------//
  private static final String TABLE_NAME = "whitelisted_account_holder_attributes";
  private static final String IFI_ID = "ifi_id";
  private static final String ATTRIBUTE_PATH = "attribute_path";
  private static final String ATTRIBUTE_NAME = "attribute_name";
  private static final String CREATED_AT = "created_at";
  private static final String UPDATED_AT =
      "modified_at"; // TODO: Make this column name same across all services

  private static final List<String> INSERT_COLUMNS = new ImmutableList.Builder<String>()
      .add(IFI_ID)
      .add(ATTRIBUTE_PATH)
      .add(ATTRIBUTE_NAME)
      .build();

  private static final List<String> SELECT_COLUMNS =
      new ImmutableList.Builder<String>()
          .addAll(INSERT_COLUMNS)
          .add(CREATED_AT)
          .add(UPDATED_AT)
          .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR =
      PgQueryGenerator.builder()
          .tableName(TABLE_NAME)
          .insertColumns(INSERT_COLUMNS)
          .selectColumns(SELECT_COLUMNS)
          .build();


  //TODO : Add Caching here in a separate diff

  @Inject
  public WhitelistedAccountHolderAttributesDAO(
      ZetaHostMessagingService hostMessagingService,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize) {
    super(hostMessagingService, basicDataSource, poolSize);
  }

  // -----------------------------------------DB Operations-----------------------------------------//


  @TimeLogger
  public CompletionStage<Void> insert(@NonNull WhitelistedAccountHolderAttributes attributes) {
    return update(getInsertInstruction(attributes)).thenApply(ignore -> null)
        .thenAccept(ignore -> {})
        .exceptionally(throwable -> {
          throw loggerUtils.logDBException(String.format("Error Inserting whitelistedAccountHolderAttribute name : %s for ifiID : %s",attributes.getAttributeName(), attributes.getIfiID()), throwable);
        });
  }

  @TimeLogger
  public CompletionStage<Void> update(@NonNull WhitelistedAccountHolderAttributes attributes) {
    final List<String> whereClauseColumnNames = Arrays.asList(ATTRIBUTE_NAME, IFI_ID);
    ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
    ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
    updateColumnNames.add(ATTRIBUTE_PATH);
    argsList.add(attributes.getAttributePath());
    argsList.add(attributes.getAttributeName());
    argsList.add(attributes.getIfiID());
    return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(updateColumnNames.build(), whereClauseColumnNames), argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(throwable -> {
          throw loggerUtils.logDBException(String.format("Error Updating whitelistedAccountHolderAttribute name : %s for ifiID : %s",attributes.getAttributeName(), attributes.getIfiID()), throwable);
        });
  }

  @TimeLogger
  public PostgresInstruction getInsertInstruction(
      @NonNull WhitelistedAccountHolderAttributes attributes) {
    List<Object> args = new ArrayList<>();
    args.add(attributes.getIfiID());
    args.add(attributes.getAttributePath());
    args.add(attributes.getAttributeName());
    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
  }

  @TimeLogger
  public CompletionStage<List<WhitelistedAccountHolderAttributes>> get(
      @NonNull Long ifiID) {
    List<String> columnNames = new ArrayList<>();
    columnNames.add(IFI_ID);
    return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(columnNames), this, ifiID);
  }

  @Override
  public WhitelistedAccountHolderAttributes mapRow(ResultSet rs, int rowNum) throws SQLException {
    return WhitelistedAccountHolderAttributes.builder()
        .attributeName(rs.getString(ATTRIBUTE_NAME))
        .attributePath(rs.getString(ATTRIBUTE_PATH))
        .ifiID(rs.getLong(IFI_ID))
        .build();
  }
}
