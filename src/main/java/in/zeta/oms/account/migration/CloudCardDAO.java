package in.zeta.oms.account.migration;

import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;

import com.google.common.base.Joiner;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.cloudcard.api.enums.CardStatus;
import in.zeta.oms.cloudcard.api.exceptions.CloudCardException;
import in.zeta.oms.cloudcard.api.model.CloudCard;
import in.zeta.oms.cloudcard.model.CardType;
import in.zeta.oms.cloudcard.model.ProductType;
import in.zeta.spectra.capture.SpectraLogger;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;

@Singleton
public class CloudCardDAO extends PostgresDAO {

  public static final Type MAP_TYPE = new TypeToken<Map<String, String>>() {}.getType();
  private static final SpectraLogger log = OlympusSpectra.getLogger(CloudCardDAO.class);
  private static final String TABLE_NAME = "cloud_card";
  private static final String COL_CARD_ID = "card_id";
  private static final String COL_CARD_PROGRAM_ID = "card_program_id";
  public static final String COL_USER_ID = "user_id";
  private static final String COL_RECIPIENT_LEDGER_ID = "recipient_ledger_id";
  private static final String COL_PRODUCT_TYPE = "product_type";
  private static final String COL_CARD_TYPE = "card_type";
  private static final String COL_ATTRIBUTES = "attributes";
  private static final String COL_CARD_STATUS = "status";
  private static final String COL_PRODUCT_ID = "product_id";
  private static final String COL_CREATED_AT = "created_at";
  private static final String COL_MODIFIED_AT = "modified_at";
  private static final String COL_ACCOUNT_ID = "account_id";
  private static final String COL_IFI_ID = "ifi_id";

  private static final List<String> SELECT_COLUMNS =
      Arrays.asList(
          COL_CARD_ID,
          COL_CARD_PROGRAM_ID,
          COL_USER_ID,
          COL_RECIPIENT_LEDGER_ID,
          COL_PRODUCT_TYPE,
          COL_CARD_TYPE,
          COL_ATTRIBUTES,
          COL_CARD_STATUS,
          COL_PRODUCT_ID,
          COL_CREATED_AT,
          COL_MODIFIED_AT,
          COL_ACCOUNT_ID,
          COL_IFI_ID);

  private static final String QUERY_GET_CARD_BY_ID =
      String.format(
          "SELECT %1$s from %2$s where card_id in (:%3$s)",
          Joiner.on(',').join(SELECT_COLUMNS), TABLE_NAME, COL_CARD_ID);
  private static final String QUERY_UPDATE_ACCOUNT_ID =
      String.format(
          "UPDATE %1$s SET %2$s = ?, %3$s = ? WHERE %4$s = ? ",
          TABLE_NAME, COL_ACCOUNT_ID, COL_IFI_ID, COL_CARD_ID);

  private final Gson gson;

  @Inject
  public CloudCardDAO(
      ZetaHostMessagingService hostMessagingService,
      @Named("cloudCardDataSource") BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  public CompletionStage<List<CloudCard>> getCards(Set<Long> cardIDs) {
    Map<String, Object> params = new HashMap<>();
    params.put(COL_CARD_ID, cardIDs);
    return queryWithINClause(QUERY_GET_CARD_BY_ID, (rs, rowNum) -> getCard(rs), params)
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              log.error("Error fetching card by cardIDs", t).attr("cardIDs", cardIDs).log();
              throw new CloudCardException(
                  String.format("Error gettings cloud cards for cardIDs: %s", cardIDs));
            });
  }

  public CompletionStage<Integer> updateAccountID(Long cardID, String accountID, Long ifiID) {

    return update(QUERY_UPDATE_ACCOUNT_ID, accountID, ifiID, cardID)
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              log.error("Error updating accountID for card", t)
                  .attr("accountID", accountID)
                  .attr("cardID", cardID)
                  .log();
              throw new CloudCardException(
                  String.format(
                      "Error updating cloud card accountID: %s for cardID: %s", cardID, accountID));
            });
  }

  private CloudCard getCard(ResultSet rs) throws SQLException {
    return CloudCard.builder()
        .cardID(rs.getLong(COL_CARD_ID))
        .cardProgramID(rs.getString(COL_CARD_PROGRAM_ID))
        .userID(rs.getLong(COL_USER_ID))
        .recipientLedgerID(rs.getLong(COL_RECIPIENT_LEDGER_ID))
        .cardType(CardType.getCardType(rs.getString(COL_CARD_TYPE)))
        .productType(ProductType.getProductType(rs.getString(COL_PRODUCT_TYPE)))
        .productID(rs.getLong(COL_PRODUCT_ID))
        .cardStatus(CardStatus.getCardStatus(rs.getString(COL_CARD_STATUS)))
        .attributes(gson.fromJson(rs.getString(COL_ATTRIBUTES), MAP_TYPE))
        .createdAt(rs.getTimestamp(COL_CREATED_AT).getTime())
        .accountID(rs.getString(COL_ACCOUNT_ID))
        .modifiedAt(rs.getTimestamp(COL_MODIFIED_AT).getTime())
        .ifiID(rs.getLong(COL_IFI_ID))
        .build();
  }
}
