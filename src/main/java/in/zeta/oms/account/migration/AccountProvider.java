package in.zeta.oms.account.migration;


import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AccountProvider {

  private final Long ifiID;
  private final String accountProviderID;

}
