package in.zeta.oms.account.migration;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.util.LoggerUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

@Singleton
public class AccountProviderDAO extends PostgresDAO implements RowMapper<AccountProvider> {

  private static final String TABLE_NAME = "ifi_account_provider_mapping";
  private static final String IFI_ID = "ifi_id";
  private static final String ACCOUNT_PROVIDER_ID = "account_provider_id";

  private static final String GET_FAILED =
      "Get ifi to account provider mapping failed for ifi id: %s";

  private static final LoggerUtils loggerUtils =
      LoggerUtils.builder().logger(OlympusSpectra.getLogger(AccountProviderDAO.class)).build();

  private static final List<String> INSERT_COLUMNS =
      new ImmutableList.Builder<String>().add(IFI_ID).add(ACCOUNT_PROVIDER_ID).build();

  private static final List<String> SELECT_COLUMNS =
      new ImmutableList.Builder<String>().addAll(INSERT_COLUMNS).build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR =
      PgQueryGenerator.builder()
          .tableName(TABLE_NAME)
          .insertColumns(INSERT_COLUMNS)
          .selectColumns(SELECT_COLUMNS)
          .build();

  @Inject
  public AccountProviderDAO(
      ZetaHostMessagingService zhms,
      @Named("cloudCardDataSource") BasicDataSource dataSource,
      @Named("postgres.jdbc.pool.size") int poolSize) {
    super(zhms, dataSource, poolSize);
  }

  @TimeLogger
  public CompletionStage<Optional<AccountProvider>> get(Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(ifiID);

    final ImmutableList<String> criteria = ImmutableList.of(IFI_ID);

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(
            t -> {
              throw loggerUtils.logDBException(String.format(GET_FAILED, ifiID), t);
            });
  }

  @Override
  public AccountProvider mapRow(ResultSet rs, int i) throws SQLException {
    return AccountProvider.builder()
        .accountProviderID(rs.getString(ACCOUNT_PROVIDER_ID))
        .ifiID(rs.getLong(IFI_ID))
        .build();
  }
}
