package in.zeta.oms.account.migration;

import static java.util.concurrent.CompletableFuture.completedFuture;

import com.google.gson.JsonObject;
import com.google.inject.Inject;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.concurrency.CompletableFutures;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.cosmosahpclient.exception.ZSIllegalArgumentException;
import in.zeta.oms.account.account.AccountService;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.api.model.AccountStatus;
import in.zeta.oms.account.constant.AccountConstant;
import in.zeta.oms.account.service.AthenaManagerService;
import in.zeta.oms.account.service.CloudCardService;
import in.zeta.oms.account.service.LedgerService;
import in.zeta.oms.athenamanager.api.model.Product;
import in.zeta.oms.cloudcard.api.CardProgram;
import in.zeta.oms.cloudcard.api.enums.CardStatus;
import in.zeta.oms.cloudcard.api.model.CloudCard;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import olympus.message.types.Request;

@EagerSingleton
public class AccountMigrationHandler extends AccountServiceBaseRequestHandler {

  private final CloudCardDAO cloudCardDAO;
  private final AccountProviderDAO accountProviderDAO;
  private final CloudCardService cloudCardService;
  private final AccountHolderVectorService accountHolderVectorService;
  private final SecureRandomGenerator secureRandomGenerator;
  private final AthenaManagerService athenaManagerService;
  private final LedgerService ledgerService;
  private final AccountService accountService;

  @Inject
  public AccountMigrationHandler(
      ZetaHostMessagingService zetaHostMessagingService,
      CloudCardDAO cloudCardDAO,
      AccountProviderDAO accountProviderDAO,
      CloudCardService cloudCardService,
      AccountHolderVectorService accountHolderVectorService,
      SecureRandomGenerator secureRandomGenerator,
      AthenaManagerService athenaManagerService,
      LedgerService ledgerService,
      AccountService accountService) {
    super(zetaHostMessagingService);
    this.cloudCardDAO = cloudCardDAO;
    this.accountProviderDAO = accountProviderDAO;
    this.cloudCardService = cloudCardService;
    this.accountHolderVectorService = accountHolderVectorService;
    this.secureRandomGenerator = secureRandomGenerator;
    this.athenaManagerService = athenaManagerService;
    this.ledgerService = ledgerService;
    this.accountService = accountService;
  }

  @Authorized(anyOf = {"api.account.migration.cc2account"})
  public void on(
      InsertAccountsForCardsRequest payload, Request<InsertAccountsForCardsRequest> request) {
    onRequest(payload, request);
    JsonObject response = new JsonObject();
    response.addProperty("success", true);
    getAccountProviderOrThrow(payload)
        .thenCombine(
            cloudCardDAO.getCards(payload.getCardIDs()),
            (accountProvider, cloudCards) ->
                getAccounts(cloudCards, payload.getIfiID(), accountProvider.getAccountProviderID())
                    .thenCompose(accounts -> migrate(accounts, accountProvider)))
        .thenCompose(x -> x)
        .thenAccept(__ -> onResult(new InsertAccountsForCardsResponse(response), request))
        .exceptionally(t -> onError(request, t));
  }

  private CompletionStage<AccountProvider> getAccountProviderOrThrow(
      InsertAccountsForCardsRequest payload) {
    return accountProviderDAO
        .get(payload.getIfiID())
        .thenApply(
            optionalAccountProvider -> {
              if (!optionalAccountProvider.isPresent()) {
                throw new ZSIllegalArgumentException(
                    "accountProvider not configured for ifiID: " + payload.getIfiID());
              }
              return optionalAccountProvider.get();
            });
  }

  private CompletionStage<Void> migrate(List<Account> accounts, AccountProvider accountProvider) {
    if (accounts.isEmpty()) {
      return completedFuture(null);
    }
    return CompletableFutures.allOf(
            accounts.stream()
                .map(
                    account -> {
                      Map<String, String> attributesToAddOrUpdate = new HashMap<>();
                      attributesToAddOrUpdate.put(
                          AccountConstant.ACCOUNT_PRODUCT_FAMILY_ID,
                          String.valueOf(account.getProductFamilyID()));
                      attributesToAddOrUpdate.put(
                          AccountConstant.ACCOUNT_PRODUCT_ID,
                          String.valueOf(account.getProductID()));
                      attributesToAddOrUpdate.put(
                          AccountConstant.ACCOUNT_ACCOUNT_HOLDER_ID,
                          account.getOwnerAccountHolderID());
                      attributesToAddOrUpdate.put(
                          AccountConstant.ATHENA_ACCOUNT_PROVIDER_ID,
                          accountProvider.getAccountProviderID());
                      return ledgerService
                          .setLedgerAttributes(
                              account.getLedgerID(),
                              attributesToAddOrUpdate,
                              null,
                              "CC2A-MIGRATION_" + account.getId())
                          .thenCompose(
                              aVoid ->
                                  cloudCardDAO.updateAccountID(
                                      account.getCardID(), account.getId(), account.getIfiID()));
                    })
                .collect(Collectors.toList()))
        .thenCompose(__ -> accountService.bulkCreateAccount(accounts));
  }

  private CompletionStage<List<Account>> getAccounts(
      List<CloudCard> cloudCards, long ifiID, String accountProviderID) {
    return CompletableFutures.allResultsOf(
        cloudCards.stream()
            .map(
                cloudCard ->
                    cloudCardService
                        .getCardProgram(cloudCard.getCardProgramID())
                        .thenCombine(
                            accountHolderVectorService.getByVector(
                                "u", String.valueOf(cloudCard.getUserID()), ifiID),
                            (cardProgram, accountHolderVector) ->
                                getAccount(
                                    ifiID,
                                    accountProviderID,
                                    cloudCard,
                                    cardProgram,
                                    accountHolderVector))
                        .thenCompose(x -> x))
            .collect(Collectors.toList()));
  }

  private CompletionStage<Account> getAccount(
      long ifiID,
      String accountProviderID,
      CloudCard cloudCard,
      CardProgram cardProgram,
      AccountHolderVector accountHolderVector) {
    return athenaManagerService
        .getProduct(ifiID, cloudCard.getProductID())
        .thenApply(
            product ->
                buildAccount(
                    ifiID,
                    accountProviderID,
                    cloudCard,
                    cardProgram,
                    accountHolderVector,
                    product));
  }

  private Account buildAccount(
      long ifiID,
      String accountProviderID,
      CloudCard cloudCard,
      CardProgram cardProgram,
      AccountHolderVector accountHolderVector,
      Product product) {
    return Account.builder()
        .id(secureRandomGenerator.generateUUID())
        .ledgerID(cloudCard.getRecipientLedgerID())
        .ifiID(ifiID)
        .ownerAccountHolderID(accountHolderVector.getAccountHolderID())
        .productID(product.getId())
        .productFamilyID(product.getProductFamilyID())
        .programIDs(new ArrayList<>())
        .attributes(cloudCard.getAttributes())
        .status(getAccountStatus(cloudCard.getCardStatus()))
        .accountProviderID(accountProviderID)
        .createdAt(
            Instant.ofEpochMilli(cloudCard.getCreatedAt())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime())
        .updatedAt(LocalDateTime.now())
        .name(cardProgram.getCardName())
        .cardID(cloudCard.getCardID())
        .build();
  }

  private String getAccountStatus(CardStatus status) {
    switch (status) {
      case LEDGER_CREATED:
      case ADDING_CARD_TO_WALLET_FAILED:
      case CARD_ADDED_TO_WALLET:
        return AccountStatus.ENABLED.name();
      case CLOSED:
        return AccountStatus.CLOSED.name();
      default:
        throw new ZSIllegalArgumentException("Status not supported: " + status.name());
    }
  }
}
