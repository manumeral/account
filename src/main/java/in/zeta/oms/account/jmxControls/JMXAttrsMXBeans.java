package in.zeta.oms.account.jmxControls;

import java.util.Set;
import javax.management.MXBean;

@MXBean
public interface JMXAttrsMXBeans {
  Boolean getRunAccountHolderMigrationCron();
  int getRunAccountHolderMigrationCronAtHours();
  int getRunAccountHolderMigrationCronMinutes();
  void setRunAccountHolderMigrationCron(Boolean runAccountHolderMigrationCron);
  void setRunAccountHolderMigrationCronAtHours(int runAccountHolderMigrationCronAtHours);
  void setRunAccountHolderMigrationCronMinutes(int runAccountHolderMigrationCronMinutes);
  void setAccountHolderMigrationForIFI(Long ifiID);
  Long getAccountHolderMigrationForIFI();
  void setRemoveAccountHolderMigrationForIFI(Long ifiID);
  Long  getRemoveAccountHolderMigrationForIFI();
  Boolean isAccountHolderMigrationAllowedForIFI(Long ifiID);
  Set<Long> getAccountHolderMigrationAllowedIfis();

  public void setCronAccountHolderMigrationForIFI(Long ifiID);
  Long getCronAccountHolderMigrationForIFI();
  void setRemoveCronAccountHolderMigrationForIFI(Long ifiID);
  Long  getRemoveCronAccountHolderMigrationForIFI();
  public Boolean isCronAccountHolderMigrationAllowedForIFI(Long ifiID);
  public Set<Long> getCronAccountHolderMigrationAllowedIfis();

  int getParallelAccountHolderMigrationRequestLimit();
  void setParallelAccountHolderMigrationRequestLimit(int parallelAccountHolderMigrationRequestList);
}
