package in.zeta.oms.account.jmxControls;

import in.zeta.spectra.capture.SpectraLogger;
import java.lang.management.ManagementFactory;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import olympus.trace.OlympusSpectra;

public class JMXManager {

  private static final SpectraLogger logger = OlympusSpectra.getLogger(JMXManager.class);

  public static boolean registerInJMX(String property, JMXAttrs jmxAttrs) {
    String key = String.format("%s:type=%s,name=%s", jmxAttrs.getClass().getPackage().getName(),
        jmxAttrs.getClass().getSimpleName(), property);
    try {
      Object mBean = new JMXAttrsMXBeanImpl(jmxAttrs);
      MBeanServer server = ManagementFactory.getPlatformMBeanServer();
      ObjectName mxBeanName = new ObjectName(key);
      if (!server.isRegistered(mxBeanName)) {
        server.registerMBean(mBean, mxBeanName);
      }
      return true;
    } catch (MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException |
        NotCompliantMBeanException ex) {
      logger.error("failedToRegister", ex)
          .attr("property", property)
          .attr("objectName", key)
          .log();
      return false;
    }
  }

}
