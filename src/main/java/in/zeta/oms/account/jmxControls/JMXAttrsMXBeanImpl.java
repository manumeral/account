package in.zeta.oms.account.jmxControls;

import java.util.Set;

public class JMXAttrsMXBeanImpl implements JMXAttrsMXBeans {

  private JMXAttrs jmxAttrs;

  public JMXAttrsMXBeanImpl(JMXAttrs jmxAttrs) {
    this.jmxAttrs = jmxAttrs;
  }

  @Override
  public Boolean getRunAccountHolderMigrationCron() {
    return jmxAttrs.getRunAccountHolderMigrationCron();
  }

  @Override
  public int getRunAccountHolderMigrationCronAtHours() {
    return jmxAttrs.getRunAccountHolderMigrationCronAtHours();
  }

  @Override
  public int getRunAccountHolderMigrationCronMinutes() {
    return jmxAttrs.getRunAccountHolderMigrationCronMinutes();
  }

  @Override
  public void setRunAccountHolderMigrationCron(Boolean runAccountHolderMigrationCron) {
    jmxAttrs.setRunAccountHolderMigrationCron(runAccountHolderMigrationCron);
  }

  @Override
  public void setRunAccountHolderMigrationCronAtHours(int runAccountHolderMigrationCronAtHours) {
    jmxAttrs.setRunAccountHolderMigrationCronAtHours(runAccountHolderMigrationCronAtHours);
  }

  @Override
  public Long getRemoveAccountHolderMigrationForIFI() {
    return jmxAttrs.getRemoveAccountHolderMigrationForIFI();
  }

  @Override
  public Long getRemoveCronAccountHolderMigrationForIFI() {
    return jmxAttrs.getRemoveCronAccountHolderMigrationForIFI();
  }

  @Override
  public void setRunAccountHolderMigrationCronMinutes(int runAccountHolderMigrationCronMinutes) {
    jmxAttrs.setRunAccountHolderMigrationCronMinutes(runAccountHolderMigrationCronMinutes);
  }

  @Override
  public void setAccountHolderMigrationForIFI(Long ifiID) {
    jmxAttrs.setAccountHolderMigrationForIFI(ifiID);
  }

  @Override
  public Long getAccountHolderMigrationForIFI() {
    return jmxAttrs.getAccountHolderMigrationForIFI();
  }

  @Override
  public void setRemoveAccountHolderMigrationForIFI(Long ifiID) {
    jmxAttrs.setRemoveAccountHolderMigrationForIFI(ifiID);
  }

  @Override
  public void setRemoveCronAccountHolderMigrationForIFI(Long ifiID) {
    jmxAttrs.setRemoveCronAccountHolderMigrationForIFI(ifiID);
  }

  @Override
  public Boolean isAccountHolderMigrationAllowedForIFI(Long ifiID) {
    return jmxAttrs.isAccountMigrationAllowedForIFI(ifiID);
  }

  @Override
  public Set<Long> getAccountHolderMigrationAllowedIfis() {
    return jmxAttrs.getAccountHolderMigrationAllowedIfis();
  }

  @Override
  public void setCronAccountHolderMigrationForIFI(Long ifiID) {
    jmxAttrs.setCronAccountHolderMigrationForIFI(ifiID);
  }

  @Override
  public Long getCronAccountHolderMigrationForIFI() {
    return jmxAttrs.getCronAccountHolderMigrationForIFI();
  }

  @Override
  public Boolean isCronAccountHolderMigrationAllowedForIFI(Long ifiID) {
    return jmxAttrs.isCronAccountHolderMigrationAllowedForIFI(ifiID);
  }

  @Override
  public Set<Long> getCronAccountHolderMigrationAllowedIfis() {
    return jmxAttrs.getCronAccountHolderMigrationAllowedIfis();
  }

  @Override
  public int getParallelAccountHolderMigrationRequestLimit() {
    return jmxAttrs.getParallelAccountHolderMigrationRequestLimit();
  }

  @Override
  public void setParallelAccountHolderMigrationRequestLimit(
      int parallelAccountHolderMigrationRequestList) {
    jmxAttrs.setParallelAccountHolderMigrationRequestLimit(parallelAccountHolderMigrationRequestList);
  }
}
