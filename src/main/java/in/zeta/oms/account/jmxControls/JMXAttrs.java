package in.zeta.oms.account.jmxControls;

import java.util.HashSet;
import java.util.Set;

public class JMXAttrs {
  private Boolean runAccountHolderMigrationCron = true;
  private int runAccountHolderMigrationCronAtHours =  23;
  private int runAccountHolderMigrationCronMinutes = 00;
  private int parallelAccountHolderMigrationRequestLimit = 5;
  private Set<Long> allowedIfiSetForCron = new HashSet<>();
  private Set<Long> allowedIfiSet = new HashSet<>();

  public Boolean getRunAccountHolderMigrationCron() {
    return runAccountHolderMigrationCron;
  }

  public int getRunAccountHolderMigrationCronAtHours() {
    return runAccountHolderMigrationCronAtHours;
  }

  public int getRunAccountHolderMigrationCronMinutes() {
    return runAccountHolderMigrationCronMinutes;
  }

  public void setRunAccountHolderMigrationCron(Boolean runAccountHolderMigrationCron) {
    this.runAccountHolderMigrationCron = runAccountHolderMigrationCron;
  }

  public void setRunAccountHolderMigrationCronAtHours(int runAccountHolderMigrationCronAtHours) {
    this.runAccountHolderMigrationCronAtHours = runAccountHolderMigrationCronAtHours;
  }

  public void setRunAccountHolderMigrationCronMinutes(int runAccountHolderMigrationCronMinutes) {
    this.runAccountHolderMigrationCronMinutes = runAccountHolderMigrationCronMinutes;
  }

  public void setAccountHolderMigrationForIFI(Long ifiID) {
    allowedIfiSet.add(ifiID);
  }


  public Long getAccountHolderMigrationForIFI() {
    return Long.valueOf(allowedIfiSet.size());
  }

  public void setRemoveAccountHolderMigrationForIFI(Long ifiID) {
    allowedIfiSet.remove(ifiID);
  }

  public Long getRemoveAccountHolderMigrationForIFI() {return Long.valueOf(allowedIfiSet.size());}

  public Boolean isAccountMigrationAllowedForIFI(Long ifiID) {
    return allowedIfiSet.contains(ifiID);
  }

  public Set<Long> getAccountHolderMigrationAllowedIfis() {
    return allowedIfiSet;
  }

  public void setCronAccountHolderMigrationForIFI(Long ifiID) {
    allowedIfiSetForCron.add(ifiID);
  }

  public Long  getCronAccountHolderMigrationForIFI() {return Long.valueOf(allowedIfiSetForCron.size());}


  public void setRemoveCronAccountHolderMigrationForIFI(Long ifiID) {
    allowedIfiSetForCron.remove(ifiID);
  }
  public Long getRemoveCronAccountHolderMigrationForIFI() { return Long.valueOf(allowedIfiSetForCron.size());}


  public Boolean isCronAccountHolderMigrationAllowedForIFI(Long ifiID) {
    return allowedIfiSetForCron.contains(ifiID);
  }

  public Set<Long> getCronAccountHolderMigrationAllowedIfis() {
    return allowedIfiSetForCron;
  }

  public int getParallelAccountHolderMigrationRequestLimit() {
    return parallelAccountHolderMigrationRequestLimit;
  }

  public void setParallelAccountHolderMigrationRequestLimit(
      int parallelAccountHolderMigrationRequestLimit) {
    this.parallelAccountHolderMigrationRequestLimit = parallelAccountHolderMigrationRequestLimit;
  }

}
