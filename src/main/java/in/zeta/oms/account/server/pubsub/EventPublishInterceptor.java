package in.zeta.oms.account.server.pubsub;

import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;

import com.google.inject.Inject;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

@EagerSingleton
public class EventPublishInterceptor implements MethodInterceptor {
  private final SpectraLogger logger = OlympusSpectra.getLogger(EventPublishInterceptor.class);
  @Inject private PubSubPublisher pubSubPublisher;

  public EventPublishInterceptor() {}

  // For testing purposes
  public EventPublishInterceptor(PubSubPublisher pubSubPublisher) {
    this.pubSubPublisher = pubSubPublisher;
  }

  @Override
  public Object invoke(MethodInvocation methodInvocation) throws Throwable {
    CompletionStage<?> completionStageResult;
    Object result = methodInvocation.proceed();
    PublishEvent publishEvent = methodInvocation.getMethod().getAnnotation(PublishEvent.class);
    String topicName = publishEvent.topicName();
    completionStageResult = (CompletionStage<?>) result;
    return completionStageResult.thenApply(
        payload -> {
          pubSubPublisher.publishEvent(topicName, payload);
          return payload;
        })
        .exceptionally(t -> {
          logger.error("Something went wrong either in MethodInvocation or publishing event "
          + methodInvocation.getMethod().getName() + " " , t);
          t = unwrapCompletionStateException(t);
          throw new CompletionException(t);
        });
  }
}
