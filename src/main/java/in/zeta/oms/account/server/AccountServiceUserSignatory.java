package in.zeta.oms.account.server;

import com.google.inject.name.Named;
import in.zeta.commons.crypto.Signatory;
import in.zeta.commons.crypto.ZetaKeyPair;
import in.zeta.commons.crypto.ZetaSignature;
import javax.inject.Inject;
import olympus.common.JID;

public class AccountServiceUserSignatory implements Signatory {

  private final JID signatoryJID;
  private final ZetaKeyPair zetaKeyPair;

  @Inject
  public AccountServiceUserSignatory(@Named("account.user.signatory.jid") String signatoryJID,
                                     @Named("account.user.signatory.privatekey.base64") String base64EncodedPrivateKey,
                                     @Named("account.user.signatory.publickey.base64") String base64EncodedPublicKey) {
    this.signatoryJID = new JID(signatoryJID);
    this.zetaKeyPair = new ZetaKeyPair(base64EncodedPrivateKey, base64EncodedPublicKey);
  }

  @Override
  public ZetaSignature sign(byte[] bytes) {
    return zetaKeyPair.createSignature(bytes);
  }

  @Override
  public JID getSignatoryJID() {
    return signatoryJID;
  }
}
