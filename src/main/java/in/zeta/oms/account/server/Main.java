package in.zeta.oms.account.server;

import com.google.inject.Guice;
import com.google.inject.Injector;
import in.zeta.commons.interceptors.authorization.AuthorizationInterceptor;
import in.zeta.commons.interceptors.authorization.AuthorizationModule;
import in.zeta.commons.zms.service.ZMSBootstrap;
import in.zeta.tags.TagModule;
import olympus.transport.network.TCPObjectPipe;

import static in.zeta.commons.zms.service.ZetaModuleCollection.*;

public class Main {
  public static void main(String[] args) {
    TCPObjectPipe.MAX_OBJECT_SIZE = 100 * 1024;

    final Injector injector = Guice.createInjector(
        CONVENTIONS_MODULE.getModule(),
        ZMS_MODULE.getModule(),
        POSTGRES_MODULE.getModule(),
        EAGER_SINGLETON_BINDER_MODULE.getModule(),
        new AccountModule(),
        new AuthorizationModule(),
        new TagModule()
    );

    ZMSBootstrap bootstrap = injector.getInstance(ZMSBootstrap.class);
    bootstrap.addInterceptor(injector.getInstance(AuthorizationInterceptor.class));

    bootstrap.registerFirstTime();
  }
}
