package in.zeta.oms.account.server;

import static in.zeta.oms.account.util.ArgValidations.checkNotNull;
import static java.sql.Connection.TRANSACTION_REPEATABLE_READ;
import static in.zeta.oms.account.constant.AppConstants.TAG_OBJECT_TYPE_ACCOUNT;
import static in.zeta.oms.account.constant.AppConstants.TAG_OBJECT_TYPE_ACCOUNT_HOLDER;

import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.matcher.Matchers;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Named;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.gson.ZetaGsonBuilder;
import in.zeta.commons.interceptors.authorization.AuthorizationStrategy;
import in.zeta.commons.interceptors.authorization.ServiceAuthorizer;
import in.zeta.commons.json.JsonSchemaValidatorAsync;
import in.zeta.commons.zms.service.ZMSModule;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.jmxControls.JMXAttrs;
import in.zeta.oms.account.jmxControls.JMXManager;
import in.zeta.oms.account.server.pubsub.EventPublishInterceptor;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.oms.account.account.AccountObjectProvider;
import in.zeta.oms.account.accountHolder.AccountHolderObjectProvider;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import in.zeta.oms.certstore.client.CertStoreClient;
import in.zeta.oms.cloudcard.client.CloudCardClient;
import in.zeta.oms.ledger.client.LedgerServiceClient;
import in.zeta.oms.payment.client.PaymentClient;
import in.zeta.oms.user.client.UserServiceClient;
import in.zeta.oms.wallet.client.WalletClient;
import in.zeta.tags.ObjectProvider;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.commons.dbcp2.BasicDataSource;

public class AccountModule extends AbstractModule {

 @Inject
 private EventPublishInterceptor eventPublishInterceptor;

  @Override
  protected void configure() {
    bind(Gson.class).toInstance(new ZetaGsonBuilder().build());
    bind(AuthorizationStrategy.class).to(ServiceAuthorizer.class);
    bindPubSubTopics();
    final EventPublishInterceptor eventPublishInterceptor = new EventPublishInterceptor();
    requestInjection(eventPublishInterceptor);
    bindInterceptor(Matchers.any(), Matchers.annotatedWith(PublishEvent.class),
        eventPublishInterceptor);

    MapBinder<String, ObjectProvider> mapBinder = MapBinder
        .newMapBinder(binder(), String.class, ObjectProvider.class);
    mapBinder.addBinding(TAG_OBJECT_TYPE_ACCOUNT).to(AccountObjectProvider.class);
    mapBinder.addBinding(TAG_OBJECT_TYPE_ACCOUNT_HOLDER).to(AccountHolderObjectProvider.class);
  }

  private void bindPubSubTopics() {
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.CREATE_ACCOUNT_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_ACCOUNT_EVENT);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.CREATE_ACCOUNT_HOLDER_EVENT);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_ACCOUNT_HOLDER_EVENT);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.ADD_ACCOUNT_ACCESSOR_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_ACCOUNT_ACCESSOR_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.CREATE_KYC_STATUS_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_KYC_STATUS_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.ADD_POP_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.DELETE_POP_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_POP_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.CREATE_ACCOUNT_HOLDER_RELATION_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_ACCOUNT_HOLDER_RELATION_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.ADD_ACCOUNT_HOLDER_VECTOR_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_ACCOUNT_HOLDER_VECTOR_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.ADD_BENEFICIARY_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.DELETE_BENEFICIARY_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_BENEFICIARY_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.ADD_BENEFICIARY_VECTOR_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_BENEFICIARY_VECTOR_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.CREATE_BENEFICIARY_ACCOUNT_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_BENEFICIARY_ACCOUNT_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.ADD_ACCOUNT_RELATION_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_ACCOUNT_RELATION_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.ADD_ACCOUNT_VECTOR_EVENT_TOPIC);
    ZMSModule.bindPubSubTopic(binder(), PubSubTopics.UPDATE_ACCOUNT_VECTOR_EVENT_TOPIC);
  }

  @Provides
  @Singleton
  public CertStoreClient certStoreClient(ZetaHostMessagingService hostMessagingService) {
    return new CertStoreClient(hostMessagingService);
  }

  @Provides
  @Singleton
  public UserServiceClient userServiceClient(ZetaHostMessagingService hostMessagingService) {
    return new UserServiceClient(hostMessagingService);
  }

  @Provides
  @Singleton
  public WalletClient getWalletClient(ZetaHostMessagingService hostMessagingService) {
    return new WalletClient(hostMessagingService);
  }

  @Provides
  @Singleton
  public CloudCardClient getCloudCardClient(ZetaHostMessagingService hostMessagingService) {
    return new CloudCardClient(hostMessagingService);
  }

  @Provides
  @Singleton
  public AthenaManagerClient athenaManagerClient(ZetaHostMessagingService hostMessagingService) {
    return new AthenaManagerClient(hostMessagingService);
  }

  @Provides
  @Singleton
  public ServiceAuthorizer getServiceAuthorizer() {
    return new ServiceAuthorizer(ServiceAuthorizer.ALWAYS_PASS_THROUGH);
  }

  @Provides
  @Singleton
  public SecureRandomGenerator secureRandomGenerator(
      @Named("rng.algorithm") final String rngAlgorithm) {
    return new SecureRandomGenerator(rngAlgorithm);
  }

  @Provides
  @Singleton
  public LedgerServiceClient getLedgerClient(ZetaHostMessagingService hostMessagingService) {
    return new LedgerServiceClient(
        hostMessagingService,
        CacheBuilder.newBuilder()
            .concurrencyLevel(4)
            .initialCapacity(20000)
            .maximumSize(100000)
            .expireAfterWrite(24, TimeUnit.HOURS));
  }

  @Provides
  @Singleton
  public JsonSchemaValidatorAsync getJsonSchemaValidator(ZetaHostMessagingService zhms) {
    return new JsonSchemaValidatorAsync(
        CacheBuilder.newBuilder().maximumSize(1000), zhms, Executors.newFixedThreadPool(4));
  }
  
  @Provides
  @Singleton
  public PaymentClient getPaymentClient(ZetaHostMessagingService hostMessagingService) {
    return new PaymentClient(hostMessagingService);
  }

  @Provides
  @Singleton
  @Named("cloudCardDataSource")
  public BasicDataSource getCloudCardDataSource(
      @Named("postgres.cloudcard.jdbc.url") String url,
      @Named("postgres.cloudcard.jdbc.username") String username,
      @Named("postgres.cloudcard.jdbc.password") String password,
      @Named("postgres.cloudcard.jdbc.driver") String driver,
      @Named("postgres.cloudcard.jdbc.query.timeout.millis") int timeout,
      @Named("postgres.cloudcard.jdbc.pool.size") int poolSize) {
    checkNotNull(driver, "driver must not be null");
    checkNotNull(url, "url must not be null");
    checkNotNull(username, "username must not be null");
    checkNotNull(password, "password must not be null");
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setMaxTotal(poolSize);
    dataSource.setMaxWaitMillis(timeout);
    dataSource.setDefaultQueryTimeout(timeout);
    dataSource.setPoolPreparedStatements(true);
    dataSource.setValidationQuery("select 1");
    dataSource.setAccessToUnderlyingConnectionAllowed(true);
    dataSource.setDefaultTransactionIsolation(TRANSACTION_REPEATABLE_READ);
    dataSource.setDriverClassName(driver);
    dataSource.setUrl(url);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    return dataSource;
  }

  @Provides
  @Singleton
  public JMXAttrs getJMXAttrs() {
    JMXAttrs jmxAttrs =  new JMXAttrs();
    JMXManager.registerInJMX("JMXAttrs", jmxAttrs);
    return jmxAttrs;
  }

}
