package in.zeta.oms.account.server.pubsub;

import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.pubsub.CrudEvent;
import in.zeta.spectra.capture.SpectraLogger;
import java.util.concurrent.CompletableFuture;
import olympus.trace.OlympusSpectra;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class PubSubPublisher {
  private final SpectraLogger logger = OlympusSpectra.getLogger(PubSubPublisher.class);
  private final ZetaHostMessagingService zetaHostMessagingService;

  @Inject
  public PubSubPublisher(ZetaHostMessagingService zetaHostMessagingService) {
    this.zetaHostMessagingService = zetaHostMessagingService;
  }

  public <T> CompletionStage<Void> publishEvent(String topic, T eventPayload) {
    CrudEvent<T> crudEvent = new CrudEvent<>(topic, eventPayload);
    return publish(crudEvent);
  }

  private CompletionStage<Void> publish(CrudEvent event) {
    return zetaHostMessagingService
        .publish(event.getTOPIC(), event, zetaHostMessagingService.getSpartan().getCurrentEntity())
        .thenAccept(aVoid -> {
          logger.info("Successfully published event of type "  + event.getType()).log();
        })
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              logger.warn("Publish event error", t).attr("topic", event.getTOPIC()).log();
              throw new CompletionException(t);
            });
  }
}
