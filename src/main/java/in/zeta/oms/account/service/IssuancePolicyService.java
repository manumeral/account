package in.zeta.oms.account.service;

import static in.zeta.commons.concurrency.CompletableFutures.allResultsOf;
import static in.zeta.oms.account.api.exception.AccountCreationException.issuancePolicyViolation;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.stream.Collectors.toList;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.athenamanager.api.model.PolicyEntityType;
import in.zeta.oms.athenamanager.api.model.PolicyState;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyResponse;
import java.time.LocalDateTime;
import java.util.concurrent.CompletionStage;

@Singleton
public class IssuancePolicyService {

  private final AthenaManagerService athenaManagerService;
  private final AccountHolderGroupService accountHolderGroupService;

  @Inject
  public IssuancePolicyService(AthenaManagerService athenaManagerService,
                               AccountHolderGroupService accountHolderGroupService) {
    this.athenaManagerService = athenaManagerService;
    this.accountHolderGroupService = accountHolderGroupService;
  }

  public CompletionStage<Void> applyIssuancePolicy(Long ifiID,
      PolicyEntityType entityType,
      Long entityID,
      AccountHolder accountHolder) {
    return applyIssuancePolicy(ifiID, entityType, String.valueOf(entityID), accountHolder);
  }
  public CompletionStage<Void> applyIssuancePolicy(Long ifiID,
                                                   PolicyEntityType entityType,
                                                   String entityID,
                                                   AccountHolder accountHolder) {
    return athenaManagerService.getIssuancePolicyList(ifiID, entityType, entityID)
        .thenCompose(issuancePolicyListResponse ->
            allResultsOf(
                issuancePolicyListResponse.getIssuancePolicyListResponse()
                    .stream()
                    .map(issuancePolicyResponse -> passesIssuancePolicy(ifiID, issuancePolicyResponse, accountHolder))
                    .collect(toList())))
        .thenApply(issuancePolicyCheckResults ->
            issuancePolicyCheckResults.stream().allMatch(passes -> passes))
        .thenAccept(passesIssuancePolicy -> {
          if (!passesIssuancePolicy) {
            String errorMessage = String
                .format("Account Holder group for account holder id: %s not permitted",
                    accountHolder.getId());
            throw issuancePolicyViolation(errorMessage);
          }
        });
  }

  private boolean isPolicyValid(IssuancePolicyResponse policyResponse) {
    LocalDateTime now = LocalDateTime.now();
    return policyResponse.getState() == PolicyState.ENABLED &&
        policyResponse.getValidFrom().isBefore(now) &&
        policyResponse.getValidTill().isAfter(now);
  }

  private CompletionStage<Boolean> passesIssuancePolicy(
      Long ifiID, IssuancePolicyResponse policy, AccountHolder accountHolder) {

    if (!isPolicyValid(policy)) {
      return completedFuture(true);
    }

    CompletionStage<Boolean> partOfAnyAllowedGroups = completedFuture(TRUE);
    if (policy.getAllowedAccountHolderGroups() != null
        && !policy.getAllowedAccountHolderGroups().isEmpty()) {
      partOfAnyAllowedGroups =
          accountHolderGroupService.isPartOfAny(
              ifiID, accountHolder, policy.getAllowedAccountHolderGroups());
    }
    CompletionStage<Boolean> partOfAnyDisAllowedGroups = completedFuture(FALSE);
    if (policy.getDisAllowedAccountHolderGroups() != null
        && !policy.getDisAllowedAccountHolderGroups().isEmpty()) {
      partOfAnyDisAllowedGroups =
          accountHolderGroupService.isPartOfAny(
              ifiID, accountHolder, policy.getDisAllowedAccountHolderGroups());
    }
    return partOfAnyAllowedGroups.thenCombine(
        partOfAnyDisAllowedGroups, (allowed, disallowed) -> {
          return allowed && !disallowed;
        });
  }
}
