package in.zeta.oms.account.service;

import in.zeta.oms.wallet.api.CardProgramInfo;
import in.zeta.oms.wallet.api.requests.GetCardsByProductTypeRequest;
import in.zeta.oms.wallet.api.requests.GetResourceListRequest;
import in.zeta.oms.wallet.api.requests.GetWalletListRequest;
import in.zeta.oms.wallet.api.responses.GetCardsByProductTypeResponse;
import in.zeta.oms.wallet.api.responses.ResourceListResponse;
import in.zeta.oms.wallet.api.responses.WalletListResponse;
import in.zeta.oms.wallet.client.WalletClient;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletionStage;

@Singleton
public class WalletService {

  private final WalletClient walletClient;

  @Inject
  public WalletService(WalletClient walletClient) {
    this.walletClient = walletClient;
  }

  public CompletionStage<WalletListResponse> getWalletList(Long ifiID, Long pageSize, Long pageIndex) {
    return walletClient.getWalletList(new GetWalletListRequest.Builder()
        .ifiID(ifiID)
        .pageSize(pageSize)
        .pageIndex(pageIndex));
  }

  public CompletionStage<ResourceListResponse> getResourceList(Long ifiID, Long pageSize, Long pageIndex) {
    return walletClient.getResourceList(new GetResourceListRequest.Builder()
        .ifiID(ifiID)
        .pageSize(pageSize)
        .pageIndex(pageIndex));
  }

  public CompletionStage<List<CardProgramInfo>> getCardsByProductType(Long userID, Long ifiID) {
    return walletClient
        .getCardsByProductType(new GetCardsByProductTypeRequest
            .Builder()
            .walletID(userID)
            .ifi(ifiID)
            .includeAllStates(true))
        .thenApply(GetCardsByProductTypeResponse::getCards);
  }

}
