package in.zeta.oms.account.service;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.commons.json.JsonSchemaValidatorAsync;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.athenamanager.api.model.EntityType;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import java.util.List;
import java.util.concurrent.CompletionStage;

@Singleton
public class AccountHolderGroupService extends EntityGroupService {

  @Inject
  public AccountHolderGroupService(AthenaManagerClient athenaManagerClient,
      Gson gson, JsonSchemaValidatorAsync jsonSchemaValidatorAsync) {
    super(athenaManagerClient, gson, jsonSchemaValidatorAsync);
  }

  public CompletionStage<Boolean> isPartOfAny(Long ifiID, AccountHolder accountHolder,
                                              List<String> groupIDs) {
    return isPartOfAny(accountHolder, groupIDs, EntityType.ACCOUNT_HOLDER, ifiID);
  }

}
