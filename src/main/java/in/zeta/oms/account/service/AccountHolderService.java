package in.zeta.oms.account.service;


import static in.zeta.commons.concurrency.CompletableFutures.allOf;
import static in.zeta.oms.account.constant.AccountErrorCode.INTERNAL_ERROR;
import static in.zeta.oms.account.constant.AppConstants.TAG_OBJECT_TYPE_ACCOUNT_HOLDER;
import static in.zeta.oms.account.pop.EntityType.ACCOUNT_HOLDER;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.accountHolder.AccountHolderTransactionDAO;
import in.zeta.oms.account.accountHolder.kyc.KYCStatus;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusService;
import in.zeta.oms.account.accountHolder.pop.POPService;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorService;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.exception.AccountHolderInsertionException;
import in.zeta.oms.account.api.exception.AccountHolderNotFoundException;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.request.CreateAccountHolderRequest;
import in.zeta.oms.account.athenamanager.accountHolderProvider.AccountHolderProviderService;
import in.zeta.oms.account.dao.AccountHolderDAO;
import in.zeta.oms.account.pop.AddAccountHolderPOPRequest;
import in.zeta.oms.account.pop.POP;
import in.zeta.oms.account.pop.UpdateAccountHolderPOPRequest;
import in.zeta.oms.athenamanager.api.model.EntityType;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.tags.TagService;
import in.zeta.tags.model.Tag;
import java.util.List;
import java.util.concurrent.CompletionStage;
import olympus.message.util.CompletableFutures;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.commons.concurrency.CompletableFutures.allOf;
import static in.zeta.oms.account.constant.AccountErrorCode.INTERNAL_ERROR;
import static in.zeta.oms.account.pop.EntityType.ACCOUNT_HOLDER;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class AccountHolderService {

  private final AccountHolderProviderService accountHolderProviderService;
  private final AccountHolderDAO accountHolderDAO;
  private final SecureRandomGenerator secureRandomGenerator;
  private final AccountHolderTransactionDAO accountHolderTransactionDAO;
  private final AccountHolderVectorService accountHolderVectorService;
  private final POPService popService;
  private final BaseSchemaIssuerService baseSchemaIssuerService;
  private final KYCStatusService kycStatusService;
  private final TagService tagService;

  @Inject
  public AccountHolderService(
      AccountHolderProviderService accountHolderProviderService,
      SecureRandomGenerator secureRandomGenerator,
      AccountHolderDAO accountHolderDAO,
      AccountHolderTransactionDAO accountHolderTransactionDAO,
      AccountHolderVectorService accountHolderVectorService,
      POPService popService,
      BaseSchemaIssuerService baseSchemaIssuerService,
      KYCStatusService kycStatusService,
      TagService tagService) {
    this.accountHolderProviderService = accountHolderProviderService;
    this.accountHolderDAO = accountHolderDAO;
    this.secureRandomGenerator = secureRandomGenerator;
    this.accountHolderTransactionDAO = accountHolderTransactionDAO;
    this.accountHolderVectorService = accountHolderVectorService;
    this.popService = popService;
    this.baseSchemaIssuerService = baseSchemaIssuerService;
    this.kycStatusService = kycStatusService;
    this.tagService = tagService;
  }

  @PublishEvent(topicName = PubSubTopics.CREATE_ACCOUNT_HOLDER_EVENT)
  public CompletionStage<AccountHolder> create(CreateAccountHolderRequest payload) {
    final String id = secureRandomGenerator.generateUUID();
    KYCStatus kycStatus = null;
    if (null != payload.getKYCStatus()) {
        kycStatus = payload.getKYCStatus()
            .toBuilder()
            .accountHolderID(id)
            .build();
    }

    final AccountHolder.Builder accountHolderBuilder = AccountHolder
        .from(payload)
        .KYCStatus(kycStatus)
        .id(id);

    final String accountHolderProviderID = payload.getAccountHolderProviderID();
    final Long ifiID = payload.getIfiID();
    final String requestID = payload.getRequestID();
    return accountHolderDAO
        .getAccountHolderWithRequestID(requestID, accountHolderProviderID, ifiID)
        .thenCompose(accountHolderOptional -> {
          if (accountHolderOptional.isPresent()) {
            if (matchesWithExistingAccountHolder(payload, accountHolderOptional.get())) {
              return assignTags(accountHolderBuilder.build())
              .thenApply(ignore -> accountHolderOptional.get());
            }
            throw AccountHolderInsertionException.duplicateRequestException(
                "An account holder is already created with this request");
          }

          return validate(accountHolderBuilder.build())
              .thenCompose(aVoid -> accountHolderTransactionDAO
                  .insertAccountHolder(accountHolderBuilder.build())
                  .handle((accountHolder, throwable) -> {
                    if (throwable != null) {
                      throwable = CompletableFutures.unwrapCompletionStateException(throwable);
                      if (throwable instanceof DuplicateKeyException) {
                          //TODO : Handle duplicate exception
                          throw new AccountHolderInsertionException("Account holder already exists with vector type", throwable, INTERNAL_ERROR);
                      }
                      throw new AccountHolderInsertionException("AccountHolder creation failed", throwable, INTERNAL_ERROR);
                    }
                    return completedFuture(accountHolder);
                  }))
              .thenCompose(future -> future)
              .thenCompose(this::assignTags);
        });
  }

  private CompletionStage<AccountHolder> assignTags(AccountHolder accountHolder) {
    return tagService.assignTags(accountHolder.getIfiID(),
        TAG_OBJECT_TYPE_ACCOUNT_HOLDER,
        accountHolder.getId(),
        accountHolder.getTags())
        .thenApply(ignore -> accountHolder);
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_HOLDER_EVENT)
  public CompletionStage<AccountHolder> updateAccountHolder(
      AccountHolder updatedAccountHolder) {
    return accountHolderDAO
        .updateAccountHolder(updatedAccountHolder)
        .thenCompose(accountHolder -> get(updatedAccountHolder.getIfiID(), updatedAccountHolder.getId()))
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              if (!(t instanceof AccountHolderNotFoundException)) {
                t = unwrapCompletionStateException(t);
                throw new AccountServiceException(
                    "DB Error while updating AccountHolder", t, INTERNAL_ERROR);
              }
              throw new AccountHolderNotFoundException(
                  String.format("Account holder not found for id: %s", updatedAccountHolder.getId()));
            });
    }


    //----------------------------------------------POP SERVICE APIS----------------------------------------------------------//

    public CompletionStage<POP> addPOP(AddAccountHolderPOPRequest payload) {
        final String id = secureRandomGenerator.generateUUID();
        final POP pop = payload.toPOP()
            .id(id)
            .build();
        return accountHolderDAO.getAccountHolderByID(payload.getAccountHolderID(), payload.getIfiID())
            .thenCompose(ignore -> popService.addPOP(pop));
    }

    public CompletionStage<POP> updatePOP(UpdateAccountHolderPOPRequest payload) {
        final POP pop = payload.toPOP()
            .build();
        return accountHolderDAO.getAccountHolderByID(payload.getAccountHolderID(), payload.getIfiID())
            .thenCompose(ignore -> popService.updatePOP(pop));
    }

    public CompletionStage<List<POP>> listPOPs(Long ifiID, String accountHolderID) {
        return accountHolderDAO.getAccountHolderByID(accountHolderID, ifiID)
            .thenCompose(ignore -> popService.listPOPs(ifiID, accountHolderID, ACCOUNT_HOLDER));
    }

    public CompletionStage<POP> setPOPAsDefault(String id, Long ifiID, String entityID, String entityType) {
        return accountHolderDAO.getAccountHolderByID(entityID, ifiID)
            .thenCompose(ignore -> popService.setPOPAsDefault(id, ifiID, entityID, entityType));
    }

    public CompletionStage<POP> getPOPByID(Long ifiID, String id, String accountHolderID, String entityType) {
        return accountHolderDAO.getAccountHolderByID(accountHolderID, ifiID)
            .thenCompose(ignore -> popService.getPOPByID(ifiID, id, accountHolderID, entityType));
    }

    public CompletionStage<POP> deletePOP(Long ifiID, String accountHolderID, String id, String entityType) {
        return accountHolderDAO.getAccountHolderByID(accountHolderID, ifiID)
            .thenCompose(ignore -> popService.deletePOP(ifiID, accountHolderID, id, entityType));
    }

    //----------------------------------------------POP SERVICE APIS ENDS----------------------------------------------------------//

  public CompletionStage<AccountHolder> getAccountHolderByID(String id,
                                                             Long ifiID) {
    return accountHolderDAO.getAccountHolderByID(id, ifiID)
        .thenCompose(accountHolder -> tagService.getTagsForObject(ifiID, TAG_OBJECT_TYPE_ACCOUNT_HOLDER, id)
            .thenApply(tags -> accountHolder.toBuilder().tags(tags).build()));
  }

  public CompletionStage<AccountHolder> get(Long ifiID, String id) {
    return accountHolderDAO.getAccountHolderByID(id, ifiID)
        .thenCompose(accountHolder -> {
          CompletionStage<List<AccountHolderVector>> vectorsCompletionStage = accountHolderVectorService.getVectorsByAccountHolderID(ifiID, id);
          CompletionStage<List<POP>> popsCompletionStage = popService.listPOPs(ifiID, id, ACCOUNT_HOLDER);
          CompletionStage<Optional<KYCStatus>> kycStatusCompletionStage = kycStatusService.getOptional(id, ifiID);
          CompletionStage<List<Tag>> tagsCompletionStage = tagService.getTagsForObject(ifiID, TAG_OBJECT_TYPE_ACCOUNT_HOLDER, id);

          return allOf(vectorsCompletionStage, popsCompletionStage, kycStatusCompletionStage, tagsCompletionStage)
              .thenApply(ignore -> accountHolder.toBuilder()
                  .vectors(vectorsCompletionStage.toCompletableFuture().join())
                  .pops(popsCompletionStage.toCompletableFuture().join())
                  .KYCStatus(kycStatusCompletionStage.toCompletableFuture().join().orElse(null))
                  .tags(tagsCompletionStage.toCompletableFuture().join())
                  .build());
        });
  }

  public CompletionStage<AccountHolder> getByVector(Long ifiID, ExternalIDType externalIDType, String externalID) {
      return accountHolderVectorService.getByVector(externalIDType.getAttributeKey(), externalID, ifiID)
          .thenCompose(vector -> get(ifiID, vector.getAccountHolderID()));
  }

  public CompletionStage<AccountHolder> getByVector(Long ifiID, String vectorType, String vectorValue) {
    return accountHolderVectorService.getByVector(vectorType, vectorValue, ifiID)
        .thenCompose(vector -> get(ifiID, vector.getAccountHolderID()));
  }

    private boolean matchesWithExistingAccountHolder(CreateAccountHolderRequest payload, AccountHolder accountHolder) {
      return payload.getAccountHolderProviderID().equals(accountHolder.getAccountHolderProviderID()) &&
          areVectorsEqual(payload.getVectors(), accountHolder.getVectors()) &&
          payload.getType() == accountHolder.getType();
    }

    private boolean areVectorsEqual(List<AccountHolderVector> payloadVectors, List<AccountHolderVector> accountHolderVectors)  {
      if (payloadVectors.size() != accountHolderVectors.size()) {
          return false;
      }

      return payloadVectors.stream().allMatch(payloadVector ->
          accountHolderVectors.stream().anyMatch(accountHolderVector ->
              accountHolderVector.getValue().equals(payloadVector.getValue()) &&
              accountHolderVector.getType().equals(payloadVector.getType()))
      );
    }

    public CompletionStage<Void> validate(AccountHolder accountHolder) {
    return allOf(
        accountHolderProviderService.validate(accountHolder),
        baseSchemaIssuerService
            .validate(accountHolder, accountHolder.getIfiID(), EntityType.ACCOUNT_HOLDER.name()));
    }
}
