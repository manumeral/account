package in.zeta.oms.account.service;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.commons.json.JsonSchemaValidatorAsync;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;

import java.util.List;
import java.util.concurrent.CompletionStage;

import static in.zeta.oms.athenamanager.api.model.EntityType.ACCOUNT;

@Singleton
public class AccountGroupService extends EntityGroupService {

  @Inject
  public AccountGroupService(
      AthenaManagerClient athenaManagerClient,
      Gson gson,
      JsonSchemaValidatorAsync jsonSchemaValidatorAsync) {
    super(athenaManagerClient, gson, jsonSchemaValidatorAsync);
  }

  public CompletionStage<Boolean> isPartOfAny(Account account, List<String> accountGroups, Long ifiID) {
      return isPartOfAny(account, accountGroups, ACCOUNT, ifiID);
  }

}
