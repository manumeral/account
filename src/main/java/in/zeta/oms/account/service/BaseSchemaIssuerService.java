package in.zeta.oms.account.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.json.JsonSchemaValidator;
import in.zeta.oms.account.json.JsonSchemaValidationService;
import in.zeta.oms.athenamanager.baseSchema.BaseSchemaValidationException;
import in.zeta.oms.athenamanager.constant.AthenaManagerErrorCode;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.apache.commons.io.IOUtils;
import org.everit.json.schema.ValidationException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.concurrent.CompletionStage;

import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@EagerSingleton
public class BaseSchemaIssuerService {

    private final AthenaManagerService athenaManagerService;
    private final JsonSchemaValidationService jsonSchemaValidationService;
    private static final SpectraLogger log = OlympusSpectra.getLogger(BaseSchemaIssuerService.class);
    private final Gson gson;

    @Inject
    public BaseSchemaIssuerService(AthenaManagerService athenaManagerService,
                                   JsonSchemaValidationService jsonSchemaValidationService,
                                   Gson gson) {
        this.athenaManagerService = athenaManagerService;
        this.jsonSchemaValidationService = jsonSchemaValidationService;
        this.gson = gson;
    }

    public CompletionStage<Void> validate(Object data, Long ifiID, String entityType) {
        return athenaManagerService.getBaseSchema(ifiID, entityType)
            .handle((baseSchema, throwable) -> {
                if(null != baseSchema && null == throwable) {
                    return jsonSchemaValidationService.validate(data, baseSchema.getSchema());
                }
                return validateDefaultSchema(data, entityType);
            })
            .thenCompose(ignore -> ignore)
            .exceptionally(e -> {
                throw createException(e, data, ifiID, entityType);
            });
    }

    public CompletionStage<Void> validateDefaultSchema(Object data, String entityType) {
        return athenaManagerService.getBaseSchema(-1L, entityType)
            .thenCompose(baseSchema -> jsonSchemaValidationService
                .validate(data, baseSchema.getSchema()));
    }

    public static JsonObject getJsonObject(String resourceName) {
        try {
            InputStream in =
                JsonSchemaValidator.class.getClassLoader().getResourceAsStream(resourceName);
            return new Gson()
                .fromJson(IOUtils.toString(in, Charset.defaultCharset()), JsonObject.class);
        } catch (IOException e) {
            return new JsonObject();
        }
    }


    public BaseSchemaValidationException createException(Throwable throwable, Object data, Long ifiID, String entityType) {
        throwable = unwrapCompletionStateException(throwable);
        log.warn("BaseIssuerSchemaValidationFailed", throwable)
            .fill("object", data)
            .log();
        final JsonObject jsonAttributes = new JsonObject();
        if (throwable instanceof ValidationException) {
            jsonAttributes.add(
                "errorMessages",
                gson.toJsonTree(
                    ((ValidationException) throwable).getAllMessages()));
        }
        final BaseSchemaValidationException exception =
            new BaseSchemaValidationException(
                throwable,
                String.format("Base Schema for ifi %s and entityType %s failed"
                    ,ifiID,entityType),
                AthenaManagerErrorCode.BASE_SCHEMA_VALIDATION_FAILED);
        exception.setJsonAttributes(jsonAttributes);
        return exception;
    }

}
