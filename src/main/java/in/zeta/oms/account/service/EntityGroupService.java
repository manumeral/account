package in.zeta.oms.account.service;

import static in.zeta.commons.concurrency.CompletableFutures.allResultsOf;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.json.JsonSchemaValidatorAsync;
import in.zeta.oms.athenamanager.api.model.EntityGroup;
import in.zeta.oms.athenamanager.api.model.EntityType;
import in.zeta.oms.athenamanager.api.request.GetEntityGroupRequest;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import in.zeta.spectra.capture.SpectraLogger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import olympus.trace.OlympusSpectra;

public abstract class EntityGroupService {
  private final AthenaManagerClient athenaManagerClient;
  private final JsonSchemaValidatorAsync jsonSchemaValidatorAsync;
  private static final SpectraLogger logger = OlympusSpectra.getLogger(EntityGroupService.class);

  private final Gson gson;

  public EntityGroupService(AthenaManagerClient athenaManagerClient, Gson gson, JsonSchemaValidatorAsync jsonSchemaValidatorAsync) {
    this.athenaManagerClient = athenaManagerClient;
    this.gson = gson;
    this.jsonSchemaValidatorAsync = jsonSchemaValidatorAsync;
  }

  @TimeLogger
  public CompletionStage<Boolean> isPartOfAny(
      Object object, List<String> groupIDs, EntityType entityType, Long ifiID) {
    return getParallelGroupChecks(object, groupIDs, entityType, ifiID)
        .thenApply(
            results -> results.parallelStream().anyMatch(Boolean::booleanValue));
  }

  private CompletableFuture<List<Boolean>> getParallelGroupChecks(Object object,
                                                                  List<String> groupIDs,
                                                                  EntityType entityType,
                                                                  Long ifiID) {
    List<CompletionStage<Boolean>> completableFutures = new ArrayList<>();
    groupIDs.forEach(groupID -> completableFutures.add(
        checkIfEntityBelongsToGroup(object, groupID, entityType, ifiID))
    );
    return allResultsOf(completableFutures);
  }

  private CompletionStage<Boolean> checkIfEntityBelongsToGroup(
      Object object, String entityGroupId, EntityType entityType, Long ifiID) {
    return getEntityGroup(entityGroupId, entityType, ifiID)
        .thenCompose(
            entityGroup -> {
              final JsonObject jsonObject = gson.toJsonTree(object).getAsJsonObject();
              return passesRules(entityGroup, jsonObject);
            });
  }

  private CompletionStage<Boolean> passesRules(EntityGroup entityGroup, JsonObject jsonObject) {
    JsonObject schema = entityGroup.getSchema();
    if (schema == null) {
      return completedFuture(Boolean.TRUE);
    }
    return jsonSchemaValidatorAsync
        .validate(schema, jsonObject)
        .handle(
            (__, t) -> {
              if (t != null) {
                t = unwrapCompletionStateException(t);
                logger
                    .warn("passesRules.error", t)
                    .attr("entityGroupID", entityGroup.getId())
                    .log();
                return Boolean.FALSE;
              }
              return Boolean.TRUE;
            });
  }

  public CompletionStage<EntityGroup> getEntityGroup(String entityID, EntityType entityType, Long ifiID) {
    return athenaManagerClient.getEntityGroup(
        new GetEntityGroupRequest.Builder()
            .id(entityID)
            .entityType(entityType)
            .ifiID(ifiID)
    );
  }

}
