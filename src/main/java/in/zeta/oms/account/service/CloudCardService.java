package in.zeta.oms.account.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.commons.Money;
import in.zeta.commons.cache.CacheJMXManager;
import in.zeta.oms.cloudcard.api.CardProgram;
import in.zeta.oms.cloudcard.api.GetCardProgramRequest.Builder;
import in.zeta.oms.cloudcard.api.GetCardProgramResponse;
import in.zeta.oms.cloudcard.api.model.CloudCard;
import in.zeta.oms.cloudcard.api.request.GetCardListRequestV3;
import in.zeta.oms.cloudcard.api.response.GetCardListResponseV3;
import in.zeta.oms.cloudcard.client.CloudCardClient;
import in.zeta.oms.cloudcard.transaction.model.request.CreateTransactionRequest;
import in.zeta.oms.cloudcard.transaction.model.response.CreateTransactionResponse;
import in.zeta.spectra.capture.SpectraLogger;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import olympus.trace.OlympusSpectra;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

@Singleton
public class CloudCardService {

  private static final SpectraLogger logger = OlympusSpectra.getLogger(CloudCardService.class);
  private final CloudCardClient cloudCardClient;
  private final CacheBuilder<Object, Object> defaultCacheBuilder =
      CacheBuilder.newBuilder()
          .concurrencyLevel(4)
          .initialCapacity(25000)
          .maximumSize(400000)
          .expireAfterWrite(4, TimeUnit.HOURS)
          .recordStats();
  private final Cache<String, CardProgram> cardProgramCache;

  @Inject
  public CloudCardService(CloudCardClient cloudCardClient) {
    this.cloudCardClient = cloudCardClient;
    this.cardProgramCache = defaultCacheBuilder.build();
    CacheJMXManager.registerInJMX("cardProgramCache", cardProgramCache);
  }

  public CompletionStage<List<CloudCard>> getCloudCards(
      Long ifiID,
      Long cardID,
      String vector,
      String programID,
      Long productID,
      Long pageNumber,
      Long pageSize) {

    return cloudCardClient
        .getCardListV3(
            GetCardListRequestV3.builder()
                .ifiID(ifiID)
                .cardID(cardID)
                .userVector(vector)
                .cardProgramID(programID)
                .productID(productID)
                .pageNumber(pageNumber)
                .pageSize(pageSize))
        .thenApply(GetCardListResponseV3::getCards);
  }

  public CompletionStage<CardProgram> getCardProgram(String cardProgramID) {
    CardProgram cardProgram = cardProgramCache.getIfPresent(cardProgramID);
    if (cardProgram != null) {
      return CompletableFuture.completedFuture(cardProgram);
    }
    return cloudCardClient
        .getCardProgram(new Builder().cardProgramID(cardProgramID))
        .thenApply(
            response -> {
              cardProgramCache.put(cardProgramID, response.getCardProgram());
              return response.getCardProgram();
            });
  }

  public CompletionStage<CreateTransactionResponse> transferAmount(
      String accountID,
      String outboundAccountID,
      String remarks,
      Money amountToTransfer,
      long ifID,
      String requestID,
      Map<String, String> attributes,
      String voucherCode) {
    CreateTransactionRequest.Builder builder =
        CreateTransactionRequest.builder()
            .debitAccountID(accountID)
            .creditAccountID(outboundAccountID)
            .remarks(remarks)
            .value(amountToTransfer)
            .ifi(ifID)
            .requestID(requestID)
            .attributes(attributes)
            .transactionTime(Calendar.getInstance().getTimeInMillis())
            .transactionCode(voucherCode);
    logger.info("transferring amount : ").attr("transaction Request", builder).log();
    return cloudCardClient.createTransaction(builder);
  }
}
