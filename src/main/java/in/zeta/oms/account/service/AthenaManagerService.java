package in.zeta.oms.account.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.commons.cache.CacheJMXManager;
import in.zeta.oms.athenamanager.api.model.AccountHolderProvider;
import in.zeta.oms.athenamanager.api.model.AccountProvider;
import in.zeta.oms.athenamanager.api.model.PolicyEntityType;
import in.zeta.oms.athenamanager.api.model.Product;
import in.zeta.oms.athenamanager.api.model.ProductFamily;
import in.zeta.oms.athenamanager.api.model.Program;
import in.zeta.oms.athenamanager.api.request.GetAccountHolderProviderRequest;
import in.zeta.oms.athenamanager.api.request.GetAccountProviderRequest;
import in.zeta.oms.athenamanager.api.request.GetIssuancePolicyListRequest;
import in.zeta.oms.athenamanager.api.request.GetProductFamilyRequest;
import in.zeta.oms.athenamanager.api.request.GetProductListRequest;
import in.zeta.oms.athenamanager.api.request.GetProductListResponse;
import in.zeta.oms.athenamanager.api.request.GetProductRequest;
import in.zeta.oms.athenamanager.api.response.IssuancePolicyListResponse;
import in.zeta.oms.athenamanager.baseSchema.BaseSchema;
import in.zeta.oms.athenamanager.baseSchema.GetBaseSchemaRequest;
import in.zeta.oms.athenamanager.client.AthenaManagerClient;
import in.zeta.oms.cloudcard.api.CardProgram;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.MoreObjects.firstNonNull;
import static java.util.concurrent.CompletableFuture.completedFuture;

@Singleton
public class AthenaManagerService {

  private final AthenaManagerClient athenaManagerClient;

  private final Cache<String, Product> productCache;
  private final Cache<String, Product> productNameCache;
  private final Cache<String, Program> programCache;
  private final Cache<String, CardProgram> cardProgramCache;
  private final Cache<String, ProductFamily> productFamilyCache;
  private final Cache<String, AccountProvider> accountProviderCache;
  private final Cache<String, AccountHolderProvider> accountHolderProviderCache;
  private final Cache<String, IssuancePolicyListResponse> issuancePolicyListResponseCache;
  private final Cache<String, BaseSchema> baseSchemaCache;
  private final CacheBuilder<Object, Object> defaultCacheBuilder = CacheBuilder.newBuilder()
      .concurrencyLevel(4)
      .initialCapacity(25000)
      .maximumSize(400000)
      .expireAfterWrite(4, TimeUnit.HOURS)
      .recordStats();


  @Inject
  public AthenaManagerService(AthenaManagerClient athenaManagerClient) {
    this.athenaManagerClient = athenaManagerClient;
    //Caches
    this.productCache = defaultCacheBuilder.build();
    this.programCache = defaultCacheBuilder.build();
    this.productNameCache = defaultCacheBuilder.build();
    this.cardProgramCache = defaultCacheBuilder.build();
    this.productFamilyCache = defaultCacheBuilder.build();
    this.accountProviderCache = defaultCacheBuilder.build();
    this.accountHolderProviderCache = defaultCacheBuilder.build();
    this.issuancePolicyListResponseCache = defaultCacheBuilder.build();
    this.baseSchemaCache = defaultCacheBuilder.build();

    CacheJMXManager.registerInJMX("productCache", productCache);
    CacheJMXManager.registerInJMX("productNameCache", productNameCache);
    CacheJMXManager.registerInJMX("programCache", programCache);
    CacheJMXManager.registerInJMX("cardProgramCache", cardProgramCache);
    CacheJMXManager.registerInJMX("productFamilyCache", productFamilyCache);
    CacheJMXManager.registerInJMX("accountProviderCache", accountProviderCache);
    CacheJMXManager.registerInJMX("accountHolderProviderCache", accountHolderProviderCache);
    CacheJMXManager.registerInJMX("issuancePolicyListResponseCache", issuancePolicyListResponseCache);
    CacheJMXManager.registerInJMX("baseSchemaCache", baseSchemaCache);
  }

  public CompletionStage<IssuancePolicyListResponse> getIssuancePolicyList(Long ifiID,
                                                                           PolicyEntityType entityType,
                                                                           String entityID) {
    final String cacheKey = getCacheKey(entityType.name(), entityID, ifiID);
    Optional<IssuancePolicyListResponse> issuancePolicyListResponseOptional =
        Optional.ofNullable(
            issuancePolicyListResponseCache.getIfPresent(cacheKey));
    if (issuancePolicyListResponseOptional.isPresent()) {
      return completedFuture(issuancePolicyListResponseOptional.get());
    }
    final GetIssuancePolicyListRequest.Builder req = new GetIssuancePolicyListRequest.Builder()
        .ifiID(ifiID)
        .policyEntityType(entityType)
        .policyEntityID(entityID);

    return athenaManagerClient.getIssuancePolicyList(req)
        .thenApply(issuancePolicyListResponse -> {
          issuancePolicyListResponseCache.put(cacheKey, issuancePolicyListResponse);
          return issuancePolicyListResponse;
        });
  }

  public CompletionStage<Product> getProduct(Long ifiID,
                                             Long productID) {
    return getProduct(ifiID, productID, null, null, null);
  }

  public CompletionStage<Product> getProduct(Long ifiID,
                                             Long productFamilyID,
                                             Long coaID,
                                             String productName) {
    return getProduct(ifiID, null, productName, coaID, productFamilyID);
  }

  public CompletionStage<Product> getProduct(Long ifiID,
                                             Long productID,
                                             String productName,
                                             Long coaID,
                                             Long productFamilyID) {
    Optional<Product> productOptional =
        Optional.ofNullable(
            productCache.getIfPresent(getCacheKey(firstNonNull(productID, productName), ifiID)));
    if (productOptional.isPresent()) {
      return completedFuture(productOptional.get());
    }
    return athenaManagerClient
        .getProduct(new GetProductRequest.Builder()
            .ifiID(ifiID)
            .id(productID)
            .name(productName)
            .coaID(coaID)
            .productFamilyID(productFamilyID))
        .thenApply(
            product -> {
              productCache.put(
                  getCacheKey(product.getId(), product.getIfiID()), product);
              productCache.put(
                  getCacheKey(product.getName(), product.getIfiID()), product);
              return product;
            });
  }

  public CompletionStage<ProductFamily> getProductFamily(Long ifiID, Long productFamilyID) {
    Optional<ProductFamily> productFamilyOptional =
        Optional.ofNullable(
            productFamilyCache.getIfPresent(getCacheKey(productFamilyID, ifiID)));
    if (productFamilyOptional.isPresent()) {
      return completedFuture(productFamilyOptional.get());
    }
    return athenaManagerClient
        .getProductFamily(new GetProductFamilyRequest.Builder().ifiID(ifiID).id(productFamilyID))
        .thenApply(
            productFamily -> {
              productFamilyCache.put(
                  getCacheKey(productFamily.getId(), productFamily.getIfiID()),
                  productFamily);
              return productFamily;
            });
  }

  public CompletionStage<Optional<Product>> getOptionalProductByName(String productName, long ifiID) {
    final String cacheKey = getCacheKey(productName, ifiID);
    Optional<Product> productOptional = Optional.ofNullable(productNameCache.getIfPresent(cacheKey));
    if(productOptional.isPresent()) {
      return completedFuture(productOptional);
    }

    return getProductList(productName, ifiID)
        .thenApply(products -> {
          if (!products.isEmpty()) {
            productNameCache.put(cacheKey, products.get(0));
            return Optional.of(products.get(0));
          }
          return Optional.empty();
        });
  }

  public CompletionStage<AccountProvider> getAccountProvider(String accountProviderID, Long ifiID) {
    Optional<AccountProvider> accountProviderOptional = Optional.ofNullable(accountProviderCache.getIfPresent(getCacheKey(accountProviderID, ifiID)));
    if(accountProviderOptional.isPresent()) {
      return completedFuture(accountProviderOptional.get());
    }
    return athenaManagerClient.getAccountProvider(
        GetAccountProviderRequest.builder().id(accountProviderID).ifiID(ifiID))
        .thenApply(accountProvider -> {
          accountProviderCache.put(getCacheKey(accountProviderID, ifiID), accountProvider);
          return accountProvider;
        });
  }

  public CompletionStage<AccountHolderProvider> getAccountHolderProvider(String accountHolderProviderID, Long ifiID) {
    Optional<AccountHolderProvider> accountHolderProviderOptional = Optional.ofNullable(accountHolderProviderCache.getIfPresent(getCacheKey(accountHolderProviderID, ifiID)));
    if(accountHolderProviderOptional.isPresent()) {
      return completedFuture(accountHolderProviderOptional.get());
    }
    return athenaManagerClient.getAccountHolderProvider(
        GetAccountHolderProviderRequest.builder().id(accountHolderProviderID).ifiID(ifiID))
        .thenApply(accountHolderProvider -> {
          accountHolderProviderCache.put(getCacheKey(accountHolderProvider.getId(), accountHolderProvider.getIfiID()), accountHolderProvider);
          return accountHolderProvider;
        });
  }

  public CompletionStage<BaseSchema> getBaseSchema(@Nonnull Long ifiID, @Nonnull String entityType) {
    //TODO: Comment below code to unblock qa. Fix this as a part of FUSION-126.
//    final String cacheKey = getCacheKey(entityType, ifiID);
//    Optional<BaseSchema> baseSchemaOptional = Optional.ofNullable(baseSchemaCache.getIfPresent(cacheKey));
//    if(baseSchemaOptional.isPresent()) {
//      return completedFuture(baseSchemaOptional.get());
//    }
    return athenaManagerClient.getBaseSchema(GetBaseSchemaRequest.builder().ifiID(ifiID).entityType(entityType));
  }

  private CompletionStage<List<Product>> getProductList(String productName, long ifiID) {
    return athenaManagerClient
        .getProductList(
            new GetProductListRequest
                .Builder()
                .name(productName)
                .ifiID(ifiID))
        .thenApply(GetProductListResponse::getProductList);
  }

  private String getCacheKey(Object id, Object ifiID) {
    return id.toString() + "_" + ifiID.toString();
  }

  private String getCacheKey(String entityType, String entityID, Object ifiID) {
    return entityType + "_" + entityID + "_" + ifiID.toString();
  }
}
