package in.zeta.oms.account.service;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.account.LedgerPolicyConfig;
import in.zeta.oms.account.account.ledger.GetAccountBalanceResponse;
import in.zeta.oms.account.server.AccountServiceUserSignatory;
import in.zeta.oms.ledger.GetLedgerInfoRequest;
import in.zeta.oms.ledger.LedgerServiceConstants;
import in.zeta.oms.ledger.api.CreateLedgerRequestPayloadV5;
import in.zeta.oms.ledger.api.CreateLedgerResponsePayloadV5;
import in.zeta.oms.ledger.api.LedgerState;
import in.zeta.oms.ledger.api.Order;
import in.zeta.oms.ledger.api.UpdateIFIProductTypePayload;
import in.zeta.oms.ledger.api.coa.GetCoARequestPayload;
import in.zeta.oms.ledger.api.coa.GetCoAResponsePayload;
import in.zeta.oms.ledger.api.policy.IfiProductType;
import in.zeta.oms.ledger.api.request.GetTransactionsByLedgerIdPayload.Builder;
import in.zeta.oms.ledger.api.request.SetLedgerAttrsPayload;
import in.zeta.oms.ledger.api.request.UpdateLedgerState;
import in.zeta.oms.ledger.api.response.GetTransactionsResponseV2;
import in.zeta.oms.ledger.api.response.SetLedgerInfoResponse;
import in.zeta.oms.ledger.client.LedgerServiceClient;
import olympus.message.types.EmptyPayload;

import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;

import static java.util.stream.Collectors.toMap;

@Singleton
public class LedgerService {

  private static final Integer START_LEVEL = 0;
  private static final Integer END_LEVEL = 1;
  private final AccountServiceUserSignatory signatory;
  private final LedgerServiceClient ledgerClient;
  private static final String LEDGER_POLICY_CONFIG_PATH =
      "/config/data/ledgerIFIProductTypePolicyConfig.json";
  private static Map<String, LedgerPolicyConfig> ledgerPolicyConfig;

  @Inject
  public LedgerService(AccountServiceUserSignatory signatory, LedgerServiceClient ledgerClient) {
    this.signatory = signatory;
    this.ledgerClient = ledgerClient;
    updateLedgerPolicyConfig();
  }

  private void updateLedgerPolicyConfig() {
    Gson gson = new Gson();
    Map<String, Map<String, JsonObject>> configMap =
        gson.fromJson(
            new InputStreamReader(this.getClass().getResourceAsStream(LEDGER_POLICY_CONFIG_PATH)),
            new TypeToken<Map<String, Map<String, JsonObject>>>() {}.getType());

    ledgerPolicyConfig =
        configMap.entrySet().stream()
            .collect(toMap(Map.Entry::getKey, e -> new LedgerPolicyConfig(e.getValue())));
  }

  public static Map<String, LedgerPolicyConfig> getLedgerPolicyConfig() {
    return ledgerPolicyConfig;
  }

  public CompletionStage<CreateLedgerResponsePayloadV5> createLedger(
      CreateLedgerRequestPayloadV5.Builder builder) {
    builder
        .adminUserID(Long.parseLong(signatory.getSignatoryJID().getNodeId()))
        .signatory(signatory);
    return ledgerClient.createLedgerV5(builder);
  }

  public CompletionStage<SetLedgerInfoResponse> setLedgerAttributes(
      Long accountingID,
      Map<String, String> attributesToAddOrUpdate,
      Set<String> attributesToRemove,
      String requestID) {
    return ledgerClient.setLedgerAttrs(
        new SetLedgerAttrsPayload.Builder()
            .ledgerID(accountingID)
            .toAddOrUpdate(attributesToAddOrUpdate)
            .toRemove(attributesToRemove)
            .requestID(requestID)
            .signatory(signatory));
  }

  public CompletionStage<EmptyPayload> updateLedgerState(Long accountingID, LedgerState state) {
    return ledgerClient.updateLedgerState(
        new UpdateLedgerState.Builder().ledgerID(accountingID).state(state).signatory(signatory));
  }

  public CompletionStage<EmptyPayload> updateIFIProductType(
      Long coaId,
      Long ledgerID,
      IfiProductType newIFIProductType,
      Long newParentNodeId,
      Long productFamilyId,
      Long productId) {
    return ledgerClient.updateIFIProductType(
        new UpdateIFIProductTypePayload.Builder()
            .coaId(coaId)
            .ledgerID(ledgerID)
            .newIFIProductType(newIFIProductType)
            .newParentNodeId(newParentNodeId)
            .productFamilyId(productFamilyId)
            .productId(productId)
            .signatory(signatory));
  }

  public CompletionStage<GetAccountBalanceResponse> getLedgerInfo(Long ledgerID) {
    return ledgerClient
        .getLedgerInfo(
            new GetLedgerInfoRequest.Builder().to(LedgerServiceConstants.getRoutingJID(ledgerID)))
        .thenApply(GetAccountBalanceResponse::from);
  }

  public CompletionStage<GetCoAResponsePayload> getCoa(long coaID, long ifi) {
    return ledgerClient.getCoa(
        GetCoARequestPayload.newBuilder()
            .coaId(coaID)
            .ifiId(ifi)
            .includeSummaries(false)
            .signatory(signatory)
            .startLevel(START_LEVEL)
            .endLevel(END_LEVEL)
            .timeStampEpoch(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
  }

  public CompletionStage<GetTransactionsResponseV2> getTransactions(
      Long ledgerID,
      String transactionID,
      Long before,
      Long after,
      Integer pageSize,
      Long pageNumber,
      String recordType,
      Order order) {
    boolean shouldGetPageSize = (pageNumber == 1) && transactionID == null;
    Long offset = (pageNumber - 1) * pageSize;
    final Builder builder =
        new Builder()
            .ledgerID(ledgerID)
            .transactionID(transactionID)
            .before(before)
            .after(after)
            .recordTypes(
                Strings.isNullOrEmpty(recordType)
                    ? ImmutableList.of("DEBIT", "CREDIT")
                    : ImmutableList.of(recordType))
            .pageSize(pageSize)
            .offset(offset)
            .order(order)
            .shouldSkipTrnxCount(!shouldGetPageSize);
    return ledgerClient.getTransactionsByLedgerId(builder);
  }
}
