package in.zeta.oms.account.dao;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorDAO;
import in.zeta.oms.account.api.exception.AccountHolderInsertionException;
import in.zeta.oms.account.api.exception.AccountHolderNotFoundException;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.util.LoggerUtils;
import in.zeta.oms.athenamanager.api.enums.AccountHolderType;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static in.zeta.oms.account.constant.AccountErrorCode.DB_ERROR;
import static in.zeta.oms.account.model.AccountHolderCallbackHandler.getPostgresCallbackHandler;

@Singleton
public class AccountHolderDAO extends PostgresDAO implements RowMapper<AccountHolder> {

  private static final LoggerUtils loggerUtils = LoggerUtils.builder()
      .logger(OlympusSpectra.getLogger(AccountHolderDAO.class))
      .build();

  private static final String TABLE_NAME = "account_holder";

  //------------------------------------------Column Names----------------------------------------//
  public static final String REQUEST_ID = "request_id";
  public static final String ID = "id";
  public static final String IFI_ID = "ifi_id";
  public static final String TYPE = "type";
  public static final String STATUS = "status";
  public static final String ACCOUNT_HOLDER_PROVIDER_ID = "account_holder_provider_id";
  public static final String ATTRIBUTES = "attributes";
  public static final String SALUTATION = "salutation";
  public static final String FIRST_NAME = "firstname";
  public static final String MIDDLE_NAME = "middlename";
  public static final String LAST_NAME = "lastname";
  public static final String PROFILE_PIC_URL = "profilepicurl";
  public static final String DOB = "dob";
  public static final String GENDER = "gender";
  public static final String MOTHERS_MAIDEN_NAME = "mothers_maiden_name";
  public static final String CREATED_AT = "created_at";


  //------------------------------------------Used for Join Query---------------------------------//
  public static final String COL_ID_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, ID);
  public static final String SELECT_COL_ID_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, ID, COL_ID_ALIAS);
  public static final String COL_IFI_ID_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, IFI_ID);
  public static final String SELECT_COL_IFI_ID_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, IFI_ID, COL_IFI_ID_ALIAS);
  public static final String COL_TYPE_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, TYPE);
  public static final String SELECT_COL_TYPE_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, TYPE, COL_TYPE_ALIAS);
  public static final String COL_STATUS_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, STATUS);
  public static final String SELECT_COL_STATUS_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, STATUS, COL_STATUS_ALIAS);
  public static final String COL_ATTRIBUTES_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, ATTRIBUTES);
  public static final String SELECT_COL_ATTRIBUTES_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, ATTRIBUTES, COL_ATTRIBUTES_ALIAS);

  //------------------------------------------Error Messages--------------------------------------//
  private static final String GET_ACCOUNT_HOLDER_REQUEST_FAILED = "Get account holder request failed";
  private static final String ACCOUNT_HOLDER_NOT_FOUND = "Account holder not found";

  //------------------------------------------All Columns-----------------------------------------//
  private static final List<String> INSERT_COLUMNS  = new ImmutableList.Builder<String>()
      .add(REQUEST_ID)
      .add(ID)
      .add(IFI_ID)
      .add(TYPE)
      .add(STATUS)
      .add(ACCOUNT_HOLDER_PROVIDER_ID)
      .add(ATTRIBUTES)
      .add(SALUTATION)
      .add(FIRST_NAME)
      .add(MIDDLE_NAME)
      .add(LAST_NAME)
      .add(PROFILE_PIC_URL)
      .add(DOB)
      .add(GENDER)
      .add(MOTHERS_MAIDEN_NAME)
      .build();
  private static final List<String> SELECT_COLUMNS  = new ImmutableList.Builder<String>()
      .addAll(INSERT_COLUMNS)
      .add(CREATED_AT)
      .build();

  private static final String SELECT_COLUMNS_FOR_JOIN  = new Builder<String>()
      .add(SELECT_COL_ID_WITH_ALIAS)
      .add(REQUEST_ID)
      .add(SALUTATION)
      .add(FIRST_NAME)
      .add(MIDDLE_NAME)
      .add(LAST_NAME)
      .add(PROFILE_PIC_URL)
      .add(DOB)
      .add(GENDER)
      .add(MOTHERS_MAIDEN_NAME)
      .add(SELECT_COL_IFI_ID_WITH_ALIAS)
      .add(SELECT_COL_TYPE_WITH_ALIAS)
      .add(SELECT_COL_STATUS_WITH_ALIAS)
      .add(ACCOUNT_HOLDER_PROVIDER_ID)
      .add(SELECT_COL_ATTRIBUTES_WITH_ALIAS)
      .add(AccountHolderVectorDAO.SELECT_COL_ID_WITH_ALIAS)
      .add(AccountHolderVectorDAO.SELECT_COL_TYPE_WITH_ALIAS)
      .add(AccountHolderVectorDAO.SELECT_COL_VALUE_WITH_ALIAS)
      .build()
      .stream()
      .collect(Collectors.joining(","));

  private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
      .tableName(TABLE_NAME)
      .insertColumns(INSERT_COLUMNS)
      .selectColumns(SELECT_COLUMNS)
      .customColumnType(ACCOUNT_HOLDER_PROVIDER_ID, UUID_TYPE)
      .customColumnType(ID, UUID_TYPE)
      .build();

  private static final String JOIN_QUERY = "select %1$s "
      + "from %2$s "
      + "left join %3$s "
      + "on %2$s.id = %3$s.account_holder_id ";
  private static final String JOIN_QUERY_INTERNAL_ID = JOIN_QUERY
      + "where %2$s.%4$s = ?::UUID and %2$s.%5$s = ?";
  private static final String JOIN_QUERY_REQUEST_ID = JOIN_QUERY
      + "where %2$s.%4$s = ? and %2$s.%5$s = ? AND %2$s.%6$s = ?::UUID";

  private static final String SELECT_QUERY_JOIN_VECTOR_BY_REQUEST_ID = String.format(JOIN_QUERY_REQUEST_ID,
      SELECT_COLUMNS_FOR_JOIN,
      TABLE_NAME,
      AccountHolderVectorDAO.TABLE_NAME,
      REQUEST_ID,
      IFI_ID,
      ACCOUNT_HOLDER_PROVIDER_ID);

  private final Gson gson;
  private static final SpectraLogger log = OlympusSpectra.getLogger(AccountHolderDAO.class);

  @Inject
  public AccountHolderDAO(
      ZetaHostMessagingService hostMessagingService,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  public PostgresInstruction getInsertInstruction(
      AccountHolder accountHolder) {
    List<Object> args = new ArrayList<>();
    args.add(accountHolder.getRequestID());
    args.add(accountHolder.getId());
    args.add(accountHolder.getIfiID());
    args.add(accountHolder.getType().name());
    args.add(accountHolder.getStatus());
    args.add(accountHolder.getAccountHolderProviderID());
    args.add(getJsonbObject(gson.toJson(accountHolder.getAttributes())));
    // TODO: Update existing test cases to update all these below variables
    args.add(accountHolder.getSalutation());
    args.add(accountHolder.getFirstName());
    args.add(accountHolder.getMiddleName());
    args.add(accountHolder.getLastName());
    args.add(accountHolder.getProfilePicURL());
    args.add(accountHolder.getDob());
    args.add(accountHolder.getGender());
    args.add(accountHolder.getMothersMaidenName());

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);

  }

  public CompletionStage<AccountHolder>  getAccountHolderByID(String id, Long ifiID) {

    final ImmutableList<String> criteria =
        new ImmutableList.Builder<String>()
            .add(ID)
            .add(IFI_ID)
            .build();

    return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, id, ifiID)
        .handle((accountHolders, throwable) -> {
          if (throwable != null) {
            log.error(GET_ACCOUNT_HOLDER_REQUEST_FAILED, throwable)
                .attr("ifiID", ifiID)
                .attr("id", id)
                .log();
            throw new AccountServiceException(GET_ACCOUNT_HOLDER_REQUEST_FAILED, DB_ERROR);
          } else if (accountHolders.isEmpty()) {
            log.info(ACCOUNT_HOLDER_NOT_FOUND)
                .attr("id", id)
                .attr("ifiID", ifiID)
                .log();
            throw new AccountHolderNotFoundException(
                String.format("Account holder not found for id: %s", id));
          }
          return accountHolders.get(0);
        });
  }

  public CompletionStage<Optional<AccountHolder>> getAccountHolderWithRequestID(String requestID, String accountHolderProviderID, Long ifiID) {
    return query(SELECT_QUERY_JOIN_VECTOR_BY_REQUEST_ID, getPostgresCallbackHandler(), requestID, ifiID, accountHolderProviderID)
        .handle((accountHolders, throwable) -> {
          if (throwable != null) {
            throw loggerUtils.logDBException("Get Account holder failed with request id " + requestID, throwable);
          }
          if (accountHolders.size() > 1) {
            throw new AccountHolderInsertionException("Multiple account holders exist with same request ID", AccountErrorCode.DUPLICATE_REQUEST_EXCEPTION);
          }
          if (accountHolders.isEmpty()) {
            return Optional.empty();
          }
          return Optional.of(accountHolders.get(0));
        });
  }

  private void updateUserProfile(AccountHolder accountHolder, List<String> columnsToUpdate, List<Object> arguments) {

    if(accountHolder.getSalutation() != null) {
      columnsToUpdate.add(SALUTATION);
      arguments.add(accountHolder.getSalutation());
    }

    if(accountHolder.getFirstName() != null) {
      columnsToUpdate.add(FIRST_NAME);
      arguments.add(accountHolder.getFirstName());
    }

    if(accountHolder.getMiddleName() != null) {
      columnsToUpdate.add(MIDDLE_NAME);
      arguments.add(accountHolder.getMiddleName());
    }

    if(accountHolder.getLastName() != null) {
      columnsToUpdate.add(LAST_NAME);
      arguments.add(accountHolder.getLastName());
    }

    if(accountHolder.getProfilePicURL() != null) {
      columnsToUpdate.add(PROFILE_PIC_URL);
      arguments.add(accountHolder.getProfilePicURL());
    }

    if(accountHolder.getDob() != null) {
      columnsToUpdate.add(DOB);
      arguments.add(accountHolder.getDob());
    }

    if(accountHolder.getGender() != null) {
      columnsToUpdate.add(GENDER);
      arguments.add(accountHolder.getGender());
    }

    if(accountHolder.getMothersMaidenName() != null) {
      columnsToUpdate.add(MOTHERS_MAIDEN_NAME);
      arguments.add(accountHolder.getMothersMaidenName());
    }
  }

  public CompletionStage<Void> updateAccountHolder(AccountHolder accountHolder) {
    final List<Object> arguments = new ArrayList<>();
    final List<String> columnsToUpdate = new ArrayList<>();
    final List<String> whereClause = new ArrayList<>();

    whereClause.add(ID);
    whereClause.add(IFI_ID);

    if(null != accountHolder.getAttributes()) {
      columnsToUpdate.add(ATTRIBUTES);
      arguments.add(getJsonbObject(gson.toJson(accountHolder.getAttributes())));
    }

    if(!Strings.isNullOrEmpty(accountHolder.getStatus())) {
      columnsToUpdate.add(STATUS);
      arguments.add(accountHolder.getStatus());
    }

    updateUserProfile(accountHolder, columnsToUpdate, arguments);

    arguments.add(accountHolder.getId());
    arguments.add(accountHolder.getIfiID());

    return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(columnsToUpdate, whereClause), arguments)
        .thenAccept(
            rows -> {
              if (rows == 0) {
                log.warn("AccountHolder not found during update")
                    .attr("id", accountHolder.getId())
                    .attr("ifiID", accountHolder.getIfiID())
                    .log();
                throw new AccountHolderNotFoundException(
                    String.format("Account Holder Not Found for id: %s ifiID: %s",accountHolder.getId(), accountHolder.getIfiID()));
              }
            });
  }

  @Override
  public AccountHolder mapRow(ResultSet rs, int rowNum) throws SQLException {
    return AccountHolder.builder()
        .requestID(rs.getString(REQUEST_ID))
        .id(rs.getString(ID))
        .ifiID(rs.getLong(IFI_ID))
        .type(AccountHolderType.valueOf(rs.getString(TYPE)))
        .status(rs.getString(STATUS))
        .accountHolderProviderID(rs.getString(ACCOUNT_HOLDER_PROVIDER_ID))
        .attributes(
            gson.fromJson(rs.getString(AccountHolderDAO.ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .salutation(rs.getString(AccountHolderDAO.SALUTATION))
        .firstName(rs.getString(AccountHolderDAO.FIRST_NAME))
        .middleName(rs.getString(AccountHolderDAO.MIDDLE_NAME))
        .lastName(rs.getString(AccountHolderDAO.LAST_NAME))
        .profilePicURL(rs.getString(AccountHolderDAO.PROFILE_PIC_URL))
        .dob(rs.getDate(AccountHolderDAO.DOB))
        .gender(rs.getString(AccountHolderDAO.GENDER))
        .mothersMaidenName(rs.getString(AccountHolderDAO.MOTHERS_MAIDEN_NAME))
        .createdAt(rs.getTimestamp(CREATED_AT).toLocalDateTime())
        .build();
  }

}
