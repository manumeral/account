package in.zeta.oms.account.accountHolder.vector;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.api.exception.AccountHolderVectorCreationException;
import in.zeta.oms.account.api.exception.AccountHolderVectorNotFoundException;
import in.zeta.oms.account.api.exception.AccountHolderVectorUpdateException;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;

import java.util.List;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class AccountHolderVectorService {
    private static final SpectraLogger logger = OlympusSpectra.getLogger(AccountHolderVectorService.class);

    private final AccountHolderVectorDAO accountHolderVectorDAO;
    private final SecureRandomGenerator secureRandomGenerator;

    @Inject
    public AccountHolderVectorService(
        AccountHolderVectorDAO accountHolderVectorDAO, SecureRandomGenerator secureRandomGenerator) {
        this.accountHolderVectorDAO = accountHolderVectorDAO;
        this.secureRandomGenerator = secureRandomGenerator;
    }

    @PublishEvent(topicName = PubSubTopics.ADD_ACCOUNT_HOLDER_VECTOR_EVENT_TOPIC)
    public CompletionStage<AccountHolderVector> addVector(AccountHolderVector accountHolderVector) {
        final AccountHolderVector accountHolderVectorToAdd =
            accountHolderVector.toBuilder().id(secureRandomGenerator.generateUUID()).build();
        return accountHolderVectorDAO
            .insert(accountHolderVectorToAdd)
            .thenCompose(
                ignore ->
                    get(
                        accountHolderVectorToAdd.getId(),
                        accountHolderVectorToAdd.getAccountHolderID(),
                        accountHolderVectorToAdd.getIfiID()))
            .exceptionally(
                t -> {
                    t = unwrapCompletionStateException(t);
                    logger.warn("Account Holder Vector creation failed")
                        .attr("accountHolderID", accountHolderVector.getAccountHolderID())
                        .attr("ifiID", accountHolderVector.getIfiID())
                        .attr("type", accountHolderVector.getType())
                        .attr("value", accountHolderVector.getValue())
                        .attr("errMsg", t.getMessage())
                        .log();
                    throw new AccountHolderVectorCreationException(
                        String.format(
                            "Account Holder Vector Creation failed for account Holder ID : %s",
                            accountHolderVectorToAdd.getAccountHolderID()),
                        t,
                        AccountErrorCode.INTERNAL_ERROR);
                });
    }

    public CompletionStage<AccountHolderVector> get(String id, String accountHolderID, Long ifiID) {
        return accountHolderVectorDAO
            .get(id, accountHolderID, ifiID)
            .thenApply(
                accountHolderVectorOptional ->
                    accountHolderVectorOptional.orElseThrow(
                        () ->
                            new AccountHolderVectorNotFoundException(
                                String.format(
                                    "Account Holder Vector for account ID : %s and account holder vector id : %s not found",
                                    accountHolderID, id),
                                AccountErrorCode.ACCOUNT_VECTOR_NOT_FOUND)));
    }

    public CompletionStage<List<AccountHolderVector>> getVectorsByAccountHolderID(Long ifiID, String accountHolderID) {
        return accountHolderVectorDAO
            .get(ifiID, accountHolderID);
    }

    public CompletionStage<AccountHolderVector> getByVectorNotThrow(String type, String value, Long ifiID, String accountHolderID) {
        return accountHolderVectorDAO
            .getByVector(type, value, ifiID)
            .thenApply(
                accountHolderVectorOptional -> {
                    if (accountHolderVectorOptional.isPresent()) {
                       return null;
                    } else {
                        return AccountHolderVector
                            .builder()
                            .id(secureRandomGenerator.generateUUID())
                            .ifiID(ifiID)
                            .type(type)
                            .value(value)
                            .status("ENABLED")
                            .accountHolderID(accountHolderID)
                            .build();
                    }
                });
    }

    public CompletionStage<AccountHolderVector> getByVector(String type, String value, Long ifiID) {
        return accountHolderVectorDAO
            .getByVector(type, value, ifiID)
            .thenApply(
                accountHolderVectorOptional ->
                    accountHolderVectorOptional.orElseThrow(
                        () ->
                            new AccountHolderVectorNotFoundException(
                                String.format(
                                    "Get AccountHolderVector failed for type: %s value : %s ifiID : %s",
                                    type, value, ifiID),
                                AccountErrorCode.ACCOUNT_HOLDER_VECTOR_NOT_FOUND)));
    }

    @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_HOLDER_VECTOR_EVENT_TOPIC)
    public CompletionStage<AccountHolderVector> update(AccountHolderVector accountHolderVector) {
        AccountHolderVector.Builder accountHolderVectorBuilder = accountHolderVector.toBuilder();
        CompletionStage<String> accountHolderVectorIDFuture = completedFuture(accountHolderVector.getId());
        if (null == accountHolderVector.getId()) {
            accountHolderVectorIDFuture =
                accountHolderVectorDAO
                    .getByVector(
                        accountHolderVector.getType(),
                        accountHolderVector.getValue(),
                        accountHolderVector.getIfiID())
                    .thenApply(
                        accountHolderVectorOptional ->
                            accountHolderVectorOptional
                                .map(AccountHolderVector::getId)
                                .orElseThrow(
                                    () ->
                                        AccountHolderVectorNotFoundException.accountNotFoundForTypeValuePair(
                                            accountHolderVector.getType(),
                                            accountHolderVector.getValue(),
                                            accountHolderVector.getIfiID(),
                                            accountHolderVector.getId())));
        }

        return accountHolderVectorIDFuture
            .thenApply(accountHolderVectorBuilder::id)
            .thenCompose(builder -> accountHolderVectorDAO.update(builder.build()))
            .thenCompose(
                ignore ->
                    accountHolderVectorDAO.get(
                        accountHolderVectorBuilder.build().getId(),
                        accountHolderVector.getAccountHolderID(),
                        accountHolderVector.getIfiID()))
            .thenApply(
                accountHolderVectorOptional ->
                    accountHolderVectorOptional.orElseThrow(
                        () ->
                            new AccountHolderVectorUpdateException(
                                String.format(
                                    "Account Holder Vector update for account Holder ID : %s and account vector id : %s failed",
                                    accountHolderVector.getAccountHolderID(), accountHolderVectorBuilder.build().getId()),
                                AccountErrorCode.INTERNAL_ERROR)));
    }
}
