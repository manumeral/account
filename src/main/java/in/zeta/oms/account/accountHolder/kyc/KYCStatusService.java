package in.zeta.oms.account.accountHolder.kyc;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.concurrency.CompletableFutures;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorService;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;

import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static in.zeta.commons.util.ZetaUrlConstants.AADHAAR_PREFIX;
import static in.zeta.commons.util.ZetaUrlConstants.PAN_PREFIX;
import static java.util.concurrent.CompletableFuture.completedFuture;

@Singleton
public class KYCStatusService {

  private final KYCStatusDAO kycStatusDAO;
  private final AccountHolderVectorService accountHolderVectorService;

  @Inject
  public KYCStatusService(
      KYCStatusDAO kycStatusDAO,
      AccountHolderVectorService accountHolderVectorService) {
    this.kycStatusDAO = kycStatusDAO;
      this.accountHolderVectorService = accountHolderVectorService;
  }

  @PublishEvent(topicName = PubSubTopics.CREATE_KYC_STATUS_EVENT_TOPIC)
  public CompletionStage<KYCStatus> createKycStatus(
      KYCStatus kycStatus) {
    return kycStatusDAO
        .get(kycStatus.getAccountHolderID(), kycStatus.getIfiID())
        .thenCompose(
            existingKYC -> {
              if (existingKYC.isPresent()) {
                return completedFuture(existingKYC.get());
              }
                  //TODO: Update Kyc status
              return kycStatusDAO.insert(kycStatus)
                  .thenCompose(ignore -> get(kycStatus.getAccountHolderID(), kycStatus.getIfiID()));
            });
  }

  public CompletionStage<KYCStatus> get(String accountHolderID, Long ifiID) {
    return getOptional(accountHolderID, ifiID)
        .thenApply(
            kycStatusOptional -> {
              if (kycStatusOptional.isPresent()) {
                return kycStatusOptional.get();
              }
              throw KYCStatusNotFoundException.exceptionByAccountHolderID(accountHolderID);
            });
  }

  public CompletionStage<Optional<KYCStatus>> getOptional(String accountHolderID, Long ifiID) {
    return kycStatusDAO
        .get(accountHolderID, ifiID);
  }

  // This function will backfill account holder vectors like pan and aadhaar from kyc status.
  // This needs to be done for existing zeta users which have ovd details.
  public CompletionStage<Void> backfillAccountHolderVectors(KYCStatus kycStatus) {
      Map<String, String> kycStatusAttributes = kycStatus.getAttributes();

      List<CompletionStage<AccountHolderVector>> accountHolderVectorCompletionStage = new ArrayList<>();
      if (null != kycStatusAttributes && !Strings.isNullOrEmpty(kycStatusAttributes.get(PAN_PREFIX))) {
          accountHolderVectorCompletionStage.add(accountHolderVectorService.getByVectorNotThrow(PAN_PREFIX, kycStatusAttributes.get(PAN_PREFIX),kycStatus.getIfiID(),kycStatus.getAccountHolderID()));
      }

      if (null != kycStatusAttributes && !Strings.isNullOrEmpty(kycStatusAttributes.get(AADHAAR_PREFIX))) {
          accountHolderVectorCompletionStage.add(accountHolderVectorService.getByVectorNotThrow(AADHAAR_PREFIX, kycStatusAttributes.get(AADHAAR_PREFIX),kycStatus.getIfiID(),kycStatus.getAccountHolderID()));
      }

      if (!accountHolderVectorCompletionStage.isEmpty()) {
          return CompletableFutures.allResultsOf(accountHolderVectorCompletionStage)
              .thenApply(accountHolderVectorsList -> accountHolderVectorsList.stream()
                  .filter(accountHolderVector -> accountHolderVector != null)
                  .map(accountHolderVector -> accountHolderVectorService.addVector(accountHolderVector))
                  .collect(Collectors.toList()))
              .thenAccept(ignore -> {});
      }

      return completedFuture(null);
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_KYC_STATUS_EVENT_TOPIC)
  public CompletionStage<KYCStatus> updateEffectiveKycStatus(UpdateKycStatusRequest payload) {
    final String accountHolderID = payload.getAccountHolderID();
    final long ifiID = payload.getIfiID();
    return get(accountHolderID, ifiID)
        .thenCompose(ignore -> {
            KYCStatus kycStatusUpdate = KYCStatus.builder()
                .accountHolderID(accountHolderID)
                .ifiID(ifiID)
                .kycStatus(payload.getKycStatus())
                .kycStatusPostExpiry(payload.getKycStatusPostExpiry())
                .expiryTime(payload.getExpiryTime())
                .attributes(payload.getAttributes())
                .build();

            return backfillAccountHolderVectors(kycStatusUpdate)
                .thenCompose(i -> kycStatusDAO.update(kycStatusUpdate));
        })
        .thenCompose(ignore -> get(accountHolderID, ifiID));
  }
}
