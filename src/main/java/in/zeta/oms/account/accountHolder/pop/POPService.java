package in.zeta.oms.account.accountHolder.pop;

import com.google.inject.Inject;
import in.zeta.oms.account.pop.AddPOPException;
import in.zeta.oms.account.pop.POP;
import in.zeta.oms.account.pop.POPNotFoundException;

import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static in.zeta.oms.account.constant.AccountErrorCode.INTERNAL_ERROR;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

public class POPService {

    private final POPDAO popDAO;

    @Inject
    public POPService(POPDAO popDAO) {
        this.popDAO = popDAO;
    }

    @PublishEvent(topicName = PubSubTopics.ADD_POP_EVENT_TOPIC)
    public CompletionStage<POP> addPOP(POP pop) {

        //TODO: Add validation of status
        return popDAO
            .getPOPByLabel(pop.getIfiID(), pop.getLabel(), pop.getEntityID(), pop.getEntityType())
            .thenCompose(popOption -> {
                if(popOption.isPresent()) {
                    return completedFuture(popOption.get());
                }
                return popDAO.addPOP(pop)
                    .thenCompose(p -> popDAO.getPOPByID(pop.getIfiID(), pop.getId(), pop.getEntityID(), pop.getEntityType()))
                    .thenApply(Optional::get);
            })
            .exceptionally(throwable -> {
                throwable = unwrapCompletionStateException(throwable);
                throw new AddPOPException("POP creation failed", throwable, INTERNAL_ERROR);
            });

    }

    @PublishEvent(topicName = PubSubTopics.UPDATE_POP_EVENT_TOPIC)
    public CompletionStage<POP> updatePOP(POP pop) {
        return getPOPByID(pop.getIfiID(), pop.getId(), pop.getEntityID(), pop.getEntityType())
            .thenCompose(ignore -> popDAO.updatePOP(pop))
            .thenCompose(ignore -> getPOPByID(pop.getIfiID(), pop.getId(), pop.getEntityID(), pop.getEntityType()));
    }

    public CompletionStage<POP> setPOPAsDefault(String id, Long ifiID, String entityID, String entityType) {
        return popDAO
            .markDefaultAsFalse(ifiID, entityID, entityType)
            .thenCompose(ignore -> popDAO.setPOPAsDefault(id, ifiID, entityID, entityType))
            .thenCompose(ignore -> getPOPByID(ifiID, id, entityID, entityType));
    }

    public CompletionStage<POP> getPOPByID(Long ifiID, String id, String entityID, String entityType) {
        return popDAO.getPOPByID(ifiID, id, entityID, entityType)
            .thenApply(popOptional -> popOptional.orElseThrow(() -> new POPNotFoundException(id)));
    }

    public CompletionStage<List<POP>> listPOPs(Long ifiID, String entityID, String entityType) {
        return  popDAO.listPOPs(ifiID, entityID, entityType);
    }

    @PublishEvent(topicName = PubSubTopics.DELETE_POP_EVENT_TOPIC)
    public CompletionStage<POP> deletePOP(Long ifiID, String entityID, String id, String entityType) {
        return  popDAO.updateStatus(id, ifiID, entityID, entityType, POPDAO.DELETED)
            .thenCompose(ignore -> popDAO.getPOPByID(ifiID, id, entityID, entityType))
            .thenApply(popOptional -> popOptional.orElseThrow(() -> new POPNotFoundException(id)));
    }
}
