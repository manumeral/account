package in.zeta.oms.account.accountHolder;

import com.google.inject.Inject;
import com.google.inject.Provider;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.service.AccountHolderService;
import in.zeta.tags.ObjectProvider;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

public class AccountHolderObjectProvider implements ObjectProvider<AccountHolder> {

  private final Provider<AccountHolderService> accountHolderServiceProvider;

  @Inject
  public AccountHolderObjectProvider(Provider<AccountHolderService> accountHolderServiceProvider) {
    this.accountHolderServiceProvider = accountHolderServiceProvider;
  }

  @Override
  public CompletionStage<Optional<AccountHolder>> getObject(Long ifiId, String objectId) {
    return accountHolderServiceProvider.get().get(ifiId, objectId)
        .thenApply(Optional::of);
  }

}
