package in.zeta.oms.account.accountHolder.kyc;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import olympus.message.types.Request;

@EagerSingleton
public class KYCStatusHandler extends AccountServiceBaseRequestHandler {

  private final KYCStatusService kycStatusService;

  @Inject
  public KYCStatusHandler(ZetaHostMessagingService zhms,
                          KYCStatusService kycStatusService) {
    super(zhms);
    this.kycStatusService = kycStatusService;
  }

  @Authorized(anyOf = {"api.account.accountHolder.kycStatus.create", "api.account.accountHolder.kycStatus.admin"})
  public void on(AddKycStatusRequest payload, Request<AddKycStatusRequest> request) {
    final KYCStatus createKYCStatus = KYCStatus.builder()
        .ifiID(payload.getIfiID())
        .accountHolderID(payload.getAccountHolderID())
        .kycStatusPostExpiry(payload.getKycStatusPostExpiry())
        .expiryTime(payload.getExpiryTime())
        .attributes(payload.getAttributes())
        .kycStatus(payload.getKycStatus())
        .build();

    onRequest(payload, request);
    kycStatusService.createKycStatus(createKYCStatus)
        .thenAccept(kycStatusResponse -> onResult(kycStatusResponse, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.kycStatus.get", "api.account.accountHolder.kycStatus.admin"})
  public void on(GetKycStatusRequest payload, Request<GetKycStatusRequest> request) {
    onRequest(payload, request);
    kycStatusService.get(payload.getAccountHolderID(), payload.getIfiID())
        .thenAccept(kycStatusResponse -> onResult(kycStatusResponse, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.kycStatus.update", "api.account.accountHolder.kycStatus.admin"})
  public void on(UpdateKycStatusRequest payload, Request<UpdateKycStatusRequest> request) {
    onRequest(payload, request);
    kycStatusService.updateEffectiveKycStatus(payload)
        .thenAccept(kycStatusResponse -> onResult(kycStatusResponse, request))
        .exceptionally(t -> onError(request, t));
  }

}
