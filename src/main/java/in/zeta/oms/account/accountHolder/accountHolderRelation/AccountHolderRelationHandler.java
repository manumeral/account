package in.zeta.oms.account.accountHolder.accountHolderRelation;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import olympus.message.types.Request;

@EagerSingleton
public class AccountHolderRelationHandler extends AccountServiceBaseRequestHandler {

  private final AccountHolderRelationService accountHolderRelationService;

  @Inject
  public AccountHolderRelationHandler(ZetaHostMessagingService zhms,
                                      AccountHolderRelationService AccountHolderRelationService) {
    super(zhms);
    this.accountHolderRelationService = AccountHolderRelationService;
  }

  @Authorized(anyOf = {"api.account.accountHolder.relation.create", "api.account.accountHolder.relation.admin"})
  public void on(AddAccountHolderRelationRequest payload, Request<AddAccountHolderRelationRequest> request) {
    final AccountHolderRelation accountHolderRelation = AccountHolderRelation.builder()
        .ifiID(payload.getIfiID())
        .accountHolderID(payload.getAccountHolderID())
        .relatedAccountHolderID(payload.getRelatedAccountHolderID())
        .relationshipType(payload.getRelationshipType())
        .attributes(payload.getAttributes())
        .build();

    onRequest(payload, request);
    accountHolderRelationService.createAccountHolderRelation(accountHolderRelation)
        .thenAccept(accountHolderRelationResponse -> onResult(accountHolderRelationResponse, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.relation.get", "api.account.accountHolder.relation.admin"})
  public void on(GetAccountHolderRelationForAccountHolderRequest payload, Request<GetAccountHolderRelationForAccountHolderRequest> request) {
    onRequest(payload, request);
    accountHolderRelationService.getRelationsByAccountHolderID(payload.getAccountHolderID(), payload.getIfiID())
        .thenAccept(accountHolderRelations -> onResult(new GetAccountHolderRelationListResponse(accountHolderRelations), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.relation.get", "api.account.accountHolder.relation.admin"})
  public void on(GetAccountHolderRelationByIDRequest payload, Request<GetAccountHolderRelationByIDRequest> request) {
    onRequest(payload, request);
    accountHolderRelationService.get(payload.getId(), payload.getAccountHolderID(), payload.getIfiID())
        .thenAccept(accountHolderRelation -> onResult(accountHolderRelation, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.relation.update", "api.account.accountHolder.relation.admin"})
  public void on(UpdateAccountHolderRelationRequest payload, Request<UpdateAccountHolderRelationRequest> request) {
    onRequest(payload, request);
    accountHolderRelationService.updateRelation(payload)
        .thenAccept(accountHolderRelation -> onResult(accountHolderRelation, request))
        .exceptionally(t -> onError(request, t));
  }

}
