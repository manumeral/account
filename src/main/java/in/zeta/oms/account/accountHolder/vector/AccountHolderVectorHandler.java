package in.zeta.oms.account.accountHolder.vector;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.account.Status;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import olympus.message.types.Request;

@EagerSingleton
public class AccountHolderVectorHandler extends AccountServiceBaseRequestHandler {


    private final AccountHolderVectorService accountHolderVectorService;

    @Inject
    public AccountHolderVectorHandler(ZetaHostMessagingService zhms,
                          AccountHolderVectorService accountHolderVectorService) {
        super(zhms);
        this.accountHolderVectorService = accountHolderVectorService;
    }

    @Authorized(anyOf = {"api.account.accountHolder.addVector", "api.account.accountHolder.admin"})
    public void on(AddAccountHolderVectorRequest payload, Request<AddAccountHolderVectorRequest> request) {
        onRequest(payload, request);
        AccountHolderVector accountHolderVector =
            AccountHolderVector.builder()
                .accountHolderID(payload.getAccountHolderID())
                .status(payload.getStatus())
                .attributes(payload.getAttributes())
                .type(payload.getType())
                .value(payload.getValue())
                .ifiID(payload.getIfiID())
                .build();
        accountHolderVectorService
            .addVector(accountHolderVector)
            .thenAccept(accountVectorResponse -> onResult(accountVectorResponse, request))
            .exceptionally(t -> onError(request, t));
    }

    @Authorized(anyOf = {"api.account.accountHolder.deleteVector", "api.account.accountHolder.admin"})
    public  void on(DeleteAccountHolderVectorRequest payload, Request<DeleteAccountHolderVectorRequest> request) {
        AccountHolderVector accountHolderVector = AccountHolderVector.builder()
            .ifiID(payload.getIfiID())
            .accountHolderID(payload.getAccountHolderID())
            .id(payload.getId())
            .status(Status.DELETED.name())
            .build();
        accountHolderVectorService
            .update(accountHolderVector)
            .thenAccept(accountVectorResponse ->  onResult(accountVectorResponse, request))
            .exceptionally(t -> onError(request, t));
    }

    @Authorized(anyOf = {"api.account.accountHolder.updateVector", "api.account.accountHolder.admin"})
    public  void on(UpdateAccountHolderVectorRequest payload, Request<UpdateAccountHolderVectorRequest> request) {
        onRequest(payload, request);
        AccountHolderVector accountHolderVector = AccountHolderVector.builder()
            .ifiID(payload.getIfiID())
            .accountHolderID(payload.getAccountHolderID())
            .id(payload.getId())
            .type(payload.getType())
            .value(payload.getValue())
            .status(payload.getStatus())
            .attributes(payload.getAttributes())
            .build();
        accountHolderVectorService
            .update(accountHolderVector)
            .thenAccept(accountVectorResponse ->  onResult(accountVectorResponse, request))
            .exceptionally(t -> onError(request, t));
    }
}
