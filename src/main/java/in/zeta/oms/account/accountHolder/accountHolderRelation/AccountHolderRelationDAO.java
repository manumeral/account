package in.zeta.oms.account.accountHolder.accountHolderRelation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.exception.AccountHolderNotFoundException;
import in.zeta.oms.account.util.LoggerUtils;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static com.google.common.base.Strings.isNullOrEmpty;
import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static java.lang.String.format;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

// TODO: Add test cases
@Singleton
public class AccountHolderRelationDAO extends PostgresDAO implements RowMapper<AccountHolderRelation> {

  private static final LoggerUtils loggerUtils = LoggerUtils.builder()
      .logger(OlympusSpectra.getLogger(AccountHolderRelationDAO.class))
      .build();

  private static final String TABLE_NAME = "account_holder_relation";

  //--------------------------------------Column Names----------------------------------------------
  private static final String ID = "id";
  private static final String IFI_ID = "ifi_id";
  private static final String ACCOUNT_HOLDER_ID = "account_holder_id";
  private static final String RELATED_ACCOUNT_HOLDER_ID = "related_account_holder_id";
  private static final String RELATIONSHIP_TYPE = "relationship_type";
  private static final String STATUS = "status";
  private static final String ATTRIBUTES = "attributes";
  private static final String CREATED_AT = "created_at";
  private static final String MODIFIED_AT = "modified_at";
  //--------------------------------------------//--------------------------------------------------

  //--------------------------------------Foreign Keys----------------------------------------------
  private static final String FK_ACCOUNT_HOLDER_ID = "account_holder_relation_account_holder_id_fkey";
  private static final String FK_RELATED_ACCOUNT_HOLDER_ID = "account_holder_relation_related_account_holder_id_fkey";
  //--------------------------------------------//--------------------------------------------------

  private static final ImmutableList<String> INSERT_COLUMNS = new Builder<String>()
      .add(ID)
      .add(IFI_ID)
      .add(ACCOUNT_HOLDER_ID)
      .add(RELATED_ACCOUNT_HOLDER_ID)
      .add(RELATIONSHIP_TYPE)
      .add(STATUS)
      .add(ATTRIBUTES)
      .build();

  private static final ImmutableList<String> SELECT_COLUMNS = new Builder<String>()
      .addAll(INSERT_COLUMNS)
      .add(CREATED_AT)
      .add(MODIFIED_AT)
      .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
      .tableName(TABLE_NAME)
      .insertColumns(INSERT_COLUMNS)
      .selectColumns(SELECT_COLUMNS)
      .customColumnType(ID, UUID_TYPE)
      .customColumnType(ACCOUNT_HOLDER_ID, UUID_TYPE)
      .customColumnType(RELATED_ACCOUNT_HOLDER_ID, UUID_TYPE)
      .customColumnType(ATTRIBUTES, JSONB_TYPE)
      .build();

  private final Gson gson;

  @Inject
  public AccountHolderRelationDAO(ZetaHostMessagingService hostMessagingService,
                                  BasicDataSource basicDataSource,
                                  @Named("postgres.jdbc.pool.size") int poolSize,
                                  Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  @TimeLogger
  public CompletionStage<AccountHolderRelation> insert(AccountHolderRelation accountHolderRelation) {
    final String accountHolderID = accountHolderRelation.getAccountHolderID();
    final String relatedAccountHolderID = accountHolderRelation.getRelatedAccountHolderID();

    List<Object> argsList = new ArrayList<>();
    argsList.add(accountHolderRelation.getId());
    argsList.add(accountHolderRelation.getIfiID());
    argsList.add(accountHolderID);
    argsList.add(relatedAccountHolderID);
    argsList.add(accountHolderRelation.getRelationshipType());
    argsList.add(accountHolderRelation.getStatus());
    argsList.add(gson.toJson(accountHolderRelation.getAttributes()));

    return update(PG_QUERY_GENERATOR.getInsertQuery(), argsList)
        .thenApply(ignore -> accountHolderRelation)
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              handleForeignKeyException(accountHolderID, relatedAccountHolderID, t);
              throw loggerUtils.logDBException("Add accountHolderRelation Failed", t);
            });
  }

  @TimeLogger
  public CompletionStage<Optional<AccountHolderRelation>> get(String id, String accountHolderID, Long ifiID) {
    final ImmutableList<String> selectionCriteria = new Builder<String>()
        .add(ID)
        .add(ACCOUNT_HOLDER_ID)
        .add(IFI_ID)
        .build();
    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionCriteria), this, id, accountHolderID, ifiID)
        .exceptionally(t -> {
          throw loggerUtils
              .logDBException(format("Get AccountHolderRelation failed by id :%s", id), t);
        });
  }

  @TimeLogger
  public CompletionStage<Optional<AccountHolderRelation>> getRelationByAccountHolderID(String accountHolderID, String relatedAccountHolderID, String relationshipType, long ifiID) {
    final ImmutableList<String> selectionCriteria = new Builder<String>()
        .add(ACCOUNT_HOLDER_ID)
        .add(RELATED_ACCOUNT_HOLDER_ID)
        .add(RELATIONSHIP_TYPE)
        .add(IFI_ID)
        .build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionCriteria), this, accountHolderID, relatedAccountHolderID, relationshipType, ifiID)
        .exceptionally(t -> {
          throw loggerUtils
              .logDBException(format("Get AccountHolderRelation failed by accountHolderID :%s", accountHolderID), t);
        });
  }

  @TimeLogger
  public CompletionStage<List<AccountHolderRelation>> getRelations(String accountHolderID, long ifiID) {
    final ImmutableList<String> selectionCriteria = new Builder<String>()
        .add(ACCOUNT_HOLDER_ID)
        .add(IFI_ID)
        .build();
    return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionCriteria), this, accountHolderID, ifiID)
        .exceptionally(t -> {
          throw loggerUtils
              .logDBException(format("Get AccountHolderRelation failed by accountHolderID :%s", accountHolderID), t);
        });
  }

  @TimeLogger
  public CompletionStage<Void> update(AccountHolderRelation accountHolderRelation) {
    final List<String> whereClauseColumnNames = Arrays.asList(ID, ACCOUNT_HOLDER_ID, IFI_ID);
    ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
    ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
    if (!isNullOrEmpty(accountHolderRelation.getStatus())) {
      updateColumnNames.add(STATUS);
      argsList.add(accountHolderRelation.getStatus());
    }
    if(null != accountHolderRelation.getAttributes()) {
      updateColumnNames.add(ATTRIBUTES);
      argsList.add(getJsonbObject(gson.toJson(accountHolderRelation.getAttributes())));
    }
    if(null != accountHolderRelation.getRelationshipType()) {
      updateColumnNames.add(RELATIONSHIP_TYPE);
      argsList.add(accountHolderRelation.getRelationshipType());
    }
    final String relatedAccountHolderID = accountHolderRelation.getRelatedAccountHolderID();
    if(null != relatedAccountHolderID) {
      updateColumnNames.add(RELATED_ACCOUNT_HOLDER_ID);
      argsList.add(relatedAccountHolderID);
    }
    argsList.add(accountHolderRelation.getId());
    final String accountHolderID = accountHolderRelation.getAccountHolderID();
    argsList.add(accountHolderID);
    argsList.add(accountHolderRelation.getIfiID());
    return update(
            PG_QUERY_GENERATOR.getUpdateQueryAndJoin(
                updateColumnNames.build(), whereClauseColumnNames),
            argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              handleForeignKeyException(accountHolderID, relatedAccountHolderID, throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Error Updating relationID : %s for accountHolderID : %s",
                      accountHolderRelation.getId(), accountHolderID),
                  throwable);
            });
  }

  private void handleForeignKeyException(String accountHolderID, String relatedAccountHolderID, Throwable t) {
    if (t instanceof DataIntegrityViolationException) {
      if (t.getLocalizedMessage().contains(FK_ACCOUNT_HOLDER_ID)) {
        throw AccountHolderNotFoundException.forAccountHolderID(accountHolderID);
      }
      if (t.getLocalizedMessage().contains(FK_RELATED_ACCOUNT_HOLDER_ID)) {
        throw AccountHolderNotFoundException.forAccountHolderID(relatedAccountHolderID);
      }
    }
  }

  @Override
  public AccountHolderRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
    return AccountHolderRelation.builder()
        .id(rs.getString(ID))
        .ifiID(rs.getLong(IFI_ID))
        .accountHolderID(rs.getString(ACCOUNT_HOLDER_ID))
        .relatedAccountHolderID(rs.getString(RELATED_ACCOUNT_HOLDER_ID))
        .relationshipType(rs.getString(RELATIONSHIP_TYPE))
        .status(rs.getString(STATUS))
        .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .createdAt(rs.getTimestamp(CREATED_AT))
        .updatedAt(rs.getTimestamp(MODIFIED_AT))
        .build();
  }


}
