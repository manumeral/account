package in.zeta.oms.account.accountHolder;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.accountHolder.kyc.KYCStatusDAO;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorDAO;
import in.zeta.oms.account.api.exception.AccountHolderInsertionException;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.dao.AccountHolderDAO;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.dao.DuplicateKeyException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;
import static in.zeta.oms.account.constant.AccountErrorCode.DB_ERROR;

@Singleton
public class AccountHolderTransactionDAO extends PostgresDAO {
  private final AccountHolderVectorDAO accountHolderVectorDAO;
  private static final SpectraLogger logger = OlympusSpectra.getLogger(AccountHolderTransactionDAO.class);
  private final AccountHolderDAO accountHolderDAO;
  private final KYCStatusDAO kycStatusDAO;
  private final SecureRandomGenerator secureRandomGenerator;

  @Inject
  public AccountHolderTransactionDAO(
      ZetaHostMessagingService zhms,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      AccountHolderVectorDAO accountHolderVectorDAO,
      AccountHolderDAO accountHolderDAO,
      KYCStatusDAO kycStatusDAO,
      SecureRandomGenerator secureRandomGenerator) {
    super(zhms, basicDataSource, poolSize);
    this.accountHolderVectorDAO = accountHolderVectorDAO;
    this.accountHolderDAO = accountHolderDAO;
    this.kycStatusDAO = kycStatusDAO;
    this.secureRandomGenerator = secureRandomGenerator;
  }

  public CompletionStage<AccountHolder> insertAccountHolder(AccountHolder accountHolder){
    List<PostgresInstruction> queryInTransaction = new ArrayList<>();

    queryInTransaction.add(accountHolderDAO.getInsertInstruction(accountHolder));
    accountHolder
        .getVectors()
        .forEach(vector -> queryInTransaction
            .add(accountHolderVectorDAO.getInsertInstruction(AccountHolderVector.builder()
                    .accountHolderID(accountHolder.getId())
                    .id(secureRandomGenerator.generateUUID())
                    .status(AccountHolderVectorDAO.ENABLED)
                    .attributes(vector.getAttributes())
                    .type(vector.getType())
                    .value(vector.getValue())
                    .ifiID(vector.getIfiID())
                    .build())));
    if (null != accountHolder.getKYCStatus()) {
      queryInTransaction.add(kycStatusDAO.getInsertInstruction(accountHolder.getKYCStatus()));
    }

    return updateInTransaction(queryInTransaction)
        .handle((ignored, throwable)-> {
          if (throwable == null)
            return accountHolder;
          throwable = unwrapCompletionStateException(throwable);
          if (throwable instanceof DuplicateKeyException) {
            logger
                .info("Duplicate key exception")
                .fill("error", throwable)
                .fill("accountHolder", accountHolder)
                .log();
            throw new CompletionException(throwable);
          }
          logger
              .error("Error while inserting account holder and accountHolderVector in transaction", throwable)
              .fill("accountHolder", accountHolder)
              .log();
          throw new AccountHolderInsertionException("Error while inserting account holder and accountHolderVector in transaction", throwable,DB_ERROR);

        });
  }
}
