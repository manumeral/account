package in.zeta.oms.account.accountHolder.kyc;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.exception.AccountHolderNotFoundException;
import in.zeta.oms.account.util.LoggerUtils;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static java.lang.String.format;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

// TODO: Add test cases
@Singleton
public class KYCStatusDAO extends PostgresDAO implements RowMapper<KYCStatus> {

  private static final LoggerUtils loggerUtils = LoggerUtils.builder()
      .logger(OlympusSpectra.getLogger(KYCStatusDAO.class))
      .build();

  private static final String TABLE_NAME = "kyc_status";

  //--------------------------------------Column Names----------------------------------------------
  private static final String IFI_ID = "ifi_id";
  private static final String ACCOUNT_HOLDER_ID = "account_holder_id";
  private static final String KYC_STATUS = "kyc_status";
  private static final String EXPIRY_TIME = "expiry_time";
  private static final String KYC_STATUS_POST_EXPIRY = "kyc_status_post_expiry";
  private static final String ATTRIBUTES = "attributes";
  private static final String CREATED_AT = "created_at";
  private static final String UPDATED_AT = "modified_at";
  //--------------------------------------------//--------------------------------------------------

  private static final ImmutableList<String> INSERT_COLUMNS = new Builder<String>()
      .add(IFI_ID)
      .add(ACCOUNT_HOLDER_ID)
      .add(KYC_STATUS)
      .add(EXPIRY_TIME)
      .add(KYC_STATUS_POST_EXPIRY)
      .add(ATTRIBUTES)
      .build();

  private static final ImmutableList<String> SELECT_COLUMNS = new Builder<String>()
      .addAll(INSERT_COLUMNS)
      .add(CREATED_AT)
      .add(UPDATED_AT)
      .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
      .tableName(TABLE_NAME)
      .insertColumns(INSERT_COLUMNS)
      .selectColumns(SELECT_COLUMNS)
      .customColumnType(ACCOUNT_HOLDER_ID, UUID_TYPE)
      .customColumnType(ATTRIBUTES, JSONB_TYPE)
      .build();
  private final Gson gson;

  private int DEFAULT_EXPIRY_TIME = 1;

  @Inject
  public KYCStatusDAO(ZetaHostMessagingService hostMessagingService,
                      BasicDataSource basicDataSource,
                      @Named("postgres.jdbc.pool.size") int poolSize,
                      Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  private Timestamp getDefaultExpiryTime() {
    return Timestamp.from(ZonedDateTime.now(ZoneOffset.UTC).plusYears(DEFAULT_EXPIRY_TIME).toInstant());
  }

  @TimeLogger
  public CompletionStage<Void> insert(KYCStatus kycStatus) {
    final String accountHolderID = kycStatus.getAccountHolderID();

    List<Object> argsList = new ArrayList<>();
    argsList.add(kycStatus.getIfiID());
    argsList.add(accountHolderID);
    argsList.add(kycStatus.getKycStatus());
    if(null != kycStatus.getExpiryTime()) {
      argsList.add(Timestamp.valueOf(kycStatus.getExpiryTime()));
    } else {
      argsList.add(getDefaultExpiryTime());
    }

    argsList.add(kycStatus.getKycStatusPostExpiry());
    argsList.add(gson.toJson(kycStatus.getAttributes()));

    return update(PG_QUERY_GENERATOR.getInsertQuery(), argsList)
        .thenAccept(ignore -> {})
        // TODO: Handle this in a function. Similar for update too
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              final String localizedMessage = t.getLocalizedMessage();
              if (t instanceof DataIntegrityViolationException
                  && localizedMessage.contains("kyc_status_account_holder_id_fkey")
                  && localizedMessage.contains(accountHolderID)) {
                throw AccountHolderNotFoundException.forAccountHolderID(accountHolderID);
              }
              throw loggerUtils.logDBException("Add KYC status Failed", t);
            });
  }

  @TimeLogger
  public CompletionStage<Optional<KYCStatus>> get(String accountHolderID, Long ifiID) {
    final ImmutableList<String> selectionCriteria = new Builder<String>()
        .add(ACCOUNT_HOLDER_ID)
        .add(IFI_ID)
        .build();
    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionCriteria), this, accountHolderID, ifiID)
        .exceptionally(t -> {
          throw loggerUtils
              .logDBException(format("Get KYCStatus failed by account Holder ID :%s", accountHolderID), t);
        });
  }

  @TimeLogger
  public CompletionStage<Void> update(KYCStatus kycStatus) {
    final List<String> whereClauseColumnNames = Arrays.asList(ACCOUNT_HOLDER_ID, IFI_ID);
    Builder<String> updateColumnNames = new Builder<>();
    Builder<Object> argsList = new Builder<>();
    if(!Strings.isNullOrEmpty(kycStatus.getKycStatus())) {
      updateColumnNames.add(KYC_STATUS);
      argsList.add(kycStatus.getKycStatus());
    }
    if(null != kycStatus.getAttributes()) {
      updateColumnNames.add(ATTRIBUTES);
      argsList.add(getJsonbObject(gson.toJson(kycStatus.getAttributes())));
    }
    if(!Strings.isNullOrEmpty(kycStatus.getKycStatusPostExpiry())) {
      updateColumnNames.add(KYC_STATUS_POST_EXPIRY);
      argsList.add(kycStatus.getKycStatusPostExpiry());
    }
    if(null != kycStatus.getExpiryTime()) {
      updateColumnNames.add(EXPIRY_TIME);
      argsList.add(Timestamp.valueOf(kycStatus.getExpiryTime()));
    }
    argsList.add(kycStatus.getAccountHolderID());
    argsList.add(kycStatus.getIfiID());
    return update(
            PG_QUERY_GENERATOR.getUpdateQueryAndJoin(
                updateColumnNames.build(), whereClauseColumnNames),
            argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Error Updating KYC for accountHolderID : %s",
                      kycStatus.getAccountHolderID()),
                  throwable);
            });
  }

  @Override
  public KYCStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
    return KYCStatus.builder()
        .ifiID(rs.getLong(IFI_ID))
        .accountHolderID(rs.getString(ACCOUNT_HOLDER_ID))
        .kycStatus(rs.getString(KYC_STATUS))
        .expiryTime(rs.getTimestamp(EXPIRY_TIME).toLocalDateTime())
        .kycStatusPostExpiry(rs.getString(KYC_STATUS_POST_EXPIRY))
        .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .updateTime(rs.getTimestamp(UPDATED_AT).toLocalDateTime())
        .build();
  }

  public PostgresInstruction getInsertInstruction(KYCStatus kycStatus) {
    List<Object> args = new ArrayList<>();

    args.add(kycStatus.getIfiID());
    args.add(kycStatus.getAccountHolderID());
    args.add(kycStatus.getKycStatus());
    if(null != kycStatus.getExpiryTime()) {
      args.add(Timestamp.valueOf(kycStatus.getExpiryTime()));
    } else {
      args.add(getDefaultExpiryTime());
    }
    args.add(kycStatus.getKycStatusPostExpiry());
    args.add(getJsonbObject(gson.toJson(kycStatus.getAttributes())));

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
  }
}
