package in.zeta.oms.account.accountHolder.pop;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import in.zeta.oms.account.api.response.GetPOPListResponse;
import in.zeta.oms.account.pop.AddAccountHolderPOPRequest;
import in.zeta.oms.account.pop.DeleteAccountHolderPOPRequest;
import in.zeta.oms.account.pop.GetAccountHolderPOPListRequest;
import in.zeta.oms.account.pop.GetAccountHolderPOPRequest;
import in.zeta.oms.account.pop.UpdateAccountHolderDefaultPOPRequest;
import in.zeta.oms.account.pop.UpdateAccountHolderPOPRequest;
import in.zeta.oms.account.service.AccountHolderService;
import olympus.message.types.Request;

import static in.zeta.oms.account.pop.EntityType.ACCOUNT_HOLDER;

@EagerSingleton
public class AccountHolderPOPHandler extends AccountServiceBaseRequestHandler {

  private final AccountHolderService accountHolderService;

  @Inject
  public AccountHolderPOPHandler(
      ZetaHostMessagingService zetaHostMessagingService,
      AccountHolderService accountHolderService) {
    super(zetaHostMessagingService);
    this.accountHolderService = accountHolderService;
  }

  @Authorized(anyOf = {"api.account.accountHolder.addPOP", "api.account.accountHolder.admin"})
  public void on(AddAccountHolderPOPRequest payload, Request<AddAccountHolderPOPRequest> request) {
    onRequest(payload, request);
    accountHolderService.addPOP(payload)
        .thenAccept(pop -> onResult(pop, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.updatePOP", "api.account.accountHolder.admin"})
  public void on(UpdateAccountHolderPOPRequest payload, Request<UpdateAccountHolderPOPRequest> request) {
    onRequest(payload, request);
    accountHolderService.updatePOP(payload)
        .thenAccept(pop -> onResult(pop, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.updatePOP", "api.account.accountHolder.admin"})
  public void on(UpdateAccountHolderDefaultPOPRequest payload, Request<UpdateAccountHolderDefaultPOPRequest> request) {
    onRequest(payload, request);
    accountHolderService.setPOPAsDefault(payload.getId(), payload.getIfiID(), payload.getAccountHolderID(), ACCOUNT_HOLDER)
        .thenAccept(pop -> onResult(pop, request))
        .exceptionally(t -> onError(request, t));

    }

  @Authorized(anyOf = {"api.account.accountHolder.getPOP", "api.account.accountHolder.admin"})
  public void on(GetAccountHolderPOPListRequest payload, Request<GetAccountHolderPOPListRequest> request) {
    onRequest(payload, request);
    accountHolderService.listPOPs(payload.getIfiID(), payload.getAccountHolderID())
        .thenAccept(popList -> onResult(GetPOPListResponse.builder()
            .popList(popList).build(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.getPOP", "api.account.accountHolder.admin"})
  public void on(GetAccountHolderPOPRequest payload, Request<GetAccountHolderPOPRequest> request) {
    onRequest(payload, request);
    accountHolderService.getPOPByID(payload.getIfiID(), payload.getId(), payload.getAccountHolderID(), ACCOUNT_HOLDER)
        .thenAccept(pop -> onResult(pop, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.deletePOP", "api.account.accountHolder.admin"})
  public void on(DeleteAccountHolderPOPRequest payload, Request<DeleteAccountHolderPOPRequest> request) {
    onRequest(payload, request);
    accountHolderService.deletePOP(payload.getIfiID(), payload.getAccountHolderID(), payload.getID(),  ACCOUNT_HOLDER)
        .thenAccept(pop -> onResult(pop, request))
        .exceptionally(t -> onError(request, t));
  }
}
