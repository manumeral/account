package in.zeta.oms.account.accountHolder.accountHolderRelation;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;

import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import java.util.List;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.completedFuture;

/** Only one issuance policy per entity */
@Singleton
public class AccountHolderRelationService {

  private static final String ENABLED = "ENABLED";
  private final AccountHolderRelationDAO accountHolderRelationDAO;
  private final SecureRandomGenerator secureRandomGenerator;

  @Inject
  public AccountHolderRelationService(
      AccountHolderRelationDAO accountHolderRelationDAO,
      SecureRandomGenerator secureRandomGenerator) {
    this.accountHolderRelationDAO = accountHolderRelationDAO;
    this.secureRandomGenerator = secureRandomGenerator;
  }

  @PublishEvent(topicName = PubSubTopics.CREATE_ACCOUNT_HOLDER_RELATION_EVENT_TOPIC)
  public CompletionStage<AccountHolderRelation> createAccountHolderRelation(
      AccountHolderRelation accountHolderRelation) {
    return accountHolderRelationDAO
        .getRelationByAccountHolderID(
            accountHolderRelation.getAccountHolderID(),
            accountHolderRelation.getRelatedAccountHolderID(),
            accountHolderRelation.getRelationshipType(),
            accountHolderRelation.getIfiID())
        .thenCompose(
            existingRelation -> {
              if (existingRelation.isPresent()) {
                return completedFuture(existingRelation.get());
              }
              final AccountHolderRelation persistAccountHolderRelation =
                  accountHolderRelation
                      .toBuilder()
                      .id(secureRandomGenerator.generateUUID())
                      .status(ENABLED)
                      .build();

              return accountHolderRelationDAO.insert(persistAccountHolderRelation)
                .thenCompose(persistedAccountHolderRelation -> get(persistedAccountHolderRelation.getId(),
                persistedAccountHolderRelation.getAccountHolderID(),
                persistedAccountHolderRelation.getIfiID()));
            });
  }

  public CompletionStage<AccountHolderRelation> get(String id, String accountHolderID, Long ifiID) {
    return accountHolderRelationDAO
        .get(id, accountHolderID, ifiID)
        .thenApply(accountHolderRelationOptional -> {
          if (accountHolderRelationOptional.isPresent()) {
            return accountHolderRelationOptional.get();
          }
          throw AccountHoldeRelationNotFoundException.exceptionByID(id);
        });
  }

  public CompletionStage<List<AccountHolderRelation>> getRelationsByAccountHolderID(
      String accountHolderID, long ifiID) {
    return accountHolderRelationDAO.getRelations(
        accountHolderID, ifiID);
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_ACCOUNT_HOLDER_RELATION_EVENT_TOPIC)
  public CompletionStage<AccountHolderRelation> updateRelation(UpdateAccountHolderRelationRequest payload) {
    final String id = payload.getId();
    final String accountHolderID = payload.getAccountHolderID();
    final long ifiID = payload.getIfiID();
    return get(id, accountHolderID, ifiID)
        .thenCompose(ignore -> {
          AccountHolderRelation accountHolderRelation = AccountHolderRelation.builder()
              .id(id)
              .accountHolderID(accountHolderID)
              .ifiID(ifiID)
              .status(payload.getStatus())
              .relatedAccountHolderID(payload.getRelatedAccountHolderID())
              .relationshipType(payload.getRelationshipType())
              .attributes(payload.getAttributes())
              .build();
          return accountHolderRelationDAO.update(accountHolderRelation);
        })
        .thenCompose(ignore -> get(id, accountHolderID, ifiID));
  }
}
