package in.zeta.oms.account.accountHolder.pop;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.model.Address;
import in.zeta.oms.account.api.model.Contact;
import in.zeta.oms.account.pop.POP;

import com.google.inject.Inject;
import in.zeta.oms.account.util.LoggerUtils;
import java.lang.reflect.Type;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class POPDAO extends PostgresDAO implements RowMapper<POP> {

    private static final LoggerUtils loggerUtils = LoggerUtils.builder()
        .logger(OlympusSpectra.getLogger(POPDAO.class))
        .build();

    private static final String TABLE_NAME = "pop";

    //------------------------------------------Column Names----------------------------------------//
    public static final String ID = "id";
    public static final String IFI_ID = "ifi_id";
    public static final String ENTITY_ID = "entity_id";
    public static final String ENTITY_TYPE = "entity_type";
    public static final String ADDRESS = "address";
    public static final String CONTACT_LIST = "contact_list";
    public static final String LABEL = "label";
    public static final String STATUS = "status";
    public static final String DEFAULT = "is_default";
    public static final String ATTRIBUTES = "attributes";
    private static final String CREATED_AT = "created_at";
    private static final String UPDATED_AT = "modified_at";

    private static final List<String> INSERT_COLUMNS  = new ImmutableList.Builder<String>()
        .add(ID)
        .add(IFI_ID)
        .add(ENTITY_ID)
        .add(ENTITY_TYPE)
        .add(ADDRESS)
        .add(CONTACT_LIST)
        .add(LABEL)
        .add(STATUS)
        .add(DEFAULT)
        .add(ATTRIBUTES)
        .build();

    private static final List<String> SELECT_COLUMNS  = new ImmutableList.Builder<String>()
        .addAll(INSERT_COLUMNS)
        .add(CREATED_AT)
        .add(UPDATED_AT)
        .build();


    private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
        .tableName(TABLE_NAME)
        .insertColumns(INSERT_COLUMNS)
        .selectColumns(SELECT_COLUMNS)
        .customColumnType(ENTITY_ID, UUID_TYPE)
        .customColumnType(ID, UUID_TYPE)
        .build();
    public static final String ENABLED = "ENABLED";
    public static final String DELETED = "DELETED";

    private static final Type LIST_CONTACT_TYPE = new TypeToken<List<Contact>>(){}.getType();

    private final Gson gson;

    @Inject
    public POPDAO(ZetaHostMessagingService hostMessagingService,
                  BasicDataSource basicDataSource,
                  @Named("postgres.jdbc.pool.size") int poolSize,
                  Gson gson)
    {
        super(hostMessagingService, basicDataSource, poolSize);
        this.gson = gson;
    }

    public CompletionStage<List<POP>> listPOPs(Long ifiID, String entityID, String entityType) {
        final ImmutableList<String> selectionFields = new ImmutableList.Builder<String>()
            .add(IFI_ID)
            .add(ENTITY_ID)
            .add(ENTITY_TYPE)
            .add(STATUS)
            .build();

        return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionFields),
            this, ifiID,
            entityID,
            entityType,
            ENABLED)
            .exceptionally(throwable -> {
                throw loggerUtils.logDBException(String.format("Get pop list for ifi %d and %s id %s failed", ifiID, entityType, entityID), throwable);
            });
    }

    public CompletionStage<Optional<POP>> getPOPByID(Long ifiID, String id, String entityID, String entityType) {
        //TODO: In case of deletion don't return POP Object
        final ImmutableList<String> selectionFields = new ImmutableList.Builder<String>()
            .add(IFI_ID)
            .add(ID)
            .add(ENTITY_ID)
            .add(ENTITY_TYPE)
            .build();

        return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionFields),
            this,
            ifiID,
            id,
            entityID,
            entityType)
            .exceptionally(throwable -> {
                throwable = unwrapCompletionStateException(throwable);
                throw loggerUtils.logDBException("Get POP failed for " + entityType + " id " + id, throwable);
            });
    }

    public CompletionStage<Optional<POP>> getPOPByLabel(Long ifiID, String label, String entityID, String entityType) {

        //TODO: In case of deletion don't return POP Object
        final ImmutableList<String> selectionFields = new ImmutableList.Builder<String>()
            .add(IFI_ID)
            .add(LABEL)
            .add(ENTITY_ID)
            .add(ENTITY_TYPE)
            .build();

        return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionFields),
            this,
            ifiID,
            label,
            entityID,
            entityType)
            .exceptionally(throwable -> {
                throwable = unwrapCompletionStateException(throwable);
                throw loggerUtils.logDBException("Get POP failed for " + entityType + " label " + label, throwable);
            });
    }

    public CompletionStage<POP> addPOP(POP pop) {

        List<Object> args = new ArrayList<>();
        args.add(pop.getId());
        args.add(pop.getIfiID());
        args.add(pop.getEntityID());
        args.add(pop.getEntityType());
        args.add(getJsonbObject(gson.toJson(pop.getAddress())));
        args.add(getJsonbObject(gson.toJson(pop.getContactList())));
        args.add(pop.getLabel());
        args.add(ENABLED);
        args.add(false);
        args.add(getJsonbObject(gson.toJson(pop.getAttributes())));

        return update(PG_QUERY_GENERATOR.getInsertQuery(), args)
            .handle((ignored, throwable)-> {
                if (throwable == null)
                    return pop;
                throwable = unwrapCompletionStateException(throwable);
                if (throwable instanceof DuplicateKeyException) {
                    throw loggerUtils.logDBException("Duplicate key exception", throwable);
                }
                throw loggerUtils.logDBException("Error while inserting pop", throwable);

            });

    }

    public CompletionStage<POP> updatePOP(POP pop) {
        final List<Object> arguments = new ArrayList<>();
        final List<String> columnsToUpdate = new ArrayList<>();
        final List<String> whereClause = new ArrayList<>();

        whereClause.add(ID);
        whereClause.add(IFI_ID);
        whereClause.add(ENTITY_ID);
        whereClause.add(ENTITY_TYPE);

        if (null != pop.getAddress()) {
            columnsToUpdate.add(ADDRESS);
            arguments.add(getJsonbObject(gson.toJson(pop.getAddress())));
        }

        if (null != pop.getContactList()) {
            columnsToUpdate.add(CONTACT_LIST);
            arguments.add(getJsonbObject(gson.toJson(pop.getContactList())));
        }

        if (!Strings.isNullOrEmpty(pop.getLabel())) {
            columnsToUpdate.add(LABEL);
            arguments.add(pop.getLabel());
        }

        if (!Strings.isNullOrEmpty(pop.getStatus())) {
            columnsToUpdate.add(STATUS);
            arguments.add(pop.getStatus());
        }

        if (null != pop.getAttributes()) {
            columnsToUpdate.add(ATTRIBUTES);
            arguments.add(getJsonbObject(gson.toJson(pop.getAttributes())));
        }

        arguments.add(pop.getId());
        arguments.add(pop.getIfiID());
        arguments.add(pop.getEntityID());
        arguments.add(pop.getEntityType());

        return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(columnsToUpdate, whereClause), arguments)
            .handle((ignored, throwable)-> {
                if (null == throwable) {
                    return pop;
                }
                throwable = unwrapCompletionStateException(throwable);
                if (throwable instanceof DuplicateKeyException) {
                    return null;
                }
                throw loggerUtils.logDBException("Error while updating Pop for id " + pop.getId(), throwable);
            });

    }

    public CompletionStage<Integer> markDefaultAsFalse(Long ifiID, String entityID, String entityType) {
        final List<Object> arguments = new ArrayList<>();
        final List<String> columnsToUpdate = new ArrayList<>();
        final List<String> whereClause = new ArrayList<>();

        whereClause.add(IFI_ID);
        whereClause.add(ENTITY_ID);
        whereClause.add(ENTITY_TYPE);
        whereClause.add(DEFAULT);

        columnsToUpdate.add(DEFAULT);
        arguments.add(false);

        arguments.add(ifiID);
        arguments.add(entityID);
        arguments.add(entityType);
        arguments.add(true);

        return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(columnsToUpdate, whereClause), arguments)
            .exceptionally(throwable -> {
                throwable = unwrapCompletionStateException(throwable);
                throw loggerUtils.logDBException("Updation of default Address failed for id " + entityID, throwable);
            });
    }

    public CompletionStage<Integer> setPOPAsDefault(String id, Long ifiID, String entityID, String entityType)  {
        final List<Object> arguments = new ArrayList<>();
        final List<String> whereClause = new ArrayList<>();

        arguments.add(true);

        whereClause.add(ID);
        whereClause.add(IFI_ID);
        whereClause.add(ENTITY_ID);
        whereClause.add(ENTITY_TYPE);

        arguments.add(id);
        arguments.add(ifiID);
        arguments.add(entityID);
        arguments.add(entityType);

        return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(Collections.singletonList(DEFAULT), whereClause), arguments)
            .exceptionally(throwable -> {
                throwable = unwrapCompletionStateException(throwable);
                throw loggerUtils.logDBException("Deletion of POP failed for id " + id, throwable);
            });
    }

    public CompletionStage<Integer> updateStatus(String id, Long ifiID, String entityID, String entityType, String status) {
        final List<Object> arguments = new ArrayList<>();

        arguments.add(status);

        final List<String> whereClause = new ArrayList<>();
        whereClause.add(IFI_ID);
        whereClause.add(ENTITY_ID);
        whereClause.add(ENTITY_TYPE);
        whereClause.add(ID);

        arguments.add(ifiID);
        arguments.add(entityID);
        arguments.add(entityType);
        arguments.add(id);

        return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(Collections.singletonList(STATUS), whereClause), arguments)
            .exceptionally(throwable -> {
            throwable = unwrapCompletionStateException(throwable);
            throw loggerUtils.logDBException("Deletion of POP failed for id " + id, throwable);
        });
    }

    @Override
    public POP mapRow(ResultSet rs, int i) throws SQLException {
        return POP
            .builder()
            .id(rs.getString(ID))
            .ifiID(rs.getLong(IFI_ID))
            .entityID(rs.getString(ENTITY_ID))
            .entityType(rs.getString(ENTITY_TYPE))
            .label(rs.getString(LABEL))
            .address(gson.fromJson(rs.getString(ADDRESS), Address.class))
            .contactList(gson.fromJson(rs.getString(CONTACT_LIST), LIST_CONTACT_TYPE))
            .status(rs.getString(STATUS))
            .isDefault(rs.getBoolean(DEFAULT))
            .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
            .createdAt(rs.getTimestamp(CREATED_AT).toLocalDateTime())
            .modifiedAt(rs.getTimestamp(UPDATED_AT).toLocalDateTime())
            .build();
    }
}
