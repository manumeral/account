package in.zeta.oms.account.beneficiary.transaction;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.util.LoggerUtils;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class BeneficiaryTransactionDAO extends PostgresDAO implements RowMapper<BeneficiaryTransaction> {

    private static final LoggerUtils loggerUtils = LoggerUtils.builder()
            .logger(OlympusSpectra.getLogger(BeneficiaryTransactionDAO.class))
            .build();

    //-----------------------------------------Column Names-----------------------------------------//
    private static final String TABLE_NAME = "beneficiary_transaction";
    private static final String REQUEST_ID = "request_id";
    private static final String ID = "id";
    private static final String IFI_ID = "ifi_id";
    private static final String AMOUNT = "amount";
    private static final String CURRENCY = "currency";
    private static final String TRANSACTION_CODE = "transaction_code";
    private static final String DEBIT_ACCOUNT_ID = "debit_account_id";
    private static final String DEBIT_ACCOUNT_HOLDER_ID = "debit_account_holder_id";
    private static final String BENEFICIARY_ID = "beneficiary_id";
    private static final String BENEFICIARY_ACCOUNT_ID = "beneficiary_account_id";
    private static final String TRANSACTION_TIME = "transaction_time";
    private static final String STATUS = "status";
    private static final String TRANSACTION_ID = "transaction_id";
    private static final String REMARKS = "remarks";
    private static final String ATTRIBUTES = "attributes";
    private static final String CREATED_AT = "created_at";

    private static final List<String> INSERT_COLUMNS = new ImmutableList.Builder<String>()
            .add(REQUEST_ID)
            .add(ID)
            .add(IFI_ID)
            .add(AMOUNT)
            .add(CURRENCY)
            .add(TRANSACTION_CODE)
            .add(DEBIT_ACCOUNT_ID)
            .add(DEBIT_ACCOUNT_HOLDER_ID)
            .add(BENEFICIARY_ID)
            .add(BENEFICIARY_ACCOUNT_ID)
            .add(TRANSACTION_TIME)
            .add(STATUS)
            .add(TRANSACTION_ID)
            .add(REMARKS)
            .add(ATTRIBUTES)
            .build();

    private static final List<String> SELECT_COLUMNS = new ImmutableList.Builder<String>()
            .addAll(INSERT_COLUMNS)
            .add(CREATED_AT)
            .build();

    private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
            .tableName(TABLE_NAME)
            .insertColumns(INSERT_COLUMNS)
            .selectColumns(SELECT_COLUMNS)
            .customColumnType(ATTRIBUTES, JSONB_TYPE)
            .build();


    private final Gson gson;

    @Inject
    public BeneficiaryTransactionDAO(ZetaHostMessagingService hostMessagingService,
                                     BasicDataSource basicDataSource,
                                     @Named("postgres.jdbc.pool.size") int poolSize,
                                     Gson gson) {
        super(hostMessagingService, basicDataSource, poolSize);
        this.gson = gson;
    }

    @TimeLogger
    public CompletionStage<String> insert(@NonNull BeneficiaryTransaction transaction) {
        return update(getInsertInstruction(transaction))
                .thenApply(ignore -> transaction.getId());
    }

    @TimeLogger
    public CompletionStage<Optional<BeneficiaryTransaction>> get(@NonNull Long ifiID, @NonNull String accountHolderID, @NonNull String transactionID) {
        List<Object> args = new ArrayList<>();
        args.add(ifiID);
        args.add(accountHolderID);
        args.add(transactionID);

        final ImmutableList<String> criteria = new ImmutableList.Builder<String>()
                .add(IFI_ID)
                .add(DEBIT_ACCOUNT_HOLDER_ID)
                .add(TRANSACTION_ID)
                .build();

        return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
                .exceptionally(t -> {
                    throw loggerUtils.logDBException(
                            String.format("Get Transaction failed by ifiId: %s, accountHolderId: %s, , transactionId: %s", ifiID, accountHolderID, transactionID), t);
                });
    }

    public CompletionStage<List<BeneficiaryTransaction>> get(@NonNull Long ifiID,
                                                             String accountHolderID,
                                                             @NonNull Long pageNumber,
                                                             @NonNull Long pageSize) {
        List<Object> args = new ArrayList<>();
        final List<String> selectionCriteria = new ArrayList<>();
        args.add(ifiID);
        selectionCriteria.add(IFI_ID);
        if (!Strings.isNullOrEmpty(accountHolderID)) {
            args.add(accountHolderID);
            selectionCriteria.add(DEBIT_ACCOUNT_HOLDER_ID);
        }
        String query = PG_QUERY_GENERATOR.getSelectQueryAndJoin(selectionCriteria);
        query = query + " order by created_at desc LIMIT ? offset ? ";
        args.add(pageSize);
        args.add((pageNumber - 1) * pageSize);
        return query(query, this,args)
                .exceptionally(throwable -> {
                    throwable = unwrapCompletionStateException(throwable);
                    throw loggerUtils.logDBException(String.format("Get Transactions failed for ifiID: %s failed", ifiID), throwable);
                });
    }

    private PostgresInstruction getInsertInstruction(@NonNull BeneficiaryTransaction transaction) {
        List<Object> args = new ArrayList<>();
        args.add(transaction.getRequestID());
        args.add(transaction.getId());
        args.add(transaction.getIfiID());
        args.add(transaction.getAmount());
        args.add(transaction.getCurrency());
        args.add(transaction.getTransactionCode());
        args.add(transaction.getDebitAccountID());
        args.add(transaction.getDebitAccountHolderID());
        args.add(transaction.getBeneficiaryID());
        args.add(transaction.getBeneficiaryAccountID());
        args.add(transaction.getTransactionTime());
        args.add(transaction.getStatus().toString());
        args.add(transaction.getTransactionID());
        args.add(transaction.getRemarks());
        args.add(getJsonbObject(gson.toJson(transaction.getAttributes())));

        return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
    }

    @Override
    public BeneficiaryTransaction mapRow(ResultSet rs, int i) throws SQLException {
        return BeneficiaryTransaction.builder()
                .id(rs.getString(ID))
                .requestID(rs.getString(REQUEST_ID))
                .ifiID(rs.getLong(IFI_ID))
                .amount(rs.getLong(AMOUNT))
                .currency(rs.getString(CURRENCY))
                .transactionCode(rs.getString(TRANSACTION_CODE))
                .debitAccountID(rs.getString(DEBIT_ACCOUNT_ID))
                .debitAccountHolderID(rs.getString(DEBIT_ACCOUNT_HOLDER_ID))
                .beneficiaryID(rs.getString(BENEFICIARY_ID))
                .beneficiaryAccountID(rs.getString(BENEFICIARY_ACCOUNT_ID))
                .transactionTime(rs.getLong(TRANSACTION_TIME))
                .status(BeneficiaryTransaction.Status.valueOf(rs.getString(STATUS)))
                .transactionID(rs.getString(TRANSACTION_ID))
                .remarks(rs.getString(REMARKS))
                .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
                .createdAt(rs.getTimestamp(CREATED_AT).toLocalDateTime())
                .build();
    }
}
