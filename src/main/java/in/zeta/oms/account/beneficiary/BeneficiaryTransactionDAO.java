package in.zeta.oms.account.beneficiary;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.model.BeneficiaryVector;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.dao.DuplicateKeyException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;
import static in.zeta.oms.account.constant.AccountErrorCode.DB_ERROR;

@Singleton
public class BeneficiaryTransactionDAO extends PostgresDAO {
  private final BeneficiaryVectorDAO beneficiaryVectorDAO;
  private static final SpectraLogger logger = OlympusSpectra.getLogger(BeneficiaryTransactionDAO.class);
  private final BeneficiaryDAO beneficiaryDAO;
  private final SecureRandomGenerator secureRandomGenerator;

  @Inject
  public BeneficiaryTransactionDAO(
      ZetaHostMessagingService zhms,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      BeneficiaryVectorDAO beneficiaryVectorDAO,
      BeneficiaryDAO beneficiaryDAO,
      SecureRandomGenerator secureRandomGenerator) {
    super(zhms, basicDataSource, poolSize);
    this.beneficiaryVectorDAO = beneficiaryVectorDAO;
    this.beneficiaryDAO = beneficiaryDAO;
    this.secureRandomGenerator = secureRandomGenerator;
  }

  public CompletionStage<Beneficiary> insertBeneficiary(Beneficiary beneficiary){
      // Was facing issue in transaction so splitting it into two parts queryInTransaction and beneficiaryVectorTransaction
    List<PostgresInstruction> queryInTransaction = new ArrayList<>();
    List<PostgresInstruction> beneficiaryVectorTransaction = new ArrayList<>();
    queryInTransaction.add(beneficiaryDAO.getInsertInstruction(
        beneficiary));
    beneficiary
        .getVectors()
        .forEach(vector -> beneficiaryVectorTransaction
            .add(beneficiaryVectorDAO.getInsertInstruction(BeneficiaryVector.builder()
                    .id(secureRandomGenerator.generateUUID())
                    .beneficiaryID(beneficiary.getId())
                    .attributes(vector.getAttributes())
                    .type(vector.getType())
                    .value(vector.getValue())
                    .ifiID(beneficiary.getIfiID())
                    .build())));
    return updateInTransaction(queryInTransaction)
        .thenCompose(ignore -> updateInTransaction(beneficiaryVectorTransaction))
        .handle((ignored, throwable)-> {
          if (throwable == null)
            return beneficiary;
          throwable = unwrapCompletionStateException(throwable);
          if (throwable instanceof DuplicateKeyException) {
            logger
                .info("Duplicate key exception")
                .fill("error", throwable)
                .fill("beneficiary", beneficiary)
                .log();
            throw new CompletionException(throwable);
          }
          logger
              .error("Error while inserting beneficiary and beneficiaryVector in transaction", throwable)
              .fill("beneficiary", beneficiary)
              .log();
          throw new BeneficiaryInsertionException("Error while inserting beneficiary and beneficiaryVector in transaction", throwable,DB_ERROR);

        });
  }
}
