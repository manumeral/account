package in.zeta.oms.account.beneficiary.transaction;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import olympus.message.types.Request;

@EagerSingleton
public class TransactionHandler extends AccountServiceBaseRequestHandler {

    private final BeneficiaryTransactionService beneficiaryTransactionService;

    @Inject
    public TransactionHandler(ZetaHostMessagingService zetaHostMessagingService,
                              BeneficiaryTransactionService beneficiaryTransactionService) {
        super(zetaHostMessagingService);
        this.beneficiaryTransactionService = beneficiaryTransactionService;
    }

    @Authorized(anyOf = {"api.beneficiary.transaction", "api.beneficiary.admin"})
    public void on(BeneficiaryTransactionRequest payload, Request<BeneficiaryTransactionRequest> request) {
        onRequest(payload, request);
        beneficiaryTransactionService.processTransactionRequest(payload)
                .thenAccept(response -> onResult(response, request))
                .exceptionally(t -> onError(request, t));
    }

    @Authorized(anyOf = {"api.beneficiary.transaction", "api.beneficiary.admin"})
    public void on(BeneficiaryAccountTransactionRequest payload, Request<BeneficiaryAccountTransactionRequest> request) {
        onRequest(payload, request);
        beneficiaryTransactionService.processTransactionRequest(payload)
                .thenAccept(response -> onResult(response, request))
                .exceptionally(t -> onError(request, t));
    }

    @Authorized(anyOf = {"api.beneficiary.transaction.get", "api.beneficiary.admin"})
    public void on(GetBeneficiaryTransactionRequest payload, Request<GetBeneficiaryTransactionRequest> request) {
        onRequest(payload, request);
        beneficiaryTransactionService.getTransaction(payload.getIfiID(), payload.getAccountHolderID(), payload.getTransactionID())
                .thenAccept(response -> onResult(response, request))
                .exceptionally(t -> onError(request, t));
    }

    @Authorized(anyOf = {"api.beneficiary.transaction.get", "api.account.admin"})
    public void on(GetBeneficiaryTransactionListRequest payload, Request<GetBeneficiaryTransactionListRequest> request) {
        onRequest(payload, request);
        beneficiaryTransactionService
                .getTransactions(payload.getIfiID(), payload.getAccountHolderID(), payload.getPageNumber(), payload.getPageSize())
                .thenAccept(transactions -> onResult(
                        new GetBeneficiaryTransactionListResponse(transactions),
                        request))
                .exceptionally(t -> onError(request, t));
    }
}
