package in.zeta.oms.account.beneficiary;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import olympus.message.types.Request;

@EagerSingleton
public class BeneficiaryHandler extends AccountServiceBaseRequestHandler {

  private final BeneficiaryService beneficiaryService;

  @Inject
  public BeneficiaryHandler(
      ZetaHostMessagingService zetaHostMessagingService,
      BeneficiaryService beneficiaryService) {
    super(zetaHostMessagingService);
    this.beneficiaryService = beneficiaryService;
  }

  @Authorized(anyOf = {"api.beneficiary.add", "api.beneficiary.admin"})
  public void on(AddBeneficiaryRequest payload, Request<AddBeneficiaryRequest> request) {
    onRequest(payload, request);
    beneficiaryService.addBeneficiary(payload)
        .thenAccept(beneficiary -> onResult(beneficiary, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.updatePOP", "api.account.accountHolder.admin"})
  public void on(UpdateBeneficiaryRequest payload, Request<UpdateBeneficiaryRequest> request) {
    onRequest(payload, request);
    beneficiaryService.updateBeneficiary(payload)
        .thenAccept(beneficiary -> onResult(beneficiary, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.updatePOP", "api.account.accountHolder.admin"})
  public void on(GetBeneficiaryRequest payload, Request<GetBeneficiaryRequest> request) {
    onRequest(payload, request);
    beneficiaryService.getBeneficiary(payload.getId(), payload.getIfiID())
        .thenAccept(beneficiary -> onResult(beneficiary, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.getPOP", "api.account.accountHolder.admin"})
  public void on(GetBeneficiaryListRequest payload, Request<GetBeneficiaryListRequest> request) {
    onRequest(payload, request);
    beneficiaryService.listBeneficiaries(payload)
        .thenAccept(beneficiaryList -> onResult(BeneficiaryListResponse.builder().beneficiaryList(beneficiaryList).build(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.deletePOP", "api.account.accountHolder.admin"})
  public void on(DeleteBeneficiaryRequest payload, Request<DeleteBeneficiaryRequest> request) {
    onRequest(payload, request);
    beneficiaryService.deleteBeneficiary(payload.getId(), payload.getIfiID())
        .thenAccept(pop -> onResult(pop, request))
        .exceptionally(t -> onError(request, t));
  }
}
