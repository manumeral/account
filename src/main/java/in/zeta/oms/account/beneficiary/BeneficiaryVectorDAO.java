package in.zeta.oms.account.beneficiary;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.model.BeneficiaryVector;
import in.zeta.oms.account.util.LoggerUtils;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import javax.inject.Singleton;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class BeneficiaryVectorDAO extends PostgresDAO implements RowMapper<BeneficiaryVector> {
    private static final LoggerUtils loggerUtils =
        LoggerUtils.builder().logger(OlympusSpectra.getLogger(BeneficiaryVectorDAO.class)).build();

    // -----------------------------------------Column
    // Names-----------------------------------------//
    public static final String TABLE_NAME = "beneficiary_vector";
    private static final String ID = "id";
    private static final String IFI_ID = "ifi_id";
    private static final String BENEFICIARY_ID = "beneficiary_id";
    private static final String TYPE = "type";
    private static final String VALUE = "value";
    private static final String ATTRIBUTES = "attributes";
    private static final String CREATED_AT = "created_at";
    private static final String UPDATED_AT = "modified_at";

    //-----------------------------------------Used in join query-----------------------------------//
    public static final String COL_ID_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, ID);
    public static final String SELECT_COL_ID_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, ID, COL_ID_ALIAS);
    public static final String COL_TYPE_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, TYPE);
    public static final String COL_VALUE_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, VALUE);
    public static final String SELECT_COL_TYPE_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, TYPE, COL_TYPE_ALIAS);
    public static final String SELECT_COL_VALUE_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, VALUE, COL_VALUE_ALIAS);

    private static final List<String> INSERT_COLUMNS =
        new ImmutableList.Builder<String>()
            .add(ID)
            .add(IFI_ID)
            .add(BENEFICIARY_ID)
            .add(TYPE)
            .add(VALUE)
            .add(ATTRIBUTES)
            .build();

    private static final List<String> SELECT_COLUMNS =
        new ImmutableList.Builder<String>()
            .addAll(INSERT_COLUMNS)
            .add(CREATED_AT)
            .add(UPDATED_AT)
            .build();

    private static final PgQueryGenerator PG_QUERY_GENERATOR =
        PgQueryGenerator.builder()
            .tableName(TABLE_NAME)
            .insertColumns(INSERT_COLUMNS)
            .selectColumns(SELECT_COLUMNS)
            .customColumnType(ID, UUID_TYPE)
            .customColumnType(BENEFICIARY_ID, UUID_TYPE)
            .customColumnType(ATTRIBUTES, JSONB_TYPE)
            .build();

    private final Gson gson;

    @Inject
    public BeneficiaryVectorDAO(
        ZetaHostMessagingService hostMessagingService,
        BasicDataSource basicDataSource,
        @Named("postgres.jdbc.pool.size") int poolSize,
        Gson gson) {
        super(hostMessagingService, basicDataSource, poolSize);
        this.gson = gson;
    }

    @TimeLogger
    public CompletionStage<Void> insert(@NonNull BeneficiaryVector beneficiaryVector) {
        return update(getInsertInstruction(beneficiaryVector)).thenAccept(ignore -> {});
    }

    public PostgresInstruction getInsertInstruction(BeneficiaryVector beneficiaryVector) {
        List<Object> args = new ArrayList<>();
        args.add(beneficiaryVector.getId());
        args.add(beneficiaryVector.getIfiID());
        args.add(beneficiaryVector.getBeneficiaryID());
        args.add(beneficiaryVector.getType());
        args.add(beneficiaryVector.getValue());
        args.add(getJsonbObject(gson.toJson(beneficiaryVector.getAttributes())));

        return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
    }

    @TimeLogger
    public CompletionStage<Optional<BeneficiaryVector>> get(
        @NonNull String id, @NonNull String beneficiaryID, @NonNull Long ifiID) {
        List<Object> args = new ArrayList<>();
        args.add(id);
        args.add(beneficiaryID);
        args.add(ifiID);

        final ImmutableList<String> criteria =
            new ImmutableList.Builder<String>().add(ID).add(BENEFICIARY_ID).add(IFI_ID).build();

        return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
            .exceptionally(
                throwable -> {
                    throwable = unwrapCompletionStateException(throwable);
                    throw loggerUtils.logDBException(
                        String.format(
                            "Get BeneficiaryVectors failed for BeneficiaryVectorID: %s", id),
                        throwable);
                });
    }

    @TimeLogger
    public CompletionStage<Optional<BeneficiaryVector>> get(
        @NonNull String type,@NonNull String value,@NonNull Long ifiID,@NonNull String beneficiaryID) {
        List<Object> args = new ArrayList<>();
        args.add(type);
        args.add(value);
        args.add(ifiID);
        args.add(beneficiaryID);

        final ImmutableList<String> criteria =
            new ImmutableList.Builder<String>()
                .add(TYPE)
                .add(VALUE)
                .add(IFI_ID)
                .add(BENEFICIARY_ID)
                .build();

        return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
            .exceptionally(
                throwable -> {
                    throwable = unwrapCompletionStateException(throwable);
                    throw loggerUtils.logDBException(
                        String.format(
                            "Get BeneficiaryVectors failed for beneficiary with type: %s value : %s ifiID : %s beneficiaryID : %s",
                            type, value, ifiID, beneficiaryID),
                        throwable);
                });
    }

    @TimeLogger
    public CompletionStage<Optional<BeneficiaryVector>> getByVector(
        @NonNull String type,@NonNull String value,@NonNull Long ifiID) {
        List<Object> args = new ArrayList<>();
        args.add(type);
        args.add(value);
        args.add(ifiID);

        final ImmutableList<String> criteria =
            new ImmutableList.Builder<String>()
                .add(TYPE)
                .add(VALUE)
                .add(IFI_ID)
                .build();

        return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
            .exceptionally(
                throwable -> {
                    throwable = unwrapCompletionStateException(throwable);
                    throw loggerUtils.logDBException(
                        String.format(
                            "Get BeneficiaryVector failed for type: %s value : %s ifiID : %s",
                            type, value, ifiID),
                        throwable);
                });
    }

    @TimeLogger
    public CompletionStage<Void> update(BeneficiaryVector beneficiaryVector) {
        final List<String> whereClauseColumnNames = Arrays.asList(ID, BENEFICIARY_ID, IFI_ID);
        ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
        ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
        if (!Strings.isNullOrEmpty(beneficiaryVector.getType())) {
            updateColumnNames.add(TYPE);
            argsList.add(beneficiaryVector.getType());
        }
        if (null != beneficiaryVector.getAttributes()) {
            updateColumnNames.add(ATTRIBUTES);
            argsList.add(getJsonbObject(gson.toJson(beneficiaryVector.getAttributes())));
        }

        if (!Strings.isNullOrEmpty(beneficiaryVector.getValue())) {
            updateColumnNames.add(VALUE);
            argsList.add(beneficiaryVector.getValue());
        }

        argsList.add(beneficiaryVector.getId());
        argsList.add(beneficiaryVector.getBeneficiaryID());
        argsList.add(beneficiaryVector.getIfiID());
        return update(
            PG_QUERY_GENERATOR.getUpdateQueryAndJoin(
                updateColumnNames.build(), whereClauseColumnNames),
            argsList.build())
            .thenAccept(ignore -> {})
            .exceptionally(
                throwable -> {
                    throwable = unwrapCompletionStateException(throwable);
                    throw loggerUtils.logDBException(
                        String.format(
                            "Error Updating vectorID : %s for beneficiaryID : %s and ifiID : %s",
                            beneficiaryVector.getId(),
                            beneficiaryVector.getBeneficiaryID(),
                            beneficiaryVector.getIfiID()),
                        throwable);
                });
    }

    @Override
    public BeneficiaryVector mapRow(ResultSet rs, int rowNum) throws SQLException {
        return BeneficiaryVector.builder()
            .id(rs.getString(ID))
            .ifiID(rs.getLong(IFI_ID))
            .beneficiaryID(rs.getString(BENEFICIARY_ID))
            .type(rs.getString(TYPE))
            .value(rs.getString(VALUE))
            .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
            .createdAt(rs.getTimestamp(CREATED_AT))
            .updatedAt(rs.getTimestamp(UPDATED_AT))
            .build();
    }
}
