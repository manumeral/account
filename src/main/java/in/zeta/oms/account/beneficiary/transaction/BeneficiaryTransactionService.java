package in.zeta.oms.account.beneficiary.transaction;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.commons.Money;
import in.zeta.oms.account.account.AccountService;
import in.zeta.oms.account.api.exception.AccountNotFoundException;
import in.zeta.oms.account.api.exception.TransactionNotFoundException;
import in.zeta.oms.account.beneficiary.BeneficiaryService;
import in.zeta.oms.account.beneficiary.beneficiaryAccount.BeneficiaryAccount;
import in.zeta.oms.account.beneficiary.beneficiaryAccount.BeneficiaryAccountService;
import in.zeta.oms.cloudcard.client.CloudCardClient;
import in.zeta.oms.cloudcard.transaction.model.request.CreateTransactionRequest;
import in.zeta.oms.cloudcard.transaction.model.response.CreateTransactionResponse;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.oms.account.beneficiary.transaction.BeneficiaryTransaction.Status.FAILED;

@Singleton
public class BeneficiaryTransactionService {

    private static final SpectraLogger logger = OlympusSpectra.getLogger(BeneficiaryTransactionService.class);
    private final BeneficiaryService beneficiaryService;
    private final BeneficiaryAccountService beneficiaryAccountService;
    private final CloudCardClient cloudCardClient;
    private final AccountService accountService;
    private final SecureRandomGenerator secureRandomGenerator;
    private final BeneficiaryTransactionDAO beneficiaryTransactionDAO;

    @Inject
    public BeneficiaryTransactionService(BeneficiaryService beneficiaryService,
                                         BeneficiaryAccountService beneficiaryAccountService,
                                         CloudCardClient cloudCardClient,
                                         AccountService accountService,
                                         SecureRandomGenerator secureRandomGenerator,
                                         BeneficiaryTransactionDAO beneficiaryTransactionDAO) {
        this.beneficiaryService = beneficiaryService;
        this.beneficiaryAccountService = beneficiaryAccountService;
        this.cloudCardClient = cloudCardClient;
        this.accountService = accountService;
        this.secureRandomGenerator = secureRandomGenerator;
        this.beneficiaryTransactionDAO = beneficiaryTransactionDAO;
    }

    public CompletionStage<BeneficiaryTransactionResponse> processTransactionRequest(BeneficiaryTransactionRequest request) {
        return beneficiaryAccountService.get(request.getDebitAccountHolderID(), request.getIfiID(), request.getBeneficiaryID(), null, null)
                .thenCompose(beneficiaryAccounts -> findCreditAccount(beneficiaryAccounts)
                        .map(creditAccount -> process(creditAccount, BeneficiaryTransaction.from(request).build()))
                        .orElseThrow(() -> new AccountNotFoundException("No valid credit account found")));
    }

    public CompletionStage<BeneficiaryTransactionResponse> processTransactionRequest(BeneficiaryAccountTransactionRequest request) {
        return beneficiaryAccountService.get(request.getDebitAccountHolderID(), request.getIfiID(), request.getBeneficiaryID(), null, null)
                .thenCompose(beneficiaryAccounts -> findCreditAccount(beneficiaryAccounts, request.getBeneficiaryAccountID())
                        .map(creditAccount -> process(creditAccount, BeneficiaryTransaction.from(request).build()))
                        .orElseThrow(() -> new AccountNotFoundException("No valid credit account found")));
    }

    private CompletionStage<BeneficiaryTransactionResponse> process(BeneficiaryAccount creditAccount, BeneficiaryTransaction transaction) {
        transaction.setId(secureRandomGenerator.generateUUID());
        transaction.setBeneficiaryAccountID(creditAccount.getId());
        return accountService.getAccount(transaction.getIfiID(), transaction.getDebitAccountID())
                .thenCompose(account -> {
                    transaction.setDebitAccountHolderID(account.getOwnerAccountHolderID());
                    return doTransaction(creditAccount.getAccountInfo().getAccountNumber(), transaction)
                            .handle((response, throwable) -> {
                                if (throwable == null) {
                                    transaction.setTransactionID(response.getTransactionID());
                                    transaction.setStatus(BeneficiaryTransaction.Status.valueOf(response.getStatus()));
                                } else {
                                    transaction.setStatus(FAILED);
                                }
                                return beneficiaryTransactionDAO.insert(transaction)
                                        .thenApply(ignored -> buildResponse(transaction.getRequestID(), transaction.getTransactionID(), transaction.getStatus().toString()));
                            })
                            .thenCompose(x -> x);
                });
    }

    private CompletionStage<CreateTransactionResponse> doTransaction(String creditAccountID, BeneficiaryTransaction transaction) {
        CreateTransactionRequest.Builder builder = new CreateTransactionRequest.Builder()
                .creditAccountID(creditAccountID)
                .debitAccountID(transaction.getDebitAccountID())
                .ifi(transaction.getIfiID())
                .remarks(transaction.getRemarks())
                .transactionCode(transaction.getTransactionCode())
                .requestID(transaction.getRequestID())
                .transactionTime(transaction.getTransactionTime())
                .value(new Money(transaction.getCurrency(), transaction.getAmount()))
                .attributes(transaction.getAttributes());

        return cloudCardClient.createTransaction(builder);
    }


    private Optional<BeneficiaryAccount> findCreditAccount(List<BeneficiaryAccount> beneficiaryAccounts) {
        logger.info("findCreditAccount")
                .attr("beneficiaryAccounts", new Gson().toJson(beneficiaryAccounts))
                .log();
        if (beneficiaryAccounts == null)
            return Optional.empty();
        return beneficiaryAccounts.stream()
                .filter(beneficiaryAccount -> beneficiaryAccount.isDefault() && beneficiaryAccount.getType().equals("ZETA"))
                .findFirst();
    }

    private Optional<BeneficiaryAccount> findCreditAccount(List<BeneficiaryAccount> beneficiaryAccounts, String accountID) {
        logger.info("findCreditAccount")
                .attr("beneficiary", new Gson().toJson(beneficiaryAccounts))
                .attr("accountID", accountID)
                .log();
        if (beneficiaryAccounts == null)
            return Optional.empty();
        return beneficiaryAccounts.stream()
                .filter(beneficiaryAccount -> beneficiaryAccount.getId().equals(accountID) && beneficiaryAccount.getType().equals("ZETA"))
                .findFirst();
    }

    private BeneficiaryTransactionResponse buildResponse(String requestID, String transactionID, String status) {
        return BeneficiaryTransactionResponse.builder()
                .requestID(requestID)
                .transactionID(transactionID)
                .status(status)
                .build();
    }

    public CompletionStage<BeneficiaryTransaction> getTransaction(Long ifiID, String accountHolderID, String transactionID) {
        return beneficiaryTransactionDAO.get(ifiID, accountHolderID, transactionID)
                .thenApply(transactionOptional -> transactionOptional.orElseThrow(() -> new TransactionNotFoundException("Transaction not found exception")));
    }

    public CompletionStage<List<BeneficiaryTransaction>> getTransactions(Long ifiID, String accountHolderID, Long pageNumber, Long pageSize) {
        return beneficiaryTransactionDAO.get(ifiID, accountHolderID, pageNumber, pageSize);
    }
}
