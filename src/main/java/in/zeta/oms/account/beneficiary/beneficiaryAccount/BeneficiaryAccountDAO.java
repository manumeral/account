package in.zeta.oms.account.beneficiary.beneficiaryAccount;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.annotations.TimeLogger;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.util.LoggerUtils;
import lombok.NonNull;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.JSONB_TYPE;
import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class BeneficiaryAccountDAO extends PostgresDAO implements RowMapper<BeneficiaryAccount> {

  private static final LoggerUtils loggerUtils =
      LoggerUtils.builder().logger(OlympusSpectra.getLogger(BeneficiaryAccountDAO.class)).build();

  // -----------------------------------------Column Names-----------------------------------------//
  private static final String TABLE_NAME = "beneficiary_account";
  private static final String ID = "id";
  private static final String IFI_ID = "ifi_id";
  private static final String PARENT_ACCOUNT_HOLDER_ID = "parent_account_holder_id";
  private static final String BENEFICIARY_ID = "beneficiary_id";
  private static final String STATUS = "status";
  private static final String NICKNAME = "nickname";
  private static final String TYPE = "type";
  private static final String ACCOUNT_INFO = "account_info";
  private static final String IS_DEFAULT = "is_default";
  private static final String IS_VERIFIED = "is_verified";
  private static final String ATTRIBUTES = "attributes";
  private static final String CREATED_AT = "created_at";
  private static final String UPDATED_AT =
      "modified_at"; // TODO: Make this column name same across all services

  private static final List<String> INSERT_COLUMNS =
      new ImmutableList.Builder<String>()
          .add(ID)
          .add(IFI_ID)
          .add(PARENT_ACCOUNT_HOLDER_ID)
          .add(BENEFICIARY_ID)
          .add(STATUS)
          .add(NICKNAME)
          .add(TYPE)
          .add(ACCOUNT_INFO)
          .add(IS_DEFAULT)
          .add(IS_VERIFIED)
          .add(ATTRIBUTES)
          .build();

  private static final List<String> SELECT_COLUMNS =
      new ImmutableList.Builder<String>()
          .addAll(INSERT_COLUMNS)
          .add(CREATED_AT)
          .add(UPDATED_AT)
          .build();

  private static final PgQueryGenerator PG_QUERY_GENERATOR =
      PgQueryGenerator.builder()
          .tableName(TABLE_NAME)
          .insertColumns(INSERT_COLUMNS)
          .selectColumns(SELECT_COLUMNS)
          .customColumnType(ID, UUID_TYPE)
          .customColumnType(PARENT_ACCOUNT_HOLDER_ID, UUID_TYPE)
          .customColumnType(BENEFICIARY_ID, UUID_TYPE)
          .customColumnType(ACCOUNT_INFO, JSONB_TYPE)
          .customColumnType(ATTRIBUTES, JSONB_TYPE)
          .build();

  private final Gson gson;

  @Inject
  public BeneficiaryAccountDAO(
      ZetaHostMessagingService hostMessagingService,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  @TimeLogger
  public CompletionStage<Void> insert(@NonNull BeneficiaryAccount beneficiaryAccount) {
    return update(getInsertInstruction(beneficiaryAccount)).thenAccept(ignore -> {});
  }

  public PostgresInstruction getInsertInstruction(BeneficiaryAccount beneficiaryAccount) {
    List<Object> args = new ArrayList<>();
    args.add(beneficiaryAccount.getId());
    args.add(beneficiaryAccount.getIfiID());
    args.add(beneficiaryAccount.getParentAccountHolderID());
    args.add(beneficiaryAccount.getBeneficiaryID());
    args.add(beneficiaryAccount.getStatus());
    args.add(beneficiaryAccount.getNickname());
    args.add(beneficiaryAccount.getType());
    args.add(gson.toJson(beneficiaryAccount.getAccountInfo()));
    args.add(beneficiaryAccount.isDefault());
    args.add(beneficiaryAccount.isVerified());
    args.add(getJsonbObject(gson.toJson(beneficiaryAccount.getAttributes())));

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);
  }

  @TimeLogger
  public CompletionStage<Optional<BeneficiaryAccount>> get(
      @NonNull String id,
      @NonNull String beneficiaryID,
      @NonNull String parentAccountHolderID,
      @NonNull Long ifiID) {
    List<Object> args = new ArrayList<>();
    args.add(id);
    args.add(parentAccountHolderID);
    args.add(beneficiaryID);
    args.add(ifiID);

    final ImmutableList<String> criteria =
        new ImmutableList.Builder<String>()
            .add(ID)
            .add(PARENT_ACCOUNT_HOLDER_ID)
            .add(BENEFICIARY_ID)
            .add(IFI_ID)
            .build();

    return queryForOptionalObject(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria), this, args)
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Get BeneficiaryAccount failed for beneficiaryAccountID: %s ifiID: %s beneficiaryID: %s",
                      id, ifiID, beneficiaryID),
                  throwable);
            });
  }

  @TimeLogger
  public CompletionStage<List<BeneficiaryAccount>> get(
      @NonNull Long ifiID,
      String beneficiaryID,
      String nickname,
      String status,
      @NonNull String parentAccountHolderID) {
    List<Object> args = new ArrayList<>();
    final ImmutableList.Builder<String> criteria = new ImmutableList.Builder<String>();
    if (null != nickname) {
      args.add(nickname);
      criteria.add(NICKNAME);
    }
    if (null != status) {
      args.add(status);
      criteria.add(STATUS);
    }
    if (null != beneficiaryID) {
      args.add(beneficiaryID);
      criteria.add(BENEFICIARY_ID);
    }
    args.add(parentAccountHolderID);
    criteria.add(PARENT_ACCOUNT_HOLDER_ID);
    args.add(ifiID);
    criteria.add(IFI_ID);
    return query(PG_QUERY_GENERATOR.getSelectQueryAndJoin(criteria.build()), this, args)
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Get BeneficiaryAccount failed for ifiID : %s parentAccountHolderID : %s",
                      ifiID, parentAccountHolderID),
                  throwable);
            });
  }

  @TimeLogger
  public CompletionStage<Void> update(BeneficiaryAccount beneficiaryAccount) {
    final List<String> whereClauseColumnNames = Arrays.asList(ID, BENEFICIARY_ID, IFI_ID);
    ImmutableList.Builder<String> updateColumnNames = new ImmutableList.Builder<>();
    ImmutableList.Builder<Object> argsList = new ImmutableList.Builder<>();
    if (!Strings.isNullOrEmpty(beneficiaryAccount.getType())) {
      updateColumnNames.add(TYPE);
      argsList.add(beneficiaryAccount.getType());
    }
    if (null != beneficiaryAccount.getAttributes()) {
      updateColumnNames.add(ATTRIBUTES);
      argsList.add(getJsonbObject(gson.toJson(beneficiaryAccount.getAttributes())));
    }
    if (!Strings.isNullOrEmpty(beneficiaryAccount.getStatus())) {
      updateColumnNames.add(STATUS);
      argsList.add(beneficiaryAccount.getStatus());
    }

    if (!Strings.isNullOrEmpty(beneficiaryAccount.getNickname())) {
      updateColumnNames.add(NICKNAME);
      argsList.add(beneficiaryAccount.getNickname());
    }

    argsList.add(beneficiaryAccount.getId());
    argsList.add(beneficiaryAccount.getBeneficiaryID());
    argsList.add(beneficiaryAccount.getIfiID());
    return update(
            PG_QUERY_GENERATOR.getUpdateQueryAndJoin(
                updateColumnNames.build(), whereClauseColumnNames),
            argsList.build())
        .thenAccept(ignore -> {})
        .exceptionally(
            throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              throw loggerUtils.logDBException(
                  String.format(
                      "Error Updating beneficiaryAccountID : %s for beneficiaryID : %s and ifiID : %s",
                      beneficiaryAccount.getId(),
                      beneficiaryAccount.getBeneficiaryID(),
                      beneficiaryAccount.getIfiID()),
                  throwable);
            });
  }

  //TODO :  This function is nin functional
  @TimeLogger
  public CompletionStage<Void> updateDefaultAccount(BeneficiaryAccount beneficiaryAccount) {
    return update(beneficiaryAccount);
  }

  @Override
  public BeneficiaryAccount mapRow(ResultSet rs, int rowNum) throws SQLException {
    return BeneficiaryAccount.builder()
        .parentAccountHolderID(rs.getString(PARENT_ACCOUNT_HOLDER_ID))
        .accountInfo(
            gson.fromJson(
                rs.getString(ACCOUNT_INFO), new TypeToken<BeneficiaryAccountInfo>() {}.getType()))
        .nickname(rs.getString(NICKNAME))
        .beneficiaryID(rs.getString(BENEFICIARY_ID))
        .id(rs.getString(ID))
        .ifiID(rs.getLong(IFI_ID))
        .status(rs.getString(STATUS))
        .isDefault(rs.getBoolean(IS_DEFAULT))
        .type(rs.getString(TYPE))
        .attributes(gson.fromJson(rs.getString(ATTRIBUTES), TYPE_MAP_STRING_TO_STRING))
        .createdAt(rs.getTimestamp(CREATED_AT))
        .updatedAt(rs.getTimestamp(UPDATED_AT))
        .build();
  }
}
