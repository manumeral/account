package in.zeta.oms.account.beneficiary;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.api.model.BeneficiaryVector;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;

import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class BeneficiaryVectorService {
    private static final SpectraLogger logger = OlympusSpectra.getLogger(BeneficiaryVectorService.class);

    private final BeneficiaryVectorDAO beneficiaryVectorDAO;
    private final SecureRandomGenerator secureRandomGenerator;

    @Inject
    public BeneficiaryVectorService(
        BeneficiaryVectorDAO beneficiaryVectorDAO, SecureRandomGenerator secureRandomGenerator) {
        this.beneficiaryVectorDAO = beneficiaryVectorDAO;
        this.secureRandomGenerator = secureRandomGenerator;
    }

    @PublishEvent(topicName = PubSubTopics.ADD_BENEFICIARY_VECTOR_EVENT_TOPIC)
    public CompletionStage<BeneficiaryVector> addVector(BeneficiaryVector beneficiaryVector) {
        final BeneficiaryVector beneficiaryVectorToAdd =
            beneficiaryVector.toBuilder().id(secureRandomGenerator.generateUUID()).build();
        return beneficiaryVectorDAO
            .insert(beneficiaryVectorToAdd)
            .thenCompose(
                ignore ->
                    get(
                        beneficiaryVectorToAdd.getId(),
                        beneficiaryVectorToAdd.getBeneficiaryID(),
                        beneficiaryVectorToAdd.getIfiID()))
            .exceptionally(
                t -> {
                    t = unwrapCompletionStateException(t);
                    logger.warn("Benficiary Vector creation failed")
                        .attr("beneficiaryID", beneficiaryVector.getBeneficiaryID())
                        .attr("ifiID", beneficiaryVector.getIfiID())
                        .attr("type", beneficiaryVector.getType())
                        .attr("value", beneficiaryVector.getValue())
                        .attr("errMsg", t.getMessage())
                        .log();
                    throw new BeneficiaryVectorCreationException(
                        String.format(
                            "Account Holder Vector Creation failed for beneficiary ID : %s",
                            beneficiaryVectorToAdd.getBeneficiaryID()),
                        t,
                        AccountErrorCode.INTERNAL_ERROR);
                });
    }

    public CompletionStage<BeneficiaryVector> get(String id, String beneficiaryID, Long ifiID) {
        return beneficiaryVectorDAO
            .get(id, beneficiaryID, ifiID)
            .thenApply(
                beneficiaryVectorOptional ->
                    beneficiaryVectorOptional.orElseThrow(
                        () ->
                            new BeneficiaryVectorNotFoundException(
                                String.format(
                                    "Account Holder Vector for beneficiary ID : %s and beneficiary vector id : %s not found",
                                    beneficiaryID, id),
                                AccountErrorCode.ACCOUNT_VECTOR_NOT_FOUND)));
    }

    public CompletionStage<BeneficiaryVector> getByVector(String type, String value, Long ifiID) {
        return beneficiaryVectorDAO
            .getByVector(type, value, ifiID)
            .thenApply(
                beneficiaryVectorOptional ->
                    beneficiaryVectorOptional.orElseThrow(
                        () ->
                            new BeneficiaryVectorNotFoundException(
                                String.format(
                                    "Get BeneficiaryVector failed for type: %s value : %s ifiID : %s",
                                    type, value, ifiID),
                                AccountErrorCode.ACCOUNT_HOLDER_VECTOR_NOT_FOUND)));
    }

    @PublishEvent(topicName = PubSubTopics.UPDATE_BENEFICIARY_VECTOR_EVENT_TOPIC)
    public CompletionStage<BeneficiaryVector> update(BeneficiaryVector beneficiaryVector) {
        BeneficiaryVector.Builder beneficiaryVectorBuilder = beneficiaryVector.toBuilder();
        CompletionStage<String> beneficiaryVectorIDFuture = completedFuture(beneficiaryVector.getId());
        if (null == beneficiaryVector.getId()) {
            beneficiaryVectorIDFuture =
                beneficiaryVectorDAO
                    .getByVector(
                        beneficiaryVector.getType(),
                        beneficiaryVector.getValue(),
                        beneficiaryVector.getIfiID())
                    .thenApply(
                        beneficiaryVectorOptional ->
                            beneficiaryVectorOptional
                                .map(BeneficiaryVector::getId)
                                .orElseThrow(
                                    () ->
                                        BeneficiaryVectorNotFoundException.beneficiaryNotFoundForTypeValuePair(
                                            beneficiaryVector.getType(),
                                            beneficiaryVector.getValue(),
                                            beneficiaryVector.getIfiID(),
                                            beneficiaryVector.getId())));
        }

        return beneficiaryVectorIDFuture
            .thenApply(beneficiaryVectorBuilder::id)
            .thenCompose(builder -> beneficiaryVectorDAO.update(builder.build()))
            .thenCompose(
                ignore ->
                    beneficiaryVectorDAO.get(
                        beneficiaryVectorBuilder.build().getId(),
                        beneficiaryVector.getBeneficiaryID(),
                        beneficiaryVector.getIfiID()))
            .thenApply(
                beneficiaryVectorOptional ->
                    beneficiaryVectorOptional.orElseThrow(
                        () ->
                            new BeneficiaryVectorUpdateException(
                                String.format(
                                    "Beneficiary Vector update for beneficiary ID : %s and beneficiary vector id : %s failed",
                                    beneficiaryVector.getBeneficiaryID(), beneficiaryVectorBuilder.build().getId()),
                                AccountErrorCode.INTERNAL_ERROR)));
    }
}
