package in.zeta.oms.account.beneficiary;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.api.enums.AccountHolderStatus;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.service.BaseSchemaIssuerService;
import in.zeta.oms.athenamanager.api.model.EntityType;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;
import olympus.message.util.CompletableFutures;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;
import java.util.concurrent.CompletionStage;

import static in.zeta.oms.account.constant.AccountErrorCode.INTERNAL_ERROR;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

@Singleton
public class BeneficiaryService {
  private final SecureRandomGenerator secureRandomGenerator;
  private final BeneficiaryDAO beneficiaryDAO;
  private final BeneficiaryTransactionDAO beneficiaryTransactionDAO;
  private final BaseSchemaIssuerService baseSchemaIssuerService;

  @Inject
  public BeneficiaryService(SecureRandomGenerator secureRandomGenerator,
                            BeneficiaryDAO beneficiaryDAO,
                            BeneficiaryTransactionDAO beneficiaryTransactionDAO,
                            BaseSchemaIssuerService baseSchemaIssuerService) {
    this.secureRandomGenerator = secureRandomGenerator;
    this.beneficiaryDAO = beneficiaryDAO;
    this.beneficiaryTransactionDAO = beneficiaryTransactionDAO;
    this.baseSchemaIssuerService = baseSchemaIssuerService;
  }

  @PublishEvent(topicName = PubSubTopics.ADD_BENEFICIARY_EVENT_TOPIC)
  public CompletionStage<Beneficiary> addBeneficiary(AddBeneficiaryRequest payload) {
    final String id = secureRandomGenerator.generateUUID();

    final Beneficiary.Builder beneficiaryBuilder = Beneficiary
        .from(payload)
        .id(id)
        .status(AccountHolderStatus.ENABLED.toString());

    final Long ifiID = payload.getIfiID();
    final String requestID = payload.getRequestID();
    return beneficiaryDAO
        .getBeneficiaryWithRequestID(requestID, ifiID)
        .thenCompose(beneficiaryOptional -> {
          if (beneficiaryOptional.isPresent()) {
            throw BeneficiaryInsertionException.duplicateRequestException(
                "A beneficiary is already created with this request");
          }

          return validate(beneficiaryBuilder.build())
            .thenCompose(ignore -> beneficiaryTransactionDAO.insertBeneficiary(beneficiaryBuilder.build()))
                  .handle((accountHolder, throwable) -> {
                    if (throwable != null) {
                      throwable = CompletableFutures.unwrapCompletionStateException(throwable);
                      if (throwable instanceof DuplicateKeyException) {
                        throw new BeneficiaryInsertionException("Beneficiary already exists with vector type", throwable, INTERNAL_ERROR);
                      }
                      throw new BeneficiaryInsertionException("Beneficiary creation failed", throwable, INTERNAL_ERROR);
                    }
                    return completedFuture(accountHolder);
                  })
              .thenCompose(future -> future);
        });

  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_BENEFICIARY_EVENT_TOPIC)
  public CompletionStage<Beneficiary> updateBeneficiary(UpdateBeneficiaryRequest payload) {
    return beneficiaryDAO
        .updateBeneficiary(Beneficiary.from(payload).build())
        .thenCompose(accountHolder -> beneficiaryDAO.getBeneficiaryByID(payload.getId(), payload.getIfiID()))
        .exceptionally(
            t -> {
              t = unwrapCompletionStateException(t);
              if (!(t instanceof BeneficiaryNotFoundException)) {
                t = unwrapCompletionStateException(t);
                throw new AccountServiceException(
                    "DB Error while updating Beneficiary", t, INTERNAL_ERROR);
              }
              throw new BeneficiaryNotFoundException(
                  String.format("Beneficiary not found for id: %s", payload.getId()));
            });

  }

  public CompletionStage<Beneficiary> getBeneficiary(String id, Long ifiID) {
    return beneficiaryDAO.getBeneficiaryByID(id, ifiID);
  }

  public CompletionStage<List<Beneficiary>> listBeneficiaries(GetBeneficiaryListRequest payload) {
    return beneficiaryDAO.getBeneficiaryByAccountHolderID(payload.getIfiID(), payload.getAccountHolderID());
  }
  // TODO, currently handled by update flows
  public CompletionStage<Beneficiary> deleteBeneficiary(String id, Long ifiID) {
    return null;
  }

  public CompletionStage<Void> validate(Beneficiary beneficiary) {
      return baseSchemaIssuerService.validate(beneficiary, beneficiary.getIfiID(), EntityType.BENEFICIARY.name());
  }
}
