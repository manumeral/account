package in.zeta.oms.account.beneficiary.beneficiaryAccount;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import in.zeta.oms.account.beneficiary.UpdateBeneficiaryRequest;
import olympus.message.types.Request;

import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

@EagerSingleton
public class BeneficiaryAccountHandler extends AccountServiceBaseRequestHandler {

  private final BeneficiaryAccountService beneficiaryAccountService;

  @Inject
  public BeneficiaryAccountHandler(ZetaHostMessagingService zetaHostMessagingService,
                                   BeneficiaryAccountService beneficiaryAccountService) {
    super(zetaHostMessagingService);
    this.beneficiaryAccountService = beneficiaryAccountService;
  }

  @Authorized(anyOf = {"api.beneficiaryAccount.create", "api.beneficiaryAccount.admin"})
  public void on(AddBeneficiaryAccountForBeneficiaryRequest payload, Request<AddBeneficiaryAccountForBeneficiaryRequest> request) {
    onRequest(payload, request);
    BeneficiaryAccount beneficiaryAccount = BeneficiaryAccount.builder()
        .nickname(payload.getNickname())
        .parentAccountHolderID(payload.getParentAccountHolderID())
        .type(payload.getType())
        .ifiID(payload.getIfiID())
        .beneficiaryID(payload.getBeneficiaryID())
        .accountInfo(payload.getAccountInfo())
        .attributes(firstNonNull(payload.getAttributes(), emptyMap()))
        .isVerified(firstNonNull(payload.isVerified(), false))
        .isDefault(firstNonNull(payload.isDefault(), false))
        .status(firstNonNull(payload.getStatus(), "ENABLED"))
        .build();
    beneficiaryAccountService.createBeneficiaryAccount(beneficiaryAccount)
        .thenAccept(response -> onResult(response, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.beneficiaryAccount.get", "api.beneficiaryAccount.admin"})
  public void on(GetBeneficiaryAccountRequest payload, Request<GetBeneficiaryAccountRequest> request) {
    onRequest(payload, request);
    beneficiaryAccountService.get(payload.getId(), payload.getBeneficiaryID(), payload.getParentAccountHolderID(), payload.getIfiID())
        .thenAccept(response -> onResult(response, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.beneficiaryAccount.get", "api.beneficiaryAccount.admin"})
  public void on(GetBeneficiaryAccountList payload, Request<GetBeneficiaryAccountList> request) {
    onRequest(payload, request);
    beneficiaryAccountService.get(payload.getParentAccountHolderID(), payload.getIfiID(), payload.getBeneficiaryID(), payload.getNickname(), payload.getStatus())
        .thenAccept(response -> onResult(BeneficiaryAccountListResponse.builder().beneficiaryAccountList(response).build(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.beneficiaryAccount.update", "api.beneficiaryAccount.admin"})
  public void on(UpdateBeneficiaryAccountRequest payload, Request<UpdateBeneficiaryRequest> request) {
    onRequest(payload, request);
    BeneficiaryAccount beneficiaryAccount = BeneficiaryAccount.builder()
        .id(payload.getId())
        .nickname(payload.getNickname())
        .parentAccountHolderID(payload.getParentAccountHolderID())
        .type(payload.getType())
        .ifiID(payload.getIfiID())
        .beneficiaryID(payload.getBeneficiaryID())
        .accountInfo(payload.getAccountInfo())
        .attributes(payload.getAttributes())
        .isVerified(payload.isVerified())
        .status(payload.getStatus())
        .build();
    beneficiaryAccountService.update(beneficiaryAccount)
        .thenAccept(response -> onResult(response, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.beneficiaryAccount.get", "api.beneficiaryAccount.admin"})
  public void on(SetDefaultBeneficiaryAccount payload, Request<SetDefaultBeneficiaryAccount> request) {
    onRequest(payload, request);
    BeneficiaryAccount beneficiaryAccount = BeneficiaryAccount.builder()
        .parentAccountHolderID(payload.getParentAccountHolderID())
        .ifiID(payload.getIfiID())
        .beneficiaryID(payload.getBeneficiaryID())
        .id(payload.getBeneficiaryAccountID())
        .build();
    beneficiaryAccountService.updateDefaultAccount(beneficiaryAccount)
        .thenAccept(response -> onResult(response, request))
        .exceptionally(t -> onError(request, t));
  }
}
