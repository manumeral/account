package in.zeta.oms.account.beneficiary;

import com.google.gson.Gson;
import in.zeta.commons.gson.ZetaGsonBuilder;
import in.zeta.commons.postgres.PostgresRowCallbackHandler;
import in.zeta.oms.account.api.model.BeneficiaryVector;
import in.zeta.oms.athenamanager.api.enums.AccountHolderType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;

public class BeneficiaryCallbackHandler
    extends PostgresRowCallbackHandler<List<Beneficiary>> {
  private final Gson gson = new ZetaGsonBuilder().build();
  private final Map<String, Beneficiary.Builder> beneficiaryMap = new HashMap<>();

  private BeneficiaryCallbackHandler(List<Beneficiary> beneficiaryList) {
    super(beneficiaryList);
  }

  @Override
  public void processRow(ResultSet rs, int rowNum) throws SQLException {
    if (!beneficiaryMap.containsKey(rs.getString(BeneficiaryDAO.COL_ID_ALIAS))) {
      Beneficiary.Builder beneficiaryBuilder = Beneficiary
          .builder()
          .requestID(rs.getString(BeneficiaryDAO.REQUEST_ID))
          .id(rs.getString(BeneficiaryDAO.COL_ID_ALIAS))
          .ifiID(rs.getLong(BeneficiaryDAO.COL_IFI_ID_ALIAS))
          .type(AccountHolderType.valueOf(rs.getString(BeneficiaryDAO.COL_TYPE_ALIAS)))
          .status(rs.getString(BeneficiaryDAO.COL_STATUS_ALIAS))
          .customFields(
              gson.fromJson(rs.getString(BeneficiaryDAO.COL_ATTRIBUTES_ALIAS), TYPE_MAP_STRING_TO_STRING))
          .salutation(rs.getString(BeneficiaryDAO.SALUTATION))
          .firstName(rs.getString(BeneficiaryDAO.FIRST_NAME))
          .middleName(rs.getString(BeneficiaryDAO.MIDDLE_NAME))
          .lastName(rs.getString(BeneficiaryDAO.LAST_NAME))
          .profilePicURL(rs.getString(BeneficiaryDAO.PROFILE_PIC_URL))
          .dob(rs.getDate(BeneficiaryDAO.DOB))
          .gender(rs.getString(BeneficiaryDAO.GENDER))
          .accountHolderID(rs.getString(BeneficiaryDAO.ACCOUNT_HOLDER_ID));
      beneficiaryMap.put(rs.getString(BeneficiaryDAO.COL_ID_ALIAS), beneficiaryBuilder);
    }

    if (rs.getString(BeneficiaryVectorDAO.COL_VALUE_ALIAS) != null) {
      beneficiaryMap
          .get(rs.getString(BeneficiaryDAO.COL_ID_ALIAS))
          .vector(BeneficiaryVector.builder()
              .id(rs.getString(BeneficiaryVectorDAO.COL_ID_ALIAS))
              .beneficiaryID(rs.getString(BeneficiaryDAO.COL_ID_ALIAS))
              .type(rs.getString(BeneficiaryVectorDAO.COL_TYPE_ALIAS))
              .value(rs.getString(BeneficiaryVectorDAO.COL_VALUE_ALIAS))
              .build());
    }
  }

  @Override
  public List<Beneficiary> get() {
    List<Beneficiary> beneficiaryList = super.get();
    beneficiaryMap
        .values()
        .stream()
        .map(Beneficiary.Builder::build)
        .collect(Collectors.toCollection(() -> beneficiaryList));
    return beneficiaryList;
  }

  public static BeneficiaryCallbackHandler getPostgresCallbackHandler() {
    return new BeneficiaryCallbackHandler(new ArrayList<>());
  }
}
