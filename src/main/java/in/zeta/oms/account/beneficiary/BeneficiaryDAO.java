package in.zeta.oms.account.beneficiary;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.athenacommons.postgres.PgQueryGenerator;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.util.LoggerUtils;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.apache.commons.dbcp2.BasicDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static in.zeta.athenacommons.postgres.PgQueryGenerator.UUID_TYPE;
import static in.zeta.oms.account.beneficiary.BeneficiaryCallbackHandler.getPostgresCallbackHandler;
import static in.zeta.oms.account.constant.AccountErrorCode.DB_ERROR;

@Singleton
public class BeneficiaryDAO extends PostgresDAO {

  private static final LoggerUtils loggerUtils = LoggerUtils.builder()
      .logger(OlympusSpectra.getLogger(BeneficiaryDAO.class))
      .build();

  private static final String TABLE_NAME = "beneficiary";

  //------------------------------------------Column Names----------------------------------------//
  public static final String REQUEST_ID = "request_id";
  public static final String ID = "id";
  public static final String IFI_ID = "ifi_id";
  public static final String ACCOUNT_HOLDER_ID = "account_holder_id";
  public static final String TYPE = "type";
  public static final String STATUS = "status";
  public static final String ATTRIBUTES = "attributes";
  public static final String SALUTATION = "salutation";
  public static final String FIRST_NAME = "firstname";
  public static final String MIDDLE_NAME = "middlename";
  public static final String LAST_NAME = "lastname";
  public static final String PROFILE_PIC_URL = "profilepicurl";
  public static final String DOB = "dob";
  public static final String GENDER = "gender";


  //------------------------------------------Used for Join Query---------------------------------//
  public static final String COL_ID_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, ID);
  public static final String SELECT_COL_ID_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, ID, COL_ID_ALIAS);
  public static final String COL_IFI_ID_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, IFI_ID);
  public static final String SELECT_COL_IFI_ID_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, IFI_ID, COL_IFI_ID_ALIAS);
  public static final String COL_TYPE_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, TYPE);
  public static final String SELECT_COL_TYPE_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, TYPE, COL_TYPE_ALIAS);
  public static final String COL_STATUS_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, STATUS);
  public static final String SELECT_COL_STATUS_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, STATUS, COL_STATUS_ALIAS);
  public static final String COL_ATTRIBUTES_ALIAS = String.format("%1$s_%2$s", TABLE_NAME, ATTRIBUTES);
  public static final String SELECT_COL_ATTRIBUTES_WITH_ALIAS = String.format("%1$s.%2$s as %3$s", TABLE_NAME, ATTRIBUTES, COL_ATTRIBUTES_ALIAS);

  //------------------------------------------Error Messages--------------------------------------//
  private static final String GET_BENEFICIARY_REQUEST_FAILED = "Get beneficiary request failed";
  private static final String BENEFICIARY_NOT_FOUND = "Beneficiary not found";

  //------------------------------------------All Columns-----------------------------------------//
  private static final List<String> INSERT_COLUMNS  = new Builder<String>()
      .add(REQUEST_ID)
      .add(ID)
      .add(IFI_ID)
      .add(TYPE)
      .add(STATUS)
      .add(ATTRIBUTES)
      .add(SALUTATION)
      .add(FIRST_NAME)
      .add(MIDDLE_NAME)
      .add(LAST_NAME)
      .add(PROFILE_PIC_URL)
      .add(DOB)
      .add(GENDER)
      .add(ACCOUNT_HOLDER_ID)
      .build();
  private static final List<String> SELECT_COLUMNS  = new Builder<String>()
      .addAll(INSERT_COLUMNS)
      .build();

  private static final String SELECT_COLUMNS_FOR_JOIN  = new Builder<String>()
      .add(SELECT_COL_ID_WITH_ALIAS)
      .add(REQUEST_ID)
      .add(SALUTATION)
      .add(FIRST_NAME)
      .add(MIDDLE_NAME)
      .add(LAST_NAME)
      .add(PROFILE_PIC_URL)
      .add(DOB)
      .add(GENDER)
      .add(ACCOUNT_HOLDER_ID)
      .add(SELECT_COL_IFI_ID_WITH_ALIAS)
      .add(SELECT_COL_TYPE_WITH_ALIAS)
      .add(SELECT_COL_STATUS_WITH_ALIAS)
      .add(SELECT_COL_ATTRIBUTES_WITH_ALIAS)
      .add(BeneficiaryVectorDAO.SELECT_COL_ID_WITH_ALIAS)
      .add(BeneficiaryVectorDAO.SELECT_COL_TYPE_WITH_ALIAS)
      .add(BeneficiaryVectorDAO.SELECT_COL_VALUE_WITH_ALIAS)
      .build()
      .stream()
      .collect(Collectors.joining(","));

  private static final PgQueryGenerator PG_QUERY_GENERATOR = PgQueryGenerator.builder()
      .tableName(TABLE_NAME)
      .insertColumns(INSERT_COLUMNS)
      .selectColumns(SELECT_COLUMNS)
      .customColumnType(ID, UUID_TYPE)
      .build();

  private static final String JOIN_QUERY = "select %1$s "
      + "from %2$s "
      + "left join %3$s "
      + "on %2$s.id = %3$s.beneficiary_id ";
  private static final String JOIN_QUERY_INTERNAL_ID = JOIN_QUERY
      + "where %2$s.%4$s = ?::UUID and %2$s.%5$s = ?";
  private static final String JOIN_QUERY_REQUEST_ID = JOIN_QUERY
      + "where %2$s.%4$s = ? and %2$s.%5$s = ?";


  private static final String SELECT_QUERY_JOIN_VECTOR_BY_ID = String.format(JOIN_QUERY_INTERNAL_ID,
      SELECT_COLUMNS_FOR_JOIN,
      TABLE_NAME,
      BeneficiaryVectorDAO.TABLE_NAME,
      ID,
      IFI_ID);

  private static final String SELECT_QUERY_JOIN_VECTOR_BY_REQUEST_ID = String.format(JOIN_QUERY_REQUEST_ID,
      SELECT_COLUMNS_FOR_JOIN,
      TABLE_NAME,
      BeneficiaryVectorDAO.TABLE_NAME,
      REQUEST_ID,
      IFI_ID);

  private static final String SELECT_QUERY_JOIN_VECTOR_BY_ACCOUNT_HOLDER_ID = String.format(JOIN_QUERY_REQUEST_ID,
      SELECT_COLUMNS_FOR_JOIN,
      TABLE_NAME,
      BeneficiaryVectorDAO.TABLE_NAME,
      ACCOUNT_HOLDER_ID,
      IFI_ID);

  private final Gson gson;
  private static final SpectraLogger log = OlympusSpectra.getLogger(BeneficiaryDAO.class);

  @Inject
  public BeneficiaryDAO(
      ZetaHostMessagingService hostMessagingService,
      BasicDataSource basicDataSource,
      @Named("postgres.jdbc.pool.size") int poolSize,
      Gson gson) {
    super(hostMessagingService, basicDataSource, poolSize);
    this.gson = gson;
  }

  public PostgresInstruction getInsertInstruction(
      Beneficiary beneficiary) {
    List<Object> args = new ArrayList<>();
    args.add(beneficiary.getRequestID());
    args.add(beneficiary.getId());
    args.add(beneficiary.getIfiID());
    args.add(beneficiary.getType().name());
    args.add(beneficiary.getStatus());
    args.add(getJsonbObject(gson.toJson(beneficiary.getCustomFields())));
    // TODO: Update existing test cases to update all these below variables
    args.add(beneficiary.getSalutation());
    args.add(beneficiary.getFirstName());
    args.add(beneficiary.getMiddleName());
    args.add(beneficiary.getLastName());
    args.add(beneficiary.getProfilePicURL());
    args.add(beneficiary.getDob());
    args.add(beneficiary.getGender());
    args.add(beneficiary.getAccountHolderID());

    return new PostgresInstruction(PG_QUERY_GENERATOR.getInsertQuery(), args);

  }

  // TODO: Remove this call and from corresponding callers too.
  public CompletionStage<Beneficiary>  getBeneficiaryByID(String id, Long ifiID) {
    return query(SELECT_QUERY_JOIN_VECTOR_BY_ID, getPostgresCallbackHandler(), id, ifiID)
        .handle((beneficiaryList, throwable) -> {
          if (throwable != null) {
            log.error(GET_BENEFICIARY_REQUEST_FAILED, throwable)
                .attr("ifiID", ifiID)
                .attr("id", id)
                .log();
            throw new AccountServiceException(GET_BENEFICIARY_REQUEST_FAILED, DB_ERROR);
          } else if (beneficiaryList.isEmpty()) {
            log.info(BENEFICIARY_NOT_FOUND)
                .attr("id", id)
                .attr("ifiID", ifiID)
                .log();
            throw new BeneficiaryNotFoundException(
                String.format("Beneficiary not found for id: %s", id));
          }
          return beneficiaryList.get(0);
        });
  }

  public CompletionStage<Optional<Beneficiary>> getBeneficiaryWithRequestID(String requestID, Long ifiID) {
    return query(SELECT_QUERY_JOIN_VECTOR_BY_REQUEST_ID, getPostgresCallbackHandler(), requestID, ifiID)
        .handle((beneficiaryList, throwable) -> {
          if (throwable != null) {
            throw loggerUtils.logDBException("Get Beneficiary failed with request id " + requestID, throwable);
          }
          if (beneficiaryList.size() > 1) {
            throw new BeneficiaryInsertionException("Multiple beneficiary exist with same request ID", AccountErrorCode.DUPLICATE_REQUEST_EXCEPTION);
          }
          if (beneficiaryList.isEmpty()) {
            return Optional.empty();
          }
          return Optional.of(beneficiaryList.get(0));
        });
  }

  public CompletionStage<Void> updateBeneficiary(Beneficiary beneficiary) {
    final List<Object> arguments = new ArrayList<>();
    final List<String> columnsToUpdate = new ArrayList<>();
    final List<String> whereClause = new ArrayList<>();

    whereClause.add(ID);
    whereClause.add(IFI_ID);

    if(null != beneficiary.getCustomFields()) {
      columnsToUpdate.add(ATTRIBUTES);
      arguments.add(getJsonbObject(gson.toJson(beneficiary.getCustomFields())));
    }

    if(!Strings.isNullOrEmpty(beneficiary.getStatus())) {
      columnsToUpdate.add(STATUS);
      arguments.add(beneficiary.getStatus());
    }

    updateUserProfile(beneficiary, columnsToUpdate, arguments);

    arguments.add(beneficiary.getId());
    arguments.add(beneficiary.getIfiID());

    return update(PG_QUERY_GENERATOR.getUpdateQueryAndJoin(columnsToUpdate, whereClause), arguments)
        .thenAccept(
            rows -> {
              if (rows == 0) {
                log.warn("Beneficiary not found during update")
                    .attr("id", beneficiary.getId())
                    .attr("ifiID", beneficiary.getIfiID())
                    .log();
                throw new BeneficiaryNotFoundException(
                    String.format("Beneficiary Not Found for id: %s ifiID: %s",beneficiary.getId(), beneficiary.getIfiID()));
              }
            });
  }

  public void updateUserProfile(Beneficiary beneficiary, List<String> columnsToUpdate, List<Object> arguments) {

    if(!Strings.isNullOrEmpty(beneficiary.getSalutation())) {
      columnsToUpdate.add(SALUTATION);
      arguments.add(beneficiary.getSalutation());
    }

    if(!Strings.isNullOrEmpty(beneficiary.getFirstName())) {
      columnsToUpdate.add(FIRST_NAME);
      arguments.add(beneficiary.getFirstName());
    }

    if(!Strings.isNullOrEmpty(beneficiary.getMiddleName())) {
      columnsToUpdate.add(MIDDLE_NAME);
      arguments.add(beneficiary.getMiddleName());
    }

    if(!Strings.isNullOrEmpty(beneficiary.getLastName())) {
      columnsToUpdate.add(LAST_NAME);
      arguments.add(beneficiary.getLastName());
    }

    if(!Strings.isNullOrEmpty(beneficiary.getProfilePicURL())) {
      columnsToUpdate.add(PROFILE_PIC_URL);
      arguments.add(beneficiary.getProfilePicURL());
    }

    if(null != beneficiary.getDob()) {
      columnsToUpdate.add(DOB);
      arguments.add(beneficiary.getDob());
    }

    if(!Strings.isNullOrEmpty(beneficiary.getGender())) {
      columnsToUpdate.add(GENDER);
      arguments.add(beneficiary.getGender());
    }

  }

  public CompletionStage<List<Beneficiary>> getBeneficiaryByAccountHolderID(Long ifiID, String accountHolderID) {
    return query(SELECT_QUERY_JOIN_VECTOR_BY_ACCOUNT_HOLDER_ID, getPostgresCallbackHandler(), accountHolderID, ifiID)
        .handle((beneficiaryList, throwable) -> {
          if (throwable != null) {
            throw loggerUtils.logDBException("Get Beneficiary failed with account holder id " + accountHolderID, throwable);
          }
          return beneficiaryList;
        });
  }
}