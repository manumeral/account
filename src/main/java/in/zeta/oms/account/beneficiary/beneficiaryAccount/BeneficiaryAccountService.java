package in.zeta.oms.account.beneficiary.beneficiaryAccount;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.athenacommons.util.SecureRandomGenerator;
import in.zeta.oms.account.api.exception.BeneficiaryAccountNotFoundException;
import in.zeta.oms.account.service.BaseSchemaIssuerService;
import in.zeta.oms.athenamanager.api.model.EntityType;
import in.zeta.oms.account.server.pubsub.PubSubTopics;
import in.zeta.oms.account.server.pubsub.PublishEvent;

import java.util.List;
import java.util.concurrent.CompletionStage;

@Singleton
public class BeneficiaryAccountService {

  private final BeneficiaryAccountDAO beneficiaryAccountDAO;
  private final SecureRandomGenerator secureRandomGenerator;
  private final BaseSchemaIssuerService baseSchemaIssuerService;

  @Inject
  public BeneficiaryAccountService(SecureRandomGenerator secureRandomGenerator,
                                   BeneficiaryAccountDAO beneficiaryAccountDAO,
                                   BaseSchemaIssuerService baseSchemaIssuerService) {
    this.secureRandomGenerator = secureRandomGenerator;
    this.beneficiaryAccountDAO = beneficiaryAccountDAO;
    this.baseSchemaIssuerService = baseSchemaIssuerService;
  }

  @PublishEvent(topicName = PubSubTopics.CREATE_BENEFICIARY_ACCOUNT_EVENT_TOPIC)
  public CompletionStage<BeneficiaryAccount> createBeneficiaryAccount(BeneficiaryAccount beneficiaryAccount) {
    final String id = secureRandomGenerator.generateUUID();

    return validate(beneficiaryAccount)
        .thenCompose(ignore -> beneficiaryAccountDAO.insert(beneficiaryAccount.toBuilder().id(id).build()))
        .thenCompose(ignore -> get(id, beneficiaryAccount.getBeneficiaryID(), beneficiaryAccount.getParentAccountHolderID(), beneficiaryAccount.getIfiID()));
  }

  public CompletionStage<BeneficiaryAccount> get(String beneficiaryAccountID, String beneficiaryID, String parentAccountHolderID, Long ifiID) {
    return beneficiaryAccountDAO.get(beneficiaryAccountID, beneficiaryID, parentAccountHolderID, ifiID)
        .thenApply(beneficiaryAccountOptional -> beneficiaryAccountOptional.orElseThrow(() ->
          new BeneficiaryAccountNotFoundException(beneficiaryAccountID)
        ));
  }

  public CompletionStage<List<BeneficiaryAccount>> get(String parentAccountHolderID, Long ifiID, String beneficiaryID, String nickname, String status) {
    return beneficiaryAccountDAO.get(ifiID,beneficiaryID,nickname,status, parentAccountHolderID);
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_BENEFICIARY_ACCOUNT_EVENT_TOPIC)
  public CompletionStage<BeneficiaryAccount> update(BeneficiaryAccount beneficiaryAccount) {
    return beneficiaryAccountDAO.update(beneficiaryAccount)
        .thenCompose(ignore -> get(beneficiaryAccount.getId(), beneficiaryAccount.getBeneficiaryID(), beneficiaryAccount.getParentAccountHolderID(), beneficiaryAccount.getIfiID()));
  }

  @PublishEvent(topicName = PubSubTopics.UPDATE_BENEFICIARY_ACCOUNT_EVENT_TOPIC)
  public CompletionStage<BeneficiaryAccount> updateDefaultAccount(BeneficiaryAccount beneficiaryAccount) {
    return beneficiaryAccountDAO.updateDefaultAccount(beneficiaryAccount)
        .thenCompose(ignore -> get(beneficiaryAccount.getId(), beneficiaryAccount.getBeneficiaryID(), beneficiaryAccount.getParentAccountHolderID(), beneficiaryAccount.getIfiID()));
  }

  public CompletionStage<Void> validate(BeneficiaryAccount beneficiaryAccount) {
    return baseSchemaIssuerService.validate(beneficiaryAccount, beneficiaryAccount.getIfiID(), EntityType.BENEFICIARY_ACCOUNT.name());
  }
}
