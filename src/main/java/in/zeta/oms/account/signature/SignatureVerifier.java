package in.zeta.oms.account.signature;

import in.zeta.commons.crypto.SignableDocument;
import in.zeta.commons.zms.api.InvalidSignatureException;
import in.zeta.oms.certstore.client.CertStoreClient;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

public class SignatureVerifier {

  private final CertStoreClient certStoreClient;

  @Inject
  public SignatureVerifier(CertStoreClient certStoreClient) {
    this.certStoreClient = certStoreClient;
  }

  public CompletionStage<Void> verifySignature(SignableDocument signableDocument) {
    return certStoreClient.verifySignature(signableDocument)
          .thenAccept(isVerified -> {
            if (!isVerified) {
              throw new InvalidSignatureException(String.format("Signature %s cannot be verified for %s",
                  signableDocument.getSignature(), signableDocument.getSignatoryJID()));
            }
          });
  }

}
