package in.zeta.oms.account.accountHolderMigration;

import static in.zeta.commons.concurrency.CompletableFutures.allResultsOf;
import static in.zeta.commons.concurrency.CompletableFutures.unwrapCompletionStateException;
import static java.util.concurrent.CompletableFuture.completedFuture;

import com.google.common.base.Function;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.inject.Inject;
import in.zeta.commons.cron.Cron;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.jmxControls.JMXAttrs;
import in.zeta.oms.user.athena.api.CreateAccountHolderRequest;
import in.zeta.oms.user.client.UserServiceClient;
import in.zeta.spectra.capture.SpectraLogger;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

import olympus.common.JID;
import olympus.spartan.RemoteServiceInstance;
import olympus.trace.OlympusSpectra;

@EagerSingleton
public class AccountHolderMigration {

  public static final List<Void> emptyList = Collections.emptyList();
  public static final String SUCCESS = "SUCCESS";
  public static final String ERROR = "ERROR";
  public static final String IGNORED = "IGNORED";
  private final SpectraLogger logger = OlympusSpectra.getLogger(this.getClass());
  private final ZetaHostMessagingService zetaHostMessagingService;
  private final Cron cron;
  private final IfiIssuanceMigrationDao ifiIssuanceMigrationDao;
  private final UserServiceClient userServiceClient;
  private final JMXAttrs jmxAttrs;
    private final ExecutorService executorService;

  @Inject
  public AccountHolderMigration(ZetaHostMessagingService zetaHostMessagingService,
                                Cron cron,
                                IfiIssuanceMigrationDao ifiIssuanceMigrationDao,
                                UserServiceClient userServiceClient,
                                JMXAttrs jmxAttrs) {
      this.zetaHostMessagingService = zetaHostMessagingService;
      this.ifiIssuanceMigrationDao = ifiIssuanceMigrationDao;
      this.cron = cron;
      this.userServiceClient = userServiceClient;
      this.jmxAttrs = jmxAttrs;
      this.executorService = Executors.newFixedThreadPool(10);
      scheduleCron();
  }

  private void scheduleCron() {
    logger.info("Account holder migration started").attr("startTime", LocalDateTime.now()).log();
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Asia/Kolkata")));
    calendar.set(Calendar.HOUR_OF_DAY, jmxAttrs.getRunAccountHolderMigrationCronAtHours());
    calendar.set(Calendar.MINUTE, jmxAttrs.getRunAccountHolderMigrationCronMinutes());
    calendar.set(Calendar.SECOND, 0);
    cron.scheduleDailyAtFixedTime(this::run, calendar.getTime());
  }

  public ListenableFuture<Object> run() {
    return this
        .zetaHostMessagingService
        .getSpartan()
        .onAllocation(zetaHostMessagingService.getInstanceJID(), new Function<RemoteServiceInstance, Object>() {
          @Nullable
          @Override
          public Object apply(@Nullable RemoteServiceInstance remoteServiceInstance) {
            return listUsersMigrate();
          }
        });
  }

  public CompletionStage<List<Void>> listUsersMigrate() {
      if (jmxAttrs.getRunAccountHolderMigrationCron() && jmxAttrs.getCronAccountHolderMigrationAllowedIfis().size() > 0) {
        Set<Long> ifiSet = jmxAttrs.getCronAccountHolderMigrationAllowedIfis();
        final CompletionStage[] future = new CompletionStage[] {completedFuture(emptyList)};
        ifiSet.forEach(ifiID -> {
          future[0] = future[0]
              .thenCompose(__ -> processIFI(ifiID)
                  .exceptionally(throwable -> {
                      String text = String.format("Error while migrating ifi %s", ifiID);
                    logger
                        .error(text, throwable)
                        .log();
                    return null;
                  }));
        });
        return future[0];
      }
      return completedFuture(Collections.emptyList());
  }

  private CompletionStage<List<Void>> processIFI(Long ifiID) {
    if (jmxAttrs.getRunAccountHolderMigrationCron() && jmxAttrs.isCronAccountHolderMigrationAllowedForIFI(ifiID)) {
      final CompletionStage<List<IfiIssuanceMigration>> userListForMigration = ifiIssuanceMigrationDao
          .getUsers(ifiID, "CREATED", 50);
      return userListForMigration
        .thenCompose(ifiIssuanceList -> allResultsOf(ifiIssuanceList
            .stream()
            .filter(ifiIssuance -> zetaHostMessagingService.isLocalNode(new JID(String.format("%s@account.zeta.in", ifiIssuance.getUserID()))))
            .map(this::processUser)
            .collect(Collectors.toList())))
          .exceptionally(t -> {
              String text = String.format("Error while migrating ifi issuance list %s", ifiID);
            logger
                .error(text, t)
                .attr("ifiID", ifiID)
                .log();
            return null;
          })
          .thenCompose(__ -> {
            if (userListForMigration.toCompletableFuture().join().size() > 0) {
              return processIFI(ifiID);
            }
            return completedFuture(emptyList);
          });
    }
    return completedFuture(emptyList);
  }

  private CompletionStage<Void> processUser(IfiIssuanceMigration ifiIssuance) {
    final IfiIssuanceMigration.IfiIssuanceMigrationBuilder builder = IfiIssuanceMigration
        .builder()
        .userID(ifiIssuance.getUserID())
        .ifi(ifiIssuance.getIfi());
    return userServiceClient
        .createAccountHolder(CreateAccountHolderRequest
            .builder()
            .ifiID(ifiIssuance.getIfi())
            .userID(ifiIssuance.getUserID()))
        .thenCompose(ignore -> {
          logger
              .info("ifi issuance migration done")
              .fill("userMigrated", builder)
              .log();
          return updateIfiIssuanceMigration(builder, SUCCESS,  "success");

        })
        .exceptionally(t -> {
          t = unwrapCompletionStateException(t);
          String text = String.format("Exception while migrating user to account holder ifi %s", ifiIssuance.toString());
          logger
              .error(text, t)
              .fill("userMigrated", builder.build())
              .log();
          String status = ERROR;
          if (t.getMessage().contains("invalid user type exception")) {
            status = IGNORED;
          }
          updateIfiIssuanceMigration(builder, status, t.toString());
          return null;
        });
  }

  private CompletionStage<Void> updateIfiIssuanceMigration(
      IfiIssuanceMigration.IfiIssuanceMigrationBuilder builder,
      String status, String reason) {
    builder
        .status(status)
        .reason(reason);
    return ifiIssuanceMigrationDao
        .updateIssuanceStatus(
            builder.build());
  }
}
