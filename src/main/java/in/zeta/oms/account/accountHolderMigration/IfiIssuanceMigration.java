package in.zeta.oms.account.accountHolderMigration;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
class IfiIssuanceMigration {
    private final long userID;
    private final long ifi;
    private final String status;
    private final String reason;
}
