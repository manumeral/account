package in.zeta.oms.account.accountHolderMigration;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import in.zeta.commons.postgres.PostgresDAO;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.springframework.jdbc.core.RowMapper;

@Singleton
public class IfiIssuanceMigrationDao extends PostgresDAO implements RowMapper<IfiIssuanceMigration> {
  private static final String TABLE = "ifi_issuance_migration";
  private static final String USER_ID = "userid";
  private static final String IFI = "ifi";
  private static final String STATUS = "status";
  private static final String REASON = "reason";

  private static final Map<String, String> DAO_ATTRS = ImmutableMap.of(
      "ifi_issuance_migration", TABLE,
      "userID", USER_ID,
      "ifi", IFI,
      "status", STATUS,
      "reason", REASON);

  private static final String updateQuery =
      StrSubstitutor.replace("update ${ifi_issuance_migration} set ${status} = ?, ${reason} = ? where ${userID} = ? and ${ifi} = ?", DAO_ATTRS);

  private static final String selectAllQuery =
      StrSubstitutor.replace("select ${userID}, ${ifi}, ${status} from ${ifi_issuance_migration} " +
          "where ${ifi} = ? and ${status} = ? limit ?", DAO_ATTRS);

  @Inject
  public IfiIssuanceMigrationDao(ZetaHostMessagingService hostMessagingService,
                                 BasicDataSource basicDataSource,
                                 @Named("postgres.jdbc.pool.size") int poolSize) {
    super(hostMessagingService, basicDataSource, poolSize);
  }

  public CompletionStage<List<IfiIssuanceMigration>> getUsers(long ifiID, String status, int limit) {
    return query(selectAllQuery, this, ifiID, status, limit);
  }



  public CompletionStage<Void> updateIssuanceStatus(IfiIssuanceMigration ifiIssuanceMigration) {
    return update(updateQuery, ifiIssuanceMigration.getStatus(),  ifiIssuanceMigration.getReason(), ifiIssuanceMigration.getUserID(), ifiIssuanceMigration.getIfi())
        .thenAccept(__ -> {});
  }

  @Override
  public IfiIssuanceMigration mapRow(ResultSet rs, int i) throws SQLException {
    return IfiIssuanceMigration
        .builder()
        .userID(rs.getLong(USER_ID))
        .ifi(rs.getLong(IFI))
        .status(rs.getString(STATUS))
        .build();
  }
}