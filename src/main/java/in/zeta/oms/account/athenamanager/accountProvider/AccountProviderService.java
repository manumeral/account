package in.zeta.oms.account.athenamanager.accountProvider;

import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.account.api.exception.ProvisioningPolicyException;
import in.zeta.oms.account.api.model.Account;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.json.JsonSchemaValidationService;
import in.zeta.oms.account.service.AthenaManagerService;
import in.zeta.oms.athenamanager.api.model.AccountProvider;

import in.zeta.spectra.capture.SpectraLogger;
import java.util.concurrent.CompletionStage;
import olympus.trace.OlympusSpectra;
import org.everit.json.schema.ValidationException;

@Singleton
public class AccountProviderService {

  private final AthenaManagerService athenaManagerService;
  private final JsonSchemaValidationService jsonSchemaValidationService;
  private static final SpectraLogger log = OlympusSpectra.getLogger(AccountProviderService.class);
  private final Gson gson;

  @Inject
  public AccountProviderService(
      AthenaManagerService athenaManagerService,
      JsonSchemaValidationService jsonSchemaValidationService,
      Gson gson) {
    this.athenaManagerService = athenaManagerService;
    this.jsonSchemaValidationService = jsonSchemaValidationService;
    this.gson = gson;
  }

  public CompletionStage<AccountProvider> get(String id, Long ifi) {
    return athenaManagerService.getAccountProvider(id, ifi);
  }

  public CompletionStage<Void> throwIfDoesntExist(String accountProviderID, Long ifi) {
    return get(accountProviderID, ifi)
        .thenApply(
            accountProvider -> {
              if (null != accountProvider) {
                return null;
              } else {
                throw new AccountServiceException(
                    "Account provider not found with id" + accountProviderID,
                    AccountErrorCode.ACCOUNT_PROVIDER_NOT_FOUND);
              }
            });
  }

  public Boolean isProductWhitelisted(AccountProvider accountProvider, Long productId) {
    return null == productId
        || null == accountProvider.getWhitelistedProducts()
        || accountProvider.getWhitelistedProducts().isEmpty()
        || accountProvider.getWhitelistedProducts().contains(productId);
  }

  public Boolean isProductFamilyWhitelisted(AccountProvider accountProvider, Long productFamilyId) {
    return null == productFamilyId
        || null == accountProvider.getWhitelistedProductFamilies()
        || accountProvider.getWhitelistedProductFamilies().isEmpty()
        || accountProvider.getWhitelistedProductFamilies().contains(productFamilyId);
  }

  public CompletionStage<Void> validate(Account account) {
    return get(account.getAccountProviderID(), account.getIfiID())
        .thenCompose(
            accountProvider ->
                jsonSchemaValidationService
                    .validate(account, accountProvider.getAccountProvisioningPolicies())
                    .exceptionally(
                        throwable -> {
                          throwable = unwrapCompletionStateException(throwable);
                          log.warn("ProvisioningPolicyFailed", throwable)
                              .fill("account", account)
                              .log();
                          final JsonObject jsonAttributes = new JsonObject();
                          if (throwable instanceof ValidationException) {
                            jsonAttributes.add(
                                "errorMessages",
                                gson.toJsonTree(
                                    ((ValidationException) throwable).getAllMessages()));
                          }
                          final ProvisioningPolicyException exception =
                              new ProvisioningPolicyException(
                                  throwable,
                                  "provisioning policy failed",
                                  AccountErrorCode.PROVISIONING_POLICY_FAILED);
                          exception.setJsonAttributes(jsonAttributes);
                          throw exception;
                        }));
  }
}
