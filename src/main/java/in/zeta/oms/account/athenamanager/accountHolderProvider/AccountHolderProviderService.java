package in.zeta.oms.account.athenamanager.accountHolderProvider;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.api.exception.ProvisioningPolicyException;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.constant.AccountErrorCode;
import in.zeta.oms.account.json.JsonSchemaValidationService;
import in.zeta.oms.account.service.AthenaManagerService;
import in.zeta.oms.athenamanager.api.model.AccountHolderProvider;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.trace.OlympusSpectra;
import org.everit.json.schema.ValidationException;

import java.util.concurrent.CompletionStage;

import static olympus.message.util.CompletableFutures.unwrapCompletionStateException;

/**
 * This Service for now seems redundant but it is being left to behind to
 * TODO: Move validation logics around accountProvider here
 */
@Singleton
public class AccountHolderProviderService {
  private final AthenaManagerService athenaManagerService;
  private final JsonSchemaValidationService jsonSchemaValidationService;
  private static final SpectraLogger log = OlympusSpectra.getLogger(AccountHolderProviderService.class);
  private final Gson gson;

  @Inject
  public AccountHolderProviderService(
      AthenaManagerService athenaManagerService,
      JsonSchemaValidationService jsonSchemaValidationService,
      Gson gson) {
    this.athenaManagerService = athenaManagerService;
    this.jsonSchemaValidationService = jsonSchemaValidationService;
    this.gson = gson;
  }

  public CompletionStage<AccountHolderProvider> get(String id, Long ifi) {
    return athenaManagerService.getAccountHolderProvider(id, ifi);
  }

  public CompletionStage<Void> validate(AccountHolder accountHolder) {
    return get(accountHolder.getAccountHolderProviderID(), accountHolder.getIfiID())
        .thenCompose(accountProvider -> jsonSchemaValidationService
            .validate(accountHolder, accountProvider.getAccountHolderProvisioningPolicies())
            .exceptionally(throwable -> {
              throwable = unwrapCompletionStateException(throwable);
              log.warn("ProvisioningPolicyFailed", throwable)
                  .fill("accountHolder", accountHolder )
                  .log();
              final JsonObject jsonAttributes = new JsonObject();
              if (throwable instanceof ValidationException) {
                jsonAttributes.add("errorMessages", gson.toJsonTree(((ValidationException) throwable).getAllMessages()));
              }
              final ProvisioningPolicyException exception = new ProvisioningPolicyException(
                  throwable, "provisioning policy failed",
                  AccountErrorCode.PROVISIONING_POLICY_FAILED);
              exception.setJsonAttributes(jsonAttributes);
              throw exception;
            }));

  }
}
