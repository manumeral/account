package in.zeta.oms.account.transaction;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.AccountServiceBaseRequestHandler;
import olympus.message.types.Request;

@EagerSingleton
public class AccountTransactionHandler extends AccountServiceBaseRequestHandler {
  private final AccountTransactionService accountTransactionService;

  @Inject
  public AccountTransactionHandler(
      ZetaHostMessagingService zhms,
      AccountTransactionService accountTransactionService) {
    super(zhms);
    this.accountTransactionService = accountTransactionService;
  }


  @Authorized(anyOf = {"api.account.transaction.get", "api.account.admin"})
  public void on(GetAccountTransactionRequest payload, Request<GetAccountTransactionRequest> request) {
    onRequest(payload, request);
    accountTransactionService
        .getAccountTransaction(payload.getIfiID(), payload.getAccountID(), payload.getTransactionID())
        .thenAccept(accountTransaction -> onResult(accountTransaction, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.transaction.get", "api.account.admin"})
  public void on(GetAccountTransactionListRequest payload, Request<GetAccountTransactionListRequest> request) {
    onRequest(payload, request);
    accountTransactionService
        .getAccountTransactionList(payload)
        .thenAccept(accountTransactionListResponse -> onResult(accountTransactionListResponse, request))
        .exceptionally(t -> onError(request, t));
  }
}
