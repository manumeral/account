package in.zeta.oms.account.transaction;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.oms.account.account.AccountService;
import in.zeta.oms.account.service.LedgerService;
import in.zeta.oms.ledger.api.Order;
import in.zeta.oms.ledger.api.response.TransactionV2.Posting;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

@Singleton
public class AccountTransactionService {
  private final LedgerService ledgerService;
  private final AccountService accountService;

  @Inject
  public AccountTransactionService(LedgerService ledgerService,
      AccountService accountService) {
    this.ledgerService = ledgerService;
    this.accountService = accountService;
  }

  public CompletionStage<AccountTransaction> getAccountTransaction(
      Long ifiID,
      String accountID,
      String transactionID) {
    return getTransactions(
        ifiID,
        accountID,
        transactionID,
        null,
        null,
        1,
        1L,
        null,
        Order.LATEST_FIRST)
        .thenApply(getAccountTransactionListResponse -> {
          if (getAccountTransactionListResponse
              .getAccountTransactionList().size() == 1) {
            return getAccountTransactionListResponse.getAccountTransactionList().get(0);
          } else {
            throw  new AccountTransactionNotFoundException(
                String.format("Account Transaction not found for id: %s", transactionID));
          }
        });
  }

  public CompletionStage<GetAccountTransactionListResponse> getAccountTransactionList(
      GetAccountTransactionListRequest payload) {
    return getTransactions(
            payload.getIfiID(),
            payload.getAccountID(),
            null,
            payload.getEndingBefore(),
            payload.getStartingAfter(),
            payload.getPageSize(),
            payload.getPageNumber(),
            payload.getRecordType(),
            Order.LATEST_FIRST);
  }

  private CompletionStage<GetAccountTransactionListResponse> getTransactions(
      Long ifiID,
      String accountID,
      String transactionID,
      Long before,
      Long after,
      Integer pageSize,
      Long pageNumber,
      String recordType,
      Order order) {
    return accountService
        .getAccount(ifiID, accountID)
        .thenCompose(account -> ledgerService
            .getTransactions(
                account.getLedgerID(),
                transactionID,
                before,
                after,
                pageSize,
                pageNumber,
                recordType,
                order))
        .thenApply(getTransactionsResponseV2 -> {
          final List<AccountTransaction> collect = getTransactionsResponseV2
              .getTransactions()
              .stream()
              .map(transactionV2 -> {
                final Posting posting = transactionV2.getPostings().get(0);
                return AccountTransaction
                    .builder()
                    .accountID(accountID)
                    .ledgerID(posting.getLedgerID())
                    .transactionID(transactionV2.getTransactionID())
                    .reversedTransactionID(posting.getReversedTransactionID())
                    .reversalTransactionIDs(posting.getReversalTransactionIDs())
                    .previousBalance(posting.getPreviousBalance())
                    .newBalance(posting.getNewBalance())
                    .timestamp(posting.getTimestamp())
                    .amount(posting.getAmount())
                    .currency(posting.getCurrency())
                    .recordType(posting.getLedgerRecordType().name())
                    .remarks(posting.getRemarks())
                    .attributes(posting.getPostingAttributes())
                    .build();
              })
              .collect(Collectors.toList());
          return GetAccountTransactionListResponse
              .builder()
              .accountTransactionList(collect)
              .totalRecord(getTransactionsResponseV2.getTotalRecords())
              .build();
        });
  }
}
