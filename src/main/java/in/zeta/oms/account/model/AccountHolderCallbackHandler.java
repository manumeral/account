package in.zeta.oms.account.model;

import com.google.gson.Gson;
import in.zeta.commons.gson.ZetaGsonBuilder;
import in.zeta.commons.postgres.PostgresRowCallbackHandler;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVectorDAO;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.model.AccountHolder.Builder;
import in.zeta.oms.account.accountHolder.vector.AccountHolderVector;
import in.zeta.oms.account.dao.AccountHolderDAO;
import in.zeta.oms.athenamanager.api.enums.AccountHolderType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static in.zeta.athenacommons.constant.TypeConstant.TYPE_MAP_STRING_TO_STRING;

public class AccountHolderCallbackHandler
    extends PostgresRowCallbackHandler<List<AccountHolder>> {
  private final Gson gson = new ZetaGsonBuilder().build();
  private final Map<String, Builder> accountHolderMap = new HashMap<>();

  private AccountHolderCallbackHandler(List<AccountHolder> accountHolder) {
    super(accountHolder);
  }

  @Override
  public void processRow(ResultSet rs, int rowNum) throws SQLException {
    if (!accountHolderMap.containsKey(rs.getString(AccountHolderDAO.COL_ID_ALIAS))) {
      AccountHolder.Builder accountHolderBuilder = AccountHolder
          .builder()
          .requestID(rs.getString(AccountHolderDAO.REQUEST_ID))
          .id(rs.getString(AccountHolderDAO.COL_ID_ALIAS))
          .ifiID(rs.getLong(AccountHolderDAO.COL_IFI_ID_ALIAS))
          .type(AccountHolderType.valueOf(rs.getString(AccountHolderDAO.COL_TYPE_ALIAS)))
          .status(rs.getString(AccountHolderDAO.COL_STATUS_ALIAS))
          .accountHolderProviderID(rs.getString(AccountHolderDAO.ACCOUNT_HOLDER_PROVIDER_ID))
          .attributes(
              gson.fromJson(rs.getString(AccountHolderDAO.COL_ATTRIBUTES_ALIAS), TYPE_MAP_STRING_TO_STRING))
          .salutation(rs.getString(AccountHolderDAO.SALUTATION))
          .firstName(rs.getString(AccountHolderDAO.FIRST_NAME))
          .middleName(rs.getString(AccountHolderDAO.MIDDLE_NAME))
          .lastName(rs.getString(AccountHolderDAO.LAST_NAME))
          .profilePicURL(rs.getString(AccountHolderDAO.PROFILE_PIC_URL))
          .dob(rs.getDate(AccountHolderDAO.DOB))
          .gender(rs.getString(AccountHolderDAO.GENDER))
          .mothersMaidenName(rs.getString(AccountHolderDAO.MOTHERS_MAIDEN_NAME));
      accountHolderMap.put(rs.getString(AccountHolderDAO.COL_ID_ALIAS), accountHolderBuilder);
    }

    if (rs.getString(AccountHolderVectorDAO.COL_VALUE_ALIAS) != null) {
      accountHolderMap
          .get(rs.getString(AccountHolderDAO.COL_ID_ALIAS))
          .vector(AccountHolderVector.builder()
              .id(rs.getString(AccountHolderVectorDAO.COL_ID_ALIAS))
              .accountHolderID(rs.getString(AccountHolderDAO.COL_ID_ALIAS))
              .type(rs.getString(AccountHolderVectorDAO.COL_TYPE_ALIAS))
              .value(rs.getString(AccountHolderVectorDAO.COL_VALUE_ALIAS))
              .build());
    }
  }

  @Override
  public List<AccountHolder> get() {
    List<AccountHolder> accountHolderList = super.get();
    accountHolderMap
        .values()
        .stream()
        .map(Builder::build)
        .collect(Collectors.toCollection(() -> accountHolderList));
    return accountHolderList;
  }

  public static AccountHolderCallbackHandler getPostgresCallbackHandler() {
    return new AccountHolderCallbackHandler(new ArrayList<>());
  }
}
