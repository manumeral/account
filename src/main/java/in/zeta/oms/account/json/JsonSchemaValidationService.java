package in.zeta.oms.account.json;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import in.zeta.commons.json.JsonSchemaValidatorAsync;
import in.zeta.spectra.capture.SpectraLogger;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import olympus.trace.OlympusSpectra;

@Singleton
public class JsonSchemaValidationService {
  private final JsonSchemaValidatorAsync jsonSchemaValidatorAsync;
  private final Gson gson;

  private static final SpectraLogger log = OlympusSpectra.getLogger(JsonSchemaValidationService.class);

  @Inject
  public JsonSchemaValidationService(
      JsonSchemaValidatorAsync jsonSchemaValidatorAsync,
      Gson gson) {
    this.jsonSchemaValidatorAsync = jsonSchemaValidatorAsync;
    this.gson = gson;
  }

  public CompletionStage<Void> validate(Object data, JsonObject schema) {
    //TODO: Fix this null check in zeta-commons validate function
    if(null != schema) {
      return jsonSchemaValidatorAsync
          .validate(schema, gson.toJsonTree(data).getAsJsonObject());
    } else {
      return CompletableFuture.completedFuture(null);
    }
  }
}
