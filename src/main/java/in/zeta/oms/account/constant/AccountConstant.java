package in.zeta.oms.account.constant;

public class AccountConstant {

  private AccountConstant() {
    // To prevent initialization
  }


  //athena manager service constants
  public static final String ACCOUNT_PRODUCT_FAMILY_ID = "account.pf-id";
  public static final String ACCOUNT_PRODUCT_ID = "account.pd-id";
  public static final String ACCOUNT_PROGRAM_ID = "account.pg-id";
  public static final String ATHENA_ACCOUNT_PROVIDER_ID = "athena.account-provider-id";

  //account service constants
  public static final String ACCOUNT_EXTERNAL_ID = "account.external-id";
  public static final String ACCOUNT_ACCOUNT_HOLDER_ID = "account.account-holder-id";

  //constants required for ledger creation
  public static final String ZETA_USER_ID = "zeta.user-id";
  public static final String ZETA_BUSINESS_ID = "zeta.business-id";
  public static final String ZETA_IFI_PRODUCT_TYPE = "zeta.ifi-product-type";
  public static final String ZETA_PRODUCT_TYPE = "zeta.product-type";
  public static final String ZETA_BUSINESS_NAME = "zeta.business-name";
  public static final String ZETA_CARD_PROGRAM_ID = "zeta.card-program-id";
  public static final String ZETA_CARD_PROGRAM_IDS = "zeta.card-program-ids";
  public static final String ZETA_CARD_PROGRAM_NAME = "zeta.card-program-name";

  // constants required for close account voucher code
  public static final String ACCOUNT_CLOSE_VOUCHER_CODE = "A2A_FundExpiry-User-CardClosure_AUTH";
}
