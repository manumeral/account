package in.zeta.oms.account.util;
import static in.zeta.oms.account.constant.AccountErrorCode.DB_ERROR;

import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.spectra.capture.MarkerType;
import in.zeta.spectra.capture.SpectraLogger;
import in.zeta.spectra.capture.SpectraLogger.LogLevel;
import java.util.UUID;
import lombok.Builder;

@Builder(builderClassName = "Builder")
public class LoggerUtils {

  private final SpectraLogger logger;

  public AccountServiceException logDBException(String errorMessage, Throwable t) {
    final String id = UUID.randomUUID().toString();
    logger.createMarker(MarkerType.OpEnd, id, errorMessage, LogLevel.ERROR)
        .log();
    return new AccountServiceException(errorMessage, t, DB_ERROR);
  }
}
