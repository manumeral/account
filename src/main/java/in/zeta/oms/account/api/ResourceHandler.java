package in.zeta.oms.account.api;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.request.GetResourceListRequest;
import in.zeta.oms.account.api.response.ResourceListResponse;
import in.zeta.oms.account.service.WalletService;
import in.zeta.oms.wallet.api.model.ResourceData;
import olympus.message.types.Request;

import java.util.List;

@EagerSingleton
public class ResourceHandler extends AccountServiceBaseRequestHandler {

  private final WalletService walletService;

  @Inject
  public ResourceHandler(ZetaHostMessagingService zetaHostMessagingService,
                         WalletService walletService) {
    super(zetaHostMessagingService);
    this.walletService = walletService;
  }

  @Authorized(anyOf = {"api.account.resourceList.get"})
  public void on(GetResourceListRequest payload, Request<GetResourceListRequest> request) {
    onRequest(payload, request);
    walletService.getResourceList(payload.getIfiID(), payload.getPageSize(), payload.getPageIndex())
        .thenAccept(resourceListResponse -> onResult(getResourceListResponse(resourceListResponse.getResources()), request))
        .exceptionally(t -> onError(request, t));
  }

  private ResourceListResponse getResourceListResponse(List<ResourceData> resourceList) {
    return new ResourceListResponse(resourceList);
  }
}
