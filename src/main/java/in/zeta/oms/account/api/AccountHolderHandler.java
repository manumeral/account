package in.zeta.oms.account.api;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.accountHolderMigration.AccountHolderMigration;
import in.zeta.oms.account.api.enums.ExternalIDType;
import in.zeta.oms.account.api.model.AccountHolder;
import in.zeta.oms.account.api.request.CreateAccountHolderRequest;
import in.zeta.oms.account.api.request.GetAccountHolderByVectorRequest;
import in.zeta.oms.account.api.request.GetAccountHolderRequest;
import in.zeta.oms.account.api.request.GetAllAccountHoldersRequest;
import in.zeta.oms.account.account.UpdateAccountHolderRequest;
import in.zeta.oms.account.api.request.TriggerAccountHolderCronRequest;
import in.zeta.oms.account.api.response.GetAccountListResponse;
import in.zeta.oms.account.api.response.GetAllAccountHoldersResponse;
import in.zeta.oms.account.service.AccountHolderService;
import in.zeta.oms.athenamanager.api.enums.AccountHolderType;
import in.zeta.oms.user.athena.api.GetAccountHoldersRequestPayload;
import in.zeta.oms.user.athena.api.GetAccountHoldersResponsePayload;
import in.zeta.oms.user.client.UserServiceClient;
import olympus.message.types.EmptyPayload;
import olympus.message.types.Request;

import java.util.concurrent.CompletionStage;

import static in.zeta.oms.account.constant.PrivilegeConstants.ADMIN_ACTION_ACCOUNT_HOLDER;
import static java.util.concurrent.CompletableFuture.completedFuture;

@EagerSingleton
public class AccountHolderHandler extends AccountServiceBaseRequestHandler {

  private final AccountHolderService accountHolderService;
  private final UserServiceClient userServiceClient;
  private final AccountHolderMigration accountHolderMigration;

  @Inject
  public AccountHolderHandler(
      ZetaHostMessagingService zetaHostMessagingService,
      AccountHolderService accountHolderService,
      UserServiceClient userServiceClient,
      AccountHolderMigration accountHolderMigration){
    super(zetaHostMessagingService);
    this.accountHolderService = accountHolderService;
    this.userServiceClient = userServiceClient;
    this.accountHolderMigration = accountHolderMigration;
  }

  @Authorized(anyOf = {"api.account.accountHolder.add", ADMIN_ACTION_ACCOUNT_HOLDER})
  public void on(CreateAccountHolderRequest payload, Request<CreateAccountHolderRequest> request) {
    onRequest(payload, request);

    accountHolderService
        .create(payload)
        .thenAccept(accountHolder -> onResult(accountHolder, request))
        .exceptionally(t ->
            onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.get", ADMIN_ACTION_ACCOUNT_HOLDER})
  public void on(GetAccountHolderRequest payload, Request<GetAccountHolderRequest> request) {
    onRequest(payload, request);

    getAccountHolder(payload.getId(), payload.getExternalIDType(), payload.getExternalID(),
                     payload.getIfiID())
            .thenAccept(accountHolder -> onResult(accountHolder, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.get", ADMIN_ACTION_ACCOUNT_HOLDER})
  public void on(GetAccountHolderByVectorRequest payload, Request<GetAccountHolderByVectorRequest> request) {
    onRequest(payload, request);

    accountHolderService.getByVector(payload.getIfiID(), payload.getVectorType(), payload.getVectorValue())
        .thenAccept(accountHolder -> onResult(accountHolder, request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.accountHolder.getAll", ADMIN_ACTION_ACCOUNT_HOLDER})
  public void on(GetAllAccountHoldersRequest payload, Request<GetAllAccountHoldersRequest> request) {
    onRequest(payload, request);

    completedFuture(payload)
        .thenApply(AccountHolderHandler::getAccountHoldersBuilder)
        .thenCompose(userServiceClient::getAccountHolders)
        .thenApply(AccountHolderHandler::getAccountHoldersResponse)
        .thenAccept(response -> onResult(response, request))
        .exceptionally(throwable -> onError(request, throwable));
  }

  @Authorized(anyOf = {"api.account.accountHolder.update", "api.account.accountHolder.admin"})
  public void on(UpdateAccountHolderRequest payload, Request<UpdateAccountHolderRequest> request) {
      onRequest(payload, request);
    accountHolderService
        .updateAccountHolder(AccountHolder.builder()
            .id(payload.getId())
            .ifiID(payload.getIfiID())
            .status(payload.getStatus())
            .attributes(payload.getAttributes())
            .salutation(payload.getSalutation())
            .firstName(payload.getFirstName())
            .middleName(payload.getMiddleName())
            .lastName(payload.getLastName())
            .profilePicURL(payload.getProfilePicURL())
            .dob(payload.getDob())
            .gender(payload.getGender())
            .mothersMaidenName(payload.getMothersMaidenName())
            .build())
        .thenAccept(accountHolder -> onResult(accountHolder, request))
        .exceptionally(t -> onError(request, t));
  }

  private static GetAccountHoldersRequestPayload.Builder getAccountHoldersBuilder(GetAllAccountHoldersRequest payload) {
    return new GetAccountHoldersRequestPayload.Builder()
    .accountHolderType(getUserServiceAccountHolderType(payload.getAccountHolderType()))
    .ifiId(payload.getIfiId())
    .pageNumber(payload.getPageNumber())
    .pageSize(payload.getPageSize());
  }

  private static in.zeta.oms.user.athena.models.AccountHolderType getUserServiceAccountHolderType(
      AccountHolderType accountHolderType) {
    if (accountHolderType == AccountHolderType.REAL) {
      return in.zeta.oms.user.athena.models.AccountHolderType.REAL;
    }

    if (accountHolderType == AccountHolderType.HEADLESS) {
      return in.zeta.oms.user.athena.models.AccountHolderType.HEADLESS;
    }

    return in.zeta.oms.user.athena.models.AccountHolderType.ALL;

  }

  private static GetAllAccountHoldersResponse getAccountHoldersResponse(
      GetAccountHoldersResponsePayload payload) {
    // TODO: GetAllAccountHoldersResponse uses AccountHolder from user-client, create a new POJO for it.
    return new GetAllAccountHoldersResponse(payload.getAccountHolder());
  }

  private CompletionStage<AccountHolder> getAccountHolder(String id,
                                                          ExternalIDType externalIDType,
                                                          String externalID,
                                                          Long ifiID) {
    if (!Strings.isNullOrEmpty(externalID)) {
      return accountHolderService
          .getByVector(ifiID, externalIDType, externalID);
    }
    return accountHolderService.get(ifiID, id);
  }

  public void on(TriggerAccountHolderCronRequest payload, Request<TriggerAccountHolderCronRequest> request) {
    accountHolderMigration.run();
    sendResponse(request, new EmptyPayload());
  }
}
