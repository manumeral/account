package in.zeta.oms.account.api;

import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.payload.HeliosPing;
import in.zeta.commons.payload.HeliosPong;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.commons.zms.service.ZetaRequestHandler;
import olympus.message.types.Request;

import javax.inject.Inject;

import static in.zeta.commons.payload.HealthStatus.GREEN;

@EagerSingleton
public class HeliosRequestHandler extends ZetaRequestHandler {

  @Inject
  public HeliosRequestHandler(ZetaHostMessagingService zetaHostMessagingService) {
    super(zetaHostMessagingService);
  }

  public void on(HeliosPing ping, Request<HeliosPing> request) {
    sendResponse(request, new HeliosPong(GREEN));
  }
}
