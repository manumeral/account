package in.zeta.oms.account.api;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.request.AssignTagsRequestPayload;
import in.zeta.oms.account.api.request.DeleteTagsRequestPayload;
import in.zeta.oms.account.api.request.GetTagsRequestPayload;
import in.zeta.oms.account.api.response.GetTagsResponse;
import in.zeta.tags.TagService;
import olympus.message.types.EmptyPayload;
import olympus.message.types.Request;

@EagerSingleton
public class TagsHandler extends AccountServiceBaseRequestHandler {

  private final TagService tagService;

  @Inject
  public TagsHandler(ZetaHostMessagingService zetaHostMessagingService,
      TagService tagService) {
    super(zetaHostMessagingService);
    this.tagService = tagService;
  }

  @Authorized(anyOf = {"api.account.tags.add"})
  public void on(AssignTagsRequestPayload payload, Request<AssignTagsRequestPayload> request) {
    onRequest(payload, request);
    tagService.verifyAndAssignTags(payload.getIfiId(), payload.getObjectType(), payload.getObjectId(),
        payload.getTags())
        .thenAccept(__ -> onResult(new EmptyPayload(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.tags.get"})
  public void on(GetTagsRequestPayload payload, Request<GetTagsRequestPayload> request) {
    onRequest(payload, request);
    tagService.verifyAndGetTags(payload.getIfiId(), payload.getObjectType(), payload.getObjectId())
        .thenAccept(tags -> onResult(GetTagsResponse.builder().tags(tags).build(), request))
        .exceptionally(t -> onError(request, t));
  }

  @Authorized(anyOf = {"api.account.tags.delete"})
  public void on(DeleteTagsRequestPayload payload, Request<DeleteTagsRequestPayload> request) {
    onRequest(payload, request);
    tagService.verifyAndDeleteTags(payload.getIfiId(), payload.getObjectType(), payload.getObjectId(),
        payload.getTags())
        .thenAccept(__ -> onResult(new EmptyPayload(), request))
        .exceptionally(t -> onError(request, t));
  }

}
