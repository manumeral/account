package in.zeta.oms.account.api;

import in.zeta.commons.concurrency.CompletableFutures;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.commons.zms.service.ZetaRequestHandler;
import in.zeta.oms.account.api.exception.AccountServiceException;
import in.zeta.oms.athenamanager.api.exception.AthenaManagerException;
import in.zeta.oms.journal.api.exception.PaymentDeclinedAndRevertedException;
import in.zeta.spectra.capture.SpectraLogger;
import olympus.message.types.Payload;
import olympus.message.types.Request;
import olympus.trace.OlympusSpectra;

public abstract class AccountServiceBaseRequestHandler extends ZetaRequestHandler {

  private final SpectraLogger log = OlympusSpectra.getLogger(this.getClass());

  public AccountServiceBaseRequestHandler(ZetaHostMessagingService zetaHostMessagingService) {
    super(zetaHostMessagingService);
  }

  protected void onRequest(Payload payload, Request envelop) {
    log.debug("Request received")
        .attr("requestClass", payload.getClass().getSimpleName())
        .attr("requestID", envelop.oid())
        .attr("from", envelop.from())
        .attr("to", envelop.to())
        .attr("sender", envelop.sender())
        .log();
  }

  protected void onResult(Payload payload, Request envelop) {
    log.debug("Sending response")
        .attr("responseClass", payload.getClass().getSimpleName())
        .attr("requestID", envelop.oid())
        .log();
    sendResponse(envelop, payload);
  }

  protected Void onError(Request envelop, Throwable t) {
    t = CompletableFutures.unwrapCompletionStateException(t);
    //logging as debug, consumers are responsible for logging it at appropriate level
    log.debug("Sending error")
        .attr("requestClass", envelop.getClass().getSimpleName())
        .attr("requestID", envelop.oid())
        .fill("error", t)
        .log();
    if (!(t instanceof AccountServiceException) && !(t instanceof AthenaManagerException) && !(t instanceof PaymentDeclinedAndRevertedException)) {
      t = new RuntimeException("Internal Error", t);
    }
    return sendError(envelop, t);
  }

}
