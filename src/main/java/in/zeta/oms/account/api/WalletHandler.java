package in.zeta.oms.account.api;

import com.google.inject.Inject;
import in.zeta.commons.annotations.Authorized;
import in.zeta.commons.eagersingleton.EagerSingleton;
import in.zeta.commons.zms.service.ZetaHostMessagingService;
import in.zeta.oms.account.api.request.GetWalletListRequest;
import in.zeta.oms.account.api.response.WalletListResponse;
import in.zeta.oms.account.service.WalletService;
import in.zeta.oms.wallet.api.model.WalletData;
import olympus.message.types.Request;

import java.util.List;

@EagerSingleton
public class WalletHandler extends AccountServiceBaseRequestHandler {

  private final WalletService walletService;

  @Inject
  public WalletHandler(ZetaHostMessagingService zetaHostMessagingService,
                       WalletService walletService) {
    super(zetaHostMessagingService);
    this.walletService = walletService;
  }

  @Authorized(anyOf = {"api.account.walletList.get"})
  public void on(GetWalletListRequest payload, Request<GetWalletListRequest> request) {
    onRequest(payload, request);

    walletService.getWalletList(payload.getIfiID(), payload.getPageSize(), payload.getPageIndex())
        .thenAccept(walletListResponse -> onResult(getWalletListResponse(walletListResponse.getWallets()), request))
        .exceptionally(t -> onError(request, t));
  }

  private WalletListResponse getWalletListResponse(List<WalletData> walletDataList) {
    return new WalletListResponse(walletDataList);
  }
}
