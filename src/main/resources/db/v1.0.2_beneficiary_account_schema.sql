insert into base_schemas values('1ad48202-d590-4bef-a1ad-5739961a1da1', -1, 'BENEFICIARY_ACCOUNT', '{
                                                                                                      "definitions": {},
                                                                                                      "$schema": "http://json-schema.org/draft-07/schema#",
                                                                                                      "$id": "http://example.com/root.json",
                                                                                                      "type": "object",
                                                                                                      "title": "The Root Schema",
                                                                                                      "required": [
                                                                                                      ],
                                                                                                      "properties": {
                                                                                                        "ifiID": {
                                                                                                          "$id": "#/properties/ifiID",
                                                                                                          "type": "integer",
                                                                                                          "title": "The Ifiid Schema",
                                                                                                          "default": 0,
                                                                                                          "examples": [
                                                                                                            140827
                                                                                                          ]
                                                                                                        },
                                                                                                        "isVerified": {
                                                                                                          "$id": "#/properties/isVerified",
                                                                                                          "type": "boolean",
                                                                                                          "title": "The Isverified Schema",
                                                                                                          "default": false,
                                                                                                          "examples": [
                                                                                                            true
                                                                                                          ]
                                                                                                        },
                                                                                                        "status": {
                                                                                                          "$id": "#/properties/status",
                                                                                                          "type": "string",
                                                                                                          "title": "The Status Schema",
                                                                                                          "default": "",
                                                                                                          "examples": [
                                                                                                            "ENABLED"
                                                                                                          ],
                                                                                                          "pattern": "^(.*)$"
                                                                                                        },
                                                                                                        "beneficiaryID": {
                                                                                                          "$id": "#/properties/beneficiaryID",
                                                                                                          "type": "string",
                                                                                                          "title": "The Beneficiaryid Schema",
                                                                                                          "default": "",
                                                                                                          "examples": [
                                                                                                            "4401578d-8972-48cc-9ae9-e3efd900497e"
                                                                                                          ],
                                                                                                          "pattern": "^(.*)$"
                                                                                                        },
                                                                                                        "parentAccountHolderID": {
                                                                                                          "$id": "#/properties/parentAccountHolderID",
                                                                                                          "type": "string",
                                                                                                          "title": "The Parentaccountholderid Schema",
                                                                                                          "default": "",
                                                                                                          "examples": [
                                                                                                            "c1d39b17-ee6f-4392-83fe-d4ac156fb662"
                                                                                                          ],
                                                                                                          "pattern": "^(.*)$"
                                                                                                        },
                                                                                                        "nickname": {
                                                                                                          "$id": "#/properties/nickname",
                                                                                                          "type": "string",
                                                                                                          "title": "The Nickname Schema",
                                                                                                          "default": "",
                                                                                                          "examples": [
                                                                                                            "New Beneficiary Account"
                                                                                                          ],
                                                                                                          "pattern": "^(.*)$"
                                                                                                        },
                                                                                                        "type": {
                                                                                                          "$id": "#/properties/type",
                                                                                                          "type": "string",
                                                                                                          "title": "The Type Schema",
                                                                                                          "default": "",
                                                                                                          "examples": [
                                                                                                            "ZETA"
                                                                                                          ],
                                                                                                          "pattern": "^(.*)$"
                                                                                                        },
                                                                                                        "accountInfo": {
                                                                                                          "$id": "#/properties/accountInfo",
                                                                                                          "type": "object",
                                                                                                          "title": "The Accountinfo Schema",
                                                                                                          "required": [
                                                                                                            "accountNumber",
                                                                                                            "accountProvider",
                                                                                                            "nameAtAccountProvider",
                                                                                                            "routingCode",
                                                                                                            "accountType",
                                                                                                            "attributes"
                                                                                                          ],
                                                                                                          "properties": {
                                                                                                            "accountNumber": {
                                                                                                              "$id": "#/properties/accountInfo/properties/accountNumber",
                                                                                                              "type": "string",
                                                                                                              "title": "The Accountnumber Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "ba8eb92f-4a44-4959-9d07-aa83dad89bac"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            },
                                                                                                            "accountProvider": {
                                                                                                              "$id": "#/properties/accountInfo/properties/accountProvider",
                                                                                                              "type": "string",
                                                                                                              "title": "The Accountprovider Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "ICICI787365e35634"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            },
                                                                                                            "nameAtAccountProvider": {
                                                                                                              "$id": "#/properties/accountInfo/properties/nameAtAccountProvider",
                                                                                                              "type": "string",
                                                                                                              "title": "The Nameataccountprovider Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "MOYONKO"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            },
                                                                                                            "routingCode": {
                                                                                                              "$id": "#/properties/accountInfo/properties/routingCode",
                                                                                                              "type": "string",
                                                                                                              "title": "The Routingcode Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "IMPS"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            },
                                                                                                            "accountType": {
                                                                                                              "$id": "#/properties/accountInfo/properties/accountType",
                                                                                                              "type": "string",
                                                                                                              "title": "The Accounttype Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "SAVINGS"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            },
                                                                                                            "attributes": {
                                                                                                              "$id": "#/properties/accountInfo/properties/attributes",
                                                                                                              "type": "object",
                                                                                                              "title": "The Attributes Schema",
                                                                                                              "required": [
                                                                                                                "additionalProp1",
                                                                                                                "additionalProp2",
                                                                                                                "additionalProp3"
                                                                                                              ],
                                                                                                              "properties": {
                                                                                                                "additionalProp1": {
                                                                                                                  "$id": "#/properties/accountInfo/properties/attributes/properties/additionalProp1",
                                                                                                                  "type": "string",
                                                                                                                  "title": "The Additionalprop1 Schema",
                                                                                                                  "default": "",
                                                                                                                  "examples": [
                                                                                                                    "INDIRANAGAR"
                                                                                                                  ],
                                                                                                                  "pattern": "^(.*)$"
                                                                                                                },
                                                                                                                "additionalProp2": {
                                                                                                                  "$id": "#/properties/accountInfo/properties/attributes/properties/additionalProp2",
                                                                                                                  "type": "string",
                                                                                                                  "title": "The Additionalprop2 Schema",
                                                                                                                  "default": "",
                                                                                                                  "examples": [
                                                                                                                    "string"
                                                                                                                  ],
                                                                                                                  "pattern": "^(.*)$"
                                                                                                                },
                                                                                                                "additionalProp3": {
                                                                                                                  "$id": "#/properties/accountInfo/properties/attributes/properties/additionalProp3",
                                                                                                                  "type": "string",
                                                                                                                  "title": "The Additionalprop3 Schema",
                                                                                                                  "default": "",
                                                                                                                  "examples": [
                                                                                                                    "string"
                                                                                                                  ],
                                                                                                                  "pattern": "^(.*)$"
                                                                                                                }
                                                                                                              }
                                                                                                            }
                                                                                                          }
                                                                                                        },
                                                                                                        "isDefault": {
                                                                                                          "$id": "#/properties/isDefault",
                                                                                                          "type": "boolean",
                                                                                                          "title": "The Isdefault Schema",
                                                                                                          "default": false,
                                                                                                          "examples": [
                                                                                                            true
                                                                                                          ]
                                                                                                        },
                                                                                                        "attributes": {
                                                                                                          "$id": "#/properties/attributes",
                                                                                                          "type": "object",
                                                                                                          "title": "The Attributes Schema",
                                                                                                          "required": [
                                                                                                            "additionalProp1",
                                                                                                            "additionalProp2",
                                                                                                            "additionalProp3"
                                                                                                          ],
                                                                                                          "properties": {
                                                                                                            "additionalProp1": {
                                                                                                              "$id": "#/properties/attributes/properties/additionalProp1",
                                                                                                              "type": "string",
                                                                                                              "title": "The Additionalprop1 Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "string"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            },
                                                                                                            "additionalProp2": {
                                                                                                              "$id": "#/properties/attributes/properties/additionalProp2",
                                                                                                              "type": "string",
                                                                                                              "title": "The Additionalprop2 Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "string"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            },
                                                                                                            "additionalProp3": {
                                                                                                              "$id": "#/properties/attributes/properties/additionalProp3",
                                                                                                              "type": "string",
                                                                                                              "title": "The Additionalprop3 Schema",
                                                                                                              "default": "",
                                                                                                              "examples": [
                                                                                                                "string"
                                                                                                              ],
                                                                                                              "pattern": "^(.*)$"
                                                                                                            }
                                                                                                          }
                                                                                                        }
                                                                                                      }
                                                                                                    }'::jsonb);