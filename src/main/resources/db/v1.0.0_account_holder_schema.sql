insert into base_schemas values('f4e5e4c8-b303-4aef-9690-bc8545f06372', -1, 'ACCOUNT_HOLDER', '{
                                                                                          "definitions": {},
                                                                                          "$schema": "http://json-schema.org/draft-07/schema#",
                                                                                          "$id": "http://example.com/root.json",
                                                                                          "type": "object",
                                                                                          "title": "The Root Schema",
                                                                                          "required": [
                                                                                            "vectors",
                                                                                            "ifiID",
                                                                                            "requestID",
                                                                                            "type",
                                                                                            "accountHolderProviderID"
                                                                                          ],
                                                                                          "properties": {
                                                                                            "vectors": {
                                                                                              "$id": "#/properties/vectors",
                                                                                              "type": "array",
                                                                                              "title": "The Vectors Schema",
                                                                                              "items": {
                                                                                                "$id": "#/properties/vectors/items",
                                                                                                "type": "object",
                                                                                                "title": "The Items Schema",
                                                                                                "required": [
                                                                                                  "type",
                                                                                                  "value"
                                                                                                ],
                                                                                                "properties": {
                                                                                                  "type": {
                                                                                                    "$id": "#/properties/vectors/items/properties/type",
                                                                                                    "type": "string",
                                                                                                    "title": "The Type Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "e"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "value": {
                                                                                                    "$id": "#/properties/vectors/items/properties/value",
                                                                                                    "type": "string",
                                                                                                    "title": "The Value Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "surajt@zeta.tech"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  }
                                                                                                },
                                                                                                "allOf": [
                                                                                                  {
                                                                                                    "if": {
                                                                                                      "properties": {
                                                                                                        "type": {
                                                                                                          "const": "e"
                                                                                                        }
                                                                                                      }
                                                                                                    },
                                                                                                    "then": {
                                                                                                      "properties": {
                                                                                                        "value": {
                                                                                                          "format" : "email"
                                                                                                        }
                                                                                                      }
                                                                                                    }
                                                                                                  },
                                                                                                  {
                                                                                                    "if": {
                                                                                                      "properties": {
                                                                                                        "type": {
                                                                                                          "const": "p"
                                                                                                        }
                                                                                                      }
                                                                                                    },
                                                                                                    "then": {
                                                                                                      "properties": {
                                                                                                        "value": {
                                                                                                          "pattern": "^(\\+91)*\\d{10}$"
                                                                                                        }
                                                                                                      }
                                                                                                    }
                                                                                                  }
                                                                                                ]
                                                                                              }
                                                                                            }
                                                                                          }
                                                                                        }'::jsonb);