insert into base_schemas values('5ccbdb05-fc62-41da-8b93-1f1c5605407c', -1, 'ACCOUNT', '{
                                                                                          "definitions": {},
                                                                                          "$schema": "http://json-schema.org/draft-07/schema#",
                                                                                          "$id": "http://example.com/root.json",
                                                                                          "type": "object",
                                                                                          "title": "The Root Schema",
                                                                                          "required": [
                                                                                            "ifiID",
                                                                                            "productFamilyID",
                                                                                            "productID",
                                                                                            "accountProviderID",
                                                                                            "status"
                                                                                          ],
                                                                                          "properties": {
                                                                                            "headers": {
                                                                                              "$id": "#/properties/headers",
                                                                                              "type": "object",
                                                                                              "title": "The Headers Schema"
                                                                                            },
                                                                                            "requestID": {
                                                                                              "$id": "#/properties/requestID",
                                                                                              "type": "string",
                                                                                              "title": "The Requestid Schema",
                                                                                              "default": "",
                                                                                              "examples": [
                                                                                                "1800eb99-1db3-452d-94de-121"
                                                                                              ],
                                                                                              "pattern": "^(.*)$"
                                                                                            },
                                                                                            "ifiID": {
                                                                                              "$id": "#/properties/ifiID",
                                                                                              "type": "integer",
                                                                                              "title": "The Ifiid Schema",
                                                                                              "default": 0,
                                                                                              "examples": [
                                                                                                152722
                                                                                              ]
                                                                                            },
                                                                                            "vectors": {
                                                                                              "$id": "#/properties/vectors",
                                                                                              "type": "array",
                                                                                              "title": "The Vectors Schema",
                                                                                              "items": {
                                                                                                "$id": "#/properties/vectors/items",
                                                                                                "type": "object",
                                                                                                "title": "The Items Schema",
                                                                                                "required": [
                                                                                                  "ifiID",
                                                                                                  "value",
                                                                                                  "type"
                                                                                                ],
                                                                                                "properties": {
                                                                                                  "ifiID": {
                                                                                                    "$id": "#/properties/vectors/items/properties/ifiID",
                                                                                                    "type": "integer",
                                                                                                    "title": "The Ifiid Schema",
                                                                                                    "default": 0,
                                                                                                    "examples": [
                                                                                                      152722
                                                                                                    ]
                                                                                                  },
                                                                                                  "value": {
                                                                                                    "$id": "#/properties/vectors/items/properties/value",
                                                                                                    "type": "string",
                                                                                                    "title": "The Value Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "p:+9199999999999"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "type": {
                                                                                                    "$id": "#/properties/vectors/items/properties/type",
                                                                                                    "type": "string",
                                                                                                    "title": "The Type Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "phone"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "status": {
                                                                                                    "$id": "#/properties/vectors/items/properties/status",
                                                                                                    "type": "string",
                                                                                                    "title": "The Status Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "ENABLED"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "attributes": {
                                                                                                    "$id": "#/properties/vectors/items/properties/attributes",
                                                                                                    "type": "object",
                                                                                                    "title": "The Attributes Schema"
                                                                                                  }
                                                                                                },
                                                                                                "allOf": [
                                                                                                  {
                                                                                                    "if": {
                                                                                                      "properties": {
                                                                                                        "type": {
                                                                                                          "const": "e"
                                                                                                        }
                                                                                                      }
                                                                                                    },
                                                                                                    "then": {
                                                                                                      "properties": {
                                                                                                        "value": {
                                                                                                          "format" : "email"
                                                                                                        }
                                                                                                      }
                                                                                                    }
                                                                                                  },
                                                                                                  {
                                                                                                    "if": {
                                                                                                      "properties": {
                                                                                                        "type": {
                                                                                                          "const": "p"
                                                                                                        }
                                                                                                      }
                                                                                                    },
                                                                                                    "then": {
                                                                                                      "properties": {
                                                                                                        "value": {
                                                                                                          "pattern": "^(\\+91)*\\d{10}$"
                                                                                                        }
                                                                                                      }
                                                                                                    }
                                                                                                  }
                                                                                                ]
                                                                                              }
                                                                                            },
                                                                                            "accessors": {
                                                                                              "$id": "#/properties/accessors",
                                                                                              "type": "array",
                                                                                              "title": "The Accessors Schema",
                                                                                              "items": {
                                                                                                "$id": "#/properties/accessors/items",
                                                                                                "type": "object",
                                                                                                "title": "The Items Schema",
                                                                                                "required": [
                                                                                                  "accountHolderID",
                                                                                                  "transactionPolicyIDs",
                                                                                                  "status",
                                                                                                  "attributes"
                                                                                                ],
                                                                                                "properties": {
                                                                                                  "accountHolderID": {
                                                                                                    "$id": "#/properties/accessors/items/properties/accountHolderID",
                                                                                                    "type": "string",
                                                                                                    "title": "The Accountholderid Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "60c1a9d0-4445-4d55-aba2-f7579494da83"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "transactionPolicyIDs": {
                                                                                                    "$id": "#/properties/accessors/items/properties/transactionPolicyIDs",
                                                                                                    "type": "array",
                                                                                                    "title": "The Transactionpolicyids Schema"
                                                                                                  },
                                                                                                  "status": {
                                                                                                    "$id": "#/properties/accessors/items/properties/status",
                                                                                                    "type": "string",
                                                                                                    "title": "The Status Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "ENABLED"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "attributes": {
                                                                                                    "$id": "#/properties/accessors/items/properties/attributes",
                                                                                                    "type": "object",
                                                                                                    "title": "The Attributes Schema"
                                                                                                  }
                                                                                                }
                                                                                              }
                                                                                            },
                                                                                            "relationships": {
                                                                                              "$id": "#/properties/relationships",
                                                                                              "type": "array",
                                                                                              "title": "The Relationships Schema",
                                                                                              "items": {
                                                                                                "$id": "#/properties/relationships/items",
                                                                                                "type": "object",
                                                                                                "title": "The Items Schema",
                                                                                                "required": [
                                                                                                  "relationshipType",
                                                                                                  "status",
                                                                                                  "attributes"
                                                                                                ],
                                                                                                "properties": {
                                                                                                  "relationshipType": {
                                                                                                    "$id": "#/properties/relationships/items/properties/relationshipType",
                                                                                                    "type": "string",
                                                                                                    "title": "The Relationshiptype Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "Sweep"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "status": {
                                                                                                    "$id": "#/properties/relationships/items/properties/status",
                                                                                                    "type": "string",
                                                                                                    "title": "The Status Schema",
                                                                                                    "default": "",
                                                                                                    "examples": [
                                                                                                      "ENABLED"
                                                                                                    ],
                                                                                                    "pattern": "^(.*)$"
                                                                                                  },
                                                                                                  "attributes": {
                                                                                                    "$id": "#/properties/relationships/items/properties/attributes",
                                                                                                    "type": "object",
                                                                                                    "title": "The Attributes Schema"
                                                                                                  }
                                                                                                }
                                                                                              }
                                                                                            },
                                                                                            "owner": {
                                                                                              "$id": "#/properties/owner",
                                                                                              "type": "string",
                                                                                              "title": "The Owner Schema",
                                                                                              "default": "",
                                                                                              "examples": [
                                                                                                "60c1a9d0-4445-4d55-aba2-f7579494da83"
                                                                                              ],
                                                                                              "pattern": "^(.*)$"
                                                                                            },
                                                                                            "name": {
                                                                                              "$id": "#/properties/name",
                                                                                              "type": "string",
                                                                                              "title": "The Name Schema",
                                                                                              "default": "",
                                                                                              "examples": [
                                                                                                "NewAccount111"
                                                                                              ],
                                                                                              "pattern": "^(.*)$"
                                                                                            },
                                                                                            "productFamilyID": {
                                                                                              "$id": "#/properties/productFamilyID",
                                                                                              "type": "integer",
                                                                                              "title": "The Productfamilyid Schema",
                                                                                              "default": 0,
                                                                                              "examples": [
                                                                                                451715461501352260
                                                                                              ]
                                                                                            },
                                                                                            "productID": {
                                                                                              "$id": "#/properties/productID",
                                                                                              "type": "integer",
                                                                                              "title": "The Productid Schema",
                                                                                              "default": 0,
                                                                                              "examples": [
                                                                                                2603940390116297700
                                                                                              ]
                                                                                            },
                                                                                            "programIDs": {
                                                                                              "$id": "#/properties/programIDs",
                                                                                              "type": "array",
                                                                                              "title": "The Programids Schema",
                                                                                              "items": {
                                                                                                "$id": "#/properties/programIDs/items",
                                                                                                "type": "string",
                                                                                                "title": "The Items Schema",
                                                                                                "default": "",
                                                                                                "examples": [
                                                                                                  "5a8a2e8b-86d9-429c-9af0-f469969c50d3"
                                                                                                ],
                                                                                                "pattern": "^(.*)$"
                                                                                              }
                                                                                            },
                                                                                            "accountProviderID": {
                                                                                              "$id": "#/properties/accountProviderID",
                                                                                              "type": "string",
                                                                                              "title": "The Accountproviderid Schema",
                                                                                              "default": "",
                                                                                              "examples": [
                                                                                                "f5d7629c-b49a-4923-9891-1fe2e614bc88"
                                                                                              ],
                                                                                              "pattern": "^(.*)$"
                                                                                            },
                                                                                            "status": {
                                                                                              "$id": "#/properties/status",
                                                                                              "type": "string",
                                                                                              "title": "The Status Schema",
                                                                                              "default": "",
                                                                                              "examples": [
                                                                                                "ENABLED"
                                                                                              ],
                                                                                              "pattern": "^(.*)$"
                                                                                            },
                                                                                            "attributes": {
                                                                                              "$id": "#/properties/attributes",
                                                                                              "type": "object",
                                                                                              "title": "The Attributes Schema"
                                                                                            }
                                                                                          }
                                                                                        }'::jsonb);