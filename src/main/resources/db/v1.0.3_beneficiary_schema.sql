insert into base_schemas values('546630c7-7854-4e93-ab77-dd364b96fedd', -1, 'BENEFICIARY', '{
                                                                                              "definitions": {},
                                                                                              "$schema": "http://json-schema.org/draft-07/schema#",
                                                                                              "$id": "http://example.com/root.json",
                                                                                              "type": "object",
                                                                                              "title": "The Root Schema",
                                                                                              "required": [
                                                                                              ],
                                                                                              "properties": {
                                                                                                "requestID": {
                                                                                                  "$id": "#/properties/requestID",
                                                                                                  "type": "string",
                                                                                                  "title": "The Requestid Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "request-puneet-1"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "firstName": {
                                                                                                  "$id": "#/properties/firstName",
                                                                                                  "type": "string",
                                                                                                  "title": "The Firstname Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "Test Beneficiary"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "type": {
                                                                                                  "$id": "#/properties/type",
                                                                                                  "type": "string",
                                                                                                  "title": "The Type Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "REAL"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "middleName": {
                                                                                                  "$id": "#/properties/middleName",
                                                                                                  "type": "string",
                                                                                                  "title": "The Middlename Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "test"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "lastName": {
                                                                                                  "$id": "#/properties/lastName",
                                                                                                  "type": "string",
                                                                                                  "title": "The Lastname Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "Yo"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "gender": {
                                                                                                  "$id": "#/properties/gender",
                                                                                                  "type": "string",
                                                                                                  "title": "The Gender Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "Male"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "salutation": {
                                                                                                  "$id": "#/properties/salutation",
                                                                                                  "type": "string",
                                                                                                  "title": "The Salutation Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "Mr."
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "relationship": {
                                                                                                  "$id": "#/properties/relationship",
                                                                                                  "type": "string",
                                                                                                  "title": "The Relationship Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "lkansdas"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                },
                                                                                                "profilePicURL": {
                                                                                                  "$id": "#/properties/profilePicURL",
                                                                                                  "type": "string",
                                                                                                  "title": "The Profilepicurl Schema",
                                                                                                  "default": "",
                                                                                                  "examples": [
                                                                                                    "asdlknasda"
                                                                                                  ],
                                                                                                  "pattern": "^(.*)$"
                                                                                                }
                                                                                              }
                                                                                            }'::jsonb);